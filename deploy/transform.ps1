# Environment parameters
param (
    [string]$ravenUrl = "",
    [string]$capUrl = "",
    [string]$carWebUrl = "",
    [string]$navConn = "",
    [string]$sbSubsConn = "",
    
    [string]$fgaFinanceUrl = "",
    [string]$navEStoreActionsUrl = "",
    [string]$navContactUsernamesUrl = "",
    [string]$navBasketUrl = "",
    [string]$navWebIntegrationUrl = "",
    [string]$navShipToAddressUrl = "",
    [string]$navSocUrl = "",
    [string]$navVehicleInterestUrl = "",

    [string]$raygunApiKey = "",
    [string]$cloudFilesImageProxyBaseUrl,
    [bool]$help = $False
)

Function Help ()
{
    Write-Output "Example usage"
    Write-Output ""
    Write-Host ".\transform.ps1 -ravenUrl -capUrl -carWebUrl -navConn -sbSubsConn [-fgaFinanceUrl] -navEStoreActionsUrl -navContactUsernamesUrl -navBasketUrl -navWebIntegrationUrl -navShipToAddressUrl -navSocUrl [-raygunApiKey] [-cloudFilesImageProxyBaseUrl]" -ForegroundColor Cyan
    Write-Output ""
    Write-Output "Paramters"
    Write-Output ""
    Write-Host "ravenUrl: "
    Write-Host "capUrl: "
    Write-Host "carWebUrl: "
    Write-Host "navConn: "
    Write-Host "sbSubsConn: "
    Write-Host "fgaFinanceUrl: "
    Write-Host "navEStoreActionsUrl: "
    Write-Host "navContactUsernamesUrl: "
    Write-Host "navBasketUrl: "
    Write-Host "navWebIntegrationUrl: "
    Write-Host "navShipToAddressUrl: "
    Write-Host "navSocUrl: "
    Write-Host "raygunApiKey: "
    Write-Host "cloudFilesImageProxyBaseUrl: "
}

if ($help) {
    Help
    return;
}

Function ParamsAreValid () {
    $required = @($ravenUrl, $capUrl, $carWebUrl, $navConn, $sbSubsConn, $navEStoreActionsUrl, $navContactUsernamesUrl, $navBasketUrl, $navWebIntegrationUrl, $navShipToAddressUrl, $navSocUrl,$navVehicleInterestUrl)
    ForEach ($p in $required) {
        If ($p -eq "") {
            return $False
        }
    }
    return $True
}

if (-not (ParamsAreValid)) {
    Write-Host "Invalid number of parameters" -ForegroundColor Red
    Write-Output ""
    Write-Output "Actual parameters given: "
    Write-Host "ravenUrl: $ravenUrl"
    Write-Host "capUrl: $capUrl"
    Write-Host "carWebUrl: $carWebUrl"
    Write-Host "navConn: $navConn"
    Write-Host "sbSubsConn: $sbSubsConn"
    Write-Host "fgaFinanceUrl: $fgaFinanceUrl"
    Write-Host "navEStoreActionsUrl: $navEStoreActionsUrl"
    Write-Host "navContactUsernamesUrl: $navContactUsernamesUrl"
    Write-Host "navBasketUrl: $navBasketUrl"
    Write-Host "navWebIntegrationUrl: $navWebIntegrationUrl"
    Write-Host "navShipToAddressUrl: $navShipToAddressUrl"
    Write-Host "navSocUrl: $navSocUrl"
    Write-Host "raygunApiKey: $raygunApiKey"
    Write-Host "cloudFilesImageProxyBaseUrl: $cloudFilesImageProxyBaseUrl"
    Write-Host "navVehicleInterestUrl: $navVehicleInterestUrl"

    Write-Output ""
    Write-Output ""
    Help
    exit 1;
}

# Do the transform
$cwd = (split-path $MyInvocation.MyCommand.Path)
$appRoot = $cwd + "\..\src\Sfs.EStore.Api"

echo "Updating web.config"
$webConfig = $appRoot + "\web.config"
[xml]$xml = (Get-Content $webConfig)

$xml.SelectSingleNode("/configuration/appSettings/add[@key='Raven/Url']").SetAttribute("value", $ravenUrl)
$xml.SelectSingleNode("/configuration/appSettings/add[@key='Cap/ProxyServiceApiBaseUrl']").SetAttribute("value", $capUrl)
$xml.SelectSingleNode("/configuration/system.serviceModel/client/endpoint[@name='BasicHttpBinding_IFurtherInfoService']").SetAttribute("address", $carWebUrl)
$xml.SelectSingleNode("/configuration/connectionStrings/add[@name='NavisionContext']").SetAttribute("connectionString", $navConn)
$xml.SelectSingleNode("/configuration/connectionStrings/add[@name='SubscriptionConnection']").SetAttribute("connectionString", $sbSubsConn)
$xml.SelectSingleNode("/configuration/appSettings/add[@key='Fga/FinanceProxy/BaseAddress']").SetAttribute("value", $fgaFinanceUrl)
$xml.SelectSingleNode("/configuration/applicationSettings/Sfs.EStore.Api.Properties.Settings/setting[@name='Sfs_EStore_Api_EStoreActionsService_eStore_Actions_Service']/value").set_InnerText($navEStoreActionsUrl)
$xml.SelectSingleNode("/configuration/applicationSettings/Sfs.EStore.Api.Properties.Settings/setting[@name='Sfs_EStore_Api_ContactUsernamesService_Contact_Usernames_Service']/value").set_InnerText($navContactUsernamesUrl)
$xml.SelectSingleNode("/configuration/applicationSettings/Sfs.EStore.Api.Properties.Settings/setting[@name='Sfs_EStore_Api_NavBasketServcie_Basket_Service']/value").set_InnerText($navBasketUrl)
$xml.SelectSingleNode("/configuration/applicationSettings/Sfs.EStore.Api.Properties.Settings/setting[@name='Sfs_EStore_Api_NavWebIntegrationService_WebIntegration']/value").set_InnerText($navWebIntegrationUrl)
$xml.SelectSingleNode("/configuration/applicationSettings/Sfs.EStore.Api.Properties.Settings/setting[@name='Sfs_EStore_Api_NavShipToAddressService_Ship_to_Address_Service']/value").set_InnerText($navShipToAddressUrl)
$xml.SelectSingleNode("/configuration/applicationSettings/Sfs.EStore.Api.Properties.Settings/setting[@name='Sfs_EStore_Api_Vehicle_interests_Service']/value").set_InnerText($navVehicleInterestUrl)

$xml.SelectSingleNode("/configuration/RaygunSettings").SetAttribute("apikey", $raygunApiKey)
if ($cloudFilesImageProxyBaseUrl -ne "") {
    $xml.SelectSingleNode("/configuration/appSettings/add[@key='CloudFiles/ImageProxyBaseUrl']").SetAttribute("value", $cloudFilesImageProxyBaseUrl)
}

$xml.Save($webConfig)

echo "Finished updating web.config"
exit 0;