# Environment parameters
param (
    [string]$dest = "",
    [bool]$help = $False
)

Function Help ()
{
    Write-Output "Example usage"
    Write-Output ""
    Write-Host ".\deploy.ps1 -dest" -ForegroundColor Cyan
    Write-Output ""
    Write-Output "Paramters"
    Write-Output ""
    Write-Output "dest: The destination path"
    Write-Output "help: Show this message (-help 1)"
    Write-Output ""
}

if ($help) {
    Write-Output ""
    Help
    return;
}

if ($dest -eq "") {
    Write-Host "Invalid number of parameters" -ForegroundColor Red
    Write-Output ""
    Help
    exit 1;
}

# Do the deployment
$cwd = (split-path $MyInvocation.MyCommand.Path)
$appRoot = "$cwd\..\src\Sfs.EStore.Api"


# Setting app as 'offline'
$appOfflinePath = "$cwd\app_offline.htm"
Write-Output "placing app_offline: $appOfflinePath"
xcopy /Y $appOfflinePath $dest

# Clean destination
Write-Output "cleaning destination"
Remove-Item ($dest+"\*") -recurse -exclude app_offline.htm, log4net.config, aspnet_client

# Write files to destination
Write-Output "Copying config files"
ROBOCOPY $appRoot $dest "favicon.ico" "Global.asax" "web.config"

Write-Output "Copying bin"
ROBOCOPY "$appRoot\bin" "$dest\bin" *.dll /E /NS /NC /NFL /NDL /NP 

If (Test-Path "$dest\app_offline.htm"){
    Write-Output "Removing app_offline"
    Remove-Item "$dest\app_offline.htm"
}
exit 0;