BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO


/********************************
 *   Tenant_ManufacturerCmsPages
 *******************************/
if object_id('dbo.Tenant_ManufacturerCmsPages') is not null
	drop table dbo.Tenant_ManufacturerCmsPages
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tenant_ManufacturerCmsPages
	(
	Id nvarchar(32) NOT NULL,
	VehicleTypeQueryTerm nvarchar(255) NULL,
	ManufacturerQueryTerm nvarchar(255) NULL,
	ManufacturerSlug nvarchar(255) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tenant_ManufacturerCmsPages SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tenant_ManufacturerCmsPages ADD CONSTRAINT
	PK_Tenant_ManufacturerCmsPages PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO


/********************************
 *   Tenant_ManufacturerModelCmsPages
 *******************************/
if object_id('dbo.Tenant_ManufacturerModelCmsPages') is not null
	drop table dbo.Tenant_ManufacturerModelCmsPages
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tenant_ManufacturerModelCmsPages
	(
	Id nvarchar(32) NOT NULL,
	VehicleTypeQueryTerm nvarchar(255) NULL,
	ManufacturerQueryTerm nvarchar(255) NULL,
	ModelQueryTerm nvarchar(255) NULL,
	ManufacturerSlug nvarchar(255) NULL,
	ModelSlug nvarchar(255) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tenant_ManufacturerModelCmsPages SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tenant_ManufacturerModelCmsPages ADD CONSTRAINT
	PK_Tenant_ManufacturerModelCmsPages PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO
