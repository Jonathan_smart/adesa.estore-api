BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO

/********************************
 *   Clean up old tables
 *******************************/
if object_id('dbo.VehicleEquipmentItems') is not null
	drop table dbo.VehicleEquipmentItems
GO
if object_id('dbo.VehicleImages') is not null
	drop table dbo.VehicleImages
GO


/********************************
 *   Lots
 *******************************/
if object_id('dbo.Lots') is not null
	drop table dbo.Lots
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Lots
	(
	Id nvarchar(16) NOT NULL,
	VehicleId nvarchar(16) NULL,
	[Status] nvarchar(32) NULL,
	[Condition] nvarchar(32) NULL,
	Title nvarchar(512) NULL,
	StartDate datetime NULL,
	EndDate datetime NULL,
	BuyItNowPrice_Amount decimal(9,2) NULL,
	BuyItNowPrice_Currency nvarchar(3) NULL,
	CurrentOffer_Amount decimal(9,2) NULL,
	CurrentOffer_Currency nvarchar(3) NULL,
	CurrentOffer_Date datetime NULL,
	CurrentOffer_Username nvarchar(255) NULL,
	StartingPrice_Amount decimal(9,2) NULL,
	StartingPrice_Currency nvarchar(3) NULL,
	ReservePrice_Amount decimal(9,2) NULL,
	ReservePrice_Currency nvarchar(3) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Lots SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Lots ADD CONSTRAINT
	PK_Lots PRIMARY KEY CLUSTERED
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT

if object_id('dbo.LotBids') is not null
	drop table dbo.LotBids
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.LotBids
	(
	PK int NOT NULL IDENTITY (1, 1),
	LotId nvarchar(16) NOT NULL,
	Bid_Amount decimal(9,2) NULL,
	Bid_Currency nvarchar(3) NULL,
	Bid_Username nvarchar(255) NULL,
	Bid_Date datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.LotBids SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.LotBids ADD CONSTRAINT
	PK_LotBids PRIMARY KEY CLUSTERED
	(
	PK
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT


/********************************
 *   Vehicles
 *******************************/
if object_id('dbo.Vehicles') is not null
	drop table dbo.Vehicles
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Vehicles
	(
	Id nvarchar(16) NOT NULL,
	CapId int NULL,
	Make nvarchar(255) NULL,
	Model nvarchar(255) NULL,
	Derivative nvarchar(255) NULL,
	Mileage int NULL,
	FuelType nvarchar(255) NULL,
	Transmission nvarchar(255) NULL,
	Registration_Plate nvarchar(255) NULL,
	Registration_Date datetime NULL,
	VatStatus nvarchar(255) NULL,
	VehicleType nvarchar(255) NULL,
	MetaData_Carweb_Equipment_Enriched datetime NULL,
	MetaData_Carweb_Last_Enriched_At datetime NULL,
	MetaData_Carweb_Next_Enrich_Attempt_After datetime NULL,
	MetaData_Carweb_Enrich_Attempts int NULL,
	MetaData_Cap_Pricing_Enriched datetime NULL,
	Specification_Environment_Co2 decimal(5,2) NULL,
	Specification_Economy_Urban decimal(5,2) NULL,
	Specification_Economy_ExtraUrban decimal(5,2) NULL,
	Specification_Economy_Combined decimal(5,2) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Vehicles SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Vehicles ADD CONSTRAINT
	PK_Vehicles PRIMARY KEY CLUSTERED
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT


/********************************
 *   MembershipUsers
 *******************************/
if object_id('dbo.MembershipUsers') is not null
	drop table dbo.MembershipUsers
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.MembershipUsers
	(
	Id nvarchar(32) NOT NULL,
	[Username] nvarchar(255) NULL,
	[FirstName] nvarchar(255) NULL,
	[LastName] nvarchar(255) NULL,
	[PhoneNumber] nvarchar(255) NULL,
	[EmailAddress] nvarchar(255) NULL,
	[IsLockedOut] bit NULL,
	[LockedOutOn] datetime NULL,
	[IsApproved] bit NULL,
	[ApprovedOn] datetime NULL,
	[Roles] nvarchar(500) NULL,
	[Franchises] nvarchar(500) NULL,
	[InvalidPasswordAttempts] nvarchar(255) NULL,
	[CreatedOn] datetime NULL,
	[LastLoggedInOn] datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.MembershipUsers SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.MembershipUsers ADD CONSTRAINT
	PK_MembershipUsers PRIMARY KEY CLUSTERED
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO

/********************************
 *   SessionInteractions
 *******************************/
if object_id('dbo.SessionInteractions') is not null
	drop table dbo.SessionInteractions
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.SessionInteractions
	(
	Id nvarchar(32) NOT NULL,
	Username nvarchar(255) NULL,
	CreatedOn datetime NULL,
	SessionId nvarchar(50) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.SessionInteractions SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.SessionInteractions ADD CONSTRAINT
	PK_SessionInteractions PRIMARY KEY CLUSTERED
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT


if object_id('dbo.SessionInteractionLotsViewed') is not null
	drop table dbo.SessionInteractionLotsViewed
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.SessionInteractionLotsViewed
	(
	PK int NOT NULL IDENTITY (1, 1),
	SessionInteractionId nvarchar(32) NOT NULL,
	LotId nvarchar(16) NULL,
	[Date] datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.SessionInteractionLotsViewed SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.SessionInteractionLotsViewed ADD CONSTRAINT
	PK_SessionInteractionLotsViewed PRIMARY KEY CLUSTERED
	(
	PK
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO


/********************************
 *   UserLotWatches
 *******************************/
if object_id('dbo.UserLotWatches') is not null
	drop table dbo.UserLotWatches
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.UserLotWatches
	(
	Id nvarchar(64) NOT NULL,
	MembershipUserId nvarchar(32) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.UserLotWatches SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.UserLotWatches ADD CONSTRAINT
	PK_UserLotWatches PRIMARY KEY CLUSTERED
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO

if object_id('dbo.UserLotWatchesLot') is not null
	drop table dbo.UserLotWatchesLot
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.UserLotWatchesLot
	(
	PK int NOT NULL IDENTITY (1, 1),
	MembershipUserId nvarchar(32) NOT NULL,
	UserLotWatchesId nvarchar(64) NOT NULL,
	LotId nvarchar(16) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.UserLotWatchesLot SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.UserLotWatchesLot ADD CONSTRAINT
	PK_UserLotWatchesLot PRIMARY KEY CLUSTERED
	(
	PK
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO


/********************************
 *   UserSavedSearches
 *******************************/
if object_id('dbo.UserSavedSearches') is not null
	drop table dbo.UserSavedSearches
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.UserSavedSearches
	(
	Id nvarchar(64) NOT NULL,
	MembershipUserId nvarchar(32) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.UserSavedSearches SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.UserSavedSearches ADD CONSTRAINT
	PK_UserSavedSearches PRIMARY KEY CLUSTERED
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO

if object_id('dbo.UserSavedSearchesSearch') is not null
	drop table dbo.UserSavedSearchesSearch
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.UserSavedSearchesSearch
	(
	Id nvarchar(64) NOT NULL,
	SavedSearchId nvarchar(64) NOT NULL,
	MembershipUserId nvarchar(32) NULL,
	Name nvarchar(255) NULL,
	SavedOn datetime NULL,
	AlertStatusEnabled bit NULL,
	AlertStatusLastNotifiedAt datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.UserSavedSearchesSearch SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.UserSavedSearchesSearch ADD CONSTRAINT
	PK_UserSavedSearchesSearch PRIMARY KEY CLUSTERED
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO

if object_id('dbo.UserSavedSearchesSearchCriteria') is not null
	drop table dbo.UserSavedSearchesSearchCriteria
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.UserSavedSearchesSearchCriteria
	(
	PK int NOT NULL IDENTITY (1, 1),
	SavedSearchId nvarchar(64) NOT NULL,
	SearchId nvarchar(64) NOT NULL,
	[Key] nvarchar(255) NOT NULL,
	[Value] nvarchar(255) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.UserSavedSearchesSearchCriteria SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.UserSavedSearchesSearchCriteria ADD CONSTRAINT
	PK_UserSavedSearchesSearchCriteria PRIMARY KEY CLUSTERED
	(
	PK
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO



/********************************
 *   UserSavedValuations
 *******************************/
if object_id('dbo.UserSavedValuations') is not null
  drop table dbo.UserSavedValuations
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.UserSavedValuations
  (
  Id nvarchar(64) NOT NULL,
  MembershipUserId nvarchar(32) NULL
  )  ON [PRIMARY]
GO
ALTER TABLE dbo.UserSavedValuations SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.UserSavedValuations ADD CONSTRAINT
  PK_UserSavedValuations PRIMARY KEY CLUSTERED
  (
  Id
  ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO



/********************************
 *   UserSavedValuationsValuation
 *******************************/
if object_id('dbo.UserSavedValuationsValuation') is not null
  drop table dbo.UserSavedValuationsValuation
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.UserSavedValuationsValuation
  (
  Id nvarchar(64) NOT NULL,
  SavedValuationId nvarchar(64) NOT NULL,
  MembershipUserId nvarchar(32) NULL,
  CreatedOn datetime NULL,
  Registration nvarchar(32) NULL,
  Mileage int NULL,
  RetailValuation decimal(9,2) NULL,
  CleanValuation decimal(9,2) NULL,
  AverageValuation decimal(9,2) NULL,
  BelowValuation decimal(9,2) NULL
  )  ON [PRIMARY]
GO
ALTER TABLE dbo.UserSavedValuationsValuation SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.UserSavedValuationsValuation ADD CONSTRAINT
  PK_UserSavedValuationsValuation PRIMARY KEY CLUSTERED
  (
  Id
  ) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
GO
