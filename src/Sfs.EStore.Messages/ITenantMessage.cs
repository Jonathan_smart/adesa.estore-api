﻿namespace Sfs.EStore.Messages
{
    public interface ITenantMessage
    {
        string TenantId { get; }
    }
}
