﻿using System;

namespace Sfs.EStore.Messages.Events
{
    public class NewBidHasBeenAccepted : ITenantMessage
    {
        public string TenantId { get; set; }

        public NewBidHasBeenAccepted_OfferDetail PreviouslyWinningOffer { get; set; }
        public NewBidHasBeenAccepted_OfferDetail CurrentOffer { get; set; }

        public int ListingId { get; set; }
        public NewBidHasBeenAccepted_ListingDetail Listing { get; set; }

        public override string ToString()
        {
            return string.Format("ListingId: {0}", ListingId);
        }

        public class NewBidHasBeenAccepted_ListingDetail
        {
            public string Title { get; set; }
            public DateTime? EndsAt { get; set; }
            public DateTime? StartsAt { get; set; }
            public string RegistrationPlate { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string Derivative { get; set; }
        }

        public class NewBidHasBeenAccepted_UserDetail
        {
            public string Id { get; set; }
            public string EmailAddress { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string FullName { get; set; }
        }

        public class NewBidHasBeenAccepted_OfferDetail
        {
            public Money Amount { get; set; }
            public DateTime Date { get; set; }
            public NewBidHasBeenAccepted_UserDetail User { get; set; }
        }
    }
}
