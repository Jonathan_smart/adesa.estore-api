﻿using System;

namespace Sfs.EStore.Messages.Events
{
    public abstract class AuctionLotHasEnded : ITenantMessage
    {
        public string TenantId { get; set; }
        public int ListingId { get; set; }
        public AuctionLotHasEnded_ListingDetail Listing { get; set; }

        public AuctionLotHasEnded_BidDetail HighestOffer { get; set; }
        public AuctionLotHasEnded_BidDetail[] OutbidOffers { get; set; }

        public class AuctionLotHasEnded_ListingDetail
        {
            public string Title { get; set; }
            public DateTime? EndsAt { get; set; }
            public DateTime? StartsAt { get; set; }
            public string RegistrationPlate { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string Derivative { get; set; }
        }

        public class AuctionLotHasEnded_UserDetail
        {
            public string Id { get; set; }
            public string EmailAddress { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string FullName { get; set; }
        }

        public class AuctionLotHasEnded_BidDetail
        {
            public Money Amount { get; set; }
            public DateTime Date { get; set; }
            public AuctionLotHasEnded_UserDetail User { get; set; }
        }
    }

    public class AuctionLotHasEndedWithSale : AuctionLotHasEnded
    {



    }

    public class AuctionLotHasEndedWithoutSale : AuctionLotHasEnded
    {
    }
}
