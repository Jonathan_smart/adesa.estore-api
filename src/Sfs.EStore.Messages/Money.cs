﻿namespace Sfs.EStore.Messages
{
    public class Money
    {
        public decimal Amount { get; set; }
        public string Currency { get; set; }

        public string ToCurrencyString()
        {
            // TODO: Lookup currency symbol based on currency string
            /*
             * foreach (var culture in cultures)
            {
                var lcid = culture.LCID;
                var regionInfo = new RegionInfo(lcid);
                var isoSymbol = regionInfo.ISOCurrencySymbol;

                if (!cultureIdLookup.ContainsKey(isoSymbol))
                {
                    cultureIdLookup[isoSymbol] = new List<Int32>();
                }

                cultureIdLookup[isoSymbol].Add(lcid);
                symbolLookup[isoSymbol] = regionInfo.CurrencySymbol;
            }
             * */
            var symbol = Currency.ToString();
            switch (Currency)
            {
                case "GBP":
                    symbol = "\u00a3";
                    break;
                case "EUR":
                    symbol = "\u20ac";
                    break;
            }

            return string.Format("{0}{1}", symbol, Amount.ToString("n0"));
        }
    }
}