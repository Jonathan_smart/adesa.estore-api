﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using CommandLine;
using CommandLine.Text;
using Mandrill;
using Raven.Client;
using Sfs.EStore.Api;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.UserAlerts.Commands;
using log4net;
using log4net.Config;

namespace Sfs.EStore.UserAlerts
{
    class Options
    {
        [Option('s', "silent", DefaultValue = false, HelpText = "If true, will run through without prompts")]
        public bool Silent { get; set; }

        [Option('r', "ravenurl", HelpText = "If set, overrides app.config with specified value")]
        public string RavenUrl { get; set; }

        [Option('d', "database", HelpText = "If set, overrides app.config with specified value")]
        public string DatabaseName { get; set; }

        [Option('e', "emailtemplate", HelpText = "If set, overrides app.config with specified value")]
        public string EmailTemplateName { get; set; }

        [Option('u', "apibaseaddress", HelpText = "If set, overrides app.config with specified value")]
        public string ApiBaseAddress { get; set; }

        [Description("If set, overrides app.config with specified value")]
        [Option('k', "apiaccesskey", HelpText = "If set, overrides app.config with specified value")]
        public string ApiAccessKey { get; set; }

        [Option('p', "minimumnotifieddaysperiod", HelpText = "If set, overrides the default mimimum period between notifications")]
        public double? MinimumNotifiedDaysPeriod { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              current => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }

    class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Program));
        private readonly static ConcurrentDictionary<string, Lazy<IDocumentStore>> Stores = new ConcurrentDictionary<string, Lazy<IDocumentStore>>();
        private static LazyAccountDocumentStoreFactory _accountStores;
        private const double DefaultMinimumNotifiedDaysPeriod = 2;

        static void Main(string[] args)
        {
            var options = new Options();
            if (!Parser.Default.ParseArguments(args, options))
            {
                return;
            }

            XmlConfigurator.ConfigureAndWatch(new FileInfo(Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory, "log4net.config")));

            int limit = 0;

            var settings = new ProgramSettings
            {
                EmailTemplateName = options.EmailTemplateName ?? ConfigurationManager.AppSettings["Email/TemplateName"],
                RavenUrl = options.RavenUrl ?? ConfigurationManager.AppSettings["Raven/Url"],
                DatabaseName = options.DatabaseName ?? ConfigurationManager.AppSettings["Account/Database"] ?? "",
                ApiAccessKey = options.ApiAccessKey ?? ConfigurationManager.AppSettings["Api/AccessKey"],
                ApiBaseAddress = new Uri(options.ApiBaseAddress ?? ConfigurationManager.AppSettings["Api/BaseAddress"]),
                MandrillApiKey = ConfigurationManager.AppSettings["Mandrill/ApiKey"],
                MinimumNotifiedDaysPeriod = (options.MinimumNotifiedDaysPeriod ?? DefaultMinimumNotifiedDaysPeriod),
                MaximumVehicleList = int.TryParse(ConfigurationManager.AppSettings["Email/Limit"], out limit) ? limit : 5,
            };

            ImageUrl.CloudFilesImageProxyUrl = ConfigurationManager.AppSettings["CloudFiles/ImageProxyBaseUrl"];

            Logger.InfoFormat("Raven Url: {0}", settings.RavenUrl);
            Logger.InfoFormat("DatabaseName: {0}", settings.DatabaseName);
            Logger.InfoFormat("EmailTemplateName: {0}", settings.EmailTemplateName);
            Logger.InfoFormat("ApiBaseAddress: {0}", settings.EmailTemplateName);
            Logger.InfoFormat("MinimumNotifiedDaysPeriod: {0}", settings.MinimumNotifiedDaysPeriod);

            Silent(options, () =>
                                {
                                    Console.WriteLine();
                                    Console.WriteLine();
                                    Console.WriteLine("Use --help to see how to remove this prompt");
                                    Console.WriteLine();
                                    Console.WriteLine("-------------------------------------------------");
                                    Console.WriteLine("Raven Url: {0}", settings.RavenUrl);
                                    Console.WriteLine("DatabaseName: {0}", settings.DatabaseName);
                                    Console.WriteLine("EmailTemplateName: {0}", settings.EmailTemplateName);
                                    Console.WriteLine("ApiBaseAddress: {0}", settings.ApiBaseAddress);
                                    Console.WriteLine("ApiAccessKey: {0}", settings.ApiAccessKey);
                                    Console.WriteLine("MinimumNotifiedDaysPeriod: {0}", settings.MinimumNotifiedDaysPeriod);
                                    Console.WriteLine("-------------------------------------------------");
                                    Console.WriteLine();
                                    Console.WriteLine("Continue with current settings?");
                                    Console.WriteLine("Quit now (Ctrl+C) or press <enter> to continue");
                                    Console.ReadLine();
                                });

            _accountStores = new LazyAccountDocumentStoreFactory(Stores, settings.RavenUrl);

            var mandrillApi = new MandrillApi(settings.MandrillApiKey);

            var store = _accountStores.GetAccountDocumentStoreFor(settings.DatabaseName);
            IEnumerable<Notify> savedSearches;
            using (var session = store.OpenSession())
            {
                var command = new GetUserSavedSearchesQuery
                {
                    Session = session
                };
                command.Execute();
                savedSearches = command.Result;
            }

            Func<ISearchForVehiclesCommand> searchCommand = () => new ApiVehicleSearchCommand(settings.ApiAccessKey,
                                                                                              settings.ApiBaseAddress);

            Func<ISendSearchAlertEmail> sendEmailCommand =
                () => new MandrillSendEmailCommand(mandrillApi, settings.EmailTemplateName,settings.MaximumVehicleList);

            var alerter = new SavedSearchAlerter(store, searchCommand, sendEmailCommand);

            if (settings.MinimumNotifiedDaysPeriod.HasValue)
                alerter.MinimumNotifiedDaysPeriod = settings.MinimumNotifiedDaysPeriod.Value;

            alerter
                .Run(savedSearches)
                .ContinueWith(x =>
                                  {
                                      if (x.Exception != null)
                                          Logger.Error(
                                              string.Format("Failed to alert users for {0}", store.Identifier),
                                              x.Exception);

                                      Logger.DebugFormat("Done with alerts for {0}", store.Identifier);
                                  })
                .Wait();

            Logger.Info("Run to end");

            Silent(options, () =>
                                {
                                    Console.WriteLine();
                                    Console.WriteLine("-------------------------------------------------");
                                    Console.WriteLine("Press any key to quit");
                                    Console.ReadKey();
                                });
        }

        private static void Silent(Options options, Action action)
        {
            if (!options.Silent)
            {
                action();
            }
        }
    }
}
