using System;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.UserAlerts.Commands
{
    /// <exception cref="AlertNotSentException"></exception>
    /// <exception cref="InvalidOperationException"></exception>
    public interface ISendSearchAlertEmail : ICommand
    {
        SavedSearch Search { set; }
        ListingSummaryViewModel[] Vehicles { set; }
        MembershipUser User { set; }
    }
}