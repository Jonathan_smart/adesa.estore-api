using System;
using System.Runtime.Serialization;

namespace Sfs.EStore.UserAlerts.Commands
{
    [Serializable]
    public class AlertNotSentException : Exception
    {
        public AlertNotSentException(string user, Guid search)
        {
            User = user;
            Search = search;
        }

        public AlertNotSentException(string user, Guid search, string message) : base(message)
        {
            User = user;
            Search = search;
        }

        public AlertNotSentException(string user, Guid search, string message, Exception inner) : base(message, inner)
        {
            User = user;
            Search = search;
        }

        protected AlertNotSentException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
            User = info.GetString("User");
            Search = (Guid) info.GetValue("Search", typeof (Guid));
        }

        public string User { get; protected set; }
        public Guid Search { get; protected set; }
    }
}