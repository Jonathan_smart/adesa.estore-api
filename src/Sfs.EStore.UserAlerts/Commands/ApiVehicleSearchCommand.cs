using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.UserAlerts.Commands
{
    public class ApiVehicleSearchCommand : Command<ListingSummaryViewModel[]>, ISearchForVehiclesCommand
    {
        private readonly HttpClient _httpClient;

        public ApiVehicleSearchCommand(string apiAccessKey, Uri apiBaseAddress)
        {
            _httpClient = new HttpClient {BaseAddress = apiBaseAddress};
            
            _httpClient.DefaultRequestHeaders.Accept.ParseAdd("application/json");
            
            var authentication = string.Format("{0}:", apiAccessKey);
            var encodedAuthentication = Convert.ToBase64String(Encoding.ASCII.GetBytes(authentication));
            _httpClient.DefaultRequestHeaders
                .Authorization = new AuthenticationHeaderValue("SFS", encodedAuthentication);
        }

        public override void Execute()
        {
            if (Criteria == null)
                throw new InvalidOperationException("Criteria not set");

            try
            {
                Result = _httpClient.PostAsync("search",
                                               new FormUrlEncodedContent(
                                                   Criteria.Select(x =>
                                                                   new KeyValuePair<string, string>(x.Key,
                                                                                                    x.Value.ToString()))))
                    .ContinueWith(
                        x =>
                            {
                                x.Result.EnsureSuccessStatusCode();

                                return x.Result.Content.ReadAsAsync<ListingSummaryViewModel[]>().Result;
                            })
                    .Result;
            }
            catch(AggregateException ex)
            {
                throw new SearchException("Search was not successful", ex);
            }
        }

        public Dictionary<string, object> Criteria { get; set; }
    }
}