using System;
using System.Globalization;
using System.Linq;
using Mandrill;
using Raven.Client.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using log4net;

namespace Sfs.EStore.UserAlerts.Commands
{
    public class MandrillSendEmailCommand : Command, ISendSearchAlertEmail
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MandrillSendEmailCommand));

        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;
        private int _maxListSize;

        public MandrillSendEmailCommand(MandrillApi mandrillApi, string templateName, int maxListSize)
        {
            _mandrillApi = mandrillApi;
            _templateName = templateName;
            _maxListSize = maxListSize;
        }

        public SavedSearch Search { get; set; }
        public ListingSummaryViewModel[] Vehicles { get; set; }
        public MembershipUser User { get; set; }

        public override void Execute()
        {
            if (Search == null)
                throw new InvalidOperationException("Search cannot be null");
            if (Vehicles == null)
                throw new InvalidOperationException("Vehicles cannot be null");
            if (User == null)
                throw new InvalidOperationException("User cannot be null");

            Logger.InfoFormat("[{0}] template: '{1}', searchId: '{2}'", User.Id, _templateName, Search.Id);

            var message = BuildMessage();

            try
            {
                _mandrillApi.SendMessageAsync(message, _templateName, new TemplateContent[0])
                    .ContinueWith(x =>
                                      {
                                          if (!x.Result.All(r => r.Status.In(EmailResultStatus.Sent, EmailResultStatus.Queued)))
                                              throw new AlertNotSentException(User.Id, Search.Id,
                                                                              string.Format("EmailResultStatus is {0}",
                                                                                            x.Result.First().Status));
                                      })
                    .Wait();
            }
            catch (Exception ex)
            {
                throw new AlertNotSentException(User.Id, Search.Id, ex.Message, ex);
            }
        }

        private EmailMessage BuildMessage()
        {
            var message = new EmailMessage { to = new[] { new EmailAddress { email = User.EmailAddress } } };

            if (User != null)
            {
                message.AddGlobalVariable("FIRST_NAME", User.FirstName);
                message.AddGlobalVariable("LAST_NAME", User.LastName);
                message.AddGlobalVariable("USERNAME", User.Username);
                message.AddGlobalVariable("EMAIL", User.EmailAddress);
            }

            if (Search != null)
            {
                message.AddGlobalVariable("SEARCH_NAME", Search.Name);
                message.AddGlobalVariable("SEARCH_SAVEDON", Search.SavedOn.ToString(CultureInfo.InvariantCulture));
                message.AddGlobalVariable("SEARCH_QUERY", Search.ToQueryString());
            }

            var totalNewVehicleCount = Vehicles.Count();
            message.AddGlobalVariable("TOTAL_NEW_VEHICLE_COUNT", totalNewVehicleCount.ToString(CultureInfo.InvariantCulture));

            foreach (var model in Vehicles.Take(_maxListSize).Select((model, i) => new VehicleIndex(model, i + 1)))
            {
                BuildVehicleIndex(message, model);
            }

            return message;
        }

        private static void BuildVehicleIndex(EmailMessage message, VehicleIndex model)
        {
            var price = model.Sale.SellingStatus.CurrentPrice;
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_ID", model.Index), Lot.IdFromLotNumber(model.Sale.ListingId));
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_NUMBER", model.Index),
                                      model.Sale.ListingId.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_IMAGEURL", model.Index),
                                      model.Sale.PrimaryImage == null
                                          ? ""
                                          : model.Sale.PrimaryImage.ToUrl(width: 320, height: 240));
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_TITLE", model.Index), model.Sale.Title);
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_YAP", model.Index), model.Sale.VehicleInfo.Registration.Yap);
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_REGISTRATION_PLATE", model.Index), model.Sale.VehicleInfo.Registration.Plate);
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_MILES", model.Index),
                                      model.Sale.VehicleInfo.Mileage.HasValue
                                          ? model.Sale.VehicleInfo.Mileage.Value.ToString(CultureInfo.InvariantCulture)
                                          : "");
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_PRICE", model.Index), price.ToCurrencyString());
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_PRICE_AMOUNT", model.Index),
                                      price.Amount.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_PRICE_CURRENCY", model.Index), price.Currency);
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_VAT_STATUS", model.Index), model.Sale.VehicleInfo.VatStatus);
            message.AddGlobalVariable(string.Format("VEHICLE_{0}_PRICES_INCLUDE_VAT", model.Index),
                                      model.Sale.PricesIncludeVat.ToString(CultureInfo.InvariantCulture));
        }

        private class VehicleIndex
        {
            public VehicleIndex(ListingSummaryViewModel sale, int index)
            {
                Sale = sale;
                Index = index;
            }

            public ListingSummaryViewModel Sale { get; private set; }
            public int Index { get; private set; }
        }
    }
}