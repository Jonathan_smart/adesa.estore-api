using System;
using System.Runtime.Serialization;

namespace Sfs.EStore.UserAlerts.Commands
{
    [Serializable]
    public class SearchException : Exception
    {
        public SearchException()
        {
        }

        public SearchException(string message) : base(message)
        {
        }

        public SearchException(string message, Exception inner) : base(message, inner)
        {
        }

        protected SearchException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}