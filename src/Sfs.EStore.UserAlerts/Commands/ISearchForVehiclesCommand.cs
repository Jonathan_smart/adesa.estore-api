using System.Collections.Generic;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.UserAlerts.Commands
{
    /// <exception cref="SearchException"></exception>
    public interface ISearchForVehiclesCommand : ICommand<ListingSummaryViewModel[]>
    {
        Dictionary<string, object> Criteria { get; set; }
    }
}