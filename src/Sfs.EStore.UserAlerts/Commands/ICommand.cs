﻿using Raven.Client;

namespace Sfs.EStore.UserAlerts.Commands
{
    public interface ICommand
    {
        void Execute();
    }

    public abstract class Command : ICommand
    {
        public abstract void Execute();

        protected virtual TResult Query<TResult>(IQuery<TResult> query)
        {
            query.Execute();
            return query.Result;
        }
    }

    public abstract class Command<TResult> : Command, ICommand<TResult>
    {
        public TResult Result { get; protected set; }
    }

    public interface ICommand<out TResult> : ICommand
    {
        TResult Result { get; }
    }

    public interface IQuery<out TResult> : ICommand<TResult>
    {
    }
}