using System;

namespace Sfs.EStore.UserAlerts
{
    public class ProgramSettings
    {
        public Uri ApiBaseAddress { get; set; }
        public string ApiAccessKey { get; set; }
        public string MandrillApiKey { get; set; }
        public string EmailTemplateName { get; set; }
        public string RavenUrl { get; set; }
        public string DatabaseName { get; set; }
        public double? MinimumNotifiedDaysPeriod { get; set; }
        public int MaximumVehicleList { get; set; }
    }
}