using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.UserAlerts
{
    public class Notify
    {
        private readonly UserSavedSearch _userSavedSearch;

        public Notify(MembershipUser user, UserSavedSearch userSavedSearch)
        {
            _userSavedSearch = userSavedSearch;
            User = user;
        }

        public string SearchId
        {
            get { return _userSavedSearch.Id; }
        }

        public MembershipUser User { get; private set; }
    }
}