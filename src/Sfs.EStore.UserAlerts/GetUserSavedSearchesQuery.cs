using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Raven.Client;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.UserAlerts.Commands;

namespace Sfs.EStore.UserAlerts
{
    public class GetUserSavedSearchesQuery : Command<IEnumerable<Notify>>
    {
        public IDocumentSession Session { get; set; }

        public override void Execute()
        {
            Debug.Assert(Session != null, "Session != null");

            Result = Session.Query<UserSavedSearch>()
                .Include<UserSavedSearch>(x => x.User)
                .Where(x => x.Searches.Any(s => s.AlertStatus.Enabled))
                .ToList()
                .Select(x => new Notify(Session.Load<MembershipUser>(x.User), x))
                .ToList();
        }
    }
}