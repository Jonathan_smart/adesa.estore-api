using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Raven.Client;
using Sfs.EStore.Api;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.UserAlerts.Commands;
using log4net;

namespace Sfs.EStore.UserAlerts
{
    /// <exception cref="AlertNotSentException"></exception>
    public class SavedSearchAlerter
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SavedSearchAlerter));

        private readonly IDocumentStore _store;
        private readonly Func<ISearchForVehiclesCommand> _searchCommandFactory;
        private readonly Func<ISendSearchAlertEmail> _sendEmailCommandFactory;

        public SavedSearchAlerter(IDocumentStore store, Func<ISearchForVehiclesCommand> searchCommandFactory, Func<ISendSearchAlertEmail> sendEmailCommandFactory)
        {
            _store = store;
            _searchCommandFactory = searchCommandFactory;
            _sendEmailCommandFactory = sendEmailCommandFactory;
        }

        private const double DefaultMinimumNotifiedDaysPeriod = 2;
        private double _minimumNotifiedDaysPeriod = DefaultMinimumNotifiedDaysPeriod;
        public double MinimumNotifiedDaysPeriod
        {
            get { return _minimumNotifiedDaysPeriod; }
            set { _minimumNotifiedDaysPeriod = value; }
        }

        public Task Run(IEnumerable<Notify> savedSearches)
        {
            return Task.Factory.StartNew(() => RunAlerter(savedSearches));
        }

        private void RunAlerter(IEnumerable<Notify> savedSearches)
        {
            Task.WaitAll(savedSearches.Select(AlertUserAsync).ToArray());
        }

        private Task AlertUserAsync(Notify notify)
        {
            return Task.Factory.StartNew(() => AlertUser(notify));
        }

        private void AlertUser(Notify notify)
        {
            using (var session = _store.OpenSession())
            {
                var user = notify.User;

                Logger.DebugFormat("[{0}] Checking saved searches for ({1})", user.Id, user.Username);

                var userSavedSearch = session.Load<UserSavedSearch>(notify.SearchId);
                var savedSearchesToAlert = userSavedSearch.Searches
                    .Where(s => s.AlertStatus.Enabled)
                    .ToArray();

                foreach (var savedSearch in savedSearchesToAlert)
                {
                    var lastNotifiedSinceDays =
                        (SystemTime.UtcNow - (savedSearch.AlertStatus.LastNotifiedAt ?? SystemTime.UtcNow)).TotalDays;

                    Logger.DebugFormat("[{0}] {1}: {2} days ago", user.Id, savedSearch.Name,
                                       lastNotifiedSinceDays.ToString("n2"));

                    if (lastNotifiedSinceDays < MinimumNotifiedDaysPeriod)
                    {
                        Logger.InfoFormat("[{0}] SKIP: Notifed only {1} days ago", user.Id, lastNotifiedSinceDays.ToString("n2"));
                        continue;
                    }

                    try
                    {
                        SendAlert(user, savedSearch, lastNotifiedSinceDays);
                    }
                    catch(SearchException ex)
                    {
                        Logger.Error("Cannot search for new vehicles", ex);
                    }
                    catch (AlertNotSentException ex)
                    {
                        Logger.Error(string.Format("Failed to send alert for user '{0}' and search '{1}'",
                            ex.User, ex.Search),
                            ex);
                    }
                }

                session.SaveChanges();
            }
        }

        private void SendAlert(MembershipUser user, SavedSearch savedSearch, double lastNotifiedSinceDays)
        {
            var searchCommand = _searchCommandFactory();
            searchCommand.Criteria =
                new Dictionary<string, object>(savedSearch.Criteria)
                    {
                        {"NewSinceDays", lastNotifiedSinceDays}
                    };

            searchCommand.Execute();

            var newVehicles = searchCommand.Result;

            if (Logger.IsDebugEnabled)
                Logger.DebugFormat("[{0}] {2}: {1} new vehicle(s) found since last alert", user.Id, newVehicles.Count(), savedSearch.Name);

            if (newVehicles.Any())
            {
                var sendEmailCommand = _sendEmailCommandFactory();
                sendEmailCommand.Search = savedSearch;
                sendEmailCommand.User = user;
                sendEmailCommand.Vehicles = newVehicles;

                sendEmailCommand.Execute();
                savedSearch.Alerted();
            }
        }
    }
}