using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Ninject.Extensions.ChildKernel;
using Shuttle.ESB.Core;

namespace Sfs.Shuttle
{
    public abstract class NinjectMessageHandlerFactoryWithMessageContainerBase : MessageHandlerFactory
    {
        private IServiceBus _bus;
        private static readonly Type Generic = typeof(IMessageHandler<>);

        protected NinjectMessageHandlerFactoryWithMessageContainerBase()
        {
            GetApplicationContainer();
        }

        public override void Initialize(IServiceBus bus)
        {
            _bus = bus;

            ApplicationContainer = GetApplicationContainer();
            ConfigureApplicationContainer(ApplicationContainer, bus);

            if (!ApplicationContainer.CanResolve<IServiceBus>())
                ApplicationContainer.Bind<IServiceBus>().ToConstant(bus);
        }

        public override IMessageHandler CreateHandler(object message)
        {
            var childContainer = CreateMessageContainer();
            ConfigureMessageContainer(childContainer, _bus, message);

            var all = childContainer.GetAll(Generic.MakeGenericType(message.GetType())).ToArray();

            return all.Any() ? (IMessageHandler)all.First() : null;
        }

        public override IEnumerable<Type> MessageTypesHandled
        {
            get { return new Type[0]; }
        }

        public IKernel ApplicationContainer { get; private set; }

        protected virtual void ConfigureApplicationContainer(IKernel container, IServiceBus bus)
        {
        }

        protected virtual void ConfigureMessageContainer(IKernel container, IServiceBus bus, object message)
        {
        }

        private static IKernel GetApplicationContainer()
        {
            return new StandardKernel();
        }

        public IKernel CreateMessageContainer()
        {
            var childContainer = new ChildKernel(ApplicationContainer, new NinjectSettings());

            return childContainer;
        }

        protected TenantConfigSettings TenantConfig(string tenantId)
        {
            return new TenantConfigSettings(tenantId);
        }
    }
}