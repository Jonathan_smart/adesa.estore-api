using System.Configuration;

namespace Sfs.Shuttle
{
    public class TenantConfigSettings
    {
        private readonly string _tenantId;

        public TenantConfigSettings(string tenantId)
        {
            _tenantId = tenantId;
        }

        public bool IsConsumerEnabled<TConsumer>()
        {
            var consumerPrefix = string.Format("{0}/{1}", _tenantId, typeof(TConsumer).Name);
            return ConfigurationManager.AppSettings[string.Format("{0}/Enabled", consumerPrefix)] == "true";
        }

        public string ConsumerSetting<TConsumer>(string key)
        {
            var consumerPrefix = string.Format("{0}/{1}", _tenantId, typeof(TConsumer).Name);
            return ConfigurationManager.AppSettings[string.Format("{0}/{1}", consumerPrefix, key)];
        }
    }
}