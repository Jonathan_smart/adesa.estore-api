﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sfs.Core;

namespace Sfs.Orders.Ports
{
    public class SalesOrderInfo
    {
        public SalesOrderInfo(string no, DateTime date, SalesOrderLines lines, CustomerInfo sellToCustomer, ShippingInfo shipping, IEnumerable<OrderObject> imagePaths)
        {
            No = no;
            Date = date;
            Lines = lines;
            SellToCustomer = sellToCustomer;
            Shipping = shipping;
            ImagePaths = imagePaths;
        }

        public string No { get; private set; }
        public DateTime Date { get; private set; }
        public SalesOrderLines Lines { get; private set; }
        public CustomerInfo SellToCustomer { get; private set; }
        public ShippingInfo Shipping { get; private set; }
        public IEnumerable<OrderObject> ImagePaths { get; set; }
    }
    public class OrderObject
    {
        public int id { get; set; }
        public int sortOrder { get; set; }
        public string FilePath { get; set; }
    }

    public class SalesOrderLines : IEnumerable<SalesOrderLine>
    {
        private readonly IEnumerable<SalesOrderLine> _list;

        public SalesOrderLines(IEnumerable<SalesOrderLine> list)
        {
            _list = list.ToArray();
            SubTotal = _list.Select(x => x.Amount).Aggregate(Money.Zero(), (working, next) => working+next);
            Vat = _list.Select(x => x.Vat).Aggregate(Money.Zero(), (working, next) => working+next);
            Total = SubTotal + Vat;
        }

        public Money SubTotal { get; private set; }
        public Money Vat { get; private set; }
        public Money Total { get; private set; }

        public IEnumerator<SalesOrderLine> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class SalesOrderLine
    {
        public SalesOrderLine(Money amount, Money vat, Money total, string description)
        {
            Amount = amount;
            Vat = vat;
            Total = total;
            Description = description;
        }

        public Money Amount { get; private set; }
        public Money Vat { get; private set; }
        public Money Total { get; private set; }
        public string Description { get; private set; }
    }

    public class CustomerInfo
    {
        public CustomerInfo(string no, string name)
        {
            No = no;
            Name = name;
        }

        public string No { get; private set; }
        public string Name { get; private set; }
    }

    public class ShippingInfo
    {
        public ShippingInfo(ShippingAddress address, string method, string agent, string service)
        {
            Address = address;
            Method = method;
            Agent = agent;
            Service = service;
        }

        public ShippingAddress Address { get; private set; }
        public string Method { get; private set; }
        public string Agent { get; private set; }
        public string Service { get; private set; }

        public class ShippingAddress
        {
            public ShippingAddress(string name, string postCode)
            {
                Name = name;
                PostCode = postCode;
            }

            public string Name { get; private set; }
            public string PostCode { get; private set; }
        }
    }
}