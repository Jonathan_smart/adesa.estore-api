﻿using System.Collections.Generic;

namespace Sfs.Orders.Ports
{
    public interface IFinanceOptionsQuery
    {
        IEnumerable<FinanceOption> GetAll();
    }

    public class FinanceOption
    {
        public string Description { get; set; }
        public string Method { get; set; }
        public string Agent { get; set; }
        public string Service { get; set; }
        public string CreditType { get; set; }
    }
}