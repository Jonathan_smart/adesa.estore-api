﻿using System.Globalization;

namespace Sfs.Orders.Ports
{
    public struct BasketEntryNo
    {
        private readonly int _value;

        public BasketEntryNo(int value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value.ToString(CultureInfo.InvariantCulture);
        }

        public static implicit operator int(BasketEntryNo item)
        {
            return item._value;
        }
    }
}