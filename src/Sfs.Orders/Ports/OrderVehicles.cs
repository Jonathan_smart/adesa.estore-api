﻿using System;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;

namespace Sfs.Orders.Ports
{
    public class OrderVehicles : Command
    {
        public OrderVehicles(OrderItem[] items)
            : base(Guid.NewGuid())
        {
            Items = items;
        }

        public OrderItem[] Items { get; private set; }
        public OrderResult[] Result { get; set; }

        public class OrderResult
        {
            private OrderResult(BasketEntryNo basketEntryNo, SerialNo serialNo, OrderNo orderNo)
            {
                Success = true;
                OrderNo = orderNo;
                SerialNo = serialNo;
                BasketEntryNo = basketEntryNo;
            }

            private OrderResult(BasketEntryNo basketEntryNo, SerialNo serialNo, string message = null, bool success = false)
            {
                Success = success;
                SerialNo = serialNo;
                Message = message;
                BasketEntryNo = basketEntryNo;
            }

            public BasketEntryNo BasketEntryNo { get; private set; }
            public bool Success { get; private set; }
            public string Message { get; private set; }
            public OrderNo OrderNo { get; private set; }
            public SerialNo SerialNo { get; private set; }

            public override string ToString()
            {
                return string.Format("Success: {0}, Basket: {3}, SerialNo: {2}, OrderNo: {1}", Success, OrderNo, SerialNo, BasketEntryNo);
            }

            public static OrderResult Successful(BasketEntryNo basket, SerialNo serialNo, OrderNo orderNo)
            {
                return new OrderResult(basket, serialNo, orderNo);
            }

            public static OrderResult Failure(BasketEntryNo basket, SerialNo serialNo, string message)
            {
                return new OrderResult(basket, serialNo, success: false, message: message);
            }
        }


        public class OrderItem
        {
            public string CustomerNo { get; set; }
            public bool FinanceRequired { get; set; }
            public ItemFinance Finance { get; set; }
            public BasketEntryNo BasketEntryNo { get; set; }
            public ItemTransport Transport { get; set; }
            public ItemShipping Shipping { get; set; }
            public ItemV5Request V5 { get; set; }
            public decimal? BuyersFee { get; set; }
            public decimal? Price { get; set; }
            public bool IsRetail { get; set; }

            public override string ToString()
            {
                return string.Format("CustomerNo: {0}, Finance: {{{1}}}, BasketEntryNo: {2}, Transport: {{{3}}}, V5: {{{4}}}, Shipping: {{{5}}}, Price: {6}, FinanceRequired: {7}", CustomerNo, Finance, BasketEntryNo, Transport, V5, Shipping, Price, FinanceRequired);
            }
        }

        public class ItemFinance
        {
            /// <exception cref="ArgumentNullException"><paramref name="method"/> is <see langword="null" />.</exception>
            /// <exception cref="ArgumentNullException"><paramref name="agent"/> is <see langword="null" />.</exception>
            /// <exception cref="ArgumentNullException"><paramref name="service"/> is <see langword="null" />.</exception>
            public ItemFinance(string method, string agent, string service)
            {
                if (method == null) throw new ArgumentNullException("method");
                if (agent == null) throw new ArgumentNullException("agent");
                if (service == null) throw new ArgumentNullException("service");

                Method = method;
                Agent = agent;
                Service = service;
            }

            public string Method { get; private set; }
            public string Agent { get; private set; }
            public string Service { get; private set; }

            protected bool Equals(ItemFinance other)
            {
                return string.Equals(Method, other.Method) && string.Equals(Agent, other.Agent) && Service == other.Service;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != GetType()) return false;
                return Equals((ItemFinance)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    int hashCode = (Method != null ? Method.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (Agent != null ? Agent.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (Service != null ? Service.GetHashCode() : 0);
                    return hashCode;
                }
            }

            public override string ToString()
            {
                return string.Format("Method: {0}, Agent: {1}, Service: {2}", Method, Agent, Service);
            }
        }

        public class ItemTransport
        {
            public ItemTransport(decimal price)
            {
                Price = price;
            }

            public decimal Price { get; private set; }

            public override string ToString()
            {
                return string.Format("Price: {0}", Price);
            }
        }

        public class ItemShipping
        {
            public ItemShipping(string method)
            {
                Method = method;
            }

            public string Method { get; private set; }

            public string Agent { get; set; }

            public string Service { get; set; }

            public AddressCode AddressCode { get; set; }

            public override string ToString()
            {
                return string.Format("Method: {0}, Agent: {1}, Service: {2}, AddressCode: {3}", Method, Agent, Service, AddressCode);
            }
        }

        public class ItemV5Request
        {
            public AddressCode ShipToAddressCode { get; set; }

            public override string ToString()
            {
                return string.Format("ShipToAddressCode: {0}", ShipToAddressCode);
            }
        }
    }
}
