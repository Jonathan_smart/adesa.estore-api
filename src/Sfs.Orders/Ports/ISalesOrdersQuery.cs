﻿using System.Collections.Generic;

namespace Sfs.Orders.Ports
{
    public interface ISalesOrdersQuery
    {
        IEnumerable<SalesOrderInfo> Handle(params string[] orderNos);
    }
}