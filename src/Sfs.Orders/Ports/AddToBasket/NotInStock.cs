﻿using Sfs.Core;

namespace Sfs.Orders.Ports.AddToBasket
{
    public class NotInStock : CannotAddToBasket
    {
        public NotInStock(SerialKey serial) : base(serial)
        {
        }
    }
}