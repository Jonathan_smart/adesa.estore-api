﻿using Sfs.Core;

namespace Sfs.Orders.Ports.AddToBasket
{
    public class AlreadyReserved : CannotAddToBasket
    {
        public AlreadyReserved(SerialKey serial) : base(serial)
        {
        }
    }
}