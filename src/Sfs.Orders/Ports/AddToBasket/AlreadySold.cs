﻿using Sfs.Core;

namespace Sfs.Orders.Ports.AddToBasket
{
    public class AlreadySold : CannotAddToBasket
    {
        public AlreadySold(SerialKey serial) : base(serial)
        {
        }
    }
}