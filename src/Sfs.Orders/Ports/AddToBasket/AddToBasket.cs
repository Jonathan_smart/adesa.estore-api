﻿using System;
using System.Globalization;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;

namespace Sfs.Orders.Ports.AddToBasket
{
    public class AddToBasket : Command
    {
        public AddToBasket(SerialKey serial, SerialNoPricesSalesChannel salesChannel, Money amount, BusinessUnit business,
            string contactNo, string salesContactNo, string customerNo, bool buyItNow, bool isAuction = false, bool isRetail = false) : base(Guid.NewGuid())
        {
            Business = business;
            Serial = serial;
            SalesChannel = salesChannel;
            Amount = amount;
            ContactNo = contactNo;
            SalesContactNo = salesContactNo;
            CustomerNo = customerNo;
            IsRetail = isRetail;
            BuyItNow = buyItNow;
            IsAuction = isAuction;

        }

        public BasketEntryNo Result { get; set; }
        public BusinessUnit Business { get; set; }
        public SerialKey Serial { get; private set; }
        public SerialNoPricesSalesChannel SalesChannel { get; private set; }
        public Money Amount { get; private set; }
        public string ContactNo { get; private set; }
        public string SalesContactNo { get; private set; }
        public bool BuyItNow { get; set; }
        public bool IsAuction { get; set; }
        public bool IsRetail { get; set; }

        public TimeSpan? ExpiresIn { get; set; }
        public string CustomerNo { get; set; }

        public struct SerialNoPricesSalesChannel
        {
            private readonly int _value;

            public SerialNoPricesSalesChannel(int value)
            {
                _value = value;
            }

            public override string ToString()
            {
                return _value.ToString(CultureInfo.InvariantCulture);
            }

            public static implicit operator int(SerialNoPricesSalesChannel item)
            {
                return item._value;
            }
        }
    }
}
