﻿using System;
using Sfs.Core;

namespace Sfs.Orders.Ports.AddToBasket
{
    public class CannotAddToBasket : Exception
    {
        public CannotAddToBasket(SerialKey serial)
        {
            Serial = serial;
        }

        public CannotAddToBasket(SerialKey serial, Exception innerException)
            : base("", innerException)
        {
            Serial = serial;
        }

        public SerialKey Serial { get; private set; }
    }
}