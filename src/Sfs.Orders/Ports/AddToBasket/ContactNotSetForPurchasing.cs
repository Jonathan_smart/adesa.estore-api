﻿using Sfs.Core;

namespace Sfs.Orders.Ports.AddToBasket
{
    public class ContactNotSetForPurchasing : CannotAddToBasket
    {
        public ContactNotSetForPurchasing(SerialKey serial, string contactNo, string customerNo) : base(serial)
        {
            ContactNo = contactNo;
            CustomerNo = customerNo;
        }

        public string ContactNo { get; private set; }
        public string CustomerNo { get; private set; }
    }
}