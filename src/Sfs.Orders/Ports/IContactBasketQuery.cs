﻿using System;
using Sfs.Core;

namespace Sfs.Orders.Ports
{
    public interface IContactBasketQuery
    {
        IPagedSet<ContactBasketQueryResponse> Handle(ContactBasketQueryRequest request);
    }

    public class ContactBasketQueryRequest
    {
        public ContactBasketQueryRequest(string contactNo, QueryRange range, BasketEntryNo[] entryNos = null)
        {
            if (contactNo == null) throw new ArgumentNullException("contactNo");

            ContactNo = contactNo;
            EntryNos = entryNos;
            Range = range ?? new QueryRange(50, 0);
        }

        public QueryRange Range { get; private set; }

        public string ContactNo { get; private set; }
        public BasketEntryNo[] EntryNos { get; private set; }

        public class QueryRange
        {
            public QueryRange(int start, int max)
            {
                Start = start;
                Max = max;
            }

            public int Start { get; private set; }
            public int Max { get; private set; }
        }
    }

    public class ContactBasketQueryResponse
    {
        public ContactBasketQueryResponse(BasketEntryNo entryNo, string title, RegistrationInfo registration, int mileage,
            Money amount, bool pricesIncludeVat, CloudFilesImage primaryImage, bool buyitNow, DateTime? expiresAt = null)
        {
            EntryNo = entryNo;
            Title = title;
            Registration = registration;
            Mileage = mileage;
            Amount = amount;
            PricesIncludeVat = pricesIncludeVat;
            PrimaryImage = primaryImage;
            ExpiresAt = expiresAt;
            BuyitNow = buyitNow;
        }

        public BasketEntryNo EntryNo { get; private set; }
        public CloudFilesImage PrimaryImage { get; private set; }
        public string Title { get; private set; }
        public RegistrationInfo Registration { get; private set; }
        public int Mileage { get; private set; }
        public bool Expires { get { return ExpiresAt.HasValue; } }
        public DateTime? ExpiresAt { get; private set; }

        public Money Amount { get; private set; }
        public bool PricesIncludeVat { get; private set; }
        public bool BuyitNow { get; private set; }
        // GM doesn't seem to store VAT values within the basket table so it can't be supported accross
        // both GM and SFS. Things maybe different when GM moves to Nav2015

        //public Money Vat { get; private set; }
        //public Money Total { get; private set; }

        public class CloudFilesImage
        {
            public CloudFilesImage(string container, string fileName)
            {
                Container = container;
                FileName = fileName;
            }

            public string Container { get; private set; }

            public string FileName { get; private set; }
        }

        public class RegistrationInfo
        {
            public RegistrationInfo(string plate, string yap)
            {
                Plate = plate;
                Yap = yap;
            }

            public string Plate { get; private set; }
            public string Yap { get; private set; }
        }
    }
}
