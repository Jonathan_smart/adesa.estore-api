﻿using System.Collections.Generic;

namespace Sfs.Orders.Ports
{
    public interface ICreateSalesOrderTransportSalesPriceQuery
    {
        IEnumerable<TransportSalesPrice> GetForMethod(string method);
    }

    public class TransportSalesPrice
    {
        public TransportSalesPrice(string methodCode, string shippingAgentCode)
        {
            MethodCode = methodCode;
            ShippingAgentCode = shippingAgentCode;
        }

        public string MethodCode { get; private set; }
        public string ShippingAgentCode { get; private set; }
    }
}