namespace Sfs.Orders.Ports
{
    public struct OrderNo
    {
        private readonly string _value;

        public OrderNo(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }

        public static implicit operator string(OrderNo item)
        {
            return item._value;
        }
    }
}