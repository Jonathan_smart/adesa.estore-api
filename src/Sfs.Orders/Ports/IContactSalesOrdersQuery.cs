﻿using System;

namespace Sfs.Orders.Ports
{
    public interface IContactSalesOrdersQuery
    {
        IPagedSet<SalesOrderInfo> Handle(GetForContactQuery query);
    }

    public class GetForContactQuery
    {
        public GetForContactQuery(string contactNo, QueryRange range)
        {
            if (contactNo == null) throw new ArgumentNullException("contactNo");

            ContactNo = contactNo;
            Range = range ?? new QueryRange(50, 0);
        }

        public QueryRange Range { get; set; }
        public string ContactNo { get; private set; }

        public class QueryRange
        {
            public QueryRange(int start, int max)
            {
                Start = start;
                Max = max;
            }

            public int Start { get; private set; }
            public int Max { get; private set; }
        }
    }
}