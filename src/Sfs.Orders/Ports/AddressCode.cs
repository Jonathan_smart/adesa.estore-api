﻿namespace Sfs.Orders.Ports
{
    public struct AddressCode
    {
        private readonly string _value;

        public AddressCode(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }

        public static implicit operator string(AddressCode item)
        {
            return item._value;
        }
    }
}