﻿namespace Sfs.Orders.Ports
{
    public interface ICalculateTransportShipToAddressQuery
    {
        ShipToAddress GetForCustomerNoAndAddressCode(string customerNo, string addressCode);
    }

    public class ShipToAddress
    {
        public string PostCode { get; set; }
    }
}