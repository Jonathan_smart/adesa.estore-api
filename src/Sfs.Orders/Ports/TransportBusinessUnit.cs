﻿namespace Sfs.Orders.Ports
{
    public struct TransportBusinessUnit
    {
        private readonly string _value;

        public TransportBusinessUnit(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }

        public static implicit operator string(TransportBusinessUnit item)
        {
            return item._value;
        }
    }
}