﻿using System;
using System.Collections.Generic;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;

namespace Sfs.Orders.Ports
{
    public class CalculateTransport : Command
    {
        public IEnumerable<TransportResponseItem> Result { get; set; }
        public bool WithVat { get; set; }
        public IEnumerable<TransportRequestItem> ItemsToBeTransported { get; private set; }

        public CalculateTransport(IEnumerable<TransportRequestItem> itemsToBeTransported,bool withVat)
            : base(Guid.NewGuid())
        {
            ItemsToBeTransported = itemsToBeTransported;
            WithVat = withVat;
        }

        public class TransportRequestItem
        {
            public BasketEntryNo Basket { get; set; }
            public string ShippingMethod { get; set; }
            public string ShippingAgent { get; set; }
            public string ShippingAgentService { get; set; }
            public SerialKey Vehicle { get; set; }
            public TransportRequestTo To { get; set; }
        }

        public class TransportRequestTo
        {
            public string CustomerNo { get; set; }
            public string AddressCode { get; set; }
        }

        public class TransportResponseItem
        {
            public TransportResponseItem(BasketEntryNo basket, decimal cost)
            {
                Basket = basket;
                Cost = cost;
            }

            public BasketEntryNo Basket { get; private set; }
            public decimal Cost { get; private set; }
        }
    }
}
