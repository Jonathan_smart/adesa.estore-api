﻿using System.Collections.Generic;
using Sfs.Core;

namespace Sfs.Orders.Ports
{
    public interface ISerialNoInformationByRegistrationQuery
    {
        IEnumerable<SerialNoInformation> Handle(IEnumerable<string> registrationNos);
    }

    public class SerialNoInformation
    {
        public SerialKey Key { get; set; }
        public string RegistrationNo { get; set; }
    }
}
