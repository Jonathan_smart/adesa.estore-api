﻿namespace Sfs.Orders.Ports
{
    public struct TransportChargeItem
    {
        private readonly string _value;

        public TransportChargeItem(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }

        public static implicit operator string(TransportChargeItem item)
        {
            return item._value;
        }
    }
}