﻿using System.Collections.Generic;
using Sfs.Core;

namespace Sfs.Orders.Ports
{
    public interface ICreateSalesOrderBasketItemsQuery
    {
        IEnumerable<BasketItem> Handle(params BasketEntryNo[] entryNos);
    }

    public class BasketItem
    {
        public BasketEntryNo EntryNo { get; set; }
        public string CustomerNo { get; set; }
        public string ContactNo { get; set; }
        public decimal Price { get; set; }
        public decimal Amount { get; set; }
        public string LocationCode { get; set; }
        public int SalesChannel { get; set; }

        public BasketVehicle Vehicle { get; set; }

        public class BasketVehicle
        {
            public SerialKey Key { get; set; }
            public bool Commercial { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string Derivative { get; set; }
            public string VatProdPostingGroup { get; set; }
            public BusinessUnit BusinessUnit { get; set; }
        }
    }
}
