﻿using System;
using Sfs.Darker.CommandProcessor;

namespace Sfs.Orders.Ports
{
    public class RemoveFromBasket : Command
    {
        public RemoveFromBasket(BasketEntryNo item) : base(Guid.NewGuid())
        {
            Item = item;
        }

        public BasketEntryNo Item { get; private set; }
    }
}