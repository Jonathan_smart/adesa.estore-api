﻿using Sfs.Orders.Ports;

namespace Sfs.Orders.Adapters
{
    public class BasketsNotFoundException : SalesOrderBuilderException
    {
        public BasketsNotFoundException(params BasketEntryNo[] entryNos)
            : base(BuildMessage(entryNos))
        { }

        private static string BuildMessage(params BasketEntryNo[] entryNos)
        {
            return string.Format("Not all baskets can be loaded: {0}\n\n" +
                                 "This may be a permissions issue.", string.Join("; ", entryNos));
        }
    }

    public class SalesOrderBuilderException : System.Exception
    {
        public SalesOrderBuilderException(string message)
            : base(message)
        { }
    }

    public class ShippingAddressMissingException : SalesOrderBuilderException
    {
        public ShippingAddressMissingException(string shippingMethod, AddressCode addressCode, string agent, string service) 
            : base(BuildMessage(shippingMethod, addressCode, agent, service))
        {
        }

        private static string BuildMessage(string shippingMethod, AddressCode addressCode, string agent, string service)
        {
            return string.Format(
                "The transport sales price for the shipping method specified ({0}) is setup with a shipping agent " +
                "but the order item hasn't specified one or more of address ({1}), agent ({2}) or service ({3}). \n\n" +
                "Ensure that the order item has full address details specified or if the transport doesn't " +
                "require an agent, change it in the database",
                shippingMethod, addressCode, agent, service);
        }
    }
}
