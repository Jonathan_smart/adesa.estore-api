﻿using System;
using System.Security.Policy;
using Sfs.Darker.CommandProcessor;
using Sfs.Orders.SfsNavBasketService;
using Sfs.Orders.Ports;
using Sfs.Orders.Ports.AddToBasket;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace Sfs.Orders.Adapters
{
    public class AddToBasketBasketHandler : RequestHandler<AddToBasket>
    {
        private readonly Basket_Service _service;

        public AddToBasketBasketHandler(Url navBasketServiceUrl, ILog logger)
            : base(logger)
        {
            _service = new Basket_Service
            {
                UseDefaultCredentials = true,
                Url = navBasketServiceUrl.Value
            };
        }

        public override AddToBasket Handle(AddToBasket command)
        {
            var basket = new Basket
            {
                Serial_No = command.Serial.SerialNo,
                Item_No = command.Serial.ItemNo,
                Variant_Code = command.Serial.VariantCode,
                PriceSpecified = true,
                Price = command.Amount.Amount,
                Contact_No = command.ContactNo,
                AmountSpecified = true,
                Amount = command.Amount.Amount,
                SNPSC_Entry_No = command.SalesChannel,
                SNPSC_Entry_NoSpecified = true,
                Customer_No = command.CustomerNo,
                Basket_Entry_Type = command.IsAuction ? Basket_Entry_Type.Auction : Basket_Entry_Type.Sales_Order,
                Basket_Entry_TypeSpecified = true,
                Basket_Expiry_Period_MINS = command.ExpiresIn.HasValue ? (int)command.ExpiresIn.Value.TotalMinutes : 0,
                Basket_Expiry_Period_MINSSpecified = command.ExpiresIn.HasValue,
                Sales_Person_Contact_No = command.SalesContactNo,
                Buy_It_Now = command.BuyItNow,
                Buy_It_NowSpecified = command.BuyItNow,
            };

            try
            {

                _service.Create(ref basket);

                command.Result = new BasketEntryNo(basket.Entry_No);

                return command;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Could not add {serialNo} to {contact} basket", _service.Url, basket.ToString(),
                    command.Serial.SerialNo,
                    command.ContactNo);

                throw ParseReservationErrorMessage(ex, command);
            }
        }

        private void LogXML(Basket Basket)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(Basket));
            var xml = "";

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, Basket);
                    xml = sww.ToString(); // Your XML
                }
            }

            Logger.Error(xml);
        }

        private static CannotAddToBasket ParseReservationErrorMessage(Exception ex, AddToBasket command)
        {
            var errorMessage = ex.Message;

            if (errorMessage.Contains("unique in the Basket"))
                return new AlreadyReserved(command.Serial);
            if (errorMessage.Contains("already reserved at Sales Line") || errorMessage.Contains("is already reserved at Sales Order"))
                return new AlreadySold(command.Serial);
            if (errorMessage.Contains("There is no Item Ledger Entry within the filter"))
                return new NotInStock(command.Serial);
            if (errorMessage.Contains("No Contact No found for given user information"))
                return new ContactNotSetForPurchasing(command.Serial, command.ContactNo, command.CustomerNo);

            return new CannotAddToBasket(command.Serial, ex);
        }
    }
}