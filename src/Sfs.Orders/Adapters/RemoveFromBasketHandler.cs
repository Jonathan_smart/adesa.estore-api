﻿using System.Security.Policy;
using Sfs.Darker.CommandProcessor;
using Sfs.Orders.Ports;
using Sfs.Orders.SfsNavBasketService;

namespace Sfs.Orders.Adapters
{
    public class RemoveFromBasketHandler : RequestHandler<RemoveFromBasket>
    {
        private readonly Basket_Service _service;

        public RemoveFromBasketHandler(Url navBasketServiceUrl, ILog logger) : base(logger)
        {
            _service = new Basket_Service
            {
                UseDefaultCredentials = true,
                Url = navBasketServiceUrl.Value
            };
        }

        public override RemoveFromBasket Handle(RemoveFromBasket command)
        {
            var navBasket = _service.Read(command.Item);
            _service.Delete(navBasket.Key);

            return command;
        }
    }
}
