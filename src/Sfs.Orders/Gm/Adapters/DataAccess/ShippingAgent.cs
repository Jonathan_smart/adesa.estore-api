﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    internal class ShippingAgent
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public byte Vehicle { get; set; }
    }

    internal class ShippingAgentMapper : EntityTypeConfiguration<ShippingAgent>
    {
        public ShippingAgentMapper()
        {
            ToTable("SFS$Shipping Agent");
            HasKey(e => e.Code);
        }
    }
}
