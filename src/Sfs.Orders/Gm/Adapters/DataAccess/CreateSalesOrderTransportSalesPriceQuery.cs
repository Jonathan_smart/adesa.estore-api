using System.Collections.Generic;
using System.Linq;
using Sfs.Core;
using Sfs.Orders.Ports;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    public class CreateSalesOrderTransportSalesPriceQuery : ICreateSalesOrderTransportSalesPriceQuery
    {
        private readonly SalesChannel _salesChannel;
        private readonly BusinessUnit _businessUnit;

        public CreateSalesOrderTransportSalesPriceQuery(SalesChannel salesChannel, BusinessUnit businessUnit)
        {
            _salesChannel = salesChannel;
            _businessUnit = businessUnit;
        }

        public IEnumerable<Ports.TransportSalesPrice> GetForMethod(string method)
        {
            using (var dbContext = new NavisionContext())
            {
                var entity = dbContext.Set<TransportSalesPrice>()
                    .Where(x => x.GlobalDimension1Code == _businessUnit)
                    .Where(x => x.SalesChannelNo == _salesChannel)
                    .Where(x => x.ShipmentMethodCode == method)
                    .ToArray();

                return entity
                    .Select(x => new Ports.TransportSalesPrice(x.ShipmentMethodCode, x.ShippingAgentCode))
                    .ToArray();
            }
        }
    }
}