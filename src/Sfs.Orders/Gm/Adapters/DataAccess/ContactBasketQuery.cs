﻿using System.Data.Entity;
using System.Linq;
using Sfs.Core;
using Sfs.Orders.Ports;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    public class ContactBasketQuery : IContactBasketQuery
    {
        private readonly BusinessUnit _businessUnit;

        public ContactBasketQuery(BusinessUnit businessUnit)
        {
            _businessUnit = businessUnit;
        }

        public IPagedSet<ContactBasketQueryResponse> Handle(ContactBasketQueryRequest request)
        {
            using (var dbContext = new NavisionContext())
            {
                var customerNos = new ContactCustomerNosQuery(dbContext, _businessUnit).Handle(request.ContactNo);
                const int salesOrderBasketType = 1;

                var query = dbContext.Set<Basket>()
                    .Include(x => x.SerialNoInformation)
                    .OrderByDescending(x => x.AddedToBasketOn)
                    .Where(x => customerNos.Contains(x.CustomerNo) || x.ContactNo == request.ContactNo)
                    .Where(x => x.BusinessUnit == _businessUnit)
                    .Where(x => x.Type == salesOrderBasketType)
                    .Select(x => new
                    {
                        x.No,
                        x.SerialNoInformation.CarlineCode,
                        x.SerialNoInformation.Bodystyle,
                        x.SerialNoInformation.Transmission,
                        x.SerialNoInformation.Engine,
                        x.SerialNoInformation.Trim,
                        x.SerialNoInformation.RegistrationNo,
                        x.SerialNoInformation.RegistrationYap,
                        x.SerialNoInformation.Mileage,
                        x.Amount,
                        x.AmountIncVat,
                        x.IsCommercial,
                        x.ExpiryDate,
                        x.BuyItNow,
                        Image = x.SerialNoInformation.Attachments.OrderBy(a => a.SortOrder).Where(a => a.VisibleToCustomers == 1).Take(1).FirstOrDefault()
                    });

                if (request.EntryNos != null && request.EntryNos.Length > 0)
                {
                    var entryNos = request.EntryNos.Select(x => (int)x).ToArray();

                    query = query.Where(x => entryNos.Contains(x.No));
                }

                var baskets = query.Skip(request.Range.Start).Take(request.Range.Max).ToArray().Select(x =>
                {
                    var title = string.Format("{0} {1} {2} {3} {4}", x.CarlineCode, x.Bodystyle, x.Transmission, x.Engine, x.Trim);

                    return new ContactBasketQueryResponse(new BasketEntryNo(x.No),
                        title, new ContactBasketQueryResponse.RegistrationInfo(x.RegistrationNo, x.RegistrationYap),
                        (int)x.Mileage,
                        new Money(x.Amount), x.IsCommercial == 0,
                        x.Image == null
                            ? null
                            : new ContactBasketQueryResponse.CloudFilesImage(x.Image.CloudFilesContainer,
                                x.Image.Filename), x.BuyItNow == 0,
                        x.ExpiryDate.AsNullableNavisionDate());
                }).ToArray();

                return new PagedSetWrapper<ContactBasketQueryResponse>(baskets,
                    request.Range.Start, request.Range.Max, query.Count());
            }
        }
    }
}
