﻿using System;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    internal static class NavisionExtensions
    {
        private static readonly DateTime NullNavisionDate = new DateTime(1753, 1, 1);

        public static DateTime? AsNullableNavisionDate(this DateTime @this)
        {
            return @this == NullNavisionDate ? (DateTime?)null : @this;
        }
    }
}