﻿using System.Data.Entity;
using System.Reflection;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    internal class NavisionContext : DbContext
    {
        public NavisionContext()
            : base("Name=NavisionContext")
        {
            Database.SetInitializer((IDatabaseInitializer<NavisionContext>)null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}