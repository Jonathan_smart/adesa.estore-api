using System.Data.Entity.ModelConfiguration;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    internal class SerialNoAttachment
    {
        public int EntryNo { get; set; }
        public string ItemNo { get; set; }
        public string VariantCode { get; set; }
        public string SerialNo { get; set; }
        public string Filename { get; set; }
        public string CloudFilesContainer { get; set; }
        public int SortOrder { get; set; }
        public byte VisibleToCustomers { get; set; }
    }

    internal class SerialNoAttachmentMapper : EntityTypeConfiguration<SerialNoAttachment>
    {
        public SerialNoAttachmentMapper()
        {
            ToTable("SFS$Serial No_ Attachments");
            HasKey(e => e.EntryNo);
            Property(e => e.EntryNo).HasColumnName("Entry No_");

            Property(e => e.ItemNo).HasColumnName("Item No_");
            Property(e => e.VariantCode).HasColumnName("Variant Code");
            Property(e => e.SerialNo).HasColumnName("Serial No_");

            Property(e => e.Filename).HasColumnName("Filename");
            Property(e => e.CloudFilesContainer).HasColumnName("Cloud Files Container");

            Property(e => e.VisibleToCustomers).HasColumnName("Visible To Customers");
            Property(e => e.SortOrder).HasColumnName("Sort Order");
        }
    }
}