using System.Linq;
using Sfs.Core;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    internal class ContactCustomerNosQuery
    {
        private readonly NavisionContext _dbContext;
        private readonly BusinessUnit _businessUnit;

        public ContactCustomerNosQuery(NavisionContext dbContext, BusinessUnit businessUnit)
        {
            _dbContext = dbContext;
            _businessUnit = businessUnit;
        }

        public string[] Handle(string contactNo)
        {
            var customerNos = _dbContext.Set<Contact>()
                .Where(x => x.GlobalDimension1Code == _businessUnit)
                .Where(x => x.No == contactNo)
                .Join(_dbContext.Set<ContactRelationship>(), c => c.No, cr => cr.ContactNo, (c, cr) => cr)
                .Where(x => x.SourceType == 1)
                .Where(x => x.AuthorizedBy != "")
                .Select(x => x.SourceNo);

            return customerNos.ToArray();
        }
    }
}