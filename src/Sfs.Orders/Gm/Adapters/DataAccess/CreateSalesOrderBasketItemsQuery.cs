using System.Collections.Generic;
using System.Linq;
using Sfs.Core;
using Sfs.Orders.Ports;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    public class CreateSalesOrderBasketItemsQuery : ICreateSalesOrderBasketItemsQuery
    {
        public IEnumerable<BasketItem> Handle(params BasketEntryNo[] entryNos)
        {
            using (var dbContext = new NavisionContext())
            {
                var basketIds = entryNos.Select(x => (int)x).ToArray();

                var baskets = dbContext.Set<Basket>().Where(x => basketIds.Contains(x.No))
                    .ToList();

                var vehicleIds = baskets.Select(x => x.SerialNo);
                var vehicles = dbContext.Set<SerialNoInformation>()
                    .Where(x => x.ItemNo == ItemNo.Vehicle && vehicleIds.Contains(x.SerialNo))
                    .ToList();

                var items = baskets.Join(vehicles, b => b.SerialNo, v => v.SerialNo,
                    (b, v) => new { basket = b, vehicle = v });

                return items.Select(x => new BasketItem
                {
                    Amount = x.basket.Amount,
                    ContactNo = x.basket.ContactNo,
                    CustomerNo = x.basket.CustomerNo,
                    EntryNo = new BasketEntryNo(x.basket.No),
                    LocationCode = x.basket.LocationCode,
                    Price = x.basket.Price,
                    Vehicle = new BasketItem.BasketVehicle
                    {
                        Key = new SerialKey(new ItemNo(x.vehicle.ItemNo),
                            new VariantCode(x.vehicle.VariantCode),
                            new SerialNo(x.vehicle.SerialNo)),
                        BusinessUnit = new BusinessUnit(x.vehicle.GlobDim1Code),
                        Derivative = x.vehicle.Model,
                        Commercial = x.vehicle.CommercialVehicle == 1,
                        Make = x.vehicle.BrandCode,
                        Model = x.vehicle.CarlineCode,
                        VatProdPostingGroup = x.vehicle.VatProdPostingGroup
                    },
                    SalesChannel = x.basket.SalesChannelNo
                })
                    .ToArray();
            }
        }
    }
}