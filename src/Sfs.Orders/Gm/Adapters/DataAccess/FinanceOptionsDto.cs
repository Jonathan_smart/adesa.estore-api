using System.Collections.Generic;
using System.Linq;
using Sfs.Core;
using Sfs.Orders.Ports;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    public class FinanceOptionsQuery : IFinanceOptionsQuery
    {
        private readonly BusinessUnit _businessUnit;

        public FinanceOptionsQuery(BusinessUnit businessUnit)
        {
            _businessUnit = businessUnit;
        }

        public IEnumerable<FinanceOption> GetAll()
        {
            using (var dbContext = new NavisionContext())
            {
                var query = dbContext.Set<FundingAgent>()
                    .Where(x => x.BusinessUnit == _businessUnit)
                    .Join(dbContext.Set<FundingAgentService>(),
                        agent => agent.Agent,
                        service => service.Agent,
                        (agent, service) => new {agent, service})
                    .OrderBy(x => x.service.Description);

                var options = query.Skip(0).Take(50).ToList()
                    .Select(x => new FinanceOption
                    {
                        Agent = x.agent.Agent,
                        Method = x.service.Method,
                        Description = x.service.Description,
                        Service = x.service.EntryNo.ToString(),
                        CreditType = x.service.FundingAgentReference
                    });

                return options;
            }
        }
    }
}