using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Sfs.Core;
using Sfs.Orders.Ports;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    public class SalesOrdersQuery : ISalesOrdersQuery
    {
        public IEnumerable<SalesOrderInfo> Handle(params string[] orderNos)
        {
            return GetResults(rangeStart: 0, rangeMax: 100, orderNos: orderNos ?? new string[0]);
        }

        private IPagedSet<SalesOrderInfo> GetResults(int rangeStart, int rangeMax, IEnumerable<string> orderNos)
        {
            var range = new { Start = rangeStart, Max = rangeMax };

            using (var dbContext = new NavisionContext())
            {
                var query = new SalesOrderInfoQuery(dbContext).Build()
                    .OrderByDescending(x => x.Header.OrderDate).ThenByDescending(x => x.Header.No)
                    .Where(x => orderNos.Contains(x.Header.No));

                var orders = query.Skip(range.Start).Take(range.Max).ToList();

                var resultOrderNos = orders.Select(o => o.Header.No).ToArray();
                var lines = dbContext.Set<SalesLine>()
                    .Include(x => x.SerialNoInformation)
                    .Where(x => resultOrderNos.Contains(x.DocumentNo))
                    .ToList();

                var result = orders.Select(order =>
                {
                    var orderLines = lines.Where(l => l.DocumentNo == order.Header.No).ToArray();

                    return new SalesOrderInfo(
                        date: order.Header.OrderDate,
                        no: order.Header.No,
                        shipping:
                            new ShippingInfo(
                                new ShippingInfo.ShippingAddress(order.Header.ShipToName, order.Header.ShipToPostcode),
                                order.ShipmentMethod,
                                order.ShippingAgent,
                                order.ShippingAgentService),
                        lines:
                            new SalesOrderLines(
                                orderLines.Select(line =>
                                {
                                    var isVehicleLine = line.No == ItemNo.Vehicle;
                                    var description = isVehicleLine
                                        ? string.Format("{0} - {1}", line.Description,
                                            line.SerialNoInformation.RegistrationNo)
                                        : line.Description;

                                    var amountValue = line.Amount;
                                    var vatValue = line.AmountIncVat - line.Amount;
                                    var totalValue = line.AmountIncVat;

                                    if ((line.SerialNoInformation.VatProdPostingGroup ?? "").StartsWith("M"))
                                    {
                                        amountValue = totalValue;
                                        vatValue = 0;
                                    }

                                    return new SalesOrderLine(new Money(amountValue),
                                        new Money(vatValue), new Money(totalValue),
                                        description);
                                }).ToArray()),
                        sellToCustomer:
                            new CustomerInfo(order.Header.SellToCustomerNo, order.Header.SellToCustomerName),
                           imagePaths: null
                        );
                }).ToArray();

                return new PagedSetWrapper<SalesOrderInfo>(result, range.Start, range.Max, query.Count());
            }
        }
    }
}