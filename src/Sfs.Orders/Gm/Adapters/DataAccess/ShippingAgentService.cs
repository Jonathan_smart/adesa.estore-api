﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    internal class ShippingAgentService
    {
        public string ShippingAgentCode { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string ShipmentMethod { get; set; }
    }

    internal class ShippingAgentServiceMapper : EntityTypeConfiguration<ShippingAgentService>
    {
        public ShippingAgentServiceMapper()
        {
            ToTable("SFS$Shipping Agent Services");
            HasKey(e => new { e.ShippingAgentCode, e.Code });
            Property(e => e.ShippingAgentCode).HasColumnName("Shipping Agent Code");
            Property(e => e.ShipmentMethod).HasColumnName("Shipment Method");
        }
    }
}
