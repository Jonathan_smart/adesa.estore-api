using System.Data.Entity;
using System.Linq;
using Sfs.Core;
using Sfs.Orders.Ports;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    public class ContactSalesOrdersQuery : IContactSalesOrdersQuery
    {
        private readonly BusinessUnit _businessUnit;

        public ContactSalesOrdersQuery(BusinessUnit businessUnit)
        {
            _businessUnit = businessUnit;
        }

        public IPagedSet<SalesOrderInfo> Handle(GetForContactQuery queryParams)
        {
            return GetResults(rangeStart: queryParams.Range.Start, rangeMax: queryParams.Range.Max,
                contactNo: queryParams.ContactNo);
        }

        private IPagedSet<SalesOrderInfo> GetResults(int rangeStart, int rangeMax, string contactNo = null)
        {
            var range = new { Start = rangeStart, Max = rangeMax };

            using (var dbContext = new NavisionContext())
            {
                var customerNos = new ContactCustomerNosQuery(dbContext, _businessUnit).Handle(contactNo);

                var query = new SalesOrderInfoQuery(dbContext).Build()
                    .OrderByDescending(x => x.Header.OrderDate).ThenByDescending(x => x.Header.No)
                    .Where(x => customerNos.Contains(x.Header.SellToCustomerNo));

                var orders = query.Skip(range.Start).Take(range.Max).ToList();

                var resultOrderNos = orders.Select(o => o.Header.No).ToArray();
                var lines = dbContext.Set<SalesLine>()
                    .Include(x => x.SerialNoInformation)
                    .Where(x => resultOrderNos.Contains(x.DocumentNo))
                    .Select(x => new
                    {
                        x.DocumentNo,
                        x.No,
                        x.Description,
                        SerialNoInformation = new
                        {
                            x.SerialNoInformation.RegistrationNo,
                            CommercialVehicle = x.SerialNoInformation.CommercialVehicle == 1,
                            x.SerialNoInformation.VatProdPostingGroup
                        },
                        x.Amount,
                        x.AmountIncVat
                    })
                    .ToList();

                var result = orders.Select(order =>
                {
                    var orderLines = lines.Where(l => l.DocumentNo == order.Header.No).ToArray();

                    return new SalesOrderInfo(
                        date: order.Header.OrderDate,
                        no: order.Header.No,
                        shipping:
                            new ShippingInfo(
                                new ShippingInfo.ShippingAddress(order.Header.ShipToName, order.Header.ShipToPostcode),
                                order.ShipmentMethod == null ? null : order.ShipmentMethod,
                                order.ShippingAgent == null ? null : order.ShippingAgent,
                                order.ShippingAgentService == null ? null : order.ShippingAgentService),
                        lines:
                            new SalesOrderLines(
                                orderLines.Select(line =>
                                {
                                    var isVehicleLine = line.No == ItemNo.Vehicle;
                                    var description = isVehicleLine
                                        ? string.Format("{0} - {1}", line.Description,
                                            line.SerialNoInformation.RegistrationNo)
                                        : line.Description;

                                    var amountValue = line.Amount;
                                    var vatValue = line.AmountIncVat - line.Amount;
                                    var totalValue = line.AmountIncVat;

                                    if ((line.SerialNoInformation.VatProdPostingGroup ?? "").StartsWith("M"))
                                    {
                                        amountValue = totalValue;
                                        vatValue = 0;
                                    }

                                    return new SalesOrderLine(new Money(amountValue),
                                        new Money(vatValue), new Money(totalValue),
                                        description);
                                }).ToArray()),
                        sellToCustomer:
                            new CustomerInfo(order.Header.SellToCustomerNo, order.Header.SellToCustomerName),
                        imagePaths: null
                        );
                }).ToArray();

                return new PagedSetWrapper<SalesOrderInfo>(result, range.Start, range.Max, query.Count());
            }
        }
    }
}