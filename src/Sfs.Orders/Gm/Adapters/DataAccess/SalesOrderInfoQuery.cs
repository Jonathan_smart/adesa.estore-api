using System;
using System.Linq;

namespace Sfs.Orders.Gm.Adapters.DataAccess
{
    internal class SalesOrderInfoQuery
    {
        private readonly NavisionContext _dbContext;

        public SalesOrderInfoQuery(NavisionContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<SalesOrderDbModel> Build()
        {
            const int orderDocumentType = 1;
            const string vehicleSupplyCode = "VEHICLE";

            var query = _dbContext.Set<SalesHeader>()
                .Where(x => x.DocumentType == orderDocumentType)
                .Where(x => x.TypeOfSupplyCode == vehicleSupplyCode)
                .GroupJoin(_dbContext.Set<ShipmentMethod>(),
                    h => h.ShipmentMethodCode,
                    m => m.Code,
                    (h, m) => new {Header = h, ShipmentMethod = m.FirstOrDefault()})
                .GroupJoin(_dbContext.Set<ShippingAgent>(),
                    h => h.Header.ShippingAgentCode,
                    a => a.Code,
                    (h, a) => new {h.Header, h.ShipmentMethod, ShippingAgent = a.FirstOrDefault()})
                .GroupJoin(_dbContext.Set<ShippingAgentService>(),
                    h => new {h.Header.ShippingAgentCode, h.Header.ShippingAgentServiceCode},
                    s => new {s.ShippingAgentCode, ShippingAgentServiceCode = s.Code},
                    (h, s) =>
                        new
                        {
                            h.Header,
                            ShipmentMethod = h.ShipmentMethod.Description,
                            ShippingAgent = h.ShippingAgent.Name,
                            ShippingAgentService = s.FirstOrDefault().Description
                        })
                .AsQueryable();

            return query.Select(x => new SalesOrderDbModel
            {
                Header = new SalesOrderDbModel.HeaderModel
                {
                    No = x.Header.No,
                    OrderDate = x.Header.OrderDate,
                    ShipToName = x.Header.ShipToName,
                    ShipToPostcode = x.Header.ShipToPostcode,
                    SellToCustomerName = x.Header.SellToCustomerName,
                    SellToCustomerNo = x.Header.SellToCustomerNo
                },
                ShipmentMethod = x.ShipmentMethod,
                ShippingAgent = x.ShippingAgent,
                ShippingAgentService = x.ShippingAgentService
            });
        }

        public class SalesOrderDbModel
        {
            public class HeaderModel
            {
                public string No { get; set; }
                public DateTime OrderDate { get; set; }
                public string ShipToName { get; set; }
                public string ShipToPostcode { get; set; }
                public string SellToCustomerName { get; set; }
                public string SellToCustomerNo { get; set; }
            }

            public HeaderModel Header { get; set; }
            public string ShipmentMethod { get; set; }
            public string ShippingAgent { get; set; }
            public string ShippingAgentService { get; set; }
        }
    }
}