﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services.Protocols;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;
using Sfs.Orders.Adapters;
using Sfs.Orders.GmNavWebIntegration;
using Sfs.Orders.GmSalesOrderConfirmationService;
using Sfs.Orders.Ports;

namespace Sfs.Orders.Gm.Adapters
{
    public class CreateSalesOrdersHandler : RequestHandler<OrderVehicles>
    {
        private const int WorlflowTemplateNo = 28;
        private const string GmacFundingAgentCode = "GMAC";

        private readonly ICreateSalesOrderBasketItemsQuery _basketsQuery;
        private readonly ICreateSalesOrderTransportSalesPriceQuery _transportSalesPriceReader;
        private readonly SalesChannel _salesChannel;
        private readonly WebIntegration _webIntegration;
        private readonly Sales_Order_Confirmation_Service _socService;
        private readonly string _salesPersonCode;

        public CreateSalesOrdersHandler(ICreateSalesOrderBasketItemsQuery basketsQuery,
            ICreateSalesOrderTransportSalesPriceQuery transportSalesPriceReader,
            WebIntegration webIntegration, Sales_Order_Confirmation_Service socService,
            SalesChannel salesChannel, string salesPersonCode,
            ILog logger) : base(logger)
        {
            _basketsQuery = basketsQuery;
            _transportSalesPriceReader = transportSalesPriceReader;
            _webIntegration = webIntegration;
            _salesChannel = salesChannel;
            _salesPersonCode = salesPersonCode;
            _socService = socService;
        }

        public override OrderVehicles Handle(OrderVehicles command)
        {
            Logger.Information("Creating GM sales orders for {items}", command.Items);

            var confirmation = new Sales_Order_Confirmation
            {
                No_Of_Vehicles = command.Items.Length,
                No_Of_VehiclesSpecified = true
            };

            try
            {
                _socService.Create(ref confirmation);
            }
            catch (SoapException ex)
            {
                Logger.Error(ex, "Failed to create Sales Orders. Couldn't create SOC for {items}", command.Items);
                command.Result = command.Items.Select(x => OrderVehicles.OrderResult.Failure(
                    x.BasketEntryNo,
                    new SerialNo(""),
                    "Failed to create SOC")).ToArray();
            }

            var orders = BuildOrdersFromItems(command.Items, confirmation.SOC_Header);

            var orderNos = CreateOrders(orders);
            command.Result = orderNos;

            return command;
        }

        private class MergedOrder
        {
            public MergedOrder(OrderVehicles.OrderItem item, SalesHeaderXP salesHeader)
            {
                Item = item;
                SalesHeader = salesHeader;
            }

            public OrderVehicles.OrderItem Item { get; private set; }
            public SalesHeaderXP SalesHeader { get; private set; }
        }

        private OrderVehicles.OrderResult[] CreateOrders(MergedOrder[] orders)
        {
            var results = orders.Select(x =>
            {
                var salesHeader = x.SalesHeader;
                var serialNo = salesHeader.SalesLineXP[0].TrackingSerialNo;

                Logger.Debug("Creating sales order for {SerialNo}", serialNo);

                try
                {
                    var orderNo = _webIntegration.CreateSO(ref salesHeader);
                    Logger.Information("Created Sales Order {OrderNo} for {SerialNo}", orderNo, serialNo);
                    return OrderVehicles.OrderResult.Successful(x.Item.BasketEntryNo, new SerialNo(serialNo), new OrderNo(orderNo));
                }
                catch (SoapException ex)
                {
                    Logger.Error(ex, "Failed to create Sales Order for {@Order}", salesHeader);
                    return OrderVehicles.OrderResult.Failure(
                        x.Item.BasketEntryNo,
                        new SerialNo(serialNo),
                        ex.Message);
                }
            }).ToArray();

            return results;
        }

        private MergedOrder[] BuildOrdersFromItems(OrderVehicles.OrderItem[] items, string socHeader)
        {
            var entryNos = items.Select(x => x.BasketEntryNo).ToArray();
            var baskets = _basketsQuery.Handle(entryNos).ToArray();

            if (baskets.Length != items.Length)
                throw new BasketsNotFoundException(entryNos);

            var bound = items.Join(baskets,
                i => i.BasketEntryNo, b => b.EntryNo,
                (i, b) => new { item = i, basket = b })
                .ToArray();

            //ensure addresscode set where required
            bound.ToList().ForEach(x =>
            {
                var methodMissing = RequiredShippingInformationNotSpecified(x.item.Shipping);
                if (methodMissing)
                    throw new ShippingAddressMissingException(x.item.Shipping.Method, x.item.Shipping.AddressCode, x.item.Shipping.Agent, x.item.Shipping.Service);
            });

            return bound.Select(sale =>
            {
                var sellToCustomerNo = (!string.IsNullOrWhiteSpace(sale.basket.CustomerNo)
                    ? sale.basket.CustomerNo
                    : sale.item.CustomerNo) ?? "";

                var header = new SalesHeaderXP
                {
                    SalesPerson = _salesPersonCode ?? "",
                    DocumentType = "Order",
                    ExternalDocumentNo = socHeader,
                    SellToCustomerNo = sellToCustomerNo,
                    PricesIncVat = !sale.basket.Vehicle.Commercial,
                    SalesChannel = sale.basket.SalesChannel,
                    ShipmentMethodCode = sale.item.Shipping.Method,
                    ShippingAgentCode = sale.item.Shipping.Agent,
                    ShippingAgentServiceCode = sale.item.Shipping.Service,
                    ShipToCode = sale.item.Shipping.AddressCode,
                    WfControlled = true,
                    WfTemplateNo = WorlflowTemplateNo,
                    CompanyCode = "VAUXHALL",
                    FundingAgent = sale.item.Finance == null ? "" : sale.item.Finance.Agent,
                    FundingMethod = sale.item.Finance == null ? "" : sale.item.Finance.Method,
                    FundingAgentService = sale.item.Finance == null ? 0 : int.Parse(sale.item.Finance.Service),
                    ShippingZone = "",
                    ShipToName = "",
                    ShipToAddress = "",
                    ShipToAddress2 = "",
                    ShipToCity = "",
                    ShipToContact = "",
                    ShipToCountry = "",
                    ShipToCounty = "",
                    ShipToName2 = "",
                    ShipToPostCode = "",
                    Release = "RELEASE",
                    GMACAssign = "GMACAssign",
                    V5ShipToCode = sale.item.V5.ShipToAddressCode,
                    BillToCustomerNo = (sale.item.Finance == null || sale.item.Finance.Agent != GmacFundingAgentCode)
                        ? ""
                        : GmacFundingAgentCode
                };
                var lines = new List<SalesLineXP>();

                lines.Add(new SalesLineXP
                {
                    Type = "Item",
                    No = ItemNo.Vehicle,
                    Quantity = 1,
                    Description = string.Join("",
                        string.Format("{0} {1} {2}", sale.basket.Vehicle.Make, sale.basket.Vehicle.Model,
                            sale.basket.Vehicle.Derivative).Take(50)),
                    UnitPrice = sale.basket.Price,
                    BasketEntryNo = sale.basket.EntryNo.ToString(),
                    LocationCode = sale.basket.LocationCode,
                    TrackingItemNo = sale.basket.Vehicle.Key.ItemNo,
                    TrackingVariantCode = sale.basket.Vehicle.Key.VariantCode,
                    TrackingSerialNo = sale.basket.Vehicle.Key.SerialNo,
                    SerialNoForItemTracking = sale.basket.Vehicle.Key.SerialNo,
                    VatProdPostingGroup = sale.basket.Vehicle.VatProdPostingGroup,
                    BinCode = ""
                });

                header.SalesLineXP = lines.ToArray();
                return new MergedOrder(sale.item, header);
            })
                .ToArray();
        }

        private bool RequiredShippingInformationNotSpecified(OrderVehicles.ItemShipping itemShipping)
        {
            Func<bool> transportRequiresAddress = () => _transportSalesPriceReader.GetForMethod(itemShipping.Method)
                .Any(y => !string.IsNullOrEmpty(y.ShippingAgentCode));

            var addressFullySpecified = !(string.IsNullOrEmpty(itemShipping.AddressCode)
                                          || string.IsNullOrEmpty(itemShipping.Agent)
                                          || string.IsNullOrEmpty(itemShipping.Service));

            return !addressFullySpecified && transportRequiresAddress();
        }
    }
}