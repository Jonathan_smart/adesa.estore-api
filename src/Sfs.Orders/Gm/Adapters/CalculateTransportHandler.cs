﻿using System.Linq;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;
using Sfs.Orders.GmNavWebIntegration;
using Sfs.Orders.Ports;

namespace Sfs.Orders.Gm.Adapters
{
    public class CalculateTransportHandler : RequestHandler<CalculateTransport>
    {
        private readonly SalesChannel _salesChannel;
        private readonly WebIntegration _webIntegration;

        public CalculateTransportHandler(SalesChannel salesChannel,
            WebIntegration webIntegration,
            ILog logger) : base(logger)
        {
            _salesChannel = salesChannel;
            _webIntegration = webIntegration;
        }

        public override CalculateTransport Handle(CalculateTransport command)
        {
            var items = command.ItemsToBeTransported.ToList();

            if (!items.Any())
            {
                command.Result = new CalculateTransport.TransportResponseItem[0];
                return command;
            }

            var prices = items.Select(x => new CalculateTransport.TransportResponseItem(x.Basket,
                _webIntegration.CalculateTransportCosts(_salesChannel, x.ShippingMethod, x.ShippingAgent,
                    x.ShippingAgentService, x.Vehicle.ItemNo, x.Vehicle.VariantCode, x.Vehicle.SerialNo))).ToList();

            command.Result = prices;

            return command;
        }
    }
}
