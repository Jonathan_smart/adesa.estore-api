using System.Collections;
using System.Collections.Generic;

namespace Sfs.Orders
{
    public interface IPagedSet<out T> : IEnumerable<T>
    {
        int Max { get; }
        int Start { get;  }
        int Total { get; }
    }

    public class PagedSetWrapper<T> : IPagedSet<T>
    {
        private readonly IEnumerable<T> _list;

        public PagedSetWrapper(IEnumerable<T> list, int start, int max, int total)
        {
            Start = start;
            Max = max;
            Total = total;
            _list = list;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Start { get; private set; }
        public int Max { get; private set; }
        public int Total { get; private set; }
    }

    public class EmptyPagesSet
    {
        public static IPagedSet<T> Instance<T>(int start, int max)
        {
            return new PagedSetWrapper<T>(new T[0], start, max, total: 0);
        }
    }
}