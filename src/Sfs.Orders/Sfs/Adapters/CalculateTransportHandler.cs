﻿using System.Linq;
using System.Security.Policy;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;
using Sfs.Orders.Ports;
using Sfs.Orders.SfsNavWebIntegration;

namespace Sfs.Orders.Sfs.Adapters
{
    public class CalculateTransportHandler : RequestHandler<CalculateTransport>
    {
        private readonly ICalculateTransportShipToAddressQuery _shipToAddressReader;
        private readonly SalesChannel _salesChannel;
        private readonly WebIntegration _webIntegration;

        public CalculateTransportHandler(ICalculateTransportShipToAddressQuery shipToAddressReader,
            SalesChannel salesChannel,
            Url serviceUrl,
            ILog logger) : base(logger)
        {
            _shipToAddressReader = shipToAddressReader;
            _salesChannel = salesChannel;
            _webIntegration = new WebIntegration
            {
                UseDefaultCredentials = true,
                Url = serviceUrl.Value
            };
        }

        public override CalculateTransport Handle(CalculateTransport command)
        {
            var items = command.ItemsToBeTransported.ToList();

            if (!items.Any())
            {
                command.Result = new CalculateTransport.TransportResponseItem[0];
                return command;
            }

            if (command.WithVat)
            {
                var prices = new GetTransportPrices
                {
                    Text = new string[0],
                    TransportVehicle = items.Select(x =>
                    {
                        var address = _shipToAddressReader.GetForCustomerNoAndAddressCode(x.To.CustomerNo,
                            x.To.AddressCode);

                        return new TransportVehicle
                        {
                            SerialNo = x.Vehicle.SerialNo,
                            CustomerNo = x.To.CustomerNo,
                            DestinationPostCode = address == null ? "" : address.PostCode,
                            ShipmentMethodCode = x.ShippingMethod,
                            ShippingAgentCode = x.ShippingAgent,
                            ShippingAgentServiceCode = x.ShippingAgentService,
                            SalesChannelNo = _salesChannel.ToString(),
                            TransportCosts = new TransportCosts
                            {
                                Text = new string[0],
                                TransportVehicleCost = new TransportVehicleCost[0]
                            }
                        };
                    }).ToArray()
                };

                _webIntegration.CalculateTransportCosts(ref prices);

                var results = prices.TransportVehicle
                    .Join(items, t => t.SerialNo, j => j.Vehicle.SerialNo, (t, j) => new
                    {
                        j.Basket,
                        Cost = decimal.Parse(t.TransportCosts.TransportVehicleCost[0].Cost)
                    });

                command.Result = results.Select(x => new CalculateTransport.TransportResponseItem(x.Basket, x.Cost));

                return command;
            }
            else
            {
                var prices = new GetTransportPrices1
                {
                    Text = new string[0],
                    TransportVehicle = items.Select(x =>
                    {
                        var address = _shipToAddressReader.GetForCustomerNoAndAddressCode(x.To.CustomerNo,
                            x.To.AddressCode);

                        return new TransportVehicle1
                        {
                            SerialNo = x.Vehicle.SerialNo,
                            CustomerNo = x.To.CustomerNo,
                            DestinationPostCode = address == null ? "" : address.PostCode,
                            ShipmentMethodCode = x.ShippingMethod,
                            ShippingAgentCode = x.ShippingAgent,
                            ShippingAgentServiceCode = x.ShippingAgentService,
                            SalesChannelNo = _salesChannel.ToString(),
                            TransportCosts = new TransportCosts1
                            {
                                Text = new string[0],
                                TransportVehicleCost = new TransportVehicleCost1[0]
                            }
                        };
                    }).ToArray()
                };

                _webIntegration.CalculateTransportCostsNoVAT(ref prices);

                var results = prices.TransportVehicle
                    .Join(items, t => t.SerialNo, j => j.Vehicle.SerialNo, (t, j) => new
                    {
                        j.Basket,
                        Cost = decimal.Parse(t.TransportCosts.TransportVehicleCost[0].Cost)
                    });

                command.Result = results.Select(x => new CalculateTransport.TransportResponseItem(x.Basket, x.Cost));

                return command;

            }
        }
    }
}
