﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Policy;
using System.Web.Services.Protocols;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;
using Sfs.Orders.Adapters;
using Sfs.Orders.Ports;
using Sfs.Orders.SfsNavWebIntegration;

namespace Sfs.Orders.Sfs.Adapters
{
    public class CreateSalesOrdersHandler : RequestHandler<OrderVehicles>
    {
        private readonly ICreateSalesOrderBasketItemsQuery _basketsQuery;
        private readonly ICreateSalesOrderTransportSalesPriceQuery _transportSalesPriceReader;
        private readonly SalesChannel _salesChannel;
        private readonly BusinessUnit _businessUnit;
        private readonly TransportChargeItem _transportChargeItem;
        private readonly TransportBusinessUnit _transportBusinessUnit;
        private readonly WebIntegration _webIntegration;

        public CreateSalesOrdersHandler(ICreateSalesOrderBasketItemsQuery basketsQuery, ICreateSalesOrderTransportSalesPriceQuery transportSalesPriceReader,
            Url serviceUrl,
            SalesChannel salesChannel, BusinessUnit businessUnit,
            TransportChargeItem transportChargeItem, TransportBusinessUnit transportBusinessUnit,
            ILog logger)
            : base(logger)
        {
            _basketsQuery = basketsQuery;
            _transportSalesPriceReader = transportSalesPriceReader;
            _salesChannel = salesChannel;
            _businessUnit = businessUnit;
            _transportChargeItem = transportChargeItem;
            _transportBusinessUnit = transportBusinessUnit;

            _webIntegration = new WebIntegration
            {
                UseDefaultCredentials = true,
                Url = serviceUrl.Value
            };
        }

        public override OrderVehicles Handle(OrderVehicles command)
        {
            Logger.Information("Creating SFS sales order for {items}", command.Items);

            var orders = BuildOrdersFromItems(command.Items);

            var orderNos = CreateOrders(orders);
            command.Result = orderNos;

            return command;
        }

        private class MergedOrder
        {
            public MergedOrder(OrderVehicles.OrderItem item, SalesHeaderXP1 salesHeader)
            {
                Item = item;
                SalesHeader = salesHeader;
            }

            public OrderVehicles.OrderItem Item { get; private set; }
            public SalesHeaderXP1 SalesHeader { get; private set; }
        }

        private OrderVehicles.OrderResult[] CreateOrders(MergedOrder[] orders)
        {
            var salesHeaders = new SalesHeaders
            {
                SalesHeaderXP = orders.Select(x => x.SalesHeader).ToArray()
            };
            try
            {
                _webIntegration.CreateSOs(ref salesHeaders);

                var orderLines = salesHeaders.SalesHeaderXP
                    .SelectMany(x => x.SalesLineXP.Select(l => new { line = l, header = x }))
                    .ToArray();

                Logger.Information("Created Sales Orders {salesOrder}", orderLines.Select(x => new { x.line.BasketEntryNo, x.header.DocumentNo, x.line.TrackingSerialNo }).ToArray());

                return orderLines
                    .Where(x => x.line.TrackingItemNo == ItemNo.Vehicle)
                    .Select(x =>
                    {
                        var line = x.line;
                        var header = x.header;

                        int basketEntryNoInt;
                        BasketEntryNo basketEntryNo;
                        if (int.TryParse(line.BasketEntryNo, out basketEntryNoInt))
                            basketEntryNo = new BasketEntryNo(basketEntryNoInt);
                        else
                            basketEntryNo =
                                orders.First(
                                    o => o.SalesHeader.SalesLineXP.Any(l => l.TrackingSerialNo == line.TrackingSerialNo))
                                    .Item.BasketEntryNo;

                        return OrderVehicles.OrderResult.Successful(
                            basketEntryNo,
                            new SerialNo(line.TrackingSerialNo),
                            new OrderNo(header.DocumentNo));
                    })
                    .ToArray();
            }
            catch (SoapException ex)
            {
                Logger.Error(ex, "Failed to create Sales Orders for {items}", orders.Select(x => x.Item));

                return orders.Select(x => OrderVehicles.OrderResult.Failure(
                    x.Item.BasketEntryNo,
                    new SerialNo(x.SalesHeader.TrackingSerialNo),
                    ex.Message)).ToArray();
            }
        }

        private MergedOrder[] BuildOrdersFromItems(OrderVehicles.OrderItem[] items)
        {
            var entryNos = items.Select(x => x.BasketEntryNo).ToArray();
            var baskets = _basketsQuery.Handle(entryNos).ToArray();

            if (baskets.Length != items.Length)
                throw new BasketsNotFoundException(entryNos);

            var bound = items.Join(baskets,
                i => i.BasketEntryNo, b => b.EntryNo,
                (i, b) => new { item = i, basket = b })
                .ToArray();

            //ensure addresscode set where required
            bound.ToList().ForEach(x =>
            {
                var methodMissing = RequiredShippingInformationNotSpecified(x.item.Shipping);
                if (methodMissing)
                    throw new ShippingAddressMissingException(x.item.Shipping.Method, x.item.Shipping.AddressCode, x.item.Shipping.Agent, x.item.Shipping.Service);
            });

            //split by relevant fields (customer, commercial vehicle, shipping method & addresscode)
            var grouped =
                bound.GroupBy(
                    x =>
                        new
                        {
                            CustomerNo =
                                !string.IsNullOrWhiteSpace(x.basket.CustomerNo)
                                    ? x.basket.CustomerNo
                                    : x.item.CustomerNo,
                            x.basket.Vehicle.Commercial,
                            ShippingMethod = x.item.Shipping.Method,
                            ShippingAgent = x.item.Shipping.Agent,
                            ShippingAgentService = x.item.Shipping.Service,
                            ShippingAddressCode = x.item.Shipping.AddressCode,
                            x.item.FinanceRequired,
                            x.item.Finance,
                            x.basket.ContactNo,
                            V5ShipToAddressCode = x.item.V5.ShipToAddressCode
                        });
            return grouped.Select(x =>
            {
                var header = new SalesHeaderXP1
                {
                    DocumentType = "Order",
                    SellToCustomerNo = x.Key.CustomerNo ?? "",
                    PricesIncVat = !x.Key.Commercial,
                    SalesChannel = _salesChannel,
                    ShipmentMethodCode = x.Key.ShippingMethod,
                    ShippingAgentCode = x.Key.ShippingAgent,
                    ShippingAgentServiceCode = x.Key.ShippingAgentService,
                    ShipToCode = x.Key.ShippingAddressCode,
                    SellToContactNo = x.Key.ContactNo,
                    CompanyCode = _businessUnit,
                    FundingAgent = x.Key.Finance == null ? "" : x.Key.Finance.Agent,
                    FundingMethod = x.Key.Finance == null ? "" : x.Key.Finance.Method,
                    FundingAgentService = x.Key.Finance == null ? "" : x.Key.Finance.Service,
                    V5ShipToCode = x.Key.V5ShipToAddressCode,
                    SalespersonContactNo = ""
                };
                var lines = new List<SalesLineXP1>();
                foreach (var salesItem in x)
                {
                    lines.Add(new SalesLineXP1
                    {
                        Type = "Item",
                        No = ItemNo.Vehicle,
                        Quantity = 1,
                        Description = string.Join("",
                            string.Format("{0} {1} {2}", salesItem.basket.Vehicle.Make, salesItem.basket.Vehicle.Model,
                                salesItem.basket.Vehicle.Derivative).Take(50)),
                        UnitPrice = salesItem.basket.Price,
                        LineAmount = salesItem.basket.Amount,
                        BasketEntryNo = salesItem.basket.EntryNo.ToString(),
                        LocationCode = salesItem.basket.LocationCode,
                        TrackingItemNo = salesItem.basket.Vehicle.Key.ItemNo,
                        TrackingVariantCode = salesItem.basket.Vehicle.Key.VariantCode,
                        TrackingSerialNo = salesItem.basket.Vehicle.Key.SerialNo,
                        SerialNoForItemTracking = salesItem.basket.Vehicle.Key.SerialNo,
                        VatProdPostingGroup = salesItem.basket.Vehicle.VatProdPostingGroup,
                        ShortcutDimensionCode1 = salesItem.basket.Vehicle.BusinessUnit
                    });
                    if (salesItem.item.Transport != null)
                        lines.Add(new SalesLineXP1
                        {
                            Type = "Charge (Item)",
                            No = _transportChargeItem,
                            Quantity = 1,
                            Description = "Transport",
                            UnitPrice = salesItem.item.Transport.Price,
                            LineAmount = salesItem.item.Transport.Price,
                            TrackingItemNo = salesItem.basket.Vehicle.Key.ItemNo,
                            TrackingVariantCode = salesItem.basket.Vehicle.Key.VariantCode,
                            TrackingSerialNo = salesItem.basket.Vehicle.Key.SerialNo,
                            ShortcutDimensionCode1 = _transportBusinessUnit
                        });
                    if (salesItem.item.BuyersFee.HasValue && salesItem.item.BuyersFee.Value > 0)
                        lines.Add(new SalesLineXP1
                        {
                            Type = "Charge (Item)",
                            No = "CHG0006",
                            Quantity = 1,
                            Description = "Buyers Fee",
                            UnitPrice = salesItem.item.BuyersFee.Value,
                            LineAmount = salesItem.item.BuyersFee.Value,
                            TrackingItemNo = salesItem.basket.Vehicle.Key.ItemNo,
                            TrackingVariantCode = salesItem.basket.Vehicle.Key.VariantCode,
                            TrackingSerialNo = salesItem.basket.Vehicle.Key.SerialNo,
                            ShortcutDimensionCode1 = salesItem.basket.Vehicle.BusinessUnit
                        });
                }
                header.SalesLineXP = lines.ToArray();
                return new MergedOrder(x.First().item, header);
            }).ToArray();
        }

        private bool RequiredShippingInformationNotSpecified(OrderVehicles.ItemShipping itemShipping)
        {
            Func<bool> transportRequiresAddress = () => _transportSalesPriceReader.GetForMethod(itemShipping.Method)
                .Any(y => !string.IsNullOrEmpty(y.ShippingAgentCode));

            var addressFullySpecified = !(string.IsNullOrEmpty(itemShipping.AddressCode)
                                          || string.IsNullOrEmpty(itemShipping.Agent)
                                          || string.IsNullOrEmpty(itemShipping.Service));

            return !addressFullySpecified && transportRequiresAddress();
        }
    }

    public class CreateOrdersRequest
    {
        public CreateOrdersRequestSalesItem[] Items { get; set; }
    }

    public class CreateOrdersRequestSalesItem
    {
        public string CustomerNo { get; set; }
        public bool FinanceRequired { get; set; }
        public CreateOrdersRequestFinance Finance { get; set; }
        public CreateOrdersRequestBasket Basket { get; set; }
        public CreateOrdersRequestTransport Transport { get; set; }
        public CreateOrdersRequestShipping Shipping { get; set; }
        public CreateOrdersRequestV5 V5 { get; set; }
        public decimal? Price { get; set; }
    }

    [DataContract]
    public class CreateOrdersRequestFinance
    {
        [Required, DataMember(IsRequired = true)]
        public string Method { get; set; }
        [Required, DataMember(IsRequired = true)]
        public string Agent { get; set; }
        [Required, DataMember(IsRequired = true)]
        public string Service { get; set; }

        protected bool Equals(CreateOrdersRequestFinance other)
        {
            return string.Equals(Method, other.Method) && string.Equals(Agent, other.Agent) && Service == other.Service;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((CreateOrdersRequestFinance)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Method != null ? Method.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Agent != null ? Agent.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Service != null ? Service.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    [DataContract]
    public class CreateOrdersRequestBasket
    {
        [Required, DataMember(IsRequired = true), Range(minimum: 1, maximum: int.MaxValue)]
        public int EntryNo { get; set; }
    }

    [DataContract]
    public class CreateOrdersRequestTransport
    {
        [Required, DataMember(IsRequired = true)]
        public decimal Price { get; set; }
    }

    [DataContract]
    public class CreateOrdersRequestShipping
    {
        [Required, DataMember(IsRequired = true)]
        public string Method { get; set; }
        [DataMember]
        public string Agent { get; set; }
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string AddressCode { get; set; }
    }

    [DataContract]
    public class CreateOrdersRequestV5
    {
        [DataMember]
        public string ShipToAddressCode { get; set; }
    }
}
