namespace Sfs.EStore.Poller
{
    public interface IAuctioneer
    {
        void Start();
        void Stop();
    }
}