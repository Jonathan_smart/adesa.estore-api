using System.Collections.Generic;
using System.Linq;

namespace Sfs.EStore.Poller
{
    public class CompositeAuctioneer : IAuctioneer
    {
        private readonly List<IAuctioneer> _auctioneers;
        public CompositeAuctioneer(IEnumerable<IAuctioneer> auctioneers)
        {
            _auctioneers = (auctioneers ?? new IAuctioneer[0]).ToList();
        }

        public void Start()
        {
            _auctioneers.ForEach(x => x.Start());
        }

        public void Stop()
        {
            _auctioneers.ForEach(x => x.Stop());
        }
    }
}