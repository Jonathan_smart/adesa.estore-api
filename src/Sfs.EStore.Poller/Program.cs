﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using Topshelf;
using Topshelf.ServiceConfigurators;
using log4net;
using log4net.Config;

namespace Sfs.EStore.Poller
{
    internal class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (Program));

        private static void Main()
        {
            try
            {

 
            XmlConfigurator.ConfigureAndWatch(new FileInfo(Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory, "log4net.config")));

            var settings = new ProgramSettings
                               {
                                   RavenUrl = ConfigurationManager.AppSettings["Raven/Url"],
                                   ApiBaseUri = new Uri(ConfigurationManager.AppSettings["EStoreApi/Url"]),
                                   Accounts = AccountsConfiguration.Settings.Instances.Accounts()
                                       .Select(x => new Account(x.DatabaseName, x.ApiAccessToken))
                                       .ToArray()
                               };

            Logger.InfoFormat("Raven Url: {0}", settings.RavenUrl);
            Logger.InfoFormat("DatabaseNames: [{0}]", string.Join(", ", settings.Accounts.Select(x => x.DatabaseName)));

            HostFactory.Run(x =>
                                {
                                    x.Service<IAuctioneer>(s => Serivce(s, settings));
                                    x.RunAsLocalSystem();

                                    x.SetDescription(
                                        "Polls for starting and ending auction lots in the specified databases.");
                                    x.SetDisplayName("EStore Poller");
                                    x.SetServiceName("EStore.Poller");
                                });
            }
            catch (Exception ex)
            {
                Logger.InfoFormat(ex.Message);
            }
        }

        private static void Serivce(ServiceConfigurator<IAuctioneer> s, ProgramSettings settings)
        {
            s.ConstructUsing(name => new AuctioneerFactory(settings.RavenUrl, settings.ApiBaseUri).Create(settings.Accounts));
            s.WhenStarted(tc => tc.Start());
            s.WhenStopped(tc => tc.Stop());
        }
    }

    public class AccountsConfiguration : ConfigurationSection
    {
        private static readonly AccountsConfiguration settings =
            ConfigurationManager.GetSection("estore.accounts") as AccountsConfiguration;

        public static AccountsConfiguration Settings
        {
            get { return settings; }
        }

        [ConfigurationProperty("", IsRequired = true, IsDefaultCollection = true)]
        public AccountsConfigurationCollection Instances
        {
            get { return (AccountsConfigurationCollection) this[""]; }
            set { this[""] = value; }
        }
    }

    public class AccountsConfigurationCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new AccountsConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((AccountsConfigurationElement)element).DatabaseName;
        }

        public IEnumerable<AccountsConfigurationElement> Accounts()
        {
            return this.Cast<AccountsConfigurationElement>().Select(x => x).ToArray();
        }
    }


    public class AccountsConfigurationElement : ConfigurationElement
    {
        [ConfigurationProperty("database", IsKey = true, IsRequired = true)]
        public string DatabaseName
        {
            get { return (string) base["database"]; }
            set { base["database"] = value; }
        }

        [ConfigurationProperty("apitoken", IsRequired = true)]
        public string ApiAccessToken
        {
            get { return (string)base["apitoken"]; }
            set { base["apitoken"] = value; }
        }
    }
}