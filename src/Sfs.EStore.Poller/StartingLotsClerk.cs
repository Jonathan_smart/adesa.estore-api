using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api;
using Sfs.EStore.Api.Entities;
using log4net;

namespace Sfs.EStore.Poller
{
    public class StartingLotsClerk
    {
        private const int MaximumLotsToActivatePerTick = 64;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(StartingLotsClerk));

        private readonly IDocumentStore _documentStore;
        private readonly HttpClient _client;

        public StartingLotsClerk(IDocumentStore documentStore, HttpClient client)
        {
            _documentStore = documentStore;
            _client = client;
        }

        public void ActivateLots()
        {
            if (IsBusy())
            {
                Logger.Warn("Previous attempt to start lots has not complete");
                return;
            }

            SetIsBusy(true);

            try
            {
                Execute();
            }
            finally
            {
                SetIsBusy(false);
            }
        }

        private long _isBusy;

        private bool IsBusy()
        {
            return Interlocked.Read(ref _isBusy) > 0;
        }

        private void SetIsBusy(bool value)
        {
            Interlocked.Exchange(ref _isBusy, value ? 1 : 0);
        }

        private void Execute()
        {
            Logger.Debug("Checking for new lots to activate");
            var activateFrom = SystemTime.CheckForTimeDiff(SystemTime.UtcNow);

            using (var session = _documentStore.OpenSession())
            {
                var lots = session.Query<Lot>()
                    .Where(x => x.Status == LotStatusTypes.Scheduled)
                    .Where(x => x.StartDate <= activateFrom)
                    .Take(MaximumLotsToActivatePerTick)
                    .ToList();

                if (lots.Count == 0) return;

                if (Logger.IsInfoEnabled)
                {
                    var lotList = string.Join(";", lots.Select(x => x.Id));
                    Logger.InfoFormat("Activating {0} lot(s): [{1}]", lots.Count, lotList);
                }

                ActivateLots(lots);
            }
        }

        private void ActivateLots(IEnumerable<Lot> lots)
        {
            var tasks = lots
                .Select(x => _client.PutAsJsonAsync(x.Id, new
                                                              {
                                                                  Status = "Activate"
                                                              })
                                 .ContinueWith(t =>
                                                   {
                                                       if (t.Exception != null)
                                                       {
                                                           Logger.Error(
                                                               "An exception occured while activating lot " + x.Id,
                                                               t.Exception);
                                                           return;
                                                       }
                                                       if (!t.Result.IsSuccessStatusCode)
                                                       {
                                                           var content = t.Result.Content.ReadAsStringAsync().Result;
                                                           Logger.ErrorFormat("Failed to activate lot {0}\n{1}",
                                                                              x.Id,
                                                                              content);
                                                       }
                                                   }))
                .ToArray();

            Task.WaitAll(tasks);
        }
    }
}