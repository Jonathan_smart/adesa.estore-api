using System;
using log4net;
using Timer = System.Timers.Timer;

namespace Sfs.EStore.Poller
{
    public class AccountAuctioneer : IAuctioneer
    {
        private readonly string _account;
        private readonly StartingLotsClerk _startingLotsClerk;
        private readonly EndingLotsClerk _endingLotsClerk;
        private readonly Timer _starterTimer;
        private readonly Timer _enderTimer;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AccountAuctioneer));

        public AccountAuctioneer(string account,
            StartingLotsClerk startingLotsClerk,
            EndingLotsClerk endingLotsClerk)
        {
            ThreadContext.Properties["account"] = account;

            _account = account;
            _startingLotsClerk = startingLotsClerk;
            _endingLotsClerk = endingLotsClerk;

            _starterTimer = new Timer(15000) {AutoReset = true};
            _starterTimer.Elapsed += (sender, eventArgs) =>
                                         {
                                             ThreadContext.Properties["account"] = account;
                                             HandleStartingLots();
                                         };

            _enderTimer = new Timer(1000) {AutoReset = true};
            _enderTimer.Elapsed += (sender, eventArgs) =>
                                       {
                                           ThreadContext.Properties["account"] = account;
                                           HandleEndingLots();
                                       };
        }

        public void Start()
        {
            ThreadContext.Properties["account"] = _account;
            Logger.Debug("Auctioneer is starting");
            _starterTimer.Start();
            _enderTimer.Start();
        }

        public void Stop()
        {
            ThreadContext.Properties["account"] = _account;
            Logger.Debug("Auctioneer is stopping");
            _starterTimer.Stop();
            _enderTimer.Stop();
        }

        private void HandleStartingLots()
        {
            _startingLotsClerk.ActivateLots();
        }

        private void HandleEndingLots()
        {
            _endingLotsClerk.EndLots();
        }
    }
}