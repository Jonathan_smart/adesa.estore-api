using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Raven.Client;
using Sfs.EStore.Api;

namespace Sfs.EStore.Poller
{
    public class AuctioneerFactory
    {
        private readonly Uri _apiBaseUri;
        private readonly static ConcurrentDictionary<string, Lazy<IDocumentStore>> Stores = new ConcurrentDictionary<string, Lazy<IDocumentStore>>();
        private readonly LazyAccountDocumentStoreFactory _accountStores;

        public AuctioneerFactory(string ravenUrl, Uri apiBaseUri)
        {
            _apiBaseUri = apiBaseUri;
            _accountStores = new LazyAccountDocumentStoreFactory(Stores, ravenUrl);
        }

        public IAuctioneer Create(params Account[] accounts)
        {
            var auctioneers = (accounts ?? new Account[0]).Select(
                account =>
                    {
                        var client = new HttpClient
                                         {
                                             BaseAddress = _apiBaseUri
                                         };
                        client.DefaultRequestHeaders.Authorization =
                            new AuthenticationHeaderValue("SFS", account.ApiAccessToken);

                        var docStore = _accountStores.GetAccountDocumentStoreFor(account.DatabaseName);
                        var startingLotsClerk = new StartingLotsClerk(docStore, client);
                        var endingLotsClerk = new EndingLotsClerk(docStore, client);
                        return new AccountAuctioneer(account.DatabaseName, startingLotsClerk, endingLotsClerk);
                    })
                .ToArray();

            return new CompositeAuctioneer(auctioneers.ToArray());
        }
    }
}