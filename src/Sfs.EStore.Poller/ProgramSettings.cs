using System;

namespace Sfs.EStore.Poller
{
    public class ProgramSettings
    {
        public string RavenUrl { get; set; }
        public Uri ApiBaseUri { get; set; }
        public Account[] Accounts { get; set; }
    }

    public class Account
    {
        public Account(string databaseName, string apiAccessToken)
        {
            ApiAccessToken = apiAccessToken;
            DatabaseName = databaseName;
        }

        public string DatabaseName { get; private set; }
        public string ApiAccessToken { get; private set; }
    }
}