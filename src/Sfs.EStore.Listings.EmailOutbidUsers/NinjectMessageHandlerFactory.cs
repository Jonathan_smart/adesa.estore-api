﻿using System.Configuration;
using Mandrill;
using Ninject;
using Sfs.EStore.Messages;
using Sfs.EStore.Messages.Events;
using Sfs.Shuttle;
using Shuttle.ESB.Core;

namespace Sfs.EStore.Listings.EmailOutbidUsers
{
    public class NinjectMessageHandlerFactory : NinjectMessageHandlerFactoryWithMessageContainerBase
    {
        protected override void ConfigureApplicationContainer(IKernel container, IServiceBus bus)
        {
            base.ConfigureApplicationContainer(container, bus);

            container.Bind<MandrillApi>().ToSelf()
                .WithConstructorArgument("apiKey", ConfigurationManager.AppSettings["Mandrill/ApiKey"]);
        }

        protected override void ConfigureMessageContainer(IKernel container, IServiceBus bus, object message)
        {
            base.ConfigureMessageContainer(container, bus, message);

            var tenentMessage = message as ITenantMessage;

            if (tenentMessage == null) return;

            var tenantConfig = TenantConfig(tenentMessage.TenantId);

            container.Bind<IMessageHandler<AuctionLotHasEndedWithSale>, IMessageHandler<AuctionLotHasEndedWithoutSale>>()
                .To<EmailOutbidBiddersOnLotEnded>()
                .When(x => tenantConfig.IsConsumerEnabled<EmailOutbidBiddersOnLotEnded>())
                .WithConstructorArgument("templateName",
                    ctx => tenantConfig.ConsumerSetting<EmailOutbidBiddersOnLotEnded>("TemplateName"));

            container.Bind<IMessageHandler<NewBidHasBeenAccepted>>().To<EmailOutbidUserConsumer>()
                .When(x => tenantConfig.IsConsumerEnabled<EmailOutbidUserConsumer>())
                .WithConstructorArgument("templateName",
                    ctx => tenantConfig.ConsumerSetting<EmailOutbidUserConsumer>("TemplateName"));
        }
    }
}