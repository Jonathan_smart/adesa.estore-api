using System;
using System.Globalization;
using System.Linq;
using Mandrill;
using Sfs.EStore.Messages.Events;
using Shuttle.ESB.Core;
using Slugify;

namespace Sfs.EStore.Listings.EmailOutbidUsers
{
    /// <summary>
    /// Send an email to anyone the bid on the lot but wasn't the highest bidder
    /// </summary>
    public class EmailOutbidBiddersOnLotEnded :
        IMessageHandler<AuctionLotHasEndedWithSale>,
        IMessageHandler<AuctionLotHasEndedWithoutSale>
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;

        public EmailOutbidBiddersOnLotEnded(MandrillApi mandrillApi, string templateName)
        {
            _mandrillApi = mandrillApi;
            _templateName = templateName;
        }

        public void ProcessMessage(HandlerContext<AuctionLotHasEnded> context)
        {
            EmailOutbidUsers(context.Message);
        }

        public void ProcessMessage(HandlerContext<AuctionLotHasEndedWithSale> context)
        {
            EmailOutbidUsers(context.Message);
        }

        public void ProcessMessage(HandlerContext<AuctionLotHasEndedWithoutSale> context)
        {
            EmailOutbidUsers(context.Message);
        }

        private void EmailOutbidUsers(AuctionLotHasEnded msg)
        {
            var outBidUsers = msg.OutbidOffers
                .Where(b => b.User.Id != msg.HighestOffer.User.Id)
                //.Select(b => b.User.EmailAddress)
                .ToArray();

            Console.WriteLine("All the following users were outbid: {0}", string.Join("; ", outBidUsers.Select(x => x.User.EmailAddress)));

            var message = new EmailMessage
            {
                to = outBidUsers.Select(bid => new EmailAddress
                {
                    email = bid.User.EmailAddress,
                    name = bid.User.FullName
                }).ToArray()
            };

            // Per user 
            outBidUsers.ToList()
                .ForEach(bid =>
                {
                    message.AddRecipientVariable(bid.User.EmailAddress, "FIRST_NAME", bid.User.FirstName ?? "");
                    message.AddRecipientVariable(bid.User.EmailAddress, "LAST_NAME", bid.User.LastName ?? "");
                    message.AddRecipientVariable(bid.User.EmailAddress, "FULL_NAME", bid.User.FullName);
                    message.AddRecipientVariable(bid.User.EmailAddress, "MAXIMUM_BID_PRICE", bid.Amount.ToCurrencyString());
                    message.AddRecipientVariable(bid.User.EmailAddress, "MAXIMUM_BID_PRICE_AMOUNT",
                        bid.Amount.Amount.ToString(CultureInfo.InvariantCulture));
                    message.AddRecipientVariable(bid.User.EmailAddress, "MAXIMUM_BID_PRICE_CURRENCY", bid.Amount.Currency);
                    message.AddRecipientVariable(bid.User.EmailAddress, "MAXIMUM_BID_DATE",
                        bid.Date.ToString("dd MMM yyyy HH:mm:ss"));
                });
            
            // Global
            message.AddGlobalVariable("HIGHEST_BID_PRICE", msg.HighestOffer.Amount.ToCurrencyString());
            message.AddGlobalVariable("HIGHEST_BID_PRICE_AMOUNT",
                msg.HighestOffer.Amount.Amount.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("HIGHEST_BID_PRICE_CURRENCY", msg.HighestOffer.Amount.Currency);
            message.AddGlobalVariable("HIGHEST_BID_DATE",
                msg.HighestOffer.Date.ToString("dd MMM yyyy HH:mm:ss"));
            message.AddGlobalVariable("LISTING_ID", msg.ListingId.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("LISTING_SLUG", new SlugHelper().GenerateSlug(msg.Listing.Title));
            message.AddGlobalVariable("LISTING_TITLE", msg.Listing.Title);
            message.AddGlobalVariable("LISTING_END_TIME",
                msg.Listing.EndsAt.HasValue ? msg.Listing.EndsAt.Value.ToString("dd MMM yyyy HH:mm:ss") : "");
            message.AddGlobalVariable("VEHICLE_REGISTRATION", msg.Listing.RegistrationPlate);
            message.AddGlobalVariable("VEHICLE_MAKE", msg.Listing.Make);
            message.AddGlobalVariable("VEHICLE_MODEL", msg.Listing.Model);
            message.AddGlobalVariable("VEHICLE_DERIVATIVE", msg.Listing.Derivative);
            
            _mandrillApi.SendMessage(message, _templateName, new TemplateContent[0]);
        }

        public bool IsReusable { get; private set; }
    }
}