﻿using System;
using Sfs.EStore.Messages.Events;
using Shuttle.Core.Data;
using Shuttle.Core.Infrastructure;
using Shuttle.ESB.Core;
using Shuttle.ESB.SqlServer;

namespace Sfs.EStore.Listings.EmailOutbidUsers
{
    public class ServiceBusHost : IDisposable
    {
        private readonly IMessageHandlerFactory _messageHandlerFactory;
        private IServiceBus _bus;

        public ServiceBusHost(IMessageHandlerFactory messageHandlerFactory)
        {
            _messageHandlerFactory = messageHandlerFactory;
        }

        public void Start()
        {
            Log.Assign(new ConsoleLog(typeof(ServiceBusHost)) { LogLevel = LogLevel.Trace });

            new ConnectionStringService().Approve();

            var subscriptionManager = SubscriptionManager.Default();

            subscriptionManager.Subscribe(new[] { typeof(NewBidHasBeenAccepted).FullName });
            subscriptionManager.Subscribe(new[] { typeof(AuctionLotHasEndedWithSale).FullName });
            subscriptionManager.Subscribe(new[] { typeof(AuctionLotHasEndedWithoutSale).FullName });

            _bus = ServiceBus
                .Create(c => c.SubscriptionManager(subscriptionManager)
                    .MessageHandlerFactory(_messageHandlerFactory)
                )
                .Start();
        }

        public void Stop()
        {
            _bus.Dispose();
        }

        public void Dispose()
        {
            if (_bus == null) return;
            
            _bus.Dispose();
        }
    }
}