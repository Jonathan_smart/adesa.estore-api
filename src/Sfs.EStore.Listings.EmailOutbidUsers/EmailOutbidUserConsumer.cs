using System;
using System.Globalization;
using System.Linq;
using Mandrill;
using Sfs.EStore.Messages.Events;
using Shuttle.Core.Infrastructure;
using Shuttle.ESB.Core;
using Slugify;

namespace Sfs.EStore.Listings.EmailOutbidUsers
{
    public class EmailOutbidUserConsumer : IMessageHandler<NewBidHasBeenAccepted>
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;

        public EmailOutbidUserConsumer(MandrillApi mandrillApi, string templateName)
        {
            _mandrillApi = mandrillApi;
            _templateName = templateName;
        }

        private static bool NewUserIsWinning(NewBidHasBeenAccepted message, out NewBidHasBeenAccepted.NewBidHasBeenAccepted_UserDetail outBidUser)
        {
            outBidUser = null;

            if (message.PreviouslyWinningOffer != null &&
                message.CurrentOffer.User.Id != message.PreviouslyWinningOffer.User.Id)
                outBidUser = message.PreviouslyWinningOffer.User;

            return outBidUser != null;
        }

        public void ProcessMessage(HandlerContext<NewBidHasBeenAccepted> context)
        {
            var comment = string.Format("Handled NewBidHasBeenAccepted message for lot {0}", context.Message.ListingId);

            ColoredConsole.WriteLine(ConsoleColor.Green, comment);

            NewBidHasBeenAccepted.NewBidHasBeenAccepted_UserDetail outBidUser;

            if (!NewUserIsWinning(context.Message, out outBidUser) || string.IsNullOrWhiteSpace(outBidUser.EmailAddress))
                return;

            var msg = context.Message;

            var message = new EmailMessage
            {
                to =
                    new[]
                    {
                        new EmailAddress
                        {email = outBidUser.EmailAddress}
                    }
            };

            message.AddGlobalVariable("FIRST_NAME", outBidUser.FirstName ?? "");
            message.AddGlobalVariable("LAST_NAME", outBidUser.LastName ?? "");
            message.AddGlobalVariable("FULL_NAME", outBidUser.FullName);
            message.AddGlobalVariable("MAXIMUM_BID_PRICE", msg.PreviouslyWinningOffer.Amount.ToCurrencyString());
            message.AddGlobalVariable("MAXIMUM_BID_PRICE_AMOUNT",
                msg.PreviouslyWinningOffer.Amount.Amount.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("MAXIMUM_BID_PRICE_CURRENCY", msg.PreviouslyWinningOffer.Amount.Currency);
            message.AddGlobalVariable("MAXIMUM_BID_DATE",
                msg.PreviouslyWinningOffer.Date.ToString("dd MMM yyyy HH:mm:ss"));
            message.AddGlobalVariable("LISTING_ID", msg.ListingId.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("LISTING_SLUG", new SlugHelper().GenerateSlug(msg.Listing.Title));
            message.AddGlobalVariable("LISTING_TITLE", msg.Listing.Title);
            message.AddGlobalVariable("LISTING_END_TIME",
                msg.Listing.EndsAt.HasValue ? msg.Listing.EndsAt.Value.ToString("dd MMM yyyy HH:mm:ss") : "");
            message.AddGlobalVariable("VEHICLE_REGISTRATION", msg.Listing.RegistrationPlate);
            message.AddGlobalVariable("VEHICLE_MAKE", msg.Listing.Make);
            message.AddGlobalVariable("VEHICLE_MODEL", msg.Listing.Model);
            message.AddGlobalVariable("VEHICLE_DERIVATIVE", msg.Listing.Derivative);
            message.AddGlobalVariable("LISTING_CURRENT_PRICE", msg.CurrentOffer.Amount.ToCurrencyString());
            message.AddGlobalVariable("LISTING_CURRENT_PRICE_AMOUNT",
                msg.CurrentOffer.Amount.Amount.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("LISTING_CURRENT_PRICE_CURRENCY", msg.CurrentOffer.Amount.Currency);

            var sendResult = _mandrillApi.SendMessage(message, _templateName, new TemplateContent[0])[0];

            if (!new[] {EmailResultStatus.Sent, EmailResultStatus.Queued}.Contains(sendResult.Status))
            {
                throw new Exception(string.Format("Failed to send out bid notification " +
                                                  "for lot {0} to {1}. Status is {2}",
                    msg.ListingId, outBidUser.EmailAddress, sendResult.Status));
            }
        }


        public bool IsReusable
        {
            get { return false; }
        }
    }
}