﻿using System;
using Topshelf;

namespace Sfs.EStore.Listings.EmailOutbidUsers
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

         
            HostFactory.Run(x =>
            {
                x.Service<ServiceBusHost>(s =>
                {
                    s.ConstructUsing(() =>
                    {
                        var messageHandlerFactory = new NinjectMessageHandlerFactory();
                        return new ServiceBusHost(messageHandlerFactory);
                    });
                    s.WhenStarted(tc =>
                    {
                        tc.Start();
                        Console.WriteLine("Started");
                    });
                    s.WhenStopped(tc =>
                    {
                        tc.Stop();
                        Console.WriteLine("Stopped");
                    });

                });
                x.RunAsPrompt();
                x.SetDescription("Email out bid users on going and on an auction lot ending");
                x.SetServiceName("Sfs.EStore.Listings.EmailOutbidUsers");
                x.SetDescription("Sfs EStore Listings EmailOutbidUsers");
            });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
