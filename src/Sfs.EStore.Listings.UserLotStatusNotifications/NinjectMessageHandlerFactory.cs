﻿using System.Configuration;
using Mandrill;
using Ninject;
using Sfs.EStore.Listings.EmailHighestBidder.Consumers;
using Sfs.EStore.Messages;
using Sfs.EStore.Messages.Events;
using Sfs.Shuttle;
using Shuttle.ESB.Core;

namespace Sfs.EStore.Listings.EmailHighestBidder
{
    public class NinjectMessageHandlerFactory : NinjectMessageHandlerFactoryWithMessageContainerBase
    {
        protected override void ConfigureApplicationContainer(IKernel container, IServiceBus bus)
        {
            base.ConfigureApplicationContainer(container, bus);

            container.Bind<MandrillApi>().ToSelf()
                .WithConstructorArgument("apiKey", ConfigurationManager.AppSettings["Mandrill/ApiKey"]);
        }

        protected override void ConfigureMessageContainer(IKernel container, IServiceBus bus, object message)
        {
            base.ConfigureMessageContainer(container, bus, message);

            var tenentMessage = message as ITenantMessage;

            if (tenentMessage == null) return;

            var tenantConfig = TenantConfig(tenentMessage.TenantId);

            container.Bind<IMessageHandler<AuctionLotHasEndedWithSale>>().To<EmailWinningBidderOnLotEnded>()
                .When(x => tenantConfig.IsConsumerEnabled<EmailWinningBidderOnLotEnded>())
                .WithConstructorArgument("templateName",
                    ctx => tenantConfig.ConsumerSetting<EmailWinningBidderOnLotEnded>("TemplateName"));

            container.Bind<IMessageHandler<AuctionLotHasEndedWithoutSale>>().To<EmailHighestBidderOnLotEnded>()
                .When(x => tenantConfig.IsConsumerEnabled<EmailHighestBidderOnLotEnded>())
                .WithConstructorArgument("templateName",
                    ctx => tenantConfig.ConsumerSetting<EmailHighestBidderOnLotEnded>("TemplateName"));
        }
    }
}