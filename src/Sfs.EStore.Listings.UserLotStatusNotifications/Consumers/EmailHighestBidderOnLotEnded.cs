﻿using System;
using System.Globalization;
using Mandrill;
using Sfs.EStore.Messages.Events;
using Shuttle.ESB.Core;
using Slugify;

namespace Sfs.EStore.Listings.EmailHighestBidder.Consumers
{
    /// <summary>
    /// Send an email to the highest bidder on the lot (the reserve was NOT met)
    /// </summary>
    public class EmailHighestBidderOnLotEnded : IMessageHandler<AuctionLotHasEndedWithoutSale>
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;

        public EmailHighestBidderOnLotEnded(MandrillApi mandrillApi, string templateName)
        {
            _mandrillApi = mandrillApi;
            _templateName = templateName;
        }

        public void ProcessMessage(HandlerContext<AuctionLotHasEndedWithoutSale> context)
        {
            Console.WriteLine("Email to {0}. They were the highest bidder but the reserve was not met", context.Message.HighestOffer.User.EmailAddress);

            var msg = context.Message;
            var bid = msg.HighestOffer;

            var message = new EmailMessage
            {
                to =
                    new[]
                    {
                        new EmailAddress
                        {email = bid.User.EmailAddress}
                    }
            };

            message.AddGlobalVariable("FIRST_NAME", bid.User.FirstName ?? "");
            message.AddGlobalVariable("LAST_NAME", bid.User.LastName ?? "");
            message.AddGlobalVariable("FULL_NAME", bid.User.FullName);
            message.AddGlobalVariable("HIGHEST_BID_PRICE", bid.Amount.ToCurrencyString());
            message.AddGlobalVariable("HIGHEST_BID_PRICE_AMOUNT",
                bid.Amount.Amount.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("HIGHEST_BID_PRICE_CURRENCY", bid.Amount.Currency);
            message.AddGlobalVariable("HIGHEST_BID_DATE",
                bid.Date.ToString("dd MMM yyyy HH:mm:ss"));
            message.AddGlobalVariable("LISTING_ID", msg.ListingId.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("LISTING_SLUG", new SlugHelper().GenerateSlug(msg.Listing.Title));
            message.AddGlobalVariable("LISTING_TITLE", msg.Listing.Title);
            message.AddGlobalVariable("LISTING_END_TIME",
                msg.Listing.EndsAt.HasValue ? msg.Listing.EndsAt.Value.ToString("dd MMM yyyy HH:mm:ss") : "");
            message.AddGlobalVariable("VEHICLE_REGISTRATION", msg.Listing.RegistrationPlate);
            message.AddGlobalVariable("VEHICLE_MAKE", msg.Listing.Make);
            message.AddGlobalVariable("VEHICLE_MODEL", msg.Listing.Model);
            message.AddGlobalVariable("VEHICLE_DERIVATIVE", msg.Listing.Derivative);
            
            _mandrillApi.SendMessage(message, _templateName, new TemplateContent[0]);
        }

        public bool IsReusable { get; private set; }
    }
}
