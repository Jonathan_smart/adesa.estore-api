﻿using System;
using Topshelf;

namespace Sfs.EStore.Listings.EmailHighestBidder
{
    class Program
    {
        private static void Main(string[] args)
        {
            try
            {


                HostFactory.Run(x =>
                {
                    x.Service<ServiceBusHost>(s =>
                    {
                        s.ConstructUsing(() =>
                        {
                            var messageHandlerFactory = new NinjectMessageHandlerFactory();
                            return new ServiceBusHost(messageHandlerFactory);
                        });
                        s.WhenStarted(tc =>
                        {
                            tc.Start();
                            Console.WriteLine("Started");
                        });
                        s.WhenStopped(tc =>
                        {
                            tc.Stop();
                            Console.WriteLine("Stopped");
                        });

                    });
                    x.RunAsPrompt();
                    x.SetDescription("Email the winning or highest bidder on an auction lot ending");
                    x.SetServiceName("Sfs.EStore.Listings.EmailHighestBidder");
                    x.SetDescription("Sfs EStore Listings EmailHighestBidder");
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
