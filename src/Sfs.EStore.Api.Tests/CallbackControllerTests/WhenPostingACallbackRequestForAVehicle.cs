using Moq;
using Raven.Client;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Tests.Base;
using Sfs.EStore.Api.ThirdParties;
using Xunit;

namespace Sfs.EStore.Api.Tests.CallbackControllerTests
{
    public class WhenPostingACallbackRequestForAVehicle : DocumentStoreTestBase
    {
        private readonly CallbacksControllerSut _sut;

        public WhenPostingACallbackRequestForAVehicle()
        {
            _sut = new CallbacksControllerSut(Store);
            _sut.IntegrateCallbackMock.Setup(x => x.Supported).Returns(true);
        }

        [Fact]
        public void ShouldCallToIntegrateCallback()
        {
            const string expectedTelephoneNumber = "0123456789";
            const string expectedMessage = "Foo bar";
            var expectedLotIds = new[] {1, 2};

            var callbackCalled = false;
            _sut.IntegrateCallbackMock
                .Setup(x => x.Callback(It.IsAny<MembershipUserIdentity>(),
                                       It.IsAny<CallbackRequestContact>(),
                                       It.IsAny<CallbackRequestVisitInfo>(),
                                       expectedMessage,
                                       expectedLotIds))
                .Callback(
                    (MembershipUserIdentity identity, CallbackRequestContact contact, CallbackRequestVisitInfo visitInfo,
                        string message, int[] lots) =>
                        {
                            callbackCalled = true;
                            Assert.Equal(expectedTelephoneNumber, contact.TelephoneNumber);
                        });
                //.Verifiable();

            using(var ctx = _sut.NewRequest())
            {
                ctx.SetAuthenticatedPrincipal("fooapp", null, "baruser");

                ctx.Controller.Post(new CallbackPostModel
                                        {
                                            TelephoneNumber = expectedTelephoneNumber,
                                            Message = expectedMessage,
                                            LotIds = expectedLotIds
                                        }).Wait();
            }

            Assert.True(callbackCalled);
            _sut.IntegrateCallbackMock.Verify();
        }

        [Fact]
        public void ShouldSetDbSession()
        {
            _sut.IntegrateCallbackMock
                .SetupSet<IAsyncDocumentSession>(x => x.Session = It.IsAny<IAsyncDocumentSession>())
                .Verifiable();

            using(var ctx = _sut.NewRequest())
            {
                ctx.SetAuthenticatedPrincipal("fooapp", null, "baruser");

                ctx.Controller.Post(new CallbackPostModel
                                        {
                                            TelephoneNumber = It.IsAny<string>(),
                                            Message = It.IsAny<string>(),
                                            LotIds = It.IsAny<int[]>()
                                        }).Wait();
            }
            _sut.IntegrateCallbackMock.Verify();
        }
    }
}