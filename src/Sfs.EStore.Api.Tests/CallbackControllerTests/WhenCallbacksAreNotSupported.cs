﻿using System.Net;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Tests.Base;
using Xunit;

namespace Sfs.EStore.Api.Tests.CallbackControllerTests
{
    public class WhenCallbacksAreNotSupported : DocumentStoreTestBase
    {
        private readonly CallbacksControllerSut _sut;

        public WhenCallbacksAreNotSupported()
        {
            _sut = new CallbacksControllerSut(Store);
            _sut.IntegrateCallbackMock.Setup(x => x.Supported).Returns(false);
        }

        [Fact]
        public void ShouldReturn501NotImplemented()
        {
            using (var ctx = _sut.NewRequest())
            {
                var response = ctx.Controller.Post(new CallbackPostModel
                                                        {
                                                            TelephoneNumber = "0123456789",
                                                            Message = "Foo bar"
                                                        }).Result;

                Assert.Equal(HttpStatusCode.NotImplemented, response.StatusCode);
            }
        }
    }
}
