using Moq;
using Raven.Client;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.ThirdParties;

namespace Sfs.EStore.Api.Tests.CallbackControllerTests
{
    public class CallbacksControllerSut
    {
        private readonly IDocumentStore _store;
        private readonly Mock<IIntegrateCallbacks> _integrateCallbackMock;

        public CallbacksControllerSut(IDocumentStore store)
        {
            _store = store;
            _integrateCallbackMock = new Mock<IIntegrateCallbacks>();
        }

        public WebRequestContext<CallbacksController> NewRequest()
        {
            return new WebRequestContext<CallbacksController>(_store, () => new CallbacksController(_integrateCallbackMock.Object));
        }

        public Mock<IIntegrateCallbacks> IntegrateCallbackMock { get { return _integrateCallbackMock; } }
    }
}