﻿using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests
{
    public class ListingViewCountTests
    {
        private readonly ListingViewCount _counter = new ListingViewCount("Listings/1");

        [Fact]
        public void Should_default_count_to_1()
        {
            Assert.Equal(1, _counter.Count);
        }

        [Fact]
        public void Should_increment_by_1()
        {
            _counter.Increment();
            _counter.Increment();
            _counter.Increment();
            Assert.Equal(4, _counter.Count);
        }
    }
}