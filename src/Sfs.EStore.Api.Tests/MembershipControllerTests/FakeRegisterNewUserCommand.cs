using System;
using System.Web.Http.ModelBinding;
using Raven.Client;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties.UserRegistration;

namespace Sfs.EStore.Api.Tests.MembershipControllerTests
{
    public class FakeRegisterNewUserCommand : IRegisterNewUsersCommand
    {
        public Action ExecuteAction { get; set; }

        public IAsyncDocumentSession Session { get; set; }

        public void Execute()
        {
            if (ExecuteAction != null)
                ExecuteAction();
        }

        public UserRegistrationPostModel Request { get; set; }
    }

    public class FakePasswordRulesValidatorCommand : IValidatePasswordRulesCommand
    {
        public IAsyncDocumentSession Session { get; set; }

        public void Execute()
        {
        }

        public UserRegistrationPostModel Request { get; set; }

        public bool Result { get; set; }

        public string Password { get; set; }

        public ModelStateDictionary ModelState { get; set; }
    }

    public class FakeValidateUserRegistrationRequestsCommand : IValidateUserRegistrationRequestsCommand
    {
        public FakeValidateUserRegistrationRequestsCommand()
        {
            ExecuteAction = () => { Result = true; };
        }

        public Action ExecuteAction { get; set; }

        public IAsyncDocumentSession Session { get; set; }

        public void Execute()
        {
            if (ExecuteAction != null)
                ExecuteAction();
        }

        public UserRegistrationPostModel Request { get; set; }

        public ModelStateDictionary ModelState { get; set; }

        public bool Result { get; set; }
    }
}