﻿using System;
using System.Net;
using System.Net.Http;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Tests.Base;
using Xunit;

namespace Sfs.EStore.Api.Tests.MembershipControllerTests
{
    public class AuthenticationTests : DocumentStoreTestBase
    {
        private readonly Func<AuthenticateController> _controllerFactory = () => new AuthenticateController();

        private const string Username = "testuser";
        private const string ToBeLockedoutUsername = "tobelockedoutuser";
        private const string UnapprovedUsername = "inactiveuser";
        private const string LockedoutUsername = "lockedoutuser";
        private const string Password = "password";
        private const string IncorrectPASSWORD = "incorrectpassword";

        public AuthenticationTests()
        {
            using (var session = Store.OpenSession())
            {
                session.Store(new MembershipUser { Username = Username }.SetPassword(Password).Approve());
                session.Store(new MembershipUser { Username = ToBeLockedoutUsername }.SetPassword(Password).Approve());
                session.Store(new MembershipUser { Username = UnapprovedUsername }.SetPassword(Password));
                session.Store(new MembershipUser { Username = LockedoutUsername }.Lock().SetPassword(Password).Approve());
                session.SaveChanges();
            }
        }

        [Fact]
        public void CanAuthenticateWithValidCredentials()
        {
            using (var ctx = new WebRequestContext<AuthenticateController>(Store, _controllerFactory))
            {
                var input = new ValidateUserPostModel { Username = Username, Password = Password };
                var response = ctx.Controller.Post(input).Result;
                Assert.NotNull(response);
                Assert.True(response.IsSuccessStatusCode);
                var authResponse = response.Content.ReadAsAsync<AuthenticationResponseModel_Authenticated>().Result;
                Assert.True(authResponse.IsAuthenticated);
            }
        }

        [Fact]
        public void NotFoundWhenUserDoesNotExist()
        {
            using (var ctx = new WebRequestContext<AuthenticateController>(Store, _controllerFactory))
            {
                var input = new ValidateUserPostModel { Username = "nonexistent", Password = "password" };
                var response = ctx.Controller.Post(input).Result;
                Assert.NotNull(response);
                Assert.Equal(response.StatusCode, HttpStatusCode.OK);
                var authResponse = response.Content.ReadAsAsync<AuthenticationResponseModel_NotAuthenticated>().Result;
                Assert.False(authResponse.IsAuthenticated);
            }
        }

        [Fact]
        public void CanNotAuthenticateWithInvalidPassword()
        {
            using (var ctx = new WebRequestContext<AuthenticateController>(Store, _controllerFactory))
            {
                var input = new ValidateUserPostModel { Username = Username, Password = IncorrectPASSWORD };
                var response = ctx.Controller.Post(input).Result;
                Assert.NotNull(response);
                Assert.True(response.IsSuccessStatusCode);
                var authResponse = response.Content.ReadAsAsync<AuthenticationResponseModel_NotAuthenticated>().Result;
                Assert.False(authResponse.IsAuthenticated);
            }
        }

        [Fact]
        public void CanNotAuthenticateWithUnApprovedUser()
        {
            using (var ctx = new WebRequestContext<AuthenticateController>(Store, _controllerFactory))
            {
                var input = new ValidateUserPostModel { Username = UnapprovedUsername, Password = Password };
                var response = ctx.Controller.Post(input).Result;
                Assert.NotNull(response);
                Assert.True(response.IsSuccessStatusCode);
                var authResponse = response.Content.ReadAsAsync<AuthenticationResponseModel_NotAuthenticated>().Result;
                Assert.False(authResponse.IsAuthenticated);
            }
        }

        [Fact]
        public void CanNotAuthenticateWithLockedOutUser()
        {
            using (var ctx = new WebRequestContext<AuthenticateController>(Store, _controllerFactory))
            {
                var input = new ValidateUserPostModel { Username = LockedoutUsername, Password = Password };
                var response = ctx.Controller.Post(input).Result;
                Assert.NotNull(response);
                Assert.True(response.IsSuccessStatusCode);
                var authResponse = response.Content.ReadAsAsync<AuthenticationResponseModel_NotAuthenticated>().Result;
                Assert.False(authResponse.IsAuthenticated);
            }
        }

        [Fact]
        public void UserGetsLockedOutAfterMultipleInvalidPasswordAttempts()
        {
            using (var ctx = new WebRequestContext<AuthenticateController>(Store, _controllerFactory))
            {
                var validPasswordInput = new ValidateUserPostModel { Username = ToBeLockedoutUsername, Password = Password };
                var response = ctx.Controller.Post(validPasswordInput).Result;
                Assert.NotNull(response);
                Assert.True(response.IsSuccessStatusCode);
                var authResponse = response.Content.ReadAsAsync<AuthenticationResponseModel_Authenticated>().Result;
                Assert.True(authResponse.IsAuthenticated);

                for (int i = 0; i < 4; i++)
                {
                    var invalidPasswordInput = new ValidateUserPostModel { Username = ToBeLockedoutUsername, Password = IncorrectPASSWORD };
                    response = ctx.Controller.Post(invalidPasswordInput).Result;
                    Assert.NotNull(response);
                    Assert.True(response.IsSuccessStatusCode);
                    authResponse = response.Content.ReadAsAsync<AuthenticationResponseModel_Authenticated>().Result;
                    Assert.False(authResponse.IsAuthenticated);
                }

                response = ctx.Controller.Post(validPasswordInput).Result;
                Assert.NotNull(response);
                Assert.True(response.IsSuccessStatusCode);
                authResponse = response.Content.ReadAsAsync<AuthenticationResponseModel_Authenticated>().Result;
                Assert.False(authResponse.IsAuthenticated);
                
            }
        }

    }
}
