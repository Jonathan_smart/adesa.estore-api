﻿using System;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Tests.Base;
using Sfs.EStore.Api.ThirdParties.UserRegistration;
using Xunit;

namespace Sfs.EStore.Api.Tests.MembershipControllerTests
{
    public class RegistrationTests : DocumentStoreTestBase
    {
        private readonly FakeRegisterNewUserCommand _registerNewUsers = new FakeRegisterNewUserCommand();
        private readonly FakePasswordRulesValidatorCommand _passwordValidator = new FakePasswordRulesValidatorCommand();
        private readonly Func<UserRegistrationController> _controllerFactory;

        private const string Username = "testuser";
        private const string UnapprovedUsername = "inactiveuser";
        private const string ApprovedLockedoutUsername = "lockedoutuser";
        private const string Password = "password";

        public RegistrationTests()
        {
            _controllerFactory = () => new UserRegistrationController(_registerNewUsers, new FakeValidateUserRegistrationRequestsCommand(), _passwordValidator);

            using (var session = Store.OpenSession())
            {
                session.Store(new MembershipUser
                                  {
                                      Username = Username
                                  }.SetPassword(Password).Approve());
                session.Store(new MembershipUser
                                  {
                                      Username = UnapprovedUsername
                                  }.SetPassword(Password));
                session.Store(new MembershipUser
                                  {
                                      Username = ApprovedLockedoutUsername
                                  }
                                  .Lock()
                                  .SetPassword(Password)
                                  .Approve());
                session.SaveChanges();
            }
        }

        [Fact]
        public void CanRegisterNewUser()
        {
            using (var ctx = new WebRequestContext<UserRegistrationController>(Store, _controllerFactory))
            {
                var input = new UserRegistrationPostModel { Username = "newuser", Password = "password" };
                var response = ctx.Controller.Post(input).Result;
                Assert.True(response.IsSuccessStatusCode);
            }
        }

        [Fact]
        public void CanNotRegisterExistingUser()
        {
            _registerNewUsers.ExecuteAction = () => { throw new FailedToRegisterUserException("foo"); };

            using (var ctx = new WebRequestContext<UserRegistrationController>(Store, _controllerFactory))
            {
                var input = new UserRegistrationPostModel { Username = Username, Password = "password" };
                var response = ctx.Controller.Post(input).Result;
                Assert.False(response.IsSuccessStatusCode);
            }
        }
    }
}
