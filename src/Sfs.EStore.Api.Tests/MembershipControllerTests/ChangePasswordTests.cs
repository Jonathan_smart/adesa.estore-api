﻿using System;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Tests.Base;
using Xunit;

namespace Sfs.EStore.Api.Tests.MembershipControllerTests
{
    public class ChangePasswordTests : DocumentStoreTestBase
    {
        private readonly Func<MembershipController> _controllerFactory = () => new MembershipController();
        
        private const string NonexistentUsername = "nonexistentuser";
        private const string CurrentPassword = "password";
        private const string NewPassword = "newpassword";
        private const string IncorrectPassword = "incorrectpassword";

        public ChangePasswordTests()
        {
            ApplyCurrentPrincipal();

            using (var session = Store.OpenSession())
            {
                session.Store(new MembershipUser {Username = MembershipUsername}
                                  .SetPassword(CurrentPassword)
                                  .Approve());
                session.SaveChanges();
            }
        }

        [Fact]
        public void CanChangePassword()
        {
            using (var ctx = new WebRequestContext<MembershipController>(Store, _controllerFactory))
            {
                var response = ctx.Controller.PutChangePassword(new PasswordChange{ CurrentPassword = CurrentPassword, NewPassword = NewPassword }).Result;
                Assert.True(response.IsSuccessStatusCode);
            }
        }

        [Fact]
        public void CanNotChangePasswordWhenCurrentPasswordIncorrect()
        {
            using (var ctx = new WebRequestContext<MembershipController>(Store, _controllerFactory))
            {
                var response = ctx.Controller.PutChangePassword(new PasswordChange { CurrentPassword = IncorrectPassword, NewPassword = NewPassword }).Result;
                Assert.False(response.IsSuccessStatusCode);
            }
        }

        [Fact]
        public void CanNotChangePasswordWhenCurrentUserDoesNotExist()
        {
            using (var ctx = new WebRequestContext<MembershipController>(Store, _controllerFactory))
            {
                ApplyTemporaryCurrentPrincipal(NonexistentUsername);

                var response = ctx.Controller.PutChangePassword(new PasswordChange { CurrentPassword = IncorrectPassword, NewPassword = NewPassword }).Result;
                Assert.False(response.IsSuccessStatusCode);

                RevertTemporaryCurrentPrincipal();
            }
        }
    }
}
