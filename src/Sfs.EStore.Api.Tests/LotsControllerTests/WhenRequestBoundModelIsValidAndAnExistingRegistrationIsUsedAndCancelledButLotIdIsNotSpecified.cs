using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Models;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public class WhenRequestBoundModelIsValidAndAnExistingRegistrationIsUsedAndCancelledButLotIdIsNotSpecified : LotsControllerTestBase
    {
        private readonly LotPutModel _boundModel;
        private readonly HttpResponseMessage _result;
        private readonly string _existingLotId;

        public WhenRequestBoundModelIsValidAndAnExistingRegistrationIsUsedAndCancelledButLotIdIsNotSpecified()
        {
            _boundModel = CreateValidNewLotPutModel();
            _boundModel.RegistrationPlate = "AB12CDE";

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _existingLotId = sut.Controller.Put(null, _boundModel).Result
                    .Content.ReadAsAsync<JObject>().Result.Value<string>("lotId");
            }
            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                var id = int.Parse(_existingLotId.Split('/')[1]);
                sut.Controller.Delete(id).Wait();
            }

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _result = sut.Controller.Put(null, _boundModel).Result;
            }
        }

        [Fact]
        public void ThenResponseStatus_ShouldBe200OK()
        {
            Assert.Equal(HttpStatusCode.OK, _result.StatusCode);
        }

        [Fact]
        public void ThenANewLot_ShouldBeCreated()
        {
            Assert.NotEqual(_existingLotId, _result.Content.ReadAsAsync<JObject>().Result.Value<string>("lotId"));
        }
    }
}