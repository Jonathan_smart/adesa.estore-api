using System;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Raven.Indexes;
using Sfs.EStore.Api.Raven.Transformers;
using Sfs.EStore.Api.Tests.Base;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public abstract class LotsControllerTestBase : DocumentStoreTestBase
    {
        protected LotsControllerTestBase()
        {
            RegisterIndex(new Lot_Search());
            RegisterTransform(new LotIdToListingSummary());
            WaitForIndexing();
        }

        private readonly Func<LotsController> _controllerFactory = () => new LotsController();
        public Func<LotsController> ControllerFactory { get { return _controllerFactory; } }

        public static LotPutModel CreateValidNewFixedPricePutModel()
        {
            return CreateValidNewLotPutModel();
        }

        public static LotPutModel CreateValidNewAuctionPutModel()
        {
            var model = CreateValidNewLotPutModel();
            model.BuyItNowPrice = null;
            model.StartingPrice = new MoneyViewModel(5000, Currency.GBP);

            return model;
        }

        public static LotPutModel CreateValidNewLotPutModel()
        {
            return new LotPutModel
                       {
                           Title = "My Test auction",
                           UniqueExternalReferenceNumber = 1,
                           BuyItNowPrice = new MoneyViewModel(500, Currency.GBP),
                           Derivative = "120d SE 5dr",
                           Make = "BMW",
                           Mileage = 1200,
                           Model = "1 SERIES",
                           RegistrationPlate = "AB13CDE",
                           RegistrationDate = new DateTime(2010, 06, 01),
                           RegistrationYearAndPlate = "10:10",
                           EngineDescription = "2.0 (177PS)",
                           ExteriorColour = "Black",
                           //Interior = "Black Cloth",
                           FuelType = "Diesel",
                           Transmission = "Manual",
                           Vin = "1234567890123456",
                           VatStatus = "Qualifying",
                           Bodystyle = "SRi",
                           EngineSizeCc = 1987,
                           VehicleType = "CAR",
                           ModelYear = "2009",
                           StartDate = SystemTime.UtcNow,
                           EndDate = SystemTime.UtcNow.AddDays(2),
                           LimitVisibilityTo = "tag1,tag2",
                           Category = "category",
                           CapId = 123456,
                           NavSystem = "colour",
                           AlloyWheels = true,
                           Co2 = 55.35m,
                           InsuranceGroup = "99Z",
                           EconomyUrban = 10m,
                           EconomyExtraUrban = 14m,
                           EconomyCombined = 12m
                       };
        }
    }
}