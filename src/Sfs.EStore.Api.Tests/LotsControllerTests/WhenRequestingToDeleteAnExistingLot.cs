using System.Net;
using System.Net.Http;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public class WhenRequestingToDeleteAnExistingLot : LotsControllerTestBase
    {
        private readonly HttpResponseMessage _result;

        private readonly int _existingLotId;

        public WhenRequestingToDeleteAnExistingLot()
        {
            var boundModel = CreateValidNewLotPutModel();

            using (var s = Store.OpenSession())
            {
                var vehicle = new Vehicle();
                boundModel.BindTo(vehicle);
                s.Store(vehicle);

                var lot = new Lot();
                boundModel.BindTo(lot);
                lot.LinkToVehicle(vehicle);
                s.Store(lot);

                s.SaveChanges();
                _existingLotId = lot.LotNumber;
            }
            
            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _result = sut.Controller.Delete(_existingLotId).Result;
            }
        }

        [Fact]
        public void ThenResultStatusCode_ShouldBe202Accepted()
        {
            Assert.Equal(HttpStatusCode.Accepted, _result.StatusCode);
        }

        [Fact]
        public void ThenLot_ShouldBeCancellded()
        {
            using (var s = Store.OpenSession())
            {
                var lot = s.Load<Lot>(_existingLotId);
                Assert.Equal(LotStatusTypes.Cancelled, lot.Status);
            }
        }
    }

    public class WhenRequestingToDeleteAnExistingLotAndLotIsSold : LotsControllerTestBase
    {
        private readonly HttpResponseMessage _result;

        private readonly int _existingLotId;

        public WhenRequestingToDeleteAnExistingLotAndLotIsSold()
        {
            var boundModel = CreateValidNewLotPutModel();

            using (var s = Store.OpenSession())
            {
                var vehicle = new Vehicle();
                boundModel.BindTo(vehicle);
                s.Store(vehicle);

                var lot = new Lot();
                boundModel.BindTo(lot);
                lot.LinkToVehicle(vehicle);
                lot.Status = LotStatusTypes.EndedWithSales;
                s.Store(lot);

                s.SaveChanges();
                _existingLotId = lot.LotNumber;
            }

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _result = sut.Controller.Delete(_existingLotId).Result;
            }
        }

        [Fact]
        public void ThenResultStatusCode_ShouldBe403Forbidden()
        {
            Assert.Equal(HttpStatusCode.Forbidden, _result.StatusCode);
        }
    }

    public class WhenRequestingToDeleteAnExistingLotAndLotIsUnsold : LotsControllerTestBase
    {
        private readonly HttpResponseMessage _result;

        private readonly int _existingLotId;

        public WhenRequestingToDeleteAnExistingLotAndLotIsUnsold()
        {
            var boundModel = CreateValidNewLotPutModel();

            using (var s = Store.OpenSession())
            {
                var vehicle = new Vehicle();
                boundModel.BindTo(vehicle);
                s.Store(vehicle);

                var lot = new Lot();
                boundModel.BindTo(lot);
                lot.LinkToVehicle(vehicle);
                lot.Status = LotStatusTypes.EndedWithoutSales;
                s.Store(lot);

                s.SaveChanges();
                _existingLotId = lot.LotNumber;
            }

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _result = sut.Controller.Delete(_existingLotId).Result;
            }
        }

        [Fact]
        public void ThenResultStatusCode_ShouldBe403Forbidden()
        {
            Assert.Equal(HttpStatusCode.Forbidden, _result.StatusCode);
        }
    }
}