using System.Net;
using Sfs.EStore.Api.Controllers;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public class WhenRequestBoundModelIsNull : LotsControllerTestBase
    {
        [Fact]
        public void ThenResponseStatusCode_ShouldBe400BadRequest()
        {
            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                var result = sut.Controller.Put(null, null).Result;

                Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            }
        }
    }
}