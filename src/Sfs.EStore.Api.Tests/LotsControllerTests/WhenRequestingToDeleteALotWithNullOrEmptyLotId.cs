using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Api.Controllers;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public class WhenRequestingToDeleteALotWithNullLotId : LotsControllerTestBase
    {
        [Fact]
        public void ThenResultStatusCode_ShouldBe400BadRequest()
        {
            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                var result = sut.Controller.Delete(null).Result;
                Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            }
        }
        [Fact]
        public void ThenResultContentMessage_ShouldBeIdNotSpecfied()
        {
            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                var result = sut.Controller.Delete(null).Result.Content.ReadAsAsync<JObject>().Result;

                Assert.Equal("id not specified",
                    result["message"].Value<string>());
            }
        }
    }
}