using System.Net.Http;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public class WhenRequestBoundModelIsValidAndLotIdIsSpecified : LotsControllerTestBase
    {
        private readonly LotPutModel _boundModel;
        private readonly string _lotId;

        public WhenRequestBoundModelIsValidAndLotIdIsSpecified()
        {
            _boundModel = CreateValidNewLotPutModel();

            using (var s = Store.OpenSession())
            {
                var vehicle = new Vehicle();
                _boundModel.BindTo(vehicle);
                s.Store(vehicle);

                var vehicleData = new VehicleData { Id = vehicle.Id + "/Data" };
                _boundModel.BindTo(vehicleData);
                s.Store(vehicleData);

                var lot = new Lot();
                _boundModel.BindTo(lot);
                lot.LinkToVehicle(vehicle);
                s.Store(lot);

                s.SaveChanges();
                _lotId = lot.Id;
            }
        }

        [Fact]
        public void Then_ShouldUpdateExistingLot()
        {
            var boundModel = CreateValidNewLotPutModel();
            boundModel.LotId = _lotId;

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                var result = sut.Controller.Put(null, boundModel).Result.Content
                    .ReadAsAsync<JObject>().Result
                    .Value<string>("lotId");

                Assert.Equal(_lotId, result);
            }
        }
    }
}