using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public class WhenRequestBoundModelIsValid : LotsControllerTestBase
    {
        private readonly LotPutModel _boundModel;
        private readonly HttpResponseMessage _response;

        public WhenRequestBoundModelIsValid()
        {
            _boundModel = CreateValidNewLotPutModel();
            _boundModel.Images = new[]
                                     {
                                         new LotPutModel.Image
                                             {Url = "http://full/path/to/image.jpg", Name = "image.jpg"},
                                         new LotPutModel.Image
                                             {Url = "http://full/path/to/image2.jpg", Name = "image2.jpg"}
                                     };
            var additionalData = new Dictionary<string, object>
                {
                    { "Audio", "Unknown" },
                    { "Electric Mirrors", 0 },
                    { "MOT Expiry Date", new DateTime() }
                };
            _boundModel.Data.Add("AdditionalData", additionalData);

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _response = sut.Controller.Put(null, _boundModel).Result;
            }
        }

        [Fact]
        public void ThenResponseStatusCode_ShouldBeOK200()
        {
            Assert.Equal(HttpStatusCode.OK, _response.StatusCode);
        }

        [Fact]
        public void ThenResponse_ShouldHaveLotId()
        {
            Assert.NotNull(_response.Content.ReadAsAsync<JObject>().Result.Value<string>("lotId"));
        }

        [Fact]
        public void ThenResponse_ShouldHaveStatus()
        {
            Assert.NotNull(_response.Content.ReadAsAsync<JObject>().Result.Value<string>("status"));
        }

        [Fact]
        public void ThenLot_ShouldHaveABuyItNowPrice()
        {
            AssertSavedLotProperty(x => x.BuyItNowPrice, new Money(_boundModel.BuyItNowPrice.Amount, _boundModel.BuyItNowPrice.CurrencyType()));
        }

        [Fact]
        public void ThenLot_ShouldHaveAStartDate()
        {
            AssertSavedLotProperty(x => x.StartDate, _boundModel.StartDate);
        }

        [Fact]
        public void ThenLot_ShouldHaveAnEndDate()
        {
            AssertSavedLotProperty(x => x.EndDate, _boundModel.EndDate);
        }

        [Fact]
        public void ThenLot_ShouldHaveATitle()
        {
            AssertSavedLotProperty(x => x.Title, _boundModel.Title);
        }

        [Fact]
        public void ThenLot_ShouldHaveAnUniqueExternalReferenceNumber()
        {
            AssertSavedLotProperty(x => x.UniqueExternalReferenceNumber, _boundModel.UniqueExternalReferenceNumber);
        }

        [Fact]
        public void ThenLot_ShouldHaveTags()
        {
            AssertSavedLotProperty(x => x.VisibilityLimitedTo, roles =>
                {
                    Assert.NotEmpty(roles);
                    foreach (var tag in _boundModel.LimitVisibilityTo.Split(','))
                    {
                        Assert.Contains(tag, roles);
                    }
                });
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAVin()
        {
            AssertSavedVehicleProperty(x => x.Vin, _boundModel.Vin);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAMake()
        {
            AssertSavedVehicleProperty(x => x.Make, _boundModel.Make);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAModel()
        {
            AssertSavedVehicleProperty(x => x.Model, _boundModel.Model);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAModelYear()
        {
            AssertSavedVehicleProperty(x => x.ModelYear, _boundModel.ModelYear);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAMileage()
        {
            AssertSavedVehicleProperty(x => x.Mileage, _boundModel.Mileage);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAnEngineDescription()
        {
            AssertSavedVehicleProperty(x => x.Specification.Engine.Description, _boundModel.EngineDescription);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAnEngineSizeCc()
        {
            AssertSavedVehicleProperty(x => x.Specification.Engine.CapacityCc, _boundModel.EngineSizeCc);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveADerivative()
        {
            AssertSavedVehicleProperty(x => x.Derivative, _boundModel.Derivative);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveABodystyle()
        {
            AssertSavedVehicleProperty(x => x.Bodystyle, _boundModel.Bodystyle);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAFuelType()
        {
            AssertSavedVehicleProperty(x => x.FuelType, _boundModel.FuelType);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveATransmission()
        {
            AssertSavedVehicleProperty(x => x.Transmission, _boundModel.Transmission);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveARegistrationPlate()
        {
            AssertSavedVehicleProperty(x => x.Registration.Plate, _boundModel.RegistrationPlate);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveARegistrationDate()
        {
            AssertSavedVehicleProperty(x => x.Registration.Date, _boundModel.RegistrationDate);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveARegistrationYearAndPlate()
        {
            AssertSavedVehicleProperty(x => x.Registration.YearAndPlate, _boundModel.RegistrationYearAndPlate);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAnExteriorColour()
        {
            AssertSavedVehicleProperty(x => x.ExteriorColour, _boundModel.ExteriorColour);
        }

        //[Fact]
        //public void ThenVehicle_ShouldHaveAnInteriorTrim()
        //{
        //    AssertSavedVehicleProperty(x => x.Interior, _boundModel.Interior);
        //}

        [Fact]
        public void ThenVehicle_ShouldHaveAVehicleType()
        {
            AssertSavedVehicleProperty(x => x.VehicleType, _boundModel.VehicleType);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAVatStatus()
        {
            AssertSavedVehicleProperty(x => x.VatStatus, _boundModel.VatStatus);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveImagePaths()
        {
            AssertSavedVehicleProperty(x => x.Images, images =>
                                                          {
                                                              Assert.NotEmpty(images);

                                                              foreach (var boundImage in _boundModel.Images)
                                                              {
                                                                  var savedUrls = images.Select(x => x.Url).ToArray();
                                                                  //var savedNames = images.Select(x => x.Name).ToArray();
                                                                  Assert.Contains(boundImage.Url, savedUrls);
                                                                  //Assert.Contains(boundImage.Name, savedNames);
                                                              }
                                                          });
        }

        [Fact]
        public void ThenVehicle_ShouldHaveDataBag()
        {
            AssertSavedVehicleDataProperty(x => x.Data, data =>
            {
                Assert.NotEmpty(data);

                foreach (var kv in _boundModel.Data)
                {
                    Assert.Contains(kv.Key, data.Keys);
                }
            });
        }

        [Fact]
        public void ThenVehicle_ShouldHaveACategory()
        {
            AssertSavedVehicleProperty(x => x.Category, _boundModel.Category);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveACapId()
        {
            AssertSavedVehicleProperty(x => x.CapId, _boundModel.CapId);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveANavSystem()
        {
            AssertSavedVehicleProperty(x => x.NavSystem, _boundModel.NavSystem);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAAlloyWheels()
        {
            AssertSavedVehicleProperty(x => x.AlloyWheels, _boundModel.AlloyWheels);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveACo2()
        {
            AssertSavedVehicleProperty(x => x.Specification.Environment.Co2, _boundModel.Co2);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAInsuranceGroup()
        {
            AssertSavedVehicleProperty(x => x.InsuranceGroup, _boundModel.InsuranceGroup);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAEconomyUrban()
        {
            AssertSavedVehicleProperty(x => x.Specification.Economy.Urban, _boundModel.EconomyUrban);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAEconomyExtraUrban()
        {
            AssertSavedVehicleProperty(x => x.Specification.Economy.ExtraUrban, _boundModel.EconomyExtraUrban);
        }

        [Fact]
        public void ThenVehicle_ShouldHaveAEconomyCombined()
        {
            AssertSavedVehicleProperty(x => x.Specification.Economy.Combined, _boundModel.EconomyCombined);
        }

        private void AssertSavedLotProperty<TProperty>(Expression<Func<Lot, TProperty>> expression, TProperty expected)
        {
            AssertSavedProperty((lot, vehicle, vehicleData) => expression.Compile()(lot), expected);
        }

        private void AssertSavedLotProperty<TProperty>(Expression<Func<Lot, TProperty>> expression, Action<TProperty> assertion)
        {
            AssertSavedProperty((lot, vehicle, vehicleData) => expression.Compile()(lot), assertion);
        }

        private void AssertSavedVehicleProperty<TProperty>(Expression<Func<Vehicle, TProperty>> expression, TProperty expected)
        {
            AssertSavedProperty((lot, vehicle, vehicleData) => expression.Compile()(vehicle), expected);
        }

        private void AssertSavedVehicleProperty<TProperty>(Expression<Func<Vehicle, TProperty>> expression, Action<TProperty> assertion)
        {
            AssertSavedProperty((lot, vehicle, vehicleData) => expression.Compile()(vehicle), assertion);
        }

        private void AssertSavedVehicleDataProperty<TProperty>(Expression<Func<VehicleData, TProperty>> expression, Action<TProperty> assertion)
        {
            AssertSavedProperty((lot, vehicle, vehicleData) => expression.Compile()(vehicleData), assertion);
        }

        private void AssertSavedProperty<TProperty>(Func<Lot, Vehicle, VehicleData, TProperty> actual, TProperty expected)
        {
            AssertSavedProperty(actual, p => Assert.Equal(expected, p));
        }

        private void AssertSavedProperty<TProperty>(Func<Lot, Vehicle, VehicleData, TProperty> actual, Action<TProperty> assertion)
        {
            var lotId = _response.Content.ReadAsAsync<JObject>().Result.Value<string>("lotId");

            using (var s = Store.OpenSession())
            {
                var lot = s
                    .Include<Lot>(x => x.VehicleId)
                    .Include(x => x.VehicleId + "/Data")
                    .Load<Lot>(lotId);
                var vehicle = s.Load<Vehicle>(lot.VehicleId);
                var vehicleData = s.Load<VehicleData>(lot.VehicleId + "/Data");

                var value = actual(lot, vehicle, vehicleData);

                assertion(value);
            }
        }
    }
}