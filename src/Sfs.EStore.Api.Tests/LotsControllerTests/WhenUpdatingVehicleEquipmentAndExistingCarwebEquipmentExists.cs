using System.Linq;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public class WhenUpdatingVehicleEquipmentAndExistingCarwebEquipmentExists : LotsControllerTestBase
    {
        private readonly LotPutModel _boundModel;
        private readonly string _existingLotId;
        private readonly Vehicle _vehicle;

        public WhenUpdatingVehicleEquipmentAndExistingCarwebEquipmentExists()
        {
            _boundModel = CreateValidNewLotPutModel();

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _existingLotId = sut.Controller.Put(null, _boundModel).Result
                    .Content.ReadAsAsync<JObject>().Result.Value<string>("lotId");
            }

            using (var session = Store.OpenSession())
            {
                var lot = session
                    .Include<Lot>(x => x.VehicleId)
                    .Load<Lot>(_existingLotId);
                var vehicle = session.Load<Vehicle>(lot.VehicleId);

                vehicle.Specification.Equipment.Set("carweb",
                                                    new Equipment.EquipmentItem("My carweb category", "Foo"),
                                                    new Equipment.EquipmentItem("My carweb category", "Bar"));

                session.SaveChanges();
            }

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _boundModel.LotId = _existingLotId;
                _boundModel.Equipment = new[]
                                            {
                                                new LotPutModel.EquipmentItem
                                                    {Category = "Direct Category", Name = "D Foo"},
                                                new LotPutModel.EquipmentItem
                                                    {Category = "Direct Category", Name = "D Bar"}
                                            };
                sut.Controller.Put(null, _boundModel).Wait();
            }

            using (var session = Store.OpenSession())
            {
                var lot = session
                    .Include<Lot>(x => x.VehicleId)
                    .Load<Lot>(_existingLotId);
                _vehicle = session.Load<Vehicle>(lot.VehicleId);
            }
        }

        [Fact]
        public void Then_vehicle_should_have_4_items()
        {
            Assert.Equal(4, _vehicle.Specification.Equipment.Items.Length);
        }

        [Fact]
        public void Then_vehicle_should_have_2_direct_items()
        {
            Assert.Equal(2, _vehicle.Specification.Equipment.Items.Where(x => x.Source == "direct").ToArray().Length);
        }
    }
}