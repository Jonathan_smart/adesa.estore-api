using System.Linq;
using System.Net;
using System.Net.Http;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public class WhenRequestBoundModelIsValidAndAnExistingRegistrationIsUsedButLotIdIsNotSpecified : LotsControllerTestBase
    {
        private readonly HttpResponseMessage _result;

        public WhenRequestBoundModelIsValidAndAnExistingRegistrationIsUsedButLotIdIsNotSpecified()
        {
            var boundModel = CreateValidNewLotPutModel();
            boundModel.RegistrationPlate = "AB12CDE";

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                sut.Controller.Put(null, boundModel).Wait();
            }

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _result = sut.Controller.Put(null, boundModel).Result;
            }
        }

        [Fact]
        public void ThenResponseStatus_ShouldBe400BadRequest()
        {
            Assert.Equal(HttpStatusCode.BadRequest, _result.StatusCode);
        }

        [Fact]
        public void ThenANewLot_ShouldNotExist()
        {
            using (var s = Store.OpenSession())
            {
                Assert.Equal(1, s.Query<Lot>().Customize(x => x.WaitForNonStaleResults()).Count());
            }
        }
    }
}