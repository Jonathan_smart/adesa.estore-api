using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Api.Controllers;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public class WhenRequestingToDeleteALotThatDoesNotExist : LotsControllerTestBase
    {
        [Fact]
        public void ThenResultStatusCode_ShouldBe404NotFound()
        {
            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                var result = sut.Controller.Delete(0).Result;
                Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);

            }
        }

        [Fact]
        public void ThenResultContentMessage_ShouldBeLotNotFound()
        {
            const int nonExistantLotId = 123;
            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                var result = sut.Controller.Delete(nonExistantLotId).Result.Content.ReadAsAsync<JObject>().Result;

                Assert.Equal(string.Format("Lot with id 'lots/{0}' not found", nonExistantLotId),
                             result["message"].Value<string>());

            }
        }
    }
}