using System.Linq;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Api.Controllers;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public class WhenRequestBoundModelLotIdDoesNotExist : LotsControllerTestBase
    {
        private readonly HttpResponseMessage _result;

        public WhenRequestBoundModelLotIdDoesNotExist()
        {
            var boundModel = CreateValidNewLotPutModel();
            boundModel.LotId = "lots/100";

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _result = sut.Controller.Put(null, boundModel).Result;
            }
        }

        [Fact]
        public void ThenResponseStatus_ShouldBe400BadRequest()
        {
            Assert.Equal(HttpStatusCode.BadRequest, _result.StatusCode);
        }

        [Fact]
        public void ThenResponseErrors_ShouldContainLotId()
        {
            var json = _result.Content.ReadAsAsync<JObject>().Result;
            var lotIdErrros = json["modelState"]["lotId"].Values<string>().ToArray();
            Assert.NotEmpty(lotIdErrros);
        }
    }
}