﻿using System.Net.Http;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests
{
    public class WhenRequestBoundModelIsValidAndNoLotIdIsSpecified : LotsControllerTestBase
    {
        private readonly LotPutModel _boundModel;
        private readonly HttpResponseMessage _response;

        public WhenRequestBoundModelIsValidAndNoLotIdIsSpecified()
        {
            _boundModel = CreateValidNewLotPutModel();

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _response = sut.Controller.Put(null, _boundModel).Result;
            }
        }

        [Fact]
        public void ThenResponse_ShouldContainTheNewLotId()
        {
            Assert.NotNull(_response.Content.ReadAsAsync<JObject>().Result.Value<string>("lotId"));
        }
        
        [Fact]
        public void ThenNewLot_ShouldBeRetrievable()
        {
            var lotId = _response.Content.ReadAsAsync<JObject>().Result.Value<string>("lotId");

            using(var s = Store.OpenSession())
            {
                var lot = s.Load<Lot>(lotId);
                Assert.NotNull(lot);
            }
        }
    }
}
