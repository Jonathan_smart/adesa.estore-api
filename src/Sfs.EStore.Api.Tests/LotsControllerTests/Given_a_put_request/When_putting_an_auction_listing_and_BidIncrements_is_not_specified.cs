using System.Linq;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Api.Controllers;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotsControllerTests.Given_a_put_request
{
    public class When_putting_an_auction_listing_and_BidIncrements_is_not_specified : LotsControllerTestBase
    {
        private readonly HttpResponseMessage _result;

        public When_putting_an_auction_listing_and_BidIncrements_is_not_specified()
        {
            var boundModel = CreateValidNewAuctionPutModel();
            boundModel.BidIncrements = null;

            using (var sut = new WebRequestContext<LotsController>(Store, ControllerFactory))
            {
                _result = sut.Controller.Put(null, boundModel).Result;
            }
        }

        [Fact]
        public void ThenResponseStatus_ShouldBe400BadRequest()
        {
            Assert.Equal(HttpStatusCode.BadRequest, _result.StatusCode);
        }

        [Fact]
        public void ThenResponseErrors_ShouldContainLotId()
        {
            var json = _result.Content.ReadAsAsync<JObject>().Result;
            var modelError = json["modelState"]["bidIncrements"].Values<string>().ToArray();
            Assert.NotEmpty(modelError);
        }
    }
}