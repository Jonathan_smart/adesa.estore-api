﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Xunit;

namespace Sfs.EStore.Api.Tests.Auction
{
    public class Bidding
    {
        //private ILotOffer Offer;
        //private MaximumBidLimit MaxiumBidLimit;
        private Lot mockLot;

        private void SetupLot()
        {
            mockLot = new Lot()
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(2),
                Status = LotStatusTypes.Active,
                CurrentOffer = new AutoBidOffer(new Guid(), DateTime.Now, new Money(2000, Currency.GBP), ""),
                Bidding = new Biddable(new Money(1000, Currency.GBP), new Money(50, Currency.GBP), new Money(12000, Currency.GBP))
            };

            mockLot.SetBidding(new Money(1000, Currency.GBP), new Money(50, Currency.GBP), new Money(12000, Currency.GBP));
        }


        [Fact]
        public void Make_FirstTime_Bid_greater_than_Reserve()
        {
            //ARRANGE    
            Action<ILotOffer> a = CurrentOffer;
            Action<MaximumBidLimit> b = CurrentMaxbid;
            SetupLot();

            //ACT
            var bid = new Money(15000, Currency.GBP);
            var outcome = new EBayStyleBiddingStrategy(mockLot, a, b)
                .TakeBid(bid, "smartj");

            //ASSERT
            Assert.Equal(BidOutcome.BidResponse.Accepted, outcome.Outcome);
            Assert.Same(mockLot.Bidding.ReservePrice, outcome.AmountAccepted);
        }

        [Fact]
        public void Make_First_Bid_Below_Reserve_Second_Greater_Than_bid_increment_From_Same_User()
        {
            //ARRANGE  
            Action<ILotOffer> a = CurrentOffer;
            Action<MaximumBidLimit> b = CurrentMaxbid;

            SetupLot();
            //ACT
            var bid = new Money(6000, Currency.GBP);
            var outcome = new EBayStyleBiddingStrategy(mockLot, a, b)
                .TakeBid(bid, "doeJ");


            bid = new Money(13500, Currency.GBP);
            outcome = new EBayStyleBiddingStrategy(mockLot, a, b)
               .TakeBid(bid, "jamesT");

            bid = new Money(51000, Currency.GBP);
            outcome = new EBayStyleBiddingStrategy(mockLot, a, b)
               .TakeBid(bid, "doeJ");

            bid = new Money(15000, Currency.GBP);
            outcome = new EBayStyleBiddingStrategy(mockLot, a, b)
               .TakeBid(bid, "jamesT");

            //ASSERT -- CHECK BID INCREMENT
            Assert.Equal(BidOutcome.BidResponse.NotAllowed, outcome.Outcome);
        }


        [Fact]
        public void Make_First_Bid_Above_Reserve_Second_Greater_Than_bid_increment_From_Same_User()
        {
            //ARRANGE  
            Action<ILotOffer> a = CurrentOffer;
            Action<MaximumBidLimit> b = CurrentMaxbid;

            SetupLot();
            //ACT
            var bid = new Money(13000, Currency.GBP);
            var outcome = new EBayStyleBiddingStrategy(mockLot, a, b)
                .TakeBid(bid, "doeJ");


            bid = new Money(13500, Currency.GBP);
            outcome = new EBayStyleBiddingStrategy(mockLot, a, b)
               .TakeBid(bid, "doeJ");


            bid = new Money(14000, Currency.GBP);
            outcome = new EBayStyleBiddingStrategy(mockLot, a, b)
               .TakeBid(bid, "smartj");

            var highestbidder = mockLot.Bidding.Bids.OrderByDescending(x => x.Amount).First();

            //ASSERT
            Assert.Equal(BidOutcome.BidResponse.AcceptedAboveReserve, outcome.Outcome);
            Assert.True(highestbidder.Amount.Amount == 13550);
        }


        [Fact]
        //Reserve 12000
        //bid increment 50
        public void BidAboveReserve_OnlyIncrease_ByBidIncrement()
        {
            //ARRANGE  
            Action<ILotOffer> a = CurrentOffer;
            Action<MaximumBidLimit> b = CurrentMaxbid;
            SetupLot();
            //ACT
            var bid = new Money(13000, Currency.GBP);
            var outcome = new EBayStyleBiddingStrategy(mockLot, a, b)
                .TakeBid(bid, "doeJ");

            bid = new Money(13500, Currency.GBP);
            outcome = new EBayStyleBiddingStrategy(mockLot, a, b)
               .TakeBid(bid, "smartj");

            var highestbidder = mockLot.Bidding.Bids.OrderByDescending(x => x.Amount).First();

            //ASSERT
            Assert.True(highestbidder.Amount.Amount == 13050 && highestbidder.UserId == "smartj");
        }

        private void CurrentMaxbid(MaximumBidLimit currentMaxbid)
        {
            List<MaximumBidLimit> list = mockLot.Bidding.BidLimits.ToList();
            list.Add(currentMaxbid);
            mockLot.Bidding.BidLimits = list;
        }

        private void CurrentOffer(ILotOffer lotOffer)
        {
            mockLot.CurrentOffer = lotOffer;

            List<ILotOffer> bids = mockLot.Bidding.Bids.ToList();
            bids.Add(lotOffer);
            mockLot.Bidding.Bids = bids;
        }
    }
}
