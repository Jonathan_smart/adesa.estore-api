using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Api.Tests.Extensions;

namespace Sfs.EStore.Api.Tests
{
    public class FakeHandler : DelegatingHandler
    {
        public HttpResponseMessage Response { get; set; }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
                                                               CancellationToken cancellationToken)
        {
            return Response == null
                       ? base.SendAsync(request, cancellationToken)
                       : Task.Factory.StartNew(() => Response);
        }
    }

    public static class HttpClientFacotry
    {
        public static HttpClient CreateFake(object response = null)
        {
            var fakeHandler = new FakeHandler
                                  {
                                      InnerHandler = new HttpClientHandler(),
                                      Response = new HttpResponseMessage(HttpStatusCode.OK)
                                                     {
                                                         Content = new StreamContent(
                                                             new MemoryStream(
                                                                 Encoding.ASCII.GetBytes(
                                                                     JObject.FromObject(response ?? new {}).ToString())))
                                                     }.SetJsonContentTypeHeader()
                                  };
            return new HttpClient(fakeHandler)
                       {
                           BaseAddress = new Uri("http://example.com/")
                       };
        }
    }
}