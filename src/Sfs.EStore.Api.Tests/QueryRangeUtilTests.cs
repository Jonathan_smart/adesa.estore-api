using Xunit;
using Xunit.Extensions;

namespace Sfs.EStore.Api.Tests
{
    public class QueryRangeUtilTests
    {
        [Fact]
        public void NullOrEmptyStringShouldReturnNull()
        {
            Assert.Null(QueryRangeUtil.StringToRange(""));
        }

        [Theory]
        [InlineData("[MALFORMED TO ANOTHER]")]
        [InlineData("MALFORMED TO ANOTHER]")]
        [InlineData("[MALFORMED TO ANOTHER")]
        [InlineData("FooBar")]
        public void MalformedQueryShouldReturnNull(string searchTerm)
        {
            Assert.Null(QueryRangeUtil.StringToRange(searchTerm));
        }

        [Fact]
        public void InclusiveIntegerRangeQueryShouldReturn2Integers()
        {
            var range = QueryRangeUtil.StringToRange("[Ix100 TO Ix400]");
            Assert.NotNull(range);

            Assert.Equal(100, range.Item1);
            Assert.Equal(400, range.Item2);
        }

        [Fact]
        public void InclusiveIntegerRangeWithNullLowerShouldReturn1Integer()
        {
            var range = QueryRangeUtil.StringToRange("[NULL TO Ix400]");
            Assert.NotNull(range);

            Assert.Null(range.Item1);
            Assert.Equal(400, range.Item2);
        }

        [Fact]
        public void InclusiveIntegerRangeWithNullUpperShouldReturn1Integer()
        {
            var range = QueryRangeUtil.StringToRange("[Ix300 TO NULL]");
            Assert.NotNull(range);

            Assert.Equal(300, range.Item1);
            Assert.Null(range.Item2);
        }

        [Fact]
        public void InclusiveDoubleRangeQueryShouldReturn2Doubles()
        {
            var range = QueryRangeUtil.StringToRange("[Dx100.0 TO Dx400.0]");
            Assert.NotNull(range);

            Assert.Equal(100.0, range.Item1);
            Assert.Equal(400.0, range.Item2);
        }
    }
}