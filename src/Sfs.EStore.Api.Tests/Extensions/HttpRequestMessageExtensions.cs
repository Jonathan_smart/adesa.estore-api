using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Sfs.EStore.Api.Tests.Extensions
{
    public static class HttpRequestMessageExtensions
    {
        public static HttpRequestMessage ConfigureJsonFormatter(this HttpRequestMessage @this)
        {
            var httpConfiguration = @this.GetOrAddHttpConfiguration();

            var settings = httpConfiguration.Formatters.JsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            return @this;
        }

        private static HttpConfiguration GetOrAddHttpConfiguration(this HttpRequestMessage @this)
        {
            return @this.Properties.ContainsKey(HttpPropertyKeys.HttpConfigurationKey)
                       ? (HttpConfiguration)@this.Properties[HttpPropertyKeys.HttpConfigurationKey]
                       : @this.CreateHttpConfiguration();
        }

        private static HttpConfiguration CreateHttpConfiguration(this HttpRequestMessage @this)
        {
            var httpConfiguration = new HttpConfiguration();
            @this.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, httpConfiguration);

            return httpConfiguration;
        }
    }

    public static class HttpResponseMessageExtensions
    {
        public static HttpResponseMessage SetJsonContentTypeHeader(this HttpResponseMessage @this)
        {
            @this.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return @this;
        }
    }
}