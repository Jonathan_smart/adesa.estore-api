using System;
using System.Text;

namespace Sfs.EStore.Api.Tests.Extensions
{
    public static class StringExtensions
    {
        public static string ToBase64String(this string source)
        {
            return Convert.ToBase64String(Encoding.ASCII.GetBytes(source));
        }

        public static string FormatWith(this string source, params object[] formatParams)
        {
            return string.Format(source, formatParams);
        }
    }
}