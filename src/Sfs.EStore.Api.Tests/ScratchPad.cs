﻿using System;
using Sfs.EStore.Api.CodeWeaversFinanceServiceReference;
using Xunit;

namespace Sfs.EStore.Api.Tests
{
    public class ScratchPad
    {
        [Fact(Skip = "ignored")]
        public void TestFinanceService()
        {
            using (var client = new FinanceClient())
            {
                var loginCredentials = new LoginCredentials { Password = "ADYRf_573", Username = "Smart Vehicle Sales" };
                var parameters = new Parameters
                                     {
                                         AnnualMileage = 10000,
                                         DepositType = DepositType.Amount,
                                         MaximumTerm = 24,
                                         TotalDeposit = 1000
                                     };
                var vehicle = new VehicleRequest
                                  {
                                      Id = "lots/123",
                                      Vehicle = new Vehicle
                                                    {
                                                        Identifier = "54723", // CAP code
                                                        CashPrice = 54000,
                                                        CurrentMileage = 3814,
                                                        DateOfRegistration = "2012-06-14",
                                                        RegistrationNumber = "OE12EVB",
                                                        Type = VehicleType.Car,
                                                        ImageUrl = "http://www.grs.co.uk/stockimages/StkImg256018.JPG",
                                                        DealerVehicleUrl = "",
                                                        DetailedPageUrl = "http://foo.bar",
                                                    },
                                      Products = new[]
                                                     {
                                                         new Product {Key = "HP"},
                                                         new Product {Key = "PCP"}
                                                     }
                                  };

                var result = client.Calculate
                    (
                        loginCredentials,
                        parameters,
                        new[] {vehicle},
                        "Reference",
                        "v1", null
                    );

                if (result.Error != null)
                {
                    Console.WriteLine(result.Error.TechnicalError);
                    Console.WriteLine(result.Error.UserError);
                }

                Assert.Null(result.Error);

                foreach (var vehicleResult in result.VehicleResults)
                {
                    foreach (var productResult in vehicleResult.ProductResults)
                    {
                        if (productResult.Error != null)
                        {
                            Console.WriteLine(productResult.Error.TechnicalError);
                            Console.WriteLine(productResult.Error.UserError);
                        }
                        Assert.Null(productResult.Error);
                    }
                }
            }
        }
    }
}
