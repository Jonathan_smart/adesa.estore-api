using Raven.Json.Linq;
using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.VehicleEnrichmentTests.CapPricingVehicleEnricherTests
{
    public class Given_CapPricingEnriched_meta_data_is_set : TestBase
    {
        [Fact]
        public void Then_ShouldEnrich_should_return_false()
        {
            Assert.False(
                Enricher().ShouldEnrich(ValidVehicle, null, new RavenJObject { { MetaDataEnrichedKey, SystemTime.UtcNow } }));
        }
    }

    public class Given_vehicle_has_no_mileage_set : TestBase
    {
        [Fact]
        public void Then_ShouldEnrich_should_return_false()
        {
            var vehicle = ValidVehicle;
            vehicle.Mileage = null;

            Assert.False(
                Enricher().ShouldEnrich(vehicle, null, new RavenJObject()));
        }
    }

    public class Given_vehicle_has_no_CapId : TestBase
    {
        [Fact]
        public void Then_ShouldEnrich_should_return_false()
        {
            var vehicle = ValidVehicle;
            vehicle.CapId = null;

            Assert.False(
                Enricher().ShouldEnrich(vehicle, null, new RavenJObject()));
        }
    }

    public class Given_vehicle_has_no_VehicleType : TestBase
    {
        [Fact]
        public void Then_ShouldEnrich_should_return_false()
        {
            var vehicle = ValidVehicle;
            vehicle.VehicleType = null;

            Assert.False(
                Enricher().ShouldEnrich(vehicle, null, new RavenJObject()));
        }
    }

    public class Given_vehicle_has_no_registration_date : TestBase
    {
        [Fact]
        public void Then_ShouldEnrich_should_return_false()
        {
            var vehicle = ValidVehicle;
            vehicle.Registration = new Registration("AB10CDE");

            Assert.False(
                Enricher().ShouldEnrich(vehicle, null, new RavenJObject()));
        }
    }
}