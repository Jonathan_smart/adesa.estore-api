using System;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.ThirdParties;

namespace Sfs.EStore.Api.Tests.VehicleEnrichmentTests.CapPricingVehicleEnricherTests
{
    public abstract class TestBase
    {
        protected const string MetaDataEnrichedKey = "Cap-Pricing-Enriched";

        protected CapPricingVehicleEnricher Enricher(CapValuation command = null)
        {
            return new CapPricingVehicleEnricher(command ?? new FakeCapValuationCommand());
        }

        protected readonly Vehicle ValidVehicle = new Vehicle
                                                      {
                                                          Mileage = 12000,
                                                          Registration = new Registration("AB12CDE", SystemTime.UtcNow),
                                                          VehicleType = "CAR",
                                                          CapId = 14989
                                                      };
    }

    public class FakeCapValuationCommand : CapValuation
    {
        public FakeCapValuationCommand()
            : base(new Uri("http://example.com/"))
        {
        }

        public override void Execute()
        {
            
        }
    }
}