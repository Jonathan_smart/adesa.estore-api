﻿using System;
using System.Linq;
using Raven.Json.Linq;
using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.VehicleEnrichmentTests.CapPricingVehicleEnricherTests
{
    public class Given_CapPricingEnriched_meta_data_is_not_set : TestBase
    {
        [Fact]
        public void Then_ShouldEnrich_should_return_true()
        {
            Assert.True(
                Enricher().ShouldEnrich(ValidVehicle, null, new RavenJObject()));
        }
    }
}
