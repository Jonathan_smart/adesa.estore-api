﻿using System.Linq;
using Raven.Json.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.FurtherVehicleInfoServiceReference;
using Sfs.EStore.Api.Tests.Base;
using Sfs.EStore.Api.ThirdParties;
using Xunit;

namespace Sfs.EStore.Api.Tests.VehicleEnrichmentTests.CarwebVehicleEnricherTests
{
    public class Basic : TestBase
    {
        private const string RegistrationNumber = "AA00BBB";
        private readonly Vehicle _vehicle;
        private readonly VehicleData _vehicleData;

        public Basic()
        {
            _vehicle = new Vehicle { Registration = new Registration(RegistrationNumber) };
            _vehicleData = new VehicleData();

            var mockService = new CarwebMockService();
            mockService.Result.Vehicle.SpecItemGroups = new[]
                                                            {
                                                                new SpecItemGroupDTO
                                                                    {
                                                                        GroupName = "Test Group",
                                                                        Specifications = new[]
                                                                                             {
                                                                                                 new SpecItemDTO
                                                                                                     {
                                                                                                         Description =
                                                                                                             "Item 1"
                                                                                                     },
                                                                                                 new SpecItemDTO
                                                                                                     {
                                                                                                         Description =
                                                                                                             "Item 2"
                                                                                                     }
                                                                                             }
                                                                    }
                                                            };

            IVehicleEnricher enricher = new CarwebVehicleEnricher(mockService);
            enricher.Enrich(_vehicle, _vehicleData, new RavenJObject { { "Carweb-Equipment-Enriched", SystemTime.UtcNow } }).Wait();
        }

        [Fact]
        public void VehicleHasBeenFullyEnriched()
        {
            Assert.DoesNotThrow(() => EnsureAllPropertiesAreSet(_vehicle.Specification,
                                                                v => v.Engine.Description,
                                                                v => v.Equipment.Items));
        }

        [Fact]
        public void All_equipment_should_have_carweb_source()
        {
            const string exprectedSourceName = "carweb";

            Assert.True(_vehicle.Specification.Equipment.Items.Any());

            foreach (var equipmentItem in _vehicle.Specification.Equipment.Items)
            {
                Assert.Equal(exprectedSourceName, equipmentItem.Source);
            }
        }
    }
}
