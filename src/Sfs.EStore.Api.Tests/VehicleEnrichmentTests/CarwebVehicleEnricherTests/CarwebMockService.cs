using Sfs.EStore.Api.FurtherVehicleInfoServiceReference;
using Sfs.EStore.Api.Services;

namespace Sfs.EStore.Api.Tests.VehicleEnrichmentTests.CarwebVehicleEnricherTests
{
    public class CarwebMockService : ICarwebService
    {
        public CarwebMockService()
        {
            Result = CreateSpecificationResult();
        }
        public SpecificationResult GetSpecificationsFor(string registration)
        {
            Result.Vehicle.Registration = new RegistrationPayload {Value = registration};
            return Result;
        }

        public SpecificationResult Result { get; set; }

        private static SpecificationResult CreateSpecificationResult(string registration = null)
        {
            var specResult = new SpecificationResult
                                 {
                                     Success = true,
                                     Vehicle = new VehicleDTO
                                                   {
                                                       Registration = new RegistrationPayload { Value = registration ?? "AB12CDE" },
                                                       Specification = new SpecificationDTO
                                                                           {
                                                                               Dimensions = new DimensionsDTO
                                                                                                {
                                                                                                    HeightMm = 1450,
                                                                                                    WidthMm = 2200,
                                                                                                    LengthMm = 4560,
                                                                                                    KerbWeightMax = 2200,
                                                                                                    KerbWeightMin = 1100,
                                                                                                    WheelbaseForVans = 3500
                                                                                                },
                                                                               Engine = new EngineDTO
                                                                                            {
                                                                                                ArrangementOfCylinders = "IN LINE",
                                                                                                CapacityCc = 1998,
                                                                                                NumberOfCylinders = 4,
                                                                                                NumberOfValvesPerCylinder = 4
                                                                                            },
                                                                               Economy = new EconomyDTO
                                                                                             {
                                                                                                 FuelConsumptionCombinedLtrPer100Km = 4.2m,
                                                                                                 FuelConsumptionCombinedMpg = 45.2m,
                                                                                                 FuelConsumptionExtraUrbanLtrPer100Km = 3.1m,
                                                                                                 FuelConsumptionExtraUrbanMpg = 58m,
                                                                                                 FuelConsumptionUrbanColdLtrPer100Km = 5.4m,
                                                                                                 FuelConsumptionUrbanColdMpg = 32m
                                                                                             },
                                                                               Environment = new EnvironmentDTO
                                                                                                 {
                                                                                                     Co2 = 5.4m
                                                                                                 },
                                                                               Performance = new PerformanceDTO
                                                                                                 {
                                                                                                     AccelerationTo100KphInSecs = 8.3m,
                                                                                                     MaximumPowerAtRpm = 3500m,
                                                                                                     MaximumPowerInBhp = 131.2m,
                                                                                                     MaximumPowerInKw = 23m,
                                                                                                     MaximumSpeedMph = 156m,
                                                                                                     MaximumTorqueAtRpm = 55.5m,
                                                                                                     MaximumTorqueLbFt = 23.67m,
                                                                                                     MaximumTorqueNm = 123m
                                                                                                 }
                                                                           }
                                                   }
                                 };
            return specResult;
        }
    }
}