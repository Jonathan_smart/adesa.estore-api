using System;
using System.Linq;
using Raven.Json.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.FurtherVehicleInfoServiceReference;
using Sfs.EStore.Api.Tests.Base;
using Sfs.EStore.Api.ThirdParties;
using Xunit;

namespace Sfs.EStore.Api.Tests.VehicleEnrichmentTests.CarwebVehicleEnricherTests
{
    public class Given_solid_paint_is_specified_in_the_results : TestBase
    {
        private readonly Vehicle _vehicle;

        public Given_solid_paint_is_specified_in_the_results()
        {
            _vehicle = new Vehicle { Registration = new Registration("AB12CDE") };
            var vehicleData = new VehicleData();

            var mockService = new CarwebMockService();
            mockService.Result.Vehicle.SpecItemGroups =
                new[]
                    {
                        new SpecItemGroupDTO
                            {
                                GroupName = "Interior Equipment",
                                Specifications = new[]
                                                     {
                                                         new SpecItemDTO {Description = "Solid Paint"}
                                                     }
                            },
                        new SpecItemGroupDTO
                            {
                                GroupName = "Paint Or Trim And Upholstery",
                                Specifications = new[]
                                                     {
                                                         new SpecItemDTO {Description = "Solid Paint"}
                                                     }
                            }
                    };

            var enricher = new CarwebVehicleEnricher(mockService);
            enricher.Enrich(_vehicle, vehicleData, new RavenJObject()).Wait();
        }

        [Fact]
        public void Then_Should_not_set_interior_equipment_on_the_vehicle()
        {
            Assert.False(_vehicle.Specification.Equipment.Items
                             .Any(x =>
                                  x.Category.Equals("interior equipment", StringComparison.InvariantCultureIgnoreCase) &&
                                  x.Name.Equals("solid paint", StringComparison.InvariantCultureIgnoreCase)));
        }

        [Fact]
        public void Then_Should_not_set_paint_or_trim_and_upholstery_on_the_vehicle()
        {
            Assert.False(_vehicle.Specification.Equipment.Items
                             .Any(x =>
                                  x.Category.Equals("Paint Or Trim And Upholstery", StringComparison.InvariantCultureIgnoreCase) &&
                                  x.Name.Equals("solid paint", StringComparison.InvariantCultureIgnoreCase)));
        }
    }
}