﻿using System;
using Xunit;

namespace Sfs.EStore.Api.Tests.TimeTests
{
    public class TimeTests
    {
        [Fact]
        public void TestUtc()
        {
            DateTime today = DateTime.Now;
            DateTime todayUTC = DateTime.UtcNow;

            Assert.NotEqual(today, todayUTC);
        }
        private static DateTime ConvertUtc(DateTime date)
        {
            TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            if (!tz.IsDaylightSavingTime(date)) return date;

            DateTime convertedDateTime = TimeZoneInfo.ConvertTimeFromUtc(date, tz);
            return convertedDateTime;
        }

        [Fact]
        public void TestUtcNewDates()
        {
            DateTime todayUtc = DateTime.UtcNow.AddHours(2);
            DateTime today = DateTime.Now.AddHours(2);

            Assert.Equal(today.TimeOfDay, ConvertUtc(todayUtc).TimeOfDay);
        }
        [Fact]
        public void NotDaylightSavingsTime()
        {
            DateTime todayUtc = DateTime.UtcNow.AddMonths(-3);
            DateTime today = DateTime.UtcNow.AddMonths(-3);

            Assert.Equal(today.TimeOfDay, ConvertUtc(todayUtc).TimeOfDay);
        }
        [Fact]
        public void UtcNotEqual()
        {
            DateTime todayUtc = DateTime.UtcNow.AddHours(2);
            DateTime today = DateTime.Now.AddHours(2);


            Assert.NotEqual(today.TimeOfDay, todayUtc.TimeOfDay);
        }
    }
}
