using System;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using Raven.Client;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Tests.Extensions;

namespace Sfs.EStore.Api.Tests
{
    public class WebRequestContext<TController> : IDisposable
        where TController : RavenEStoreController
    {
        private readonly IDocumentStore _store;
        private readonly IAsyncDocumentSession _session;
        private readonly TController _controller;

        public WebRequestContext(IDocumentStore store, Func<TController> controllerFactory)
        {
            _store = store;
            _session = _store.OpenAsyncSession();

            _controller = controllerFactory();
            _controller.Session = _session;
            _controller.Request = new HttpRequestMessage(HttpMethod.Get, "http://localhost/fake/uri")
                .ConfigureJsonFormatter();
        }

        public IAsyncDocumentSession Session { get { return _session; } }
        public TController Controller { get { return _controller; } }

        public void SetRequest(HttpRequestMessage request)
        {
            _controller.Request = request;
        }

        public void SetAuthenticatedPrincipal(string accessKeyId, string[] roles = null, string endUsername = null)
        {
            MembershipUserPrincipal endUserPrincial = null;
            if (!string.IsNullOrWhiteSpace(endUsername))
                endUserPrincial =
                    new MembershipUserPrincipal(new MembershipUserIdentity(MembershipUserInfo.CreateFrom(endUsername, endUsername)));

            SetAuthenticatedPrincipal(new ApiApplicationPrincipal(
                                          new AccountIdentity(new Account {AccessKeyId = accessKeyId}),
                                          endUserPrincial));
        }

        public void SetAuthenticatedPrincipal(IPrincipal principal)
        {
            Thread.CurrentPrincipal = principal;
        }

        public void Dispose()
        {
            if (_session == null) return;

            _session.SaveChangesAsync().Wait();
            _session.Dispose();
        }
    }
}