﻿using System;
using System.Linq;
using System.Net.Http;
using Raven.Client;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Raven.Indexes;
using Sfs.EStore.Api.Raven.Transformers;
using Sfs.EStore.Api.Tests.Base;
using Xunit;

namespace Sfs.EStore.Api.Tests.UsersAlsoViewedTests
{
    public class ControllerTests : DocumentStoreTestBase
    {
        private readonly Func<UsersAlsoViewedController> _controllerFactory = () => new UsersAlsoViewedController(); 
        private readonly static string SessionId = Guid.NewGuid().ToString();

        public ControllerTests()
        {
            RegisterIndex(new SessionInteractions_UsersAlsoViewed());
            RegisterTransform(new LotIdToListingSummary());
        }

        [Fact]
        public void CanQueryControllerForNoResultsWhereRelatedVehicleIsNotActive()
        {
            Lot lot;
            using (var session = Store.OpenSession())
            {
                var vehicle = CreateVehicle(session, "VEHICLE1");
                var vehicle2 = CreateVehicle(session, "VEHICLE2");
                lot = CreateLot(session, vehicle);
                var lot2 = CreateLot(session, vehicle2, LotStatusTypes.Cancelled);
                CreateSessionInteraction(session, new[] { lot, lot2 }.Select(x => new SessionInteractions.ViewedLot { LotId = x.Id, Date = DateTime.Now }).ToArray());
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var ctx = new WebRequestContext<UsersAlsoViewedController>(Store, _controllerFactory))
            {
                var model = new UsersAlsoViewedController.UsersAlsoViewedGetModel
                    {
                        LotNumber = lot.LotNumber,
                        Top = 100
                    };
                var results = ctx.Controller.Get(model).Result
                    .Content.ReadAsAsync<ListingSummaryViewModel[]>().Result;
                Assert.Empty(results);
            }
        }

        [Fact]
        public void CanQueryControllerForNoResultsWhereThisVehicleIsNotActive()
        {
            Lot lot;
            using (var session = Store.OpenSession())
            {
                var vehicle = CreateVehicle(session, "VEHICLE1");
                var vehicle2 = CreateVehicle(session, "VEHICLE2");
                lot = CreateLot(session, vehicle, LotStatusTypes.Cancelled);
                var lot2 = CreateLot(session, vehicle2);
                CreateSessionInteraction(session, new[] { lot, lot2 }.Select(x => new SessionInteractions.ViewedLot { LotId = x.Id, Date = DateTime.Now }).ToArray());
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var ctx = new WebRequestContext<UsersAlsoViewedController>(Store, _controllerFactory))
            {
                var model = new UsersAlsoViewedController.UsersAlsoViewedGetModel
                {
                    LotNumber = lot.LotNumber,
                    Top = 100
                };
                var results = ctx.Controller.Get(model).Result
                    .Content.ReadAsAsync<ListingSummaryViewModel[]>().Result;
                Assert.Empty(results);
            }
        }

        private SessionInteractions CreateLot_Vehicle_SessionInteraction(IDocumentSession session)
        {
            var vehicle = CreateVehicle(session);
            var lot = CreateLot(session, vehicle);
            var sessionInteraction = CreateSessionInteraction(session, new SessionInteractions.ViewedLot { LotId = lot.Id, Date = DateTime.UtcNow });

            return sessionInteraction;
        }

        private SessionInteractions CreateSessionInteraction(IDocumentSession session, params SessionInteractions.ViewedLot[] lotIds)
        {
            var sessionInteraction = new SessionInteractions { Username = MembershipUsername, SessionId = SessionId, LotsViewed = lotIds };
            session.Store(sessionInteraction);
            return sessionInteraction;
        }

        private static Lot CreateLot(IDocumentSession session, Vehicle vehicle, LotStatusTypes lotStatus = LotStatusTypes.Active, string[] limitedToRoles = null)
        {
            var lot = new Lot { Title = "LotTitle", Details = "LotDetails", Status = lotStatus, BuyItNowPrice = Money.From(10000m), VisibilityLimitedTo = limitedToRoles };
            lot.LinkToVehicle(vehicle);
            session.Store(lot);
            return lot;
        }

        private static Vehicle CreateVehicle(IDocumentSession session, string reg = "AA00ZZZ")
        {
            var vehicle = new Vehicle { Registration = new Registration(reg, DateTime.Now.Date) };
            session.Store(vehicle);
            return vehicle;
        }
    }
}
