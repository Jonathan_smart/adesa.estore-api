﻿using System;
using System.Linq;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Raven.Indexes;
using Sfs.EStore.Api.Raven.Transformers;
using Sfs.EStore.Api.Tests.Base;
using Xunit;

namespace Sfs.EStore.Api.Tests.UsersAlsoViewedTests
{
    public class IndexTests : DocumentStoreTestBase
    {
        private static readonly string SessionId = Guid.NewGuid().ToString();

        public IndexTests()
        {
            RegisterIndex(new SessionInteractions_UsersAlsoViewed());
            RegisterTransform(new LotIdToListingSummary());
        }

        [Fact]
        public void CanQueryIndexForNoResultsWhenOnlyOneSessionWithSingleVehicle()
        {
            using (var session = Store.OpenSession())
            {
                CreateLotVehicleSessionInteraction(session);
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var session = Store.OpenSession())
            {
                var results = session.Query<SessionInteractions_UsersAlsoViewed.ReduceResult, SessionInteractions_UsersAlsoViewed>()
                    .TransformWith<LotIdToListingSummary, ListingSummaryViewModel>()
                    .ToList();
                Assert.Empty(results);
            }
        }

        //public void CanQueryIndexForVehicleWithRelatedVehices()
        //{
        //    Lot lot, lot2, lot3;
        //    using (var session = Store.OpenSession())
        //    {
        //        var vehicle = CreateVehicle(session, "VEHICLE1");
        //        var vehicle2 = CreateVehicle(session, "VEHICLE2");
        //        var vehicle3 = CreateVehicle(session, "VEHICLE3");
        //        lot = CreateLot(session, vehicle);
        //        lot2 = CreateLot(session, vehicle2);
        //        lot3 = CreateLot(session, vehicle3);
        //        CreateSessionInteraction(session, new[] { lot, lot2, lot3, lot2, lot3, lot3 }.Select(x => new SessionInteractions.ViewedLot { LotId = x.Id, Date = DateTime.Now }).ToArray());
        //        session.SaveChanges();
        //    }

        //    WaitForIndexingAndEnsureNoStoreErrors();

        //    using (var session = Store.OpenSession())
        //    {
        //        var query = session.Query<SessionInteractions_UsersAlsoViewed.ReduceResult, SessionInteractions_UsersAlsoViewed>()
        //            .Where(x => x.ViewedLotId == lot.Id)
        //            .TransformWith<LotIdToListingSummary, ListingSummaryViewModel>();
        //        var results = query.ToList();
        //        Assert.Equal(2, results.Count);
        //        Assert.Equal(lot3.LotNumber, results.First().ListingId);
        //    }
        //}

        private void CreateLotVehicleSessionInteraction(IDocumentSession session)
        {
            var vehicle = CreateVehicle(session);
            var lot = CreateLot(session, vehicle);
            CreateSessionInteraction(session, new SessionInteractions.ViewedLot { LotId = lot.Id, Date = DateTime.UtcNow });
        }

        private void CreateSessionInteraction(IDocumentSession session, params SessionInteractions.ViewedLot[] lotIds)
        {
            var sessionInteraction = new SessionInteractions { Username = MembershipUsername, SessionId = SessionId, LotsViewed = lotIds };
            session.Store(sessionInteraction);
        }

        private static Lot CreateLot(IDocumentSession session, Vehicle vehicle, LotStatusTypes lotStatus = LotStatusTypes.Active, string[] limitedToRoles = null)
        {
            var lot = new Lot { Title = "LotTitle", Details = "LotDetails", Status = lotStatus, BuyItNowPrice = Money.From(10000m), VisibilityLimitedTo = limitedToRoles };
            lot.LinkToVehicle(vehicle);
            session.Store(lot);
            return lot;
        }

        private static Vehicle CreateVehicle(IDocumentSession session, string reg = "AA00ZZZ")
        {
            var vehicle = new Vehicle { Registration = new Registration(reg, DateTime.Now.Date) };
            session.Store(vehicle);
            return vehicle;
        }
    }
}
