﻿using System;
using System.Linq;
using System.Net.Http;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Raven.Indexes;
using Sfs.EStore.Api.Raven.Transformers;
using Sfs.EStore.Api.Tests.Base;
using Xunit;

namespace Sfs.EStore.Api.Tests.MostViewedControllerTests
{
    public class CanQueryController : DocumentStoreTestBase, IDisposable
    {
        private readonly string _sessionId = Guid.NewGuid().ToString();

        private readonly Func<MostViewedController> _controllerFactory = () => new MostViewedController(); 
        public CanQueryController()
        {
            SystemTime.UtcDateTime = () => new DateTime(2013 ,01 ,01);
            ApplyCurrentPrincipal();

            RegisterIndex(new ListingViewCount_Search());
            RegisterTransform(new LotIdToListingSummary());
        }

        public void Dispose()
        {
            SystemTime.UtcDateTime = null;
        }

        [Fact]
        public void ForNoMostViewedVehicles()
        {
            using (var ctx = new WebRequestContext<MostViewedController>(Store, _controllerFactory))
            {
                var model = new MostViewedController.MostViewedParams();
                var results = ctx.Controller.Post(model)
                    .Result.Content
                    .ReadAsAsync<ListingSummaryViewModel[]>().Result;
                Assert.NotNull(results);
                Assert.Empty(results);
            }
        }

        [Fact]
        public void ForSingleMostViewedVehicle()
        {
            using (var session = Store.OpenSession())
            {
                var vehicle = new Vehicle { Registration = new Registration("AA00ZZZ", DateTime.Now.Date) };
                session.Store(vehicle);
                var lot = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m) };
                lot.LinkToVehicle(vehicle);
                session.Store(lot);
                session.Store(new ListingViewCount(lot.Id));
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var ctx = new WebRequestContext<MostViewedController>(Store, _controllerFactory))
            {
                var model = new MostViewedController.MostViewedParams { Top = "4" };
                var results = ctx.Controller.Post(model)
                    .Result.Content
                    .ReadAsAsync<ListingSummaryViewModel[]>().Result;
                Assert.NotNull(results);
                Assert.Single(results);
            }
        }

        [Fact]
        public void ForSingleMostViewedCar()
        {
            using (var session = Store.OpenSession())
            {
                var car = new Vehicle { Registration = new Registration("AA00ZZZ", DateTime.Now.Date), VehicleType = "CAR"};
                session.Store(car);
                var carLot = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m) };
                carLot.LinkToVehicle(car);
                session.Store(carLot);
                var van = new Vehicle { Registration = new Registration("AA00ZZZ", DateTime.Now.Date), VehicleType = "VAN" };
                session.Store(van);
                var vanLot = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m) };
                vanLot.LinkToVehicle(van);
                session.Store(vanLot);
                var vanLot2 = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m) };
                vanLot2.LinkToVehicle(van);
                session.Store(vanLot2);
                session.Store(new ListingViewCount(carLot.Id));
                session.Store(new ListingViewCount(vanLot.Id));
                session.Store(new ListingViewCount(vanLot2.Id));
                session.SaveChanges();
            }

            WaitForIndexing();

            using (var ctx = new WebRequestContext<MostViewedController>(Store, _controllerFactory))
            {
                var model = new MostViewedController.MostViewedParams { Top = "4", VehicleType = "CAR" };
                var results = ctx.Controller.Post(model)
                    .Result.Content
                    .ReadAsAsync<ListingSummaryViewModel[]>().Result;
                Assert.NotNull(results);
                Assert.Single(results);
            }
        }

        [Fact]
        public void ForMostViewedVehicleWithTags()
        {
            using (var session = Store.OpenSession())
            {
                var vehicle = new Vehicle { Registration = new Registration("AA00ZZZ", DateTime.Now.Date) };
                session.Store(vehicle);
                var lot = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m), VisibilityLimitedTo = new[] { "tag1" }};
                lot.LinkToVehicle(vehicle);
                session.Store(lot);
                var lot2 = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m), VisibilityLimitedTo = new[] { "tag2" } };
                lot2.LinkToVehicle(vehicle);
                session.Store(lot2);
                var lot3 = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m), VisibilityLimitedTo = new[] { "tag3" } };
                lot3.LinkToVehicle(vehicle);
                session.Store(lot3);
                session.Store(new ListingViewCount(lot.Id));
                session.Store(new ListingViewCount(lot2.Id));
                session.Store(new ListingViewCount(lot3.Id));
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var ctx = new WebRequestContext<MostViewedController>(Store, _controllerFactory))
            {
                var model = new MostViewedController.MostViewedParams { Top = "4", LimitToRoles = new[] { "tag1" } };
                var results = ctx.Controller.Post(model)
                    .Result.Content
                    .ReadAsAsync<ListingSummaryViewModel[]>().Result;
                Assert.NotNull(results);
                Assert.Single(results);
            }
        }

        [Fact]
        public void WhichShouldReturnOnlyActiveLots()
        {
            using (var session = Store.OpenSession())
            {
                var vehicle = new Vehicle { Registration = new Registration("AA00ZZZ", DateTime.Now.Date) };
                session.Store(vehicle);
                var activeLot = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m) };
                activeLot.LinkToVehicle(vehicle);
                session.Store(activeLot);
                var activeLot2 = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m) };
                activeLot2.LinkToVehicle(vehicle);
                session.Store(activeLot2);
                var cancelledLot = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Cancelled, BuyItNowPrice = Money.From(10000m) };
                cancelledLot.LinkToVehicle(vehicle);
                session.Store(cancelledLot);
                session.Store(new ListingViewCount(activeLot.Id));
                session.Store(new ListingViewCount(activeLot2.Id));
                session.Store(new ListingViewCount(cancelledLot.Id));
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var ctx = new WebRequestContext<MostViewedController>(Store, _controllerFactory))
            {
                var model = new MostViewedController.MostViewedParams { Top = "4" };
                var results = ctx.Controller.Post(model)
                    .Result.Content
                    .ReadAsAsync<ListingSummaryViewModel[]>().Result;
                Assert.NotNull(results);
                Assert.Equal(2, results.Count());
            }
        }

        [Fact]
        public void WhichShouldReturnOnlyWithinTimespanResults()
        {
            var now = new DateTime(2014, 05, 30);
            Lot activeLot3;
            using (var session = Store.OpenSession())
            {
                var vehicle = new Vehicle { Registration = new Registration("AA00ZZZ", DateTime.Now.Date) };
                session.Store(vehicle);
                var activeLot1 = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m) };
                activeLot1.LinkToVehicle(vehicle);
                session.Store(activeLot1);
                var activeLot2 = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m) };
                activeLot2.LinkToVehicle(vehicle);
                session.Store(activeLot2);
                activeLot3 = new Lot { Title = "LotTitle", Details = "LotDetails", Status = LotStatusTypes.Active, BuyItNowPrice = Money.From(10000m) };
                activeLot3.LinkToVehicle(vehicle);
                session.Store(activeLot3);

                SystemTime.UtcDateTime = () => now.AddDays(-1);
                session.Store(new ListingViewCount(activeLot1.Id, 4));
                SystemTime.UtcDateTime = () => now;
                session.Store(new ListingViewCount(activeLot3.Id, 4));
                SystemTime.UtcDateTime = () => now.AddDays(-4);
                session.Store(new ListingViewCount(activeLot2.Id, 6));

                SystemTime.UtcDateTime = () => now;
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var ctx = new WebRequestContext<MostViewedController>(Store, _controllerFactory))
            {
                var model = new MostViewedController.MostViewedParams { Top = "4", ViewingAge = TimeSpan.FromDays(3) };
                var results = ctx.Controller.Post(model)
                    .Result.Content
                    .ReadAsAsync<ListingSummaryViewModel[]>().Result;
                Assert.NotNull(results);
                Assert.Equal(2, results.Count());
                Assert.Equal(activeLot3.LotNumber, results.First().ListingId);
            }
        }
    }
}
