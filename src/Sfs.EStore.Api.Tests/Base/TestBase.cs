using System;
using System.Linq;
using System.Linq.Expressions;

namespace Sfs.EStore.Api.Tests.Base
{
    public abstract class TestBase
    {
        protected static void EnsureAllPropertiesAreSet<T>(T obj, params Expression<Func<T, object>>[] expression)
        {
            var ignores = expression.Select(x => string.Join(".", x.Body.ToString().Split('.').Skip(1))).ToArray();
            EnsureAllPropertiesAreSet(obj, ignores);
        }

        protected static void EnsureAllPropertiesAreSet(object obj, params string[] ignoreProperties)
        {
            EnsureAllPropertiesAreSet(obj, null, ignoreProperties);
        }

        private static void EnsureAllPropertiesAreSet(object obj, string parentPath = null, params string[] ignoreProperties)
        {
            var assemblyName = obj.GetType().Assembly.FullName;
            if (assemblyName.Contains("System") || assemblyName.Contains("mscorlib"))
                return;
            foreach (var prop in obj.GetType().GetProperties())
            {
                var currentPath = string.Join(".", parentPath, prop.Name).Trim('.');
                if (ignoreProperties.Any(x => x == currentPath))
                    continue;
                var value = prop.GetValue(obj, null);
                if (value == null)
                    throw new NullReferenceException(string.Format("property \"{0}\" is null", currentPath));
                EnsureAllPropertiesAreSet(value, currentPath, ignoreProperties);
            }
        }
    }
}