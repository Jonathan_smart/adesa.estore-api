using System;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using Raven.Client;
using Raven.Client.Embedded;
using Raven.Client.Indexes;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Entities;
using Environment = System.Environment;

namespace Sfs.EStore.Api.Tests.Base
{
    public abstract class DocumentStoreTestBase : TestBase
    {
        private readonly IDocumentStore _store;

        protected DocumentStoreTestBase(bool initialize)
        {
            _store = new EmbeddableDocumentStore
                {
                    RunInMemory = true
                };
            if (initialize)
                _store.Initialize();
        }

        protected DocumentStoreTestBase() : this(true)
        {
        }

        protected void RegisterIndex<TDocument, TReduceResult>(AbstractIndexCreationTask<TDocument, TReduceResult> index)
        {
            index.Execute(_store);
        }

        protected void RegisterTransform<TFrom>(AbstractTransformerCreationTask<TFrom> transformer)
        {
            transformer.Execute(_store);
        }

        protected void WaitForIndexing()
        {
            WaitForIndexing(_store);
        }

        protected void WaitForIndexing(IDocumentStore store, string db = null)
        {
            var databaseCommands = store.DatabaseCommands;
            if (db != null)
                databaseCommands = databaseCommands.ForDatabase(db);
            SpinWait.SpinUntil(() => databaseCommands.GetStatistics().StaleIndexes.Length == 0);
        }

        protected void EnsureNoStoreErrors()
        {
            EnsureNoStoreErrors(Store);
        }

        protected void EnsureNoStoreErrors(IDocumentStore store, string db = null)
        {
            var commands = db == null ? store.DatabaseCommands : store.DatabaseCommands.ForDatabase(db);
            var ravenErrors = commands.GetStatistics().Errors;
            if (ravenErrors.Length > 0)
                throw new InvalidOperationException("Raven has errors: " + string.Join(Environment.NewLine, ravenErrors.Select(x => x.Error)));
        }

        protected void WaitForIndexingAndEnsureNoStoreErrors()
        {
            WaitForIndexing();
            EnsureNoStoreErrors();
        }

        public IDocumentStore Store { get { return _store; } }

        private static string _membershipUsername = "testuser";
        protected static string MembershipUsername
        {
            get { return _membershipUsername; }
            set { _membershipUsername = value; }
        }

        private static IPrincipal _principalBackup;

        protected static void ApplyCurrentPrincipal()
        {
            ApplyCurrentPrincipal(MembershipUsername);
        }

        protected static void ApplyCurrentPrincipal(string membershipUsername)
        {
            Thread.CurrentPrincipal =
                new ApiApplicationPrincipal(
                    new AccountIdentity(new Account()),
                    new MembershipUserPrincipal(
                        new MembershipUserIdentity(MembershipUserInfo.CreateFrom(membershipUsername, membershipUsername, new string[0]))));
        }

        protected void ApplyTemporaryCurrentPrincipal(string username)
        {
            if (Thread.CurrentPrincipal == null)
                throw new InvalidOperationException("Cannot apply temporary principal when current is null");
            if (_principalBackup != null)
                throw new InvalidOperationException("Cannot apply temporary principal when one is already set");

            _principalBackup = Thread.CurrentPrincipal;
            ApplyCurrentPrincipal(username);
        }

        protected void RevertTemporaryCurrentPrincipal()
        {
            if (_principalBackup ==  null)
                throw new InvalidOperationException("Temporary principal has not been applied");

            Thread.CurrentPrincipal = _principalBackup;
            _principalBackup = null;
        }
    }
}