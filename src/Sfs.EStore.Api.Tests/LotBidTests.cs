﻿using System;
using System.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Xunit;

namespace Sfs.EStore.Api.Tests
{
    public class LotBidTests
    {
        //[Fact]
        //public void ShouldNotAcceptBidIfEndDateIsInThePast()
        //{
        //    var endDate = SystemTime.UtcNow;
        //    var lot = CreateLot();
        //    lot.Activate();
        //    lot.EndDate = endDate;
        //    SystemTime.UtcDateTime = () => endDate.AddSeconds(1);

        //    var exception = Assert.Throws<BidsDisabledException>(() => lot.TakeBid(Money.From(100), "foo",lot,null,""));
        //    Assert.Equal(lot.Id, exception.LotId);
        //}

        //[Fact]
        //public void FirstBidAtStartingPriceShouldBeAccepted()
        //{
        //    var lot = CreateLot();
        //    lot.Activate();

        //    lot.TakeBid(Money.From(100), "foo",lot,null,"");

        //    Assert.Equal(Money.From(100), lot.CurrentOffer.Amount);
        //    Assert.Equal(1, lot.Bidding.Bids.Count());
        //}

        //[Fact]
        //public void FirstBidOverStartingPriceShouldBeAcceptedAtStartingPrice()
        //{
        //    var lot = CreateLot();
        //    lot.Activate();

        //    lot.TakeBid(Money.From(150), "foo",lot,null,"");

        //    Assert.Equal(Money.From(100), lot.CurrentOffer.Amount);
        //    Assert.Equal(1, lot.Bidding.Bids.Count());
        //}

        //[Fact]
        //public void SecondBidAboveMinimumRequiredBidShouldBeAccepted()
        //{
        //    var lot = CreateLot();
        //    lot.Activate();

        //    lot.TakeBid(Money.From(100), "foo", lot, null, "");
        //    lot.TakeBid(Money.From(110), "bar", lot, null, "");

        //    Assert.Equal(Money.From(110), lot.CurrentOffer.Amount);
        //    Assert.Equal("bar", lot.CurrentOffer.UserId);
        //    Assert.Equal(2, lot.Bidding.Bids.Count());
        //}

        //[Fact]
        //public void SecondBidBelowMinimumAmountRequiredShouldNotAcceptBid()
        //{
        //    var lot = CreateLot();
        //    lot.Activate();

        //    lot.TakeBid(Money.From(100), "foo", lot, null, "");
        //    lot.TakeBid(Money.From(105), "bar", lot, null, "");

        //    Assert.Equal(Money.From(100), lot.CurrentOffer.Amount);
        //    Assert.Equal("foo", lot.CurrentOffer.UserId);
        //    Assert.Equal(1, lot.Bidding.Bids.Count());
        //}

        //[Fact]
        //public void SecondBidShouldTakeMinimumAmountRequiredToBeatFirstBid()
        //{
        //    var lot = CreateLot();
        //    lot.Activate();

        //    lot.TakeBid(Money.From(100), "foo", lot, null, "");
        //    lot.TakeBid(Money.From(150), "bar", lot, null, "");

        //    Assert.Equal(Money.From(110), lot.CurrentOffer.Amount);
        //    Assert.Equal("bar", lot.CurrentOffer.UserId);
        //    Assert.Equal(2, lot.Bidding.Bids.Count());
        //}

        //[Fact]
        //public void SecondBidAboveMinimumBidRequiredShouldPushUpTheFirstBid()
        //{
        //    var lot = CreateLot();
        //    lot.Activate();

        //    lot.TakeBid(Money.From(140), "foo", lot, null, "");
        //    lot.TakeBid(Money.From(150), "bar", lot, null, "");

        //    Assert.Equal(Money.From(150), lot.CurrentOffer.Amount);
        //    Assert.Equal("bar", lot.CurrentOffer.UserId);
        //    Assert.Equal(3, lot.Bidding.Bids.Count());
        //}

        //[Fact]
        //public void ExistingBidDoesNotCoverNewBidPlusIncrement_ShouldTakeExistingBidLimit()
        //{
        //    var lot = CreateLot();
        //    lot.Activate();

        //    lot.TakeBid(Money.From(100), "foo", lot, null, "");
        //    lot.TakeBid(Money.From(155), "bar", lot, null, "");
        //    lot.TakeBid(Money.From(150), "foo", lot, null, "");

        //    Assert.Equal(Money.From(155), lot.CurrentOffer.Amount);
        //    Assert.Equal("bar", lot.CurrentOffer.UserId);
        //    Assert.Equal(4, lot.Bidding.Bids.Count());
        //}

        //[Fact]
        //public void RealWorldTest()
        //{
        //    var lot = CreateLot();
        //    lot.SetBidding(startingPrice: Money.From(6345), bidIncrements: Money.From(25));
        //    lot.BuyItNowPrice = Money.From(16920);

        //    var initialBidDate = new DateTime(2013, 1, 1);
        //    SystemTime.UtcDateTime = () => initialBidDate;

        //    lot.Activate();

        //    lot.TakeBid(Money.From(6345), "testuser", lot, null, "");
        //    AssertLotBid(lot, expectedCurrentOfferAmount: 6345,
        //                 expectedCurrentOfferUsername: "testuser");

        //    SystemTime.UtcDateTime = () => initialBidDate.AddMinutes(1);
        //    lot.TakeBid(Money.From(6450), "brownd", lot, null, "");
        //    AssertLotBid(lot, expectedCurrentOfferAmount: 6370,
        //                 expectedCurrentOfferUsername: "brownd");

        //    SystemTime.UtcDateTime = () => initialBidDate.AddMinutes(2);
        //    lot.TakeBid(Money.From(6395), "testuser", lot, null, "");
        //    AssertLotBid(lot, expectedCurrentOfferAmount: 6420,
        //                 expectedCurrentOfferUsername: "brownd");

        //    SystemTime.UtcDateTime = () => initialBidDate.AddMinutes(3);
        //    lot.TakeBid(Money.From(6450), "testuser", lot, null, "");
        //    AssertLotBid(lot, expectedCurrentOfferAmount: 6450,
        //                 expectedCurrentOfferUsername: "brownd");

        //    SystemTime.UtcDateTime = () => initialBidDate.AddMinutes(4);
        //    lot.TakeBid(Money.From(6510), "nixon", lot, null, "");
        //    AssertLotBid(lot, expectedCurrentOfferAmount: 6475,
        //                 expectedCurrentOfferUsername: "nixon");

        //    SystemTime.UtcDateTime = () => initialBidDate.AddMinutes(5);
        //    lot.TakeBid(Money.From(6500), "testuser", lot, null, "");
        //    AssertLotBid(lot, expectedCurrentOfferAmount: 6510,
        //                 expectedCurrentOfferUsername: "nixon");
        //}

        private static void AssertLotBid(Lot lot, decimal expectedCurrentOfferAmount, string expectedCurrentOfferUsername, int? expectedNumberOfBids = null)
        {
            Assert.Equal(Money.From(expectedCurrentOfferAmount), lot.CurrentOffer.Amount);
            Assert.Equal(expectedCurrentOfferUsername, lot.CurrentOffer.UserId);
            if (expectedNumberOfBids.HasValue)
                Assert.Equal(expectedNumberOfBids, lot.Bidding.Bids.Count());
        }

        private static Lot CreateLot()
        {
            var lot = new Lot
                          {
                              StartDate = SystemTime.UtcNow,
                              EndDate = SystemTime.UtcNow.AddDays(7)
                          };
            lot.SetBidding(Money.From(100), Money.From(10));

            return lot;
        }
    }
}
