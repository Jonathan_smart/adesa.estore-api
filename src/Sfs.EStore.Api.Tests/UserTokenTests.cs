﻿using System;
using Sfs.EStore.Api.Authentication;
using Xunit;

namespace Sfs.EStore.Api.Tests
{
    public class UserTokenTests
    {
        [Fact]
        public void CanEncrypt()
        {
            var username = "michael.payton@smartfleetsolutions.com";
            var encrypted = UserToken.Encrypt(new Token(username));
            Console.WriteLine(encrypted);
        }
    }
}
