﻿using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Xunit;

namespace Sfs.EStore.Api.Tests
{
    public class UrlImageTests
    {
        [Fact]
        public void Should_generate_correct_raw_url()
        {
            const string imageUrl = "https://www.mfldirect.co.uk/storefront/renderImage.image?theme=mfldirect&imageName=YH60KYX.JPG";
            const string expectedUrl = imageUrl + "&width=150&height=110";

            var url = ImageUrl.Create(new ImageModel
            {
                Url = imageUrl
            }, width: 150, height: 110);

            Assert.Equal(expectedUrl, url);
        }

        [Fact]
        public void Should_generate_correct_cropped_url()
        {
            const string imageUrl = "https://www.mfldirect.co.uk/storefront/renderImage.image?theme=mfldirect&imageName=YH60KYX.JPG";
            const string expectedUrl = imageUrl;

            var url = ImageUrl.Create(new ImageModel
            {
                Url = imageUrl
            });

            Assert.Equal(expectedUrl, url);
        }
    }

    public class CloudFilesImageTests
    {
        const string ActualContainerName = "TestContainer";
        const string ActualImageName = "image-name.jpg";

        public CloudFilesImageTests()
        {
            ImageUrl.CloudFilesImageProxyUrl = "http://example.com/";
        }

        [Fact]
        public void Should_generate_correct_raw_url()
        {
            var expectedUrl = string.Format("{0}{1}/-/{2}", ImageUrl.CloudFilesImageProxyUrl, ActualContainerName, ActualImageName);

            var url = ImageUrl.Create(new ImageModel
                                          {
                                              Container = ActualContainerName,
                                              Name = ActualImageName
                                          });

            Assert.Equal(expectedUrl, url);
        }

        [Fact]
        public void Should_generate_correct_cropped_url()
        {
            var expectedUrl = string.Format("{0}{1}/c_lpad,b_white,w_150,h_100/{2}", ImageUrl.CloudFilesImageProxyUrl, ActualContainerName, ActualImageName);

            var url = ImageUrl.Create(new ImageModel
                                          {
                                              Container = ActualContainerName,
                                              Name = ActualImageName
                                          }, width: 150, height: 100);

            Assert.Equal(expectedUrl, url);
        }
    }
}
