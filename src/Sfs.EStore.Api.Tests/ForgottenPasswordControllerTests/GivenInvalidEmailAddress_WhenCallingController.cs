using System.Net;
using System.Net.Http;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Tests.Base;
using Xunit;

namespace Sfs.EStore.Api.Tests.ForgottenPasswordControllerTests
{
    public class GivenInvalidEmailAddress_WhenCallingController : DocumentStoreTestBase
    {
        private readonly HttpResponseMessage _responseMessage;

        public GivenInvalidEmailAddress_WhenCallingController()
        {
            using (var ctx = new WebRequestContext<ForgottenPasswordController>(Store,
                                                                                () => new ForgottenPasswordController(new FakeInitiateForgottenPasswordRetrievalCommand())))
            {
                var model = new ForgottenPasswordModel { Username = "not.an@email.com" };
                _responseMessage = ctx.Controller.Post(model).Result;
            }
        }

        [Fact]
        public void ShouldReturnStatus404NotFound()
        {
            Assert.Equal(HttpStatusCode.NotFound, _responseMessage.StatusCode);
        }
    }
}