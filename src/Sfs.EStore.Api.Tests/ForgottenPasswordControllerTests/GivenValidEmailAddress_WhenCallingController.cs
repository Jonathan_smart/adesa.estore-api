using System.Net;
using System.Net.Http;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Tests.Base;
using Sfs.EStore.Api.ThirdParties;
using Xunit;

namespace Sfs.EStore.Api.Tests.ForgottenPasswordControllerTests
{
    public class GivenValidEmailAddress_WhenCallingController : DocumentStoreTestBase
    {
        private const string Email = "test@test.com";
        private readonly HttpResponseMessage _responseMessage;
        private ICommand _executedCommand;

        public GivenValidEmailAddress_WhenCallingController()
        {
            using (var s = Store.OpenSession())
            {
                s.Store(new MembershipUser
                            {
                                Username = Email,
                                EmailAddress = Email
                            }.SetPassword("foo"));

                s.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var ctx = new WebRequestContext<ForgottenPasswordController>(Store,
                                                                                () => new ForgottenPasswordController(new FakeInitiateForgottenPasswordRetrievalCommand())))
            {
                ctx.Controller.AlternativeExecuteCommand = x => _executedCommand = x;

                var model = new ForgottenPasswordModel { Username = Email };
                _responseMessage = ctx.Controller.Post(model).Result;
            }
        }

        [Fact]
        public void Should_ExecuteInitiateForgottenPasswordRetrieval()
        {
            var command = _executedCommand as IInitiateForgottenPasswordRetrievalCommand;
            Assert.NotNull(command);
        }

        [Fact]
        public void Should_SetUserOnCommand()
        {
            var command = _executedCommand as IInitiateForgottenPasswordRetrievalCommand;
            Assert.NotNull(command.User);
        }

        [Fact]
        public void Should_ReturnStatus202Accepted()
        {
            Assert.Equal(HttpStatusCode.Accepted, _responseMessage.StatusCode);
        }
    }
}