﻿using Raven.Client;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.ThirdParties;

namespace Sfs.EStore.Api.Tests.ForgottenPasswordControllerTests
{
    public class FakeInitiateForgottenPasswordRetrievalCommand : IInitiateForgottenPasswordRetrievalCommand
    {
        public IAsyncDocumentSession Session { get; set; }
        public MembershipUser User { get; set; }

        public void Execute()
        {
        }
    }
}
