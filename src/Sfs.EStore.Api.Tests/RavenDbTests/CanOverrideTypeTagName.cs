﻿using Raven.Client.Document;
using Sfs.EStore.Api.Tests.Base;
using Xunit;

namespace Sfs.EStore.Api.Tests.RavenDbTests
{
    public class CanOverrideTypeTagName : DocumentStoreTestBase
    {
        public CanOverrideTypeTagName() : base(false)
        {
            Store.Conventions.FindTypeTagName = t =>
                {
                    if (t == typeof(TestVehicleData))
                        return DocumentConvention.DefaultTypeTagName(typeof (TestVehicle));
                    return DocumentConvention.DefaultTypeTagName(t);

                };
            Store.Initialize();
        }

        [Fact]
        public void AndVerifyAgainstMetadata()
        {
            string id;

            using (var session = Store.OpenSession())
            {
                var testVehicle = new TestVehicle();
                var testVehicleData = new TestVehicleData();

                session.Store(testVehicle);
                id = testVehicle.Id;
                Assert.NotNull(testVehicle.Id);
                testVehicleData.Id = testVehicle.Id + "/Data";
                session.Store(testVehicleData);
                session.SaveChanges();
            }

            using (var session = Store.OpenSession())
            {
                var testVehicle = session.Include<TestVehicle>(tv => tv.Id + "/Data").Load<TestVehicle>(id);
                Assert.NotNull(testVehicle);
                var testVehicleData = session.Load<TestVehicleData>(id + "/Data");
                Assert.NotNull(testVehicleData);
                var vehicleMetaData = session.Advanced.GetMetadataFor(testVehicle);
                Assert.Equal(vehicleMetaData["Raven-Entity-Name"],
                             session.Advanced.GetMetadataFor(testVehicleData)["Raven-Entity-Name"]);
            }
        }

        public class TestVehicle
        {
            public string Id { get; set; }
        }

        public class TestVehicleData
        {
            public string Id { get; set; }
        }
    }
}
