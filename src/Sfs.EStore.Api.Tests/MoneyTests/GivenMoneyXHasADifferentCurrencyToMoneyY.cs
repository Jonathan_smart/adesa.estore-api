﻿using System;
using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.MoneyTests
{
    public class GivenMoneyXHasADifferentCurrencyToMoneyY
    {
        private readonly Money _moneyX = new Money(123.45m, Currency.GBP);
        private readonly Money _moneyY = new Money(123.45m, Currency.USD);

        [Fact]
        public void EqualTo_ShouldReturnFalse()
        {
            Assert.False(_moneyX == _moneyY);
        }

        [Fact]
        public void NotEqualTo_ShouldReturnTrue()
        {
            Assert.True(_moneyX != _moneyY);
        }

        [Fact]
        public void GreaterThanEqualTo_ShouldThrowInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => _moneyX >= _moneyY);
        }

        [Fact]
        public void LessThanEqualTo_ShouldThrowInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => _moneyX <= _moneyY);
        }

        [Fact]
        public void GreaterThan_ShouldThrowInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => _moneyX > _moneyY);
        }

        [Fact]
        public void LessThan_ShouldThrowInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => _moneyX < _moneyY);
        }

        [Fact]
        public void Equals_ShouldReturnFalse()
        {
            Assert.False(_moneyX.Equals(_moneyY));
        }

        [Fact]
        public void CompareTo_ShouldThrowInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => _moneyX.CompareTo(_moneyY));
        }

        [Fact]
        public void Addition_ShouldThrowInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => _moneyX + _moneyY);
        }

        [Fact]
        public void Subtraction_ShouldThrowInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => _moneyX - _moneyY);
        }
    }
}
