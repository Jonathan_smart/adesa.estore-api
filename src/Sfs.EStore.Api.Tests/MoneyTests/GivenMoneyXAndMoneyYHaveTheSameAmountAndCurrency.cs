using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.MoneyTests
{
    public class GivenMoneyXAndMoneyYHaveTheSameAmountAndCurrency
    {
        private readonly Money _moneyX = new Money(123.45m, Currency.GBP);
        private readonly Money _moneyY = new Money(123.45m, Currency.GBP);

        [Fact]
        public void EqualTo_ShouldReturnTrue()
        {
            Assert.True(_moneyX == _moneyY);
        }

        [Fact]
        public void NotEqualTo_ShouldReturnFalse()
        {
            Assert.False(_moneyX != _moneyY);
        }

        [Fact]
        public void GreaterThanEqualTo_ShouldReturnTrue()
        {
            Assert.True(_moneyX >= _moneyY);
        }

        [Fact]
        public void LessThanEqualTo_ShouldReturnTrue()
        {
            Assert.True(_moneyX <= _moneyY);
        }

        [Fact]
        public void GreaterThan_ShouldReturnFalse()
        {
            Assert.False(_moneyX > _moneyY);
        }

        [Fact]
        public void LessThan_ShouldReturnFalse()
        {
            Assert.False(_moneyX < _moneyY);
        }

        [Fact]
        public void Equals_ShouldReturnTrue()
        {
            Assert.True(_moneyX.Equals(_moneyY));
        }

        [Fact]
        public void CompareTo_ShouldReturn0()
        {
            Assert.Equal(0, _moneyX.CompareTo(_moneyY));
        }

        [Fact]
        public void Addition_ShouldReturnSameCurrencyAndAdditionOfAmounts()
        {
            var result = _moneyX + _moneyY;
            Assert.Equal(_moneyX.Currency, result.Currency);
            Assert.Equal(_moneyX.Amount + _moneyY.Amount, result.Amount);
        }

        [Fact]
        public void Subtraction_ShouldReturnSameCurrencyAndSubtractionOfAmounts()
        {
            var result = _moneyX - _moneyY;
            Assert.Equal(_moneyX.Currency, result.Currency);
            Assert.Equal(_moneyX.Amount - _moneyY.Amount, result.Amount);
        }
    }
}