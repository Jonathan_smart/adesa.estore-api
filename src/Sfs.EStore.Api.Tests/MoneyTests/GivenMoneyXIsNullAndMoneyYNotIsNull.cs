using System;
using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.MoneyTests
{
    public class GivenMoneyXIsNullAndMoneyYNotIsNull
    {
        private readonly Money _moneyX = null;
        private readonly Money _moneyY = new Money(123.45m, Currency.GBP);

        [Fact]
        public void EqualTo_ShouldReturnFalse()
        {
            Assert.False(_moneyX == _moneyY);
        }

        [Fact]
        public void NotEqualTo_ShouldReturnTrue()
        {
            Assert.True(_moneyX != _moneyY);
        }

        [Fact]
        public void GreaterThanEqualTo_ShouldReturnFalse()
        {
            Assert.False(_moneyX >= _moneyY);
        }

        [Fact]
        public void LessThanEqualTo_ShouldReturnTrue()
        {
            Assert.True(_moneyX <= _moneyY);
        }

        [Fact]
        public void GreaterThan_ShouldReturnFalse()
        {
            Assert.False(_moneyX > _moneyY);
        }

        [Fact]
        public void LessThan_ShouldReturnTrue()
        {
            Assert.True(_moneyX < _moneyY);
        }

        [Fact]
        public void Addition_ShouldThrowNullReferenceException()
        {
            Assert.Throws<NullReferenceException>(() => _moneyX + _moneyY);
        }

        [Fact]
        public void Subtraction_ShouldThrowNullReferenceException()
        {
            Assert.Throws<NullReferenceException>(() => _moneyX - _moneyY);
        }
    }
}