using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.MoneyTests
{
    public class GivenMoneyXAndMoneyYAndMoneyXAmountIsLessThanMoneyYAmount
    {
        private readonly Money _moneyX = new Money(123.45m, Currency.GBP);
        private readonly Money _moneyY = new Money(678.9m, Currency.GBP);

        [Fact]
        public void EqualTo_ShouldReturnFalse()
        {
            Assert.False(_moneyX == _moneyY);
        }

        [Fact]
        public void NotEqualTo_ShouldReturnTrue()
        {
            Assert.True(_moneyX != _moneyY);
        }

        [Fact]
        public void GreaterThanEqualTo_ShouldReturnFalse()
        {
            Assert.False(_moneyX >= _moneyY);
        }

        [Fact]
        public void LessThanEqualTo_ShouldReturnTrue()
        {
            Assert.True(_moneyX <= _moneyY);
        }

        [Fact]
        public void GreaterThan_ShouldReturnFalse()
        {
            Assert.False(_moneyX > _moneyY);
        }

        [Fact]
        public void LessThan_ShouldReturnTrue()
        {
            Assert.True(_moneyX < _moneyY);
        }

        [Fact]
        public void Equals_ShouldReturnFalse()
        {
            Assert.False(_moneyX.Equals(_moneyY));
        }

        [Fact]
        public void CompareTo_ShouldReturn0()
        {
            Assert.True(_moneyX.CompareTo(_moneyY) < 0);
        }
    }
}