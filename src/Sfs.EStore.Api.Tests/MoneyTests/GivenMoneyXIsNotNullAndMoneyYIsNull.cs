using System;
using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.MoneyTests
{
    public class GivenMoneyXIsNotNullAndMoneyYIsNull
    {
        private readonly Money _moneyX = new Money(123.45m, Currency.GBP);
        private readonly Money _moneyY = null;

        [Fact]
        public void EqualTo_ShouldReturnFalse()
        {
            Assert.False(_moneyX == _moneyY);
        }

        [Fact]
        public void NotEqualTo_ShouldReturnTrue()
        {
            Assert.True(_moneyX != _moneyY);
        }

        [Fact]
        public void GreaterThanEqualTo_ShouldReturnTrue()
        {
            Assert.True(_moneyX >= _moneyY);
        }

        [Fact]
        public void LessThanEqualTo_ShouldReturnFalse()
        {
            Assert.False(_moneyX <= _moneyY);
        }

        [Fact]
        public void GreaterThan_ShouldReturnTrue()
        {
            Assert.True(_moneyX > _moneyY);
        }

        [Fact]
        public void LessThan_ShouldReturnFalse()
        {
            Assert.False(_moneyX < _moneyY);
        }

        [Fact]
        public void Equals_ShouldReturnFalse()
        {
            Assert.False(_moneyX.Equals(_moneyY));
        }

        [Fact]
        public void CompareTo_ShouldReturn1()
        {
            Assert.Equal(1, _moneyX.CompareTo(_moneyY));
        }

        [Fact]
        public void Addition_ShouldThrowNullReferenceException()
        {
            Assert.Throws<NullReferenceException>(() => _moneyX + _moneyY);
        }

        [Fact]
        public void Subtraction_ShouldThrowNullReferenceException()
        {
            Assert.Throws<NullReferenceException>(() => _moneyX - _moneyY);
        }
    }
}