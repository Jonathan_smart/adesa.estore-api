using System;
using Sfs.EStore.Api.Calculators.VehicleTaxRates;
using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.VehicleTaxRates.Remaining_LGVs_after_01_03_2001
{
    public class All_vans
    {
        private readonly VehicleTaxRate _vehicleTaxRate;

        public All_vans()
        {
            var calc = new VehicleTaxRateCalculator();
            var registrationDate = new DateTime(2001, 03, 01);

            _vehicleTaxRate = calc.CalculateFor(
                new VehicleTaxCalculationArgs(registrationDate, VehicleType.Van)
                );
        }

        [Fact]
        public void Should_have_12_months_rate()
        {
            Assert.Equal(Money.From(225, Currency.GBP), _vehicleTaxRate.Cost12Months);
        }

        [Fact]
        public void Should_have_6_months_rate()
        {
            Assert.Equal(Money.From(123.75m, Currency.GBP), _vehicleTaxRate.Cost6Months);
        }
    }
}