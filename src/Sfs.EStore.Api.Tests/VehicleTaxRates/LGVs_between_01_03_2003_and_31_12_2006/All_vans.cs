using System;
using Sfs.EStore.Api.Calculators.VehicleTaxRates;
using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.VehicleTaxRates.LGVs_between_01_03_2003_and_31_12_2006
{
    public class All_vans
    {
        private readonly VehicleTaxRate _vehicleTaxRate;

        public All_vans()
        {
            var calc = new VehicleTaxRateCalculator();
            var registrationDate = new DateTime(2003, 03, 01);

            _vehicleTaxRate = calc.CalculateFor(
                new VehicleTaxCalculationArgs(registrationDate, VehicleType.Van)
                );
        }

        [Fact]
        public void Should_have_12_months_rate()
        {
            Assert.Equal(Money.From(140, Currency.GBP), _vehicleTaxRate.Cost12Months);
        }

        [Fact]
        public void Should_have_6_months_rate()
        {
            Assert.Equal(Money.From(77, Currency.GBP), _vehicleTaxRate.Cost6Months);
        }
    }
}