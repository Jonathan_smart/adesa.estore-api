using System;
using System.Linq.Expressions;
using Sfs.EStore.Api.Calculators.VehicleTaxRates;
using Sfs.EStore.Api.Entities;
using Xunit;
using Xunit.Extensions;

namespace Sfs.EStore.Api.Tests.VehicleTaxRates.Cars_registered_on_or_after_1_March_2001
{
    public abstract class TestBase
    {
        private readonly DateTime _registrationDate;

        protected TestBase()
            : this(new DateTime(2001, 03, 01))
        {

        }

        protected TestBase(DateTime registrationDate)
        {
            _registrationDate = registrationDate;
        }

        protected VehicleTaxRate CalculateForCo2(int co2)
        {
            var calc = new VehicleTaxRateCalculator();
            
            return calc.CalculateFor(
                new VehicleTaxCalculationArgs(_registrationDate, VehicleType.Car, co2GPerKm: co2)
                );
        }

        protected void AssertMoneyEquals(Money expectedRate, Money taxRate)
        {
            Assert.Equal(expectedRate, taxRate);
        }

        protected void AssertNotAvailable(Money taxRate)
        {
            Assert.Null(taxRate);
        }

        protected void AssertNotAvailable<T>(int co2, Expression<Func<VehicleTaxRate, T>> expression)
        {
            var vehicleTaxRate = CalculateForCo2(co2);
            var value = expression.Compile()(vehicleTaxRate);
            Assert.Null(value);
        }

        protected void AssertEquals<T>(int co2, T expected, Expression<Func<VehicleTaxRate, T>> expression)
        {
            var vehicleTaxRate = CalculateForCo2(co2);
            var value = expression.Compile()(vehicleTaxRate);
            Assert.Equal(expected, value);
        }

        protected void AssertMoneyEquals(int co2, Money expectedRate, Expression<Func<VehicleTaxRate, Money>> expression)
        {
            var vehicleTaxRate = CalculateForCo2(co2);
            var taxRate = expression.Compile()(vehicleTaxRate);
            Assert.Equal(expectedRate, taxRate);
        }

        protected void AssertNotAvailable(int co2, Expression<Func<VehicleTaxRate, Money>> expression)
        {
            var vehicleTaxRate = CalculateForCo2(co2);
            var taxRate = expression.Compile()(vehicleTaxRate);
            Assert.Null(taxRate);
        }
    }

    public class Band_A_co2_upto_100 : TestBase
    {
        [Theory]
        [InlineData(100)]
        public void Should_have_12_months_rate_0_GBP(int co2)
        {
            AssertMoneyEquals(co2, Money.Zero(Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(100)]
        public void Should_not_have_6_months_rate(int co2)
        {
            AssertNotAvailable(co2, x => x.Cost6Months);
        }

        [Theory]
        [InlineData(100)]
        public void Should_be_band_A(int co2)
        {
            AssertEquals(co2, 'A', x => x.Band);
        }
    }

    public class Band_B_co2_between_101_and_110 : TestBase
    {
        [Theory]
        [InlineData(101)]
        [InlineData(110)]
        public void Should_have_12_months_rate_20_GBP(int co2)
        {
            AssertMoneyEquals(co2, Money.From(20, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(101)]
        [InlineData(110)]
        public void Should_not_have_6_months_rate(int co2)
        {
            AssertNotAvailable(co2, x => x.Cost6Months);
        }

        [Theory]
        [InlineData(101)]
        [InlineData(110)]
        public void Should_be_band_B(int co2)
        {
            AssertEquals(co2, 'B', x => x.Band);
        }
    }

    public class Band_C_co2_between_111_and_120 : TestBase
    {
        [Theory]
        [InlineData(111)]
        [InlineData(120)]
        public void Should_have_12_months_rate_30_GBP(int co2)
        {
            AssertMoneyEquals(co2, Money.From(30, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(111)]
        [InlineData(120)]
        public void Should_not_have_6_months_rate(int co2)
        {
            AssertNotAvailable(co2, x => x.Cost6Months);
        }

        [Theory]
        [InlineData(111)]
        [InlineData(120)]
        public void Should_be_band_C(int co2)
        {
            AssertEquals(co2, 'C', x => x.Band);
        }
    }

    public class Band_D_co2_between_121_and_130 : TestBase
    {
        [Theory]
        [InlineData(121)]
        [InlineData(130)]
        public void Should_have_12_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(110, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(121)]
        [InlineData(130)]
        public void Should_have_6_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(60.50m, Currency.GBP), x => x.Cost6Months);
        }

        [Theory]
        [InlineData(121)]
        [InlineData(130)]
        public void Should_be_band_D(int co2)
        {
            AssertEquals(co2, 'D', x => x.Band);
        }
    }

    public class Band_E_co2_between_131_and_140 : TestBase
    {
        [Theory]
        [InlineData(131)]
        [InlineData(140)]
        public void Should_have_12_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(130, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(131)]
        [InlineData(140)]
        public void Should_have_6_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(71.50m, Currency.GBP), x => x.Cost6Months);
        }

        [Theory]
        [InlineData(131)]
        [InlineData(140)]
        public void Should_be_band_E(int co2)
        {
            AssertEquals(co2, 'E', x => x.Band);
        }
    }

    public class Band_F_co2_between_141_and_150 : TestBase
    {
        [Theory]
        [InlineData(141)]
        [InlineData(150)]
        public void Should_have_12_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(145, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(141)]
        [InlineData(150)]
        public void Should_have_6_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(79.75m, Currency.GBP), x => x.Cost6Months);
        }

        [Theory]
        [InlineData(141)]
        [InlineData(150)]
        public void Should_be_band_F(int co2)
        {
            AssertEquals(co2, 'F', x => x.Band);
        }
    }

    public class Band_G_co2_between_151_and_165 : TestBase
    {
        [Theory]
        [InlineData(151)]
        [InlineData(165)]
        public void Should_have_12_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(180, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(151)]
        [InlineData(165)]
        public void Should_have_6_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(99.00m, Currency.GBP), x => x.Cost6Months);
        }

        [Theory]
        [InlineData(151)]
        [InlineData(165)]
        public void Should_be_band_G(int co2)
        {
            AssertEquals(co2, 'G', x => x.Band);
        }
    }

    public class Band_H_co2_between_166_and_175 : TestBase
    {
        [Theory]
        [InlineData(166)]
        [InlineData(175)]
        public void Should_have_12_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(205, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(166)]
        [InlineData(175)]
        public void Should_have_6_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(112.75m, Currency.GBP), x => x.Cost6Months);
        }

        [Theory]
        [InlineData(166)]
        [InlineData(175)]
        public void Should_be_band_H(int co2)
        {
            AssertEquals(co2, 'H', x => x.Band);
        }
    }

    public class Band_I_co2_between_176_and_185 : TestBase
    {
        [Theory]
        [InlineData(176)]
        [InlineData(185)]
        public void Should_have_12_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(225, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(176)]
        [InlineData(185)]
        public void Should_have_6_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(123.75m, Currency.GBP), x => x.Cost6Months);
        }

        [Theory]
        [InlineData(176)]
        [InlineData(185)]
        public void Should_be_band_I(int co2)
        {
            AssertEquals(co2, 'I', x => x.Band);
        }
    }

    public class Band_J_co2_between_186_and_200 : TestBase
    {
        [Theory]
        [InlineData(186)]
        [InlineData(200)]
        public void Should_have_12_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(265, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(186)]
        [InlineData(200)]
        public void Should_have_6_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(145.75m, Currency.GBP), x => x.Cost6Months);
        }

        [Theory]
        [InlineData(186)]
        [InlineData(200)]
        public void Should_be_band_J(int co2)
        {
            AssertEquals(co2, 'J', x => x.Band);
        }
    }

    public class Band_K_co2_between_201_and_225 : TestBase
    {
        [Theory]
        [InlineData(201)]
        [InlineData(225)]
        public void Should_have_12_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(290.00m, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(201)]
        [InlineData(225)]
        public void Should_have_6_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(159.50m, Currency.GBP), x => x.Cost6Months);
        }

        [Theory]
        [InlineData(201)]
        [InlineData(225)]
        public void Should_be_band_K(int co2)
        {
            AssertEquals(co2, 'K', x => x.Band);
        }
    }

    public class Band_L_co2_between_226_and_255_registered_on_or_after_23rd_March_2006 : TestBase
    {
        public Band_L_co2_between_226_and_255_registered_on_or_after_23rd_March_2006()
            : base(new DateTime(2006, 03, 23))
        {

        }

        [Theory]
        [InlineData(226)]
        [InlineData(255)]
        public void Should_have_12_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(490, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(226)]
        [InlineData(255)]
        public void Should_have_6_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(269.50m, Currency.GBP), x => x.Cost6Months);
        }

        [Theory]
        [InlineData(226)]
        [InlineData(255)]
        public void Should_be_band_L(int co2)
        {
            AssertEquals(co2, 'L', x => x.Band);
        }
    }

    public class Band_M_co2_over_255_registered_on_or_after_23rd_March_2006 : TestBase
    {
        public Band_M_co2_over_255_registered_on_or_after_23rd_March_2006()
            : base(new DateTime(2006, 03, 23))
        {

        }

        [Theory]
        [InlineData(256)]
        [InlineData(257)]
        public void Should_have_12_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(505, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(256)]
        [InlineData(257)]
        public void Should_have_6_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(277.75m, Currency.GBP), x => x.Cost6Months);
        }

        [Theory]
        [InlineData(256)]
        [InlineData(257)]
        public void Should_be_band_M(int co2)
        {
            AssertEquals(co2, 'M', x => x.Band);
        }
    }

    public class Registered_before_23rd_March_2006_with_co2_over_225 : TestBase
    {
        public Registered_before_23rd_March_2006_with_co2_over_225()
            : base(new DateTime(2006, 03, 22))
        {

        }

        [Theory]
        [InlineData(226)]
        [InlineData(255)]
        public void Should_have_12_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(290, Currency.GBP), x => x.Cost12Months);
        }

        [Theory]
        [InlineData(226)]
        [InlineData(295)]
        public void Should_have_6_months_rate(int co2)
        {
            AssertMoneyEquals(co2, Money.From(159.50m, Currency.GBP), x => x.Cost6Months);
        }

        [Theory]
        [InlineData(226)]
        [InlineData(255)]
        public void Should_be_band_M(int co2)
        {
            AssertEquals(co2, 'K', x => x.Band);
        }
    }
}