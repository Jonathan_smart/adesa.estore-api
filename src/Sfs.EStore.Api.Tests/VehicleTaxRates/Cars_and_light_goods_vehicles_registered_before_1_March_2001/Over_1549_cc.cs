using System;
using Sfs.EStore.Api.Calculators.VehicleTaxRates;
using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.VehicleTaxRates.Cars_and_light_goods_vehicles_registered_before_1_March_2001
{
    public class Over_1549_cc
    {
        private readonly VehicleTaxRate _vehicleTaxRate;

        public Over_1549_cc()
        {
            var calc = new VehicleTaxRateCalculator();
            var registrationDate = new DateTime(2001, 03, 01).AddDays(-1);
            const int engineSizeCc = 1550;

            _vehicleTaxRate = calc.CalculateFor(
                new VehicleTaxCalculationArgs(registrationDate, VehicleType.Car, engineSizeCc: engineSizeCc)
                );
        }

        [Fact]
        public void Should_have_12_months_rate()
        {
            Assert.Equal(Money.From(230, Currency.GBP), _vehicleTaxRate.Cost12Months);
        }

        [Fact]
        public void Should_have_6_months_rate()
        {
            Assert.Equal(Money.From(126.50m, Currency.GBP), _vehicleTaxRate.Cost6Months);
        }

        [Fact]
        public void Should_not_have_a_band()
        {
            Assert.Equal(null, _vehicleTaxRate.Band);
        }
    }
}