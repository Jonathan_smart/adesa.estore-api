﻿using System;
using Sfs.EStore.Api.Calculators;
using Sfs.EStore.Api.Entities;
using Xunit;
using Xunit.Extensions;

namespace Sfs.EStore.Api.Tests.PencePerMileTests
{
    public class FuelCostPerMileCalculator
    {
        [Theory]
        [InlineData(100.0, 50.0, 2.0)]
        [InlineData(500.0, 25.0, 20.0)]
        [InlineData(null, 25.0, null)]
        public void Should_equal_expected_cost(double? costPerUnitOfFuel, double unitsOfFuelPerDistanceTravelled, double? expectedResult)
        {
            Func<FuelTypes, Money> fuelCosts = fuel => costPerUnitOfFuel.HasValue 
              ? new Money((decimal)costPerUnitOfFuel, Currency.GBP)
              : null;

            var actualResult = new FuelCostCalculator(fuelCosts).PencePerUnitDistanceTravelled(FuelTypes.Diesel, (decimal)unitsOfFuelPerDistanceTravelled);

            if (expectedResult.HasValue)
            {
                Assert.NotNull(actualResult);
                Assert.Equal((decimal?) expectedResult, actualResult.Amount);
            }
            else
                Assert.Null(actualResult);
        }
    }
}
