﻿using System;
using System.Net.Http;
using Raven.Client;
using Raven.Client.Linq;
using System.Linq;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Raven.Indexes;
using Sfs.EStore.Api.Raven.Transformers;
using Sfs.EStore.Api.Tests.Base;
using Xunit;

namespace Sfs.EStore.Api.Tests.RecentlyViewedControllerTests
{
    public class QueryTests : DocumentStoreTestBase
    {
        private static readonly string SessionId = Guid.NewGuid().ToString();

        private readonly Func<RecentlyViewedController> _controllerFactory = () => new RecentlyViewedController();
        public QueryTests()
        {
            ApplyCurrentPrincipal();

            RegisterIndex(new SessionInteractions_RecentlyViewed());
            RegisterTransform(new LotIdToListingSummary());
        }

        [Fact]
        public void CanQueryControllerForNoRecentlyViewedVehicles()
        {
            using (var ctx = new WebRequestContext<RecentlyViewedController>(Store, _controllerFactory))
            {
                var model = new RecentlyViewedGetModel { Top = 10 };
                var results = ctx.Controller.Get(model).Result.Content
                    .ReadAsAsync<ListingSummaryViewModel[]>().Result;
                Assert.NotNull(results);
                Assert.Empty(results);
            }
        }

        [Fact]
        public void CanQueryIndexForSingleRecentlyViewedVehicle()
        {
            using (var session = Store.OpenSession())
            {
                CreateLotVehicleSessionInteraction(session);
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var session = Store.OpenSession())
            {
                var results = session.Query<SessionInteractions_RecentlyViewed.ReduceResult, SessionInteractions_RecentlyViewed>()
                    .TransformWith<LotIdToListingSummary, ListingSummaryViewModel>()
                    .ToList();
                Assert.NotNull(results);
                Assert.Single(results);
                var reduceResult = results[0];
                Assert.NotNull(reduceResult.ListingId);
                Assert.Equal(LotStatusTypes.Active.ToString(), reduceResult.SellingStatus.SellingState);
            }
        }

        [Fact]
        public void CanQueryIndexToFindZeroResultsWhereLotDoesntExistForInteraction()
        {
            using (var session = Store.OpenSession())
            {
                CreateLotVehicleSessionInteraction(session);
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var session = Store.OpenSession())
            {
                var results = session.Query<SessionInteractions_RecentlyViewed.ReduceResult, SessionInteractions_RecentlyViewed>()
                    .TransformWith<LotIdToListingSummary, ListingSummaryViewModel>()
                    .ToList();
                Assert.NotNull(results);
                Assert.Single(results);
                var reduceResult = results[0];
                Assert.NotNull(reduceResult.ListingId);
            }

            using (var session = Store.OpenSession())
            {
                session.Advanced.DocumentStore.DatabaseCommands.Delete("lots/1", null);
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var session = Store.OpenSession())
            {
                var results = session.Query<SessionInteractions_RecentlyViewed.ReduceResult, SessionInteractions_RecentlyViewed>()
                    .TransformWith<LotIdToListingSummary, ListingSummaryViewModel>()
                    .ToList();
                Assert.NotNull(results);
                Assert.Empty(results);
            }
        }

        [Fact]
        public void CanQueryControllerForSingleRecentlyViewedVehicle()
        {
            using (var session = Store.OpenSession())
            {
                CreateLotVehicleSessionInteraction(session);
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var ctx = new WebRequestContext<RecentlyViewedController>(Store, _controllerFactory))
            {
                var model = new RecentlyViewedGetModel { Top = 10 };
                var results = ctx.Controller.Get(model).Result.Content
                    .ReadAsAsync<ListingSummaryViewModel[]>().Result;
                Assert.NotNull(results);
                Assert.Single(results);
            }
        }

        [Fact]
        public void CanQueryControllerForRecentlyViewedVehicleWithTags()
        {
            using (var session = Store.OpenSession())
            {
                var vehicle = CreateVehicle(session);
                var tag1Lot = CreateLot(session, vehicle, limitedToRoles: new[] { "tag1" });
                var tag2Lot1 = CreateLot(session, vehicle, limitedToRoles: new[] { "tag2" });
                var tag2Lot2 = CreateLot(session, vehicle, limitedToRoles: new[] { "tag2" });
                CreateSessionInteraction(session,
                    new SessionInteractions.ViewedLot { LotId = tag1Lot.Id, Date = DateTime.UtcNow },
                    new SessionInteractions.ViewedLot { LotId = tag2Lot1.Id, Date = DateTime.UtcNow },
                    new SessionInteractions.ViewedLot { LotId = tag2Lot2.Id, Date = DateTime.UtcNow }
                    );
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var ctx = new WebRequestContext<RecentlyViewedController>(Store, _controllerFactory))
            {
                var model = new RecentlyViewedGetModel { LimitToRoles = new[] { "tag1" }, Top = 10};
                var results = ctx.Controller.Get(model).Result.Content
                    .ReadAsAsync<ListingSummaryViewModel[]>().Result;
                Assert.NotNull(results);
                Assert.Single(results);
            }
        }

        [Fact]
        public void CanQueryControllerForMostRecentResult()
        {
            Lot lot1;
            using (var session = Store.OpenSession())
            {
                var vehicle = CreateVehicle(session);
                lot1 = CreateLot(session, vehicle);
                var lot2 = CreateLot(session, vehicle);
                var lot3 = CreateLot(session, vehicle);
                CreateSessionInteraction(session,
                        new SessionInteractions.ViewedLot { LotId = lot2.Id, Date = new DateTime(2013, 01, 01) },
                        new SessionInteractions.ViewedLot { LotId = lot3.Id, Date = new DateTime(2013, 01, 02) },
                        new SessionInteractions.ViewedLot { LotId = lot3.Id, Date = new DateTime(2013, 01, 03) },
                        new SessionInteractions.ViewedLot { LotId = lot2.Id, Date = new DateTime(2013, 01, 04) },
                        new SessionInteractions.ViewedLot { LotId = lot1.Id, Date = new DateTime(2013, 01, 05) }
                        );
                session.SaveChanges();
            }

            WaitForIndexingAndEnsureNoStoreErrors();

            using (var session = Store.OpenSession())
            {
                var results = session.Query<SessionInteractions_RecentlyViewed.ReduceResult, SessionInteractions_RecentlyViewed>()
                    .Where(x => x.LotStatus == LotStatusTypes.Active)
                    .OrderByDescending(x => x.Date)
                    .TransformWith<LotIdToListingSummary, ListingSummaryViewModel>()
                    .ToList();
                var lotSummaryViewModel = results.FirstOrDefault();
                Assert.NotNull(lotSummaryViewModel);
                Assert.True(lotSummaryViewModel != null && lotSummaryViewModel.ListingId == lot1.LotNumber);
            }
        }

        private void CreateLotVehicleSessionInteraction(IDocumentSession session)
        {
            var vehicle = CreateVehicle(session);
            var lot = CreateLot(session, vehicle);
            CreateSessionInteraction(session, new SessionInteractions.ViewedLot { LotId = lot.Id, Date = DateTime.UtcNow });
        }

        private void CreateSessionInteraction(IDocumentSession session, params SessionInteractions.ViewedLot[] lotIds)
        {
            var sessionInteraction = new SessionInteractions { Username = MembershipUsername, SessionId = SessionId, LotsViewed = lotIds };
            session.Store(sessionInteraction);
        }

        private static Lot CreateLot(IDocumentSession session, Vehicle vehicle, LotStatusTypes lotStatus = LotStatusTypes.Active, string[] limitedToRoles = null)
        {
            var lot = new Lot { Title = "LotTitle", Details = "LotDetails", Status = lotStatus, BuyItNowPrice = Money.From(10000m), VisibilityLimitedTo = limitedToRoles};
            lot.LinkToVehicle(vehicle);
            session.Store(lot);
            return lot;
        }

        private static Vehicle CreateVehicle(IDocumentSession session, string reg = "AA00ZZZ", string vehicleType = "CAR")
        {
            var vehicle = new Vehicle { Registration = new Registration(reg, DateTime.Now.Date), VehicleType = vehicleType};
            session.Store(vehicle);
            return vehicle;
        }
    }
}
