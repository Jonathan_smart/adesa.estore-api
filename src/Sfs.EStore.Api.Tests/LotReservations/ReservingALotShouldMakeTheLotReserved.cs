﻿using Sfs.EStore.Api.DomainEvents;
using Sfs.EStore.Api.Entities;
using Xunit;

namespace Sfs.EStore.Api.Tests.LotReservations
{
    public class When_a_lot_is_reserved
    {
        [Fact]
        public void Then_its_status_should_be_reserved()
        {
            var lot = new Lot();
            lot.Activate();

            LotHasBeenReserved lotEvent = null;

            DomainEvent.Register<LotHasBeenReserved>(x => lotEvent = x);

            lot.Reserve();

            Assert.NotNull(lotEvent);
        }
    }

    public class When_a_lot_is_unreserved
    {
        [Fact]
        public void Then_its_status_should_be_Active()
        {
            var lot = new Lot();
            lot.Activate();
            lot.Reserve();

            LotReservationHasBeenRemoved lotEvent = null;

            DomainEvent.Register<LotReservationHasBeenRemoved>(x => lotEvent = x);

            lot.UnReserve();

            Assert.NotNull(lotEvent);
        }
    }

    public class When_a_lot_is_activated
    {
        [Fact]
        public void Then_its_status_should_be_Active()
        {
            var lot = new Lot();

            LotHasBeenActivated lotEvent = null;

            DomainEvent.Register<LotHasBeenActivated>(x => lotEvent = x);

            lot.Activate();

            Assert.NotNull(lotEvent);
        }
    }

    public class When_a_lot_is_archived
    {
        [Fact]
        public void Then_its_status_should_be_Archived()
        {
            var lot = new Lot();
            lot.Activate();

            LotHasBeenArchived lotEvent = null;

            DomainEvent.Register<LotHasBeenArchived>(x => lotEvent = x);

            lot.Archive();

            Assert.NotNull(lotEvent);
        }
    }

    public class When_a_lot_is_cancelled
    {
        [Fact]
        public void Then_its_status_should_be_Cancelled()
        {
            var lot = new Lot();
            lot.Activate();

            LotHasBeenCancelled lotEvent = null;

            DomainEvent.Register<LotHasBeenCancelled>(x => lotEvent = x);

            lot.Cancel();

            Assert.NotNull(lotEvent);
        }
    }

    public class When_a_lot_ends_as_sold
    {
        [Fact]
        public void Then_its_status_should_be_Sold()
        {
            var lot = new Lot();
            lot.SetBidding(Money.Zero(Currency.GBP), Money.Zero(Currency.GBP));
            lot.Activate();
            lot.TakeBid(new Money(1, Currency.GBP), "foo", lot, null, "");

            LotHasEndedWithSale lotEvent = null;

            DomainEvent.Register<LotHasEndedWithSale>(x => lotEvent = x);

            lot.End();

            Assert.NotNull(lotEvent);
        }
    }

    public class When_a_lot_ends_through_buy_it_now
    {
        [Fact]
        public void Then_its_status_should_be_Sold()
        {
            var lot = new Lot();
            lot.SetBidding(Money.Zero(), Money.Zero());
            lot.BuyItNowPrice = Money.Zero();
            lot.Activate();
            
            LotHasEndedWithSale lotEvent = null;

            DomainEvent.Register<LotHasEndedWithSale>(x => lotEvent = x);

            lot.BuyItNow("test-user");

            Assert.NotNull(lotEvent);
        }
    }

    public class When_a_ends_as_unsold
    {
        [Fact]
        public void Then_its_status_should_be_Unsold()
        {
            var lot = new Lot();
            lot.Activate();

            LotHasEndedWithoutSale lotEvent = null;

            DomainEvent.Register<LotHasEndedWithoutSale>(x => lotEvent = x);

            lot.End();

            Assert.NotNull(lotEvent);
        }
    }
}
