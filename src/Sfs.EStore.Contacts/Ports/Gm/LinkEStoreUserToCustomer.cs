﻿using System;
using Sfs.Darker.CommandProcessor;

namespace Sfs.EStore.Contacts.Ports.Gm
{
    public class LinkEStoreUserToCustomer : Command
    {
        public LinkEStoreUserToCustomer(string eStoreUsername, string customerNo) : base(Guid.NewGuid())
        {
            EStoreUsername = eStoreUsername;
            CustomerNo = customerNo;
        }

        public string EStoreUsername { get; private set; }
        public string CustomerNo { get; private set; }
    }
}
