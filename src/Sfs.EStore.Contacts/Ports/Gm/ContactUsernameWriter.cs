using System.Security.Policy;
using Sfs.EStore.Contacts.GmContactUsernamesService;
using Sfs.Core;

namespace Sfs.EStore.Contacts.Ports.Gm
{
    public class ContactUsernameWriter : IContactUsernameWriter
    {
        private readonly SalesChannel _salesChannel;
        private readonly Contact_Usernames_Service _client;

        public ContactUsernameWriter(SalesChannel salesChannel, Url serviceUrl)
        {
            _salesChannel = salesChannel;
            _client = new Contact_Usernames_Service
            {
                UseDefaultCredentials = true,
                Url = serviceUrl.Value
            };
        }

        public void AddNew(NewContactUsername username)
        {
            var contact = new Contact_Usernames
            {
                Application_Account = "",
                Application_Name = "ESTORE",
                Sales_Channel_No = _salesChannel,
                Sales_Channel_NoSpecified = true,
                Username = username.Username,
                Contact_No = username.ContactNo
            };
            _client.Create(ref contact);
        }

        public void Delete(ContactUsernameEntryNo entryNo)
        {
            var entry = _client.Read(entryNo);

            if (entry == null) return;

            _client.Delete(entry.Key);
        }
    }
}