using System.Globalization;

namespace Sfs.EStore.Contacts.Ports.Gm
{
    public struct ContactUsernameEntryNo
    {
        private readonly int _value;

        public ContactUsernameEntryNo(int value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value.ToString(CultureInfo.InvariantCulture);
        }

        public static implicit operator int (ContactUsernameEntryNo item)
        {
            return item._value;
        }
    }
}