﻿using System.Linq;
using Data = Sfs.EStore.Contacts.Adapters.Gm.DataAccess;

namespace Sfs.EStore.Contacts.Ports.Gm
{
    public interface ICustomerReaderDto
    {
        Customer Get(string customerNo);
    }

    public class CustomerReaderDto : ICustomerReaderDto
    {
        public Customer Get(string customerNo)
        {
            using (var context = new Data.NavisionContext())
            {
                var customer = context.Set<Data.Customer>()
                    .FirstOrDefault(x => x.No == customerNo);

                return customer == null ? null : new Customer(customer.No, customer.WebContactNo);
            }
        }
    }

    public class Customer
    {
        public Customer(string no, string webContactNo)
        {
            No = no;
            WebContactNo = webContactNo;
        }

        public string No { get; private set; }
        public string WebContactNo { get; private set; }
    }
}
