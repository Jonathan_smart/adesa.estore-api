namespace Sfs.EStore.Contacts.Ports.Gm
{
    public interface IContactUsernameWriter
    {
        void AddNew(NewContactUsername username);
        void Delete(ContactUsernameEntryNo entryNo);
    }

    public class NewContactUsername
    {
        public NewContactUsername(string username, string contactNo)
        {
            Username = username;
            ContactNo = contactNo;
        }

        public string Username { get; private set; }
        public string ContactNo { get; private set; }
    }
}