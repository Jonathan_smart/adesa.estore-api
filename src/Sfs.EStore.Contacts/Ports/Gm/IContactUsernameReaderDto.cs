namespace Sfs.EStore.Contacts.Ports.Gm
{
    public interface IContactUsernameReaderDto
    {
        ContactUsername[] GetForUsername(string username);
        ContactUsername GetForContact(string contactNo, string username);
    }

    public class ContactUsername
    {
        public ContactUsername(string contactNo, ContactUsernameEntryNo entryNo, string username)
        {
            ContactNo = contactNo;
            EntryNo = entryNo;
            Username = username;
        }
        
        public ContactUsernameEntryNo EntryNo { get; private set; }
        public string Username { get; private set; }
        public string ContactNo { get; private set; }
    }
}