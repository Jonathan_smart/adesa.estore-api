using System.Linq;
using Sfs.Core;

namespace Sfs.EStore.Contacts.Ports.Gm
{
    public class ContactUsernameReaderDto : IContactUsernameReaderDto
    {
        private readonly SalesChannel _salesChannel;

        public ContactUsernameReaderDto(SalesChannel salesChannel)
        {
            _salesChannel = salesChannel;
        }

        public ContactUsername[] GetForUsername(string username)
        {
            using (var context = new Adapters.Gm.DataAccess.NavisionContext())
            {
                var usernames = context.Set<Adapters.Gm.DataAccess.ContactUsername>()
                    .Where(x => x.SalesChannelNo == _salesChannel && x.Username == username)
                    .ToList();

                return usernames.Select(contactUsername =>
                    new ContactUsername(contactUsername.ContactNo,
                        new ContactUsernameEntryNo(contactUsername.EntryNo), contactUsername.Username)).ToArray();
            }
        }

        public ContactUsername GetForContact(string contactNo, string username)
        {
            using (var context = new Adapters.Gm.DataAccess.NavisionContext())
            {
                var contactUsername = context.Set<Adapters.Gm.DataAccess.ContactUsername>().FirstOrDefault(x =>
                    x.SalesChannelNo == _salesChannel &&
                    x.ContactNo == contactNo && 
                    x.Username == username);

                if (contactUsername == null) return null;

                return new ContactUsername(contactUsername.ContactNo,
                    new ContactUsernameEntryNo(contactUsername.EntryNo), contactUsername.Username);
            }
        }
    }
}