using System;
using Sfs.Darker.CommandProcessor;

namespace Sfs.EStore.Contacts.Ports.Gm
{
    public class RemoveEStoreUserToCustomerLink : Command
    {
        public RemoveEStoreUserToCustomerLink(string eStoreUsername, string customerNo) : base(Guid.NewGuid())
        {
            EStoreUsername = eStoreUsername;
            CustomerNo = customerNo;
        }

        public string EStoreUsername { get; private set; }
        public string CustomerNo { get; private set; }
    }
}