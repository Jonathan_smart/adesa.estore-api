﻿using Sfs.EStore.Contacts.Ports.Gm;
using Sfs.Darker.CommandProcessor;

namespace Sfs.EStore.Contacts.Adapters.Gm
{
    public class LinkEStoreUserToCustomerHandler : RequestHandler<LinkEStoreUserToCustomer>
    {
        private readonly ICustomerReaderDto _customerDto;
        private readonly IContactUsernameReaderDto _contactDto;
        private readonly IContactUsernameWriter _contactUsernameWriter;

        public LinkEStoreUserToCustomerHandler(ICustomerReaderDto customerDto, IContactUsernameReaderDto contactDto, IContactUsernameWriter contactUsernameWriter, ILog logger) : base(logger)
        {
            _customerDto = customerDto;
            _contactDto = contactDto;
            _contactUsernameWriter = contactUsernameWriter;
        }

        /// <exception cref="CustomerNotFound"></exception>
        /// <exception cref="MissingDefaultContactForCustomer"></exception>
        public override LinkEStoreUserToCustomer Handle(LinkEStoreUserToCustomer command)
        {
            var customer = _customerDto.Get(command.CustomerNo);

            if (customer == null)
                throw new CustomerNotFound(command.CustomerNo);

            if (string.IsNullOrEmpty(customer.WebContactNo))
                throw new MissingDefaultContactForCustomer(command.CustomerNo);

            var existingContactUsername = _contactDto.GetForContact(customer.WebContactNo, command.EStoreUsername);

            if (existingContactUsername != null)
                return command;

            _contactUsernameWriter.AddNew(new NewContactUsername(command.EStoreUsername, customer.WebContactNo));

            return command;
        }
    }
}
