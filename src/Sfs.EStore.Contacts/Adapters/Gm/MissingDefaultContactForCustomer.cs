﻿using System;
using System.Runtime.Serialization;

namespace Sfs.EStore.Contacts.Adapters.Gm
{
    [Serializable]
    public class MissingDefaultContactForCustomer : Exception
    {
        public MissingDefaultContactForCustomer(string customerNo)
            : base(string.Format("Customer \"{0}\" not found", customerNo))
        {
            CustomerNo = customerNo;
        }

        protected MissingDefaultContactForCustomer(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
            CustomerNo = info.GetString("CustomerNo");
        }

        public string CustomerNo { get; private set; }
    }
}