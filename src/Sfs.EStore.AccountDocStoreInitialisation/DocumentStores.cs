using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Linq;
using Sfs.EStore.Api;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.AccountDocStoreInitialisation
{
    public class DocumentStores
    {
        private readonly string _ravenUrl;
        private readonly LazyAccountDocumentStoreFactory _accountStoresFactory;
        private readonly ConcurrentDictionary<string, Lazy<IDocumentStore>> _stores;

        public DocumentStores(string ravenUrl)
        {
            _ravenUrl = ravenUrl;
            _stores = new ConcurrentDictionary<string, Lazy<IDocumentStore>>();
            _accountStoresFactory = new LazyAccountDocumentStoreFactory(_stores,
                                                                        _ravenUrl);
        }

        public IDocumentStore Api()
        {
            const string databaseName = "eStore.api";

            var store = _stores.GetOrAdd(databaseName,
                                         dbName => new Lazy<IDocumentStore>(
                                                       () => new DocumentStore
                                                                 {
                                                                     Url = _ravenUrl,
                                                                     DefaultDatabase = databaseName
                                                                 }.Initialize()));
            return store.Value;
        }

        public IEnumerable<DocumentStore> ApiAccounts()
        {
            var apiDocStore = Api();

            var accountDocStoreNames = apiDocStore.OpenSession().Query<Account>()
                .Select(x => x.DatabaseName)
                .Distinct()
                .ToList();

            foreach (var accountDocStoreName in accountDocStoreNames)
            {
                yield return (DocumentStore)_accountStoresFactory.GetAccountDocumentStoreFor(accountDocStoreName);
            }
        }
    }
}