using System;
using Raven.Abstractions.Data;
using Raven.Client;

namespace Sfs.EStore.AccountDocStoreInitialisation.Commands
{
    [Obsolete]
    public class PatchVehicleRandomisedValue : ICommand
    {
        private readonly IDocumentStore _docStore;

        public PatchVehicleRandomisedValue(IDocumentStore docStore)
        {
            _docStore = docStore;
        }

        public void Execute()
        {
            _docStore.DatabaseCommands.UpdateByIndex(
                "Raven/DocumentsByEntityName",
                new IndexQuery {Query = "Tag:Vehicles"},
                new ScriptedPatchRequest
                {
                    Script = @"
if (!this.RandomisedValue) this.RandomisedValue = Math.random().toString();"
                });
        }
    }
}