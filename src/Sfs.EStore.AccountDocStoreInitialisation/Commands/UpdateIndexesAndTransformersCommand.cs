using System;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using Raven.Client;
using Raven.Client.Indexes;
using Sfs.EStore.Api.Entities;
using log4net;

namespace Sfs.EStore.AccountDocStoreInitialisation.Commands
{
    public class UpdateIndexesAndTransformersCommand : ICommand
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(UpdateIndexesAndTransformersCommand));
        private readonly IDocumentStore _docStore;

        public UpdateIndexesAndTransformersCommand(IDocumentStore docStore)
        {
            _docStore = docStore;
        }

        public void Execute()
        {

            var assemblyToScanForIndexingTasks = typeof(Account).Assembly;
            try
            {
                IndexCreation.CreateIndexes(assemblyToScanForIndexingTasks, _docStore);
            }
            catch (Exception ex)
            {
                Logger.Error("Failed to create indexes", ex);
            }
            try
            {
                CreateTransformers(_docStore, assemblyToScanForIndexingTasks);
            }
            catch (Exception ex)
            {
                Logger.Error("Failed to create transformers", ex);
            }
        }

        private static void CreateTransformers(IDocumentStore docStore, Assembly assemblyToScanForIndexingTasks)
        {
            var exportProvider = (ExportProvider)new CompositionContainer(new AssemblyCatalog(assemblyToScanForIndexingTasks), new ExportProvider[0]);

            foreach (var transformerCreationTask in exportProvider.GetExportedValues<AbstractTransformerCreationTask>())
                transformerCreationTask.Execute(docStore);
        }
    }
}