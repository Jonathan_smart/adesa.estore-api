using System.Collections.Generic;
using Raven.Client;
using Raven.Database.Bundles.SqlReplication;

namespace Sfs.EStore.AccountDocStoreInitialisation.Commands
{
    public class CreateSqlReplicationDocumentsCommand : ICommand
    {
        private readonly IDocumentStore _docStore;

        public CreateSqlReplicationDocumentsCommand(IDocumentStore docStore)
        {
            _docStore = docStore;
        }

        public void Execute()
        {
            using (var session = _docStore.OpenSession())
            {
                session.Store(LotsReplicationConfig());
                session.Store(MembershipUsersReplicationConfig());
                session.Store(SessionInteractionsReplicationConfig());
                session.Store(UserLotWatchesReplicationConfig());
                session.Store(UserSavedSearchesReplicationConfig());
                session.Store(UserSavedValuationsReplicationConfig());
                session.Store(VehiclesReplicationConfig());

                session.SaveChanges();
            }
        }

        private static SqlReplicationConfig LotsReplicationConfig()
        {
            return new SqlReplicationConfig
            {
                Id = "Raven/SqlReplication/Configuration/Lots",
                Name = "Lots",
                RavenEntityName = "Lots",
                PredefinedConnectionStringSettingName = "Replication",
                PerformTableQuatation = true,
                SqlReplicationTables = new List<SqlReplicationTable>
                {
                    new SqlReplicationTable
                    {
                        TableName = "Lots",
                        DocumentKeyColumn = "Id"
                    },
                    new SqlReplicationTable
                    {
                        TableName = "LotBids",
                        DocumentKeyColumn = "LotId"
                    }
                },
                Script = @"var lotData = {
	Id: documentId,
	VehicleId: this.VehicleId,
	Status: this.Status,
	BuyItNowPrice_Amount: !this.BuyItNowPrice ? null : this.BuyItNowPrice.Amount,
	BuyItNowPrice_Currency: !this.BuyItNowPrice ? null : this.BuyItNowPrice.Currency,
	Title: this.Title,
	StartDate: this.StartDate,
	EndDate: this.EndDate
};

if (this.CurrentOffer) {
	lotData.CurrentOffer_Amount = this.CurrentOffer.Amount.Amount;
	lotData.CurrentOffer_Currency = this.CurrentOffer.Amount.Currency;
	lotData.CurrentOffer_Date = this.CurrentOffer.Date;
	lotData.CurrentOffer_Username = this.CurrentOffer.Username;
}

if (this.Bidding) {
	if (this.Bidding.StartingPrice) {
		lotData.StartingPrice_Amount = this.Bidding.StartingPrice.Amount;
		lotData.StartingPrice_Currency = this.Bidding.StartingPrice.Currency;
	}
	if (this.Bidding.ReservePrice) {
		lotData.ReservePrice_Amount = this.Bidding.ReservePrice.Amount;
		lotData.ReservePrice_Currency = this.Bidding.ReservePrice.Currency;
	}

	for(var i = 0; i < this.Bidding.Bids.length; i++) {
		var bid = this.Bidding.Bids[i];
		var bidData = {
			LotId: documentId,
			Bid_Amount: bid.Amount.Amount,
			Bid_Currency: bid.Amount.Currency,
			Bid_Username: bid.Username,
			Bid_Date: bid.Date
		}
		replicateToLotBids(bidData);
	}
}

replicateToLots(lotData);"
            };
        }

        private static SqlReplicationConfig MembershipUsersReplicationConfig()
        {
            return new SqlReplicationConfig
            {
                Id = "Raven/SqlReplication/Configuration/MembershipUsers",
                Name = "MembershipUsers",
                RavenEntityName = "MembershipUsers",
                PredefinedConnectionStringSettingName = "Replication",
                PerformTableQuatation = true,
                SqlReplicationTables = new List<SqlReplicationTable>
                {
                    new SqlReplicationTable
                    {
                        TableName = "MembershipUsers",
                        DocumentKeyColumn = "Id"
                    }
                },
                Script = @"var userData = {
	Id: documentId,
	Username: this.Username,
	FirstName: this.FirstName,
	LastName: this.LastName,
	PhoneNumber: this.PhoneNumber,
	EmailAddress: this.EmailAddress,
	IsLockedOut: this.IsLockedOut,
	LockedOutOn: this.LockedOutOn,
	IsApproved: this.IsApproved,
	ApprovedOn: this.ApprovedOn,
	InvalidPasswordAttempts: this.InvalidPasswordAttempts,
	CreatedOn: this.CreatedOn,
	LastLoggedInOn: this.LastLoggedInOn,
	Roles: (this.Roles || []).toString(),
	Franchises: (this.Franchises || []).toString()
};

replicateToMembershipUsers(userData);"
            };
        }

        private static SqlReplicationConfig SessionInteractionsReplicationConfig()
        {
            return new SqlReplicationConfig
            {
                Id = "Raven/SqlReplication/Configuration/SessionInteractions",
                Name = "SessionInteractions",
                RavenEntityName = "SessionInteractions",
                PredefinedConnectionStringSettingName = "Replication",
                PerformTableQuatation = true,
                SqlReplicationTables = new List<SqlReplicationTable>
                {
                    new SqlReplicationTable
                    {
                        TableName = "SessionInteractions",
                        DocumentKeyColumn = "Id"
                    },
                    new SqlReplicationTable
                    {
                        TableName = "SessionInteractionLotsViewed",
                        DocumentKeyColumn = "SessionInteractionId"
                    }
                },
                Script = @"replicateToSessionInteractions({
	Id: documentId,
	Username: this.Username,
	CreatedOn: this.CreatedOn,
	SessionId: this.SessionId
});

if (this.LotsViewed) {
	for(var i = 0; i < this.LotsViewed.length; i++) {
		var item = this.LotsViewed[i];

		replicateToSessionInteractionLotsViewed({
			SessionInteractionId: documentId,
			LotId: item.LotId,
			Date: item.Date
		});
	}
}"
            };
        }

        private static SqlReplicationConfig UserLotWatchesReplicationConfig()
        {
            return new SqlReplicationConfig
            {
                Id = "Raven/SqlReplication/Configuration/UserLotWatches",
                Name = "UserLotWatches",
                RavenEntityName = "UserLotWatches",
                PredefinedConnectionStringSettingName = "Replication",
                PerformTableQuatation = true,
                SqlReplicationTables = new List<SqlReplicationTable>
                {
                    new SqlReplicationTable
                    {
                        TableName = "UserLotWatchesLot",
                        DocumentKeyColumn = "UserLotWatchesId"
                    },
                    new SqlReplicationTable
                    {
                        TableName = "UserLotWatches",
                        DocumentKeyColumn = "Id"
                    }
                },
                Script = @"replicateToUserLotWatches({
	Id: documentId,
	MembershipUserId: this.User
});

if (this.WatchedLots) {
	for(var i = 0; i < this.WatchedLots.length; i++) {
		var item = this.WatchedLots[i];

		replicateToUserLotWatchesLot({
			UserLotWatchesId: documentId,
			MembershipUserId: this.User,
			LotId: item
		});
	}
}"
            };
        }

        private static SqlReplicationConfig UserSavedSearchesReplicationConfig()
        {
            return new SqlReplicationConfig
            {
                Id = "Raven/SqlReplication/Configuration/UserSavedSearches",
                Name = "UserSavedSearches",
                RavenEntityName = "UserSavedSearches",
                PredefinedConnectionStringSettingName = "Replication",
                PerformTableQuatation = true,
                SqlReplicationTables = new List<SqlReplicationTable>
                {
                    new SqlReplicationTable
                    {
                        TableName = "UserSavedSearches",
                        DocumentKeyColumn = "Id"
                    },
                    new SqlReplicationTable
                    {
                        TableName = "UserSavedSearchesSearch",
                        DocumentKeyColumn = "SavedSearchId"
                    },
                    new SqlReplicationTable
                    {
                        TableName = "UserSavedSearchesSearchCriteria",
                        DocumentKeyColumn = "SavedSearchId"
                    }
                },
                Script = @"replicateToUserSavedSearches({
	Id: documentId,
	MembershipUserId: this.User
});

if (this.Searches) {
	for(var i = 0; i < this.Searches.length; i++) {
		var search = this.Searches[i];

		replicateToUserSavedSearchesSearch({
			Id: search.Id,
			SavedSearchId: documentId,
			MembershipUserId: this.User,
			Name: search.Name,
			SavedOn: search.SavedOn,
			AlertStatusEnabled: search.AlertStatus.Enabled,
			AlertStatusLastNotifiedAt: search.AlertStatus.LastNotifiedAt
		});

		if (search.Criteria) {
			for(var key in search.Criteria){
				replicateToUserSavedSearchesSearchCriteria({
					SavedSearchId: documentId,
					SearchId: search.Id,
					Key: key,
					Value: search.Criteria[key]
				});
			}
		}
	}
}"
            };
        }

        private static SqlReplicationConfig UserSavedValuationsReplicationConfig()
        {
            return new SqlReplicationConfig
            {
                Id = "Raven/SqlReplication/Configuration/UserSavedValuations",
                Name = "UserSavedValuations",
                RavenEntityName = "UserSavedValuations",
                PredefinedConnectionStringSettingName = "Replication",
                PerformTableQuatation = true,
                SqlReplicationTables = new List<SqlReplicationTable>
                {
                    new SqlReplicationTable
                    {
                        TableName = "UserSavedValuations",
                        DocumentKeyColumn = "Id"
                    },
                    new SqlReplicationTable
                    {
                        TableName = "UserSavedValuationsValuation",
                        DocumentKeyColumn = "SavedValuationId"
                    }
                },
                Script = @"replicateToUserSavedValuations({
	Id: documentId,
	MembershipUserId: this.User
});

if (this.Valuations) {
	for(var i = 0; i < this.Valuations.length; i++) {
		var valuation = this.Valuations[i];

		replicateToUserSavedValuationsValuation({
			Id: valuation.Id,
			SavedValuationId: documentId,
			MembershipUserId: this.User,
			CreatedOn: valuation.CreatedOn,
			Registration: valuation.Registration,
			Mileage: valuation.Mileage,
			RetailValuation: valuation.Valuation.Retail.Amount,
			CleanValuation: valuation.Valuation.Clean.Amount,
			AverageValuation: valuation.Valuation.Average.Amount,
			BelowValuation: valuation.Valuation.Below.Amount
		});
	}
}"
            };
        }

        private static SqlReplicationConfig VehiclesReplicationConfig()
        {
            return new SqlReplicationConfig
            {
                Id = "Raven/SqlReplication/Configuration/Vehicles",
                Name = "Vehicles",
                RavenEntityName = "Vehicles",
                PredefinedConnectionStringSettingName = "Replication",
                PerformTableQuatation = true,
                SqlReplicationTables = new List<SqlReplicationTable>
                {
                    new SqlReplicationTable
                    {
                        TableName = "Vehicles",
                        DocumentKeyColumn = "Id"
                    }
                },
                Script = @"
var vehicleData = {
	Id: documentId,
	CapId: this.CapId,
	Make: this.Make,
	Model: this.Model,
	Derivative: this.Derivative,
	Mileage: this.Mileage,
	FuelType: this.FuelType,
	Transmission: this.Transmission,
	Registration_Plate: !this.Registration ? null : this.Registration.Plate,
	Registration_Date: !this.Registration ? null : this.Registration.Date,
	VatStatus: this.VatStatus,
	VehicleType: this.VehicleType,
	MetaData_Carweb_Equipment_Enriched: this['@metadata']['Carweb-Equipment-Enriched'],
	MetaData_Carweb_Last_Enriched_At: this['@metadata']['Carweb-Last-Enriched-At'],
	MetaData_Carweb_Next_Enrich_Attempt_After: this['@metadata']['Carweb-Next-Enrich-Attempt-After'],
	MetaData_Carweb_Enrich_Attempts: this['@metadata']['Carweb-Enrich-Attempts'],
	MetaData_Cap_Pricing_Enriched: this['@metadata']['Cap-Pricing-Enriched']
};

if (this.Specification && this.Specification.Environment) {
	vehicleData.Specification_Environment_Co2 = this.Specification.Environment.Co2;
}
if (this.Specification && this.Specification.Economy) {
	vehicleData.Specification_Economy_Urban = this.Specification.Economy.Urban;
	vehicleData.Specification_Economy_ExtraUrban = this.Specification.Economy.ExtraUrban;
	vehicleData.Specification_Economy_Combined = this.Specification.Economy.Combined;
}

replicateToVehicles(vehicleData);
"
            };
        }
    }
}
