using System;
using System.Collections.Generic;
using Raven.Abstractions.Data;
using Raven.Client;

namespace Sfs.EStore.AccountDocStoreInitialisation.Commands
{
    public class CreateFacets : ICommand
    {
        private readonly IDocumentStore _docStore;

        public CreateFacets(IDocumentStore docStore)
        {
            _docStore = docStore;
        }

        public void Execute()
        {
            var filtersFacets = new List<Facet>
            {
                new Facet {Name = "VehicleType", DisplayName = "VehicleType"},
                new Facet {Name = "SaleTitle", DisplayName = "SaleTitle"},
                new Facet {Name = "Category", DisplayName = "Category"},
                new Facet {Name = "Make", DisplayName = "Make"},
                new Facet {Name = "Model", DisplayName = "Model"},
                new Facet {Name = "Derivative", DisplayName = "Derivative"},
                new Facet {Name = "FuelType", DisplayName = "FuelType"},
                new Facet {Name = "Engine", DisplayName = "Engine"},
                new Facet {Name = "TransmissionSimple", DisplayName = "TransmissionSimple"},
                new Facet {Name = "RegistrationYaP", DisplayName = "RegistrationYaP"},
                new Facet {Name = "Source", DisplayName = "Source"},
                new Facet
                {
                    Name = "Mileage_Range",
                    DisplayName = "Mileage_Range",
                    Mode = FacetMode.Ranges,
                    Ranges = new List<string>
                    {
                        "[NULL TO Ix100]",
                        "[NULL TO Ix5000]",
                        "[NULL TO Ix10000]",
                        "[NULL TO Ix20000]",
                        "[NULL TO Ix40000]",
                        "[NULL TO Ix60000]",
                        "[NULL TO Ix80000]",
                        "[NULL TO Ix100000]",
                        "[Ix100000 TO NULL]"
                    }
                },
                new Facet {Name = "ExteriorColour", DisplayName = "ExteriorColour"},
                new Facet {Name = "AuctionStatus", DisplayName = "AuctionStatus"},
            };

            var vehicleTypeFacets = new List<Facet>
            {
                new Facet {Name = "VehicleType", DisplayName = "VehicleType"}
            };

            var makeFacets = new List<Facet>
            {
                new Facet {Name = "Make", DisplayName = "Make"}
            };

            var modelFacets = new List<Facet>
            {
                new Facet {Name = "Model", DisplayName = "Model"}
            };

            var equipmentFacets = new List<Facet>
            {
                new Facet {Name = "Equipment", DisplayName = "Equipment"}
            };

            using (var session = _docStore.OpenSession())
            {
                StoreFacet(session, "VehicleTypes", vehicleTypeFacets);
                StoreFacet(session, "Makes", makeFacets);
                StoreFacet(session, "Models", modelFacets);
                StoreFacet(session, "LotFacets", filtersFacets);
                StoreFacet(session, "Equipment", equipmentFacets);
                session.SaveChanges();
            }
        }

        private static void StoreFacet(IDocumentSession session, string name, List<Facet> facets)
        {
            var facetSetup = new FacetSetup { Id = "Facets/" + name, Facets = facets };
            Console.WriteLine("  " + facetSetup.Id);
            session.Store(facetSetup);
        }
    }
}