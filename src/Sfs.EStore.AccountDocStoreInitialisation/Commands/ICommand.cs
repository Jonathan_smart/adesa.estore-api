namespace Sfs.EStore.AccountDocStoreInitialisation.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}