using Raven.Abstractions.Data;
using Raven.Client;

namespace Sfs.EStore.AccountDocStoreInitialisation.Commands
{
    public class PatchReQueryCarwebDataCommand : ICommand
    {
        private readonly IDocumentStore _docStore;

        public PatchReQueryCarwebDataCommand(IDocumentStore docStore)
        {
            _docStore = docStore;
        }

        public void Execute()
        {
            _docStore.DatabaseCommands.UpdateByIndex(
                "Raven/DocumentsByEntityName",
                new IndexQuery {Query = "Tag:Vehicles"},
                new ScriptedPatchRequest
                {
                    Script = @"
if (this['@metadata']['Carweb-Equipment-Enriched'] && 
        (!this.Specification || !this.Specification.Environment || !this.Specification.Environment.Co2)) {
    this['@metadata']['Carweb-Next-Enrich-Attempt-After'] = '2015-01-02T00:00:00.0000000Z';
}"
                });
        }
    }
}