using System;
using Raven.Abstractions.Data;
using Raven.Client;

namespace Sfs.EStore.AccountDocStoreInitialisation.Commands
{
    [Obsolete("Patch was run when v2.18.0-41 was deployed")]
    public class PatchListingVehicleConditionToPreppedCommand : ICommand
    {
        private readonly IDocumentStore _docStore;

        public PatchListingVehicleConditionToPreppedCommand(IDocumentStore docStore)
        {
            _docStore = docStore;
        }

        public void Execute()
        {
            _docStore.DatabaseCommands.UpdateByIndex(
                "Raven/DocumentsByEntityName",
                new IndexQuery {Query = "Tag:Lots"},
                new ScriptedPatchRequest
                {
                    Script = @"
                            this.Condition = 'Prepped';
                        "
                });
        }
    }
}