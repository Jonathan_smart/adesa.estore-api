﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;
using log4net.Config;
using PowerArgs;
using Raven.Client.Document;
using Sfs.EStore.AccountDocStoreInitialisation.Commands;

namespace Sfs.EStore.AccountDocStoreInitialisation
{
    public class AppArgs
    {
        [ArgShortcut("s")]
        public string Server { get; set; }
        [ArgShortcut("c")]
        public string Command { get; set; }
        [ArgShortcut("d")]
        public string[] Databases { get; set; }
        [ArgShortcut("q")]
        public bool Quiet { get; set; }
    }

    internal class Program
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Program));

        private static readonly Dictionary<string, CommandDescriptor> Commands = new Dictionary<string, CommandDescriptor>
        {
            {
                "IDX",
                new CommandDescriptor("Update indexes",
                    docStore => new UpdateIndexesAndTransformersCommand(docStore))
            },
            {
                "FACETS",
                new CommandDescriptor("Recreate facets",
                    docStore => new CreateFacets(docStore))
            },
            {
                "REPLICATION",
                new CommandDescriptor("Create SQL Replication documents",
                    docStore => new CreateSqlReplicationDocumentsCommand(docStore))
            }
        };

        private static void Main(string[] args)
        {
            XmlConfigurator.ConfigureAndWatch(new FileInfo(Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory, "log4net.config")));

            string[] databases;

            AppArgs appArgs;

            try
            {
                appArgs = Args.Parse<AppArgs>(args);
            }
            catch (ArgException ex)
            {
                Logger.InfoFormat(ex.Message);
                Logger.Info(ArgUsage.GenerateUsageFromTemplate<AppArgs>());
                return;
            }

            var server = !string.IsNullOrWhiteSpace(appArgs.Server) ? new Uri(appArgs.Server) : GetServer();
            var command = !string.IsNullOrWhiteSpace(appArgs.Command) ? Commands[appArgs.Command] : GetCommand();

            if (appArgs.Databases != null && appArgs.Databases.Length > 0)
                databases = appArgs.Databases;
            else
                databases = GetDatabases(server).ToArray();

            Console.WriteLine();
            Console.WriteLine();
            Logger.InfoFormat("{0}: {1}", "Server".PadRight(15), server);
            Logger.InfoFormat("{0}: {1}", "Command".PadRight(15), command.Name);
            Logger.InfoFormat("{0}: {1}", "Databases".PadRight(15), string.Join(", ", databases));
            Console.WriteLine();

            if (!appArgs.Quiet)
            {
                Console.WriteLine();
                Console.Write("Press <enter> to continue");
                Console.ReadLine();
            }

            foreach (var databaseName in databases)
            {
                Console.WriteLine();
                Logger.InfoFormat("Executing command against {0}", databaseName);
                var docStore = new DocumentStore
                {
                    Url = server.AbsoluteUri,
                    DefaultDatabase = databaseName
                }.Initialize();

                try
                {
                    command.ExecuteAgainst(docStore);
                }
                catch (Exception ex)
                {
                    Logger.Error(new
                    {
                        server.AbsoluteUri,
                        command.Name,
                        databases,
                    }, ex);

                    if (!appArgs.Quiet)
                    {
                        Console.WriteLine();
                        Console.Write("Continue? (yN): ");

                        if (!"y".Equals(Console.ReadLine(), StringComparison.OrdinalIgnoreCase))
                        {
                            break;
                        }
                    }
                }
            }

            Logger.InfoFormat("--- DONE ---");

            if (!appArgs.Quiet)
                Console.ReadLine();
        }

        private static Uri GetServer()
        {
            const string defaultRavenUrl = "http://ravendb01_staging:8080/";

            Console.Write("Raven server ({0}):", defaultRavenUrl);
            var server = Console.ReadLine();
            if (string.IsNullOrEmpty(server)) server = defaultRavenUrl;

            return new Uri(server);
        }

        private static CommandDescriptor GetCommand()
        {
            Console.WriteLine();
            Console.WriteLine("Available commands...");
            Console.WriteLine();
            
            foreach (var kv in Commands)
            {

                var num = Commands.Keys.ToList().IndexOf(kv.Key) + 1;
                Console.WriteLine("{0}) {1} - {2}", num, kv.Value.Name, kv.Key);
            }

            Console.WriteLine();
            Console.Write("Select a command (1): ");
            var selectedIndex = Console.ReadLine();
            
            if (string.IsNullOrEmpty(selectedIndex))
                return Commands.First().Value;

            int index;
            return (!int.TryParse(selectedIndex, out index) || index < 0 || index > Commands.Count)
                ? GetCommand()
                : Commands[Commands.Keys.ToList()[index - 1]];
        }

        private static IEnumerable<string> GetDatabases(Uri server)
        {
            var stores = new DocumentStores(server.AbsoluteUri);

            foreach (var accountStore in stores.ApiAccounts())
            {
                Console.Write("Execute command against {0}? (y/N): ", accountStore.DefaultDatabase);
                
                if ("y".Equals(Console.ReadLine(), StringComparison.OrdinalIgnoreCase))
                    yield return accountStore.DefaultDatabase;
            }
        }
    }
}
