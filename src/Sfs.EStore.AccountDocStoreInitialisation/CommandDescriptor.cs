using System;
using Raven.Client;
using Sfs.EStore.AccountDocStoreInitialisation.Commands;

namespace Sfs.EStore.AccountDocStoreInitialisation
{
    internal class CommandDescriptor
    {
        public CommandDescriptor(string name, Func<IDocumentStore, ICommand> factory)
        {
            Name = name;
            _factory = factory;
        }

        public string Name { get; private set; }
        private readonly Func<IDocumentStore, ICommand> _factory;

        public void ExecuteAgainst(IDocumentStore docStore)
        {
            _factory(docStore).Execute();
        }
    }
}