﻿using System.Threading;
using Ninject;
using Ninject.Modules;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api
{
    public class AuthNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IApiApplicationPrincipal>().ToMethod(ctx => Thread.CurrentPrincipal as IApiApplicationPrincipal);

            Bind<MembershipUser>().ToMethod(ctx =>
            {
                var principal = ctx.Kernel.Get<IApiApplicationPrincipal>();
                if (string.IsNullOrEmpty(principal.MembershipUser.Identity.Id)) return null;

                using (var session = WebApiApplication.GetAccountDocumentStoreFor(principal.Identity.Advanced.DatabaseName).OpenSession())
                {
                    return session.Load<MembershipUser>(principal.MembershipUser.Identity.Id);
                }
            });
        }
    }
}