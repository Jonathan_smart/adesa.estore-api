﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Http;
using Mandrill;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using Raven.Client;
using Sfs.EStore.Api;
using Sfs.EStore.Api.Authentication;
using log4net;
using Sfs.Core;
using Sfs.EStore.Api.Calculators;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.NavShipToAddressService;
using Sfs.EStore.Api.NavVehicle_Interests;
using Sfs.EStore.Api.NavWebIntegrationService;
using Currency = Sfs.EStore.Api.Entities.Currency;
using Money = Sfs.EStore.Api.Entities.Money;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace Sfs.EStore.Api
{
    public class NinjectWebCommon
    {
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        private static IKernel _kernel;

        public static IKernel Kernel { get { return _kernel; } }

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);
        }

        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            if (_kernel != null)
                throw new InvalidOperationException("CreateKernal can only be called once");

            _kernel = new StandardKernel();
            _kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            _kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            RegisterServices(_kernel);

            GlobalConfiguration.Configuration.DependencyResolver = new NinjectResolver(_kernel);
            DomainEvent.Container = _kernel;

            return _kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Load(Assembly.GetExecutingAssembly());
            kernel.Bind<ILog>().ToMethod(context => LogManager.GetLogger(context.Request.Target.Type)).InTransientScope();
            kernel.Bind<MandrillApi>().ToSelf()
                .WithConstructorArgument("apiKey", ConfigurationManager.AppSettings["Mandrill/ApiKey"]);


            kernel.Bind<IDocumentStore>()
                .ToMethod(context =>
                              {
                                  var principal = (IApiApplicationPrincipal)Thread.CurrentPrincipal;
                                  return
                                      WebApiApplication.GetAccountDocumentStoreFor(
                                          principal.Identity.Advanced.DatabaseName);
                              })
                .InRequestScope();

            kernel.Bind<FuelCostCalculator>()
                .ToMethod(context => new FuelCostCalculator(new StaticFuelCosts(new Dictionary<FuelTypes, Money>
                {
                    {FuelTypes.Diesel, new Money(decimal.Parse(ConfigurationManager.AppSettings["FuelCostCalculator/FuelCosts/Diesel"]), Currency.GBP)},
                    {FuelTypes.Petrol, new Money(decimal.Parse(ConfigurationManager.AppSettings["FuelCostCalculator/FuelCosts/Petrol"]), Currency.GBP)}
                }).CostOf))
                .InSingletonScope();

            kernel.Bind<SalesChannel>()
                .ToMethod(ctx =>
                {
                    var principal = ctx.Kernel.Get<IApiApplicationPrincipal>();
                    string value;
                    principal.Identity.Advanced.IntegrationSettings.TryGetValue("Sfs/SalesChannel", out value);
                    return new SalesChannel(int.Parse(value ?? "0"));
                })
                .InRequestScope();
            kernel.Bind<BusinessUnit>()
                .ToMethod(ctx =>
                {
                    var principal = ctx.Kernel.Get<IApiApplicationPrincipal>();
                    string value;
                    principal.Identity.Advanced.IntegrationSettings.TryGetValue("Sfs/BusinessUnit", out value);

                    return new BusinessUnit(value);
                })
                .InRequestScope();
            kernel.Bind<BasketLimit>()
                .ToMethod(ctx =>
                {
                    var principal = ctx.Kernel.Get<IApiApplicationPrincipal>();
                    string value;
                    principal.Identity.Advanced.IntegrationSettings.TryGetValue("Sfs/BasketLimit", out value);

                    return new BasketLimit(int.Parse(value ?? "0"));
                })
                .InRequestScope();

            kernel.Bind<FgaFinanceProxyBaseAddress>()
                .ToConstant(new FgaFinanceProxyBaseAddress(ConfigurationManager.AppSettings["Fga/FinanceProxy/BaseAddress"]));
        }
    }

    public class NavisionWebServicesNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<WebIntegration>().ToMethod(s => new WebIntegration { UseDefaultCredentials = true });
            Kernel.Bind<Ship_to_Address_Service>().ToMethod(s => new Ship_to_Address_Service { UseDefaultCredentials = true });
            Kernel.Bind<Vehicle_Interests_Service>().ToMethod(s => new Vehicle_Interests_Service { UseDefaultCredentials = true });
        }
    }
}