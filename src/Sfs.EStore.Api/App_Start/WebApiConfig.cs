﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;
using Ninject;

namespace Sfs.EStore.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config, IKernel kernel)
        {
            //RaygunWebApiClient.Attach(config);

            config.MapHttpAttributeRoutes();
            
            config.Routes.MapHttpRoute(
                name: "LotSubRouteApi",
                routeTemplate: "lots/{lotnumber}/{controller}"
                );

            config.Routes.MapHttpRoute(
                name: "ListingSubRouteApi",
                routeTemplate: "listings/{listingId}/{controller}"
                );

            config.Routes.MapHttpRoute("Membership_Put", "membership/{action}", new { controller = "membership", action = "putupdate" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            config.Routes.MapHttpRoute("Membership_Upsert", "membership/{id}/upsert",
                new { controller = "membership", action = "upsert" },
                new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );

            config.Filters.Add(new AuthorizeAttribute());
            config.Filters.Add(new ValidationFilterAttribute());
        }
    }
}
