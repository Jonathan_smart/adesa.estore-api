﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Controllers
{
    public class RecentlyViewedGetModel
    {
        public string SessionId { get; set; }
        public int? Top { get; set; }
        public string[] LimitToRoles { get; set; }
    }

    public class RecentlyViewedController : RavenEStoreController
    {
        public Task<HttpResponseMessage> Get([FromUri]RecentlyViewedGetModel model)
        {
            return Task.Factory.StartNew(() => GetRecentlyViewed(model));
        }

        private HttpResponseMessage GetRecentlyViewed(RecentlyViewedGetModel model)
        {
            var user = User.MembershipUser.Identity;
            if (!user.IsAuthenticated && string.IsNullOrEmpty(model.SessionId))
                return Request.CreateResponse(HttpStatusCode.OK, new ListingSummaryViewModel[0]);

            var query = Session.Query<SessionInteractions>();
            query = User.MembershipUser.Identity.IsAuthenticated
                        ? query.Where(x => x.Username == user.Name)
                        : query.Where(x => x.SessionId == model.SessionId);

            var sessionInteractionLots = query
                .OrderByDescending(x => x.CreatedOn)
                .Take(2)
                .ToListAsync()
                .Result
                .SelectMany(x => x.LotsViewed.Select(l => l.LotId));

            var sessionLots = Session
                .Include<Lot>(x => x.VehicleId)
                .LoadAsync<Lot>(sessionInteractionLots.Distinct().ToList())
                .Result;

            var lots = sessionLots.Where(x => x.Status == LotStatusTypes.Active);

            if (model.LimitToRoles != null)
                lots = lots.Where(x => x.VisibilityLimitedTo.Any(y => y.In(model.LimitToRoles)));

            var results = lots
                .Take(model.Top ?? 1)
                .Select(x => ListingSummaryViewModel.Map(x,
                    Session.LoadAsync<Vehicle>(x.VehicleId).Result,
                    Session.LoadAsync<VehicleData>(x.VehicleId + "/Data").Result))
                .ToArray();

            return Request.CreateResponse(HttpStatusCode.OK,
                                          results);
        }
    }
}
