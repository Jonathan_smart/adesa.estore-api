using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Controllers
{
    [MembershipUserAuthorise]
    public class SavedSearchesController : RavenEStoreController
    {
        public Task<HttpResponseMessage> Get()
        {
            return Task.Factory.StartNew(() => Request.CreateResponse(HttpStatusCode.OK,
                                                                      (GetUserSavedSearch() ??
                                                                       new UserSavedSearch(
                                                                           User.MembershipUser.Identity.Id))
                                                                          .Searches));
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Get(Guid id)
        {
            return Task.Factory.StartNew(() =>
            {
                var savedSearch = GetOrCreateUserSavedSearch()[id];

                if (savedSearch != null)
                    return Request.CreateResponse(HttpStatusCode.OK, savedSearch);

                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                                   string.Format("Cannot find search with id {0}", id));
            });
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Delete(Guid id)
        {
            return Task.Factory.StartNew(() =>
                                             {
                                                 var savedSearches = GetUserSavedSearch();

                                                 if (savedSearches != null)
                                                     savedSearches.RemoveSearch(id);

                                                 return Request.CreateResponse(HttpStatusCode.OK);
            });
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Post(SavedSearchesPostModel search)
        {
            return Task.Factory.StartNew(() =>
                                             {
                                                 var savedSearches = GetOrCreateUserSavedSearch();

                                                 var newSearch = savedSearches.AddSearch(search.ToDictionary(), search.Name);

                                                 if (search.AlertStatus != null && search.AlertStatus.Enabled.HasValue)
                                                 {
                                                     if (search.AlertStatus.Enabled.Value) newSearch.EnableAlert();
                                                     else newSearch.DisableAlert();
                                                 }

                                                 return Request.CreateResponse(HttpStatusCode.OK, newSearch);
                                             });
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Put(Guid? id, SavedSearchesPostModel search)
        {
            return Task.Factory.StartNew(() =>
                                             {
                                                 var savedSearches = GetOrCreateUserSavedSearch();
                                                 SavedSearch savedSearch;

                                                 if (id.HasValue)
                                                 {
                                                     savedSearch = savedSearches[id.Value];

                                                     if (savedSearch != null)
                                                         search.CopyTo(savedSearch);
                                                     else
                                                         savedSearch = savedSearches.AddSearch(search.ToDictionary(), search.Name);
                                                 }
                                                 else
                                                     savedSearch = savedSearches.AddSearch(search.ToDictionary(), search.Name);

                                                 if (search.AlertStatus != null && search.AlertStatus.Enabled.HasValue)
                                                 {
                                                     if (search.AlertStatus.Enabled.Value) savedSearch.EnableAlert();
                                                     else savedSearch.DisableAlert();
                                                 }

                                                 return Request.CreateResponse(HttpStatusCode.OK, savedSearch);
                                             });
        }

        private UserSavedSearch GetOrCreateUserSavedSearch()
        {
            var savedSearches = GetUserSavedSearch();

            if (savedSearches == null)
            {
                savedSearches = new UserSavedSearch(User.MembershipUser.Identity.Id);
                Session.StoreAsync(savedSearches).Wait();
            }

            return savedSearches;
        }

        private UserSavedSearch GetUserSavedSearch()
        {
            var savedSearch =
                Session.LoadAsync<UserSavedSearch>(UserSavedSearch.IdFrom(User.MembershipUser.Identity.Id))
                    .Result;

            return savedSearch;
        }
    }
}