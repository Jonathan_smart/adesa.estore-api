using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using Ninject;
using Raven.Client;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Commands;
using log4net;

namespace Sfs.EStore.Api.Controllers
{
    public class RavenEStoreController : ApiController
    {
        public RavenEStoreController()
        {
            _user = new Lazy<IApiApplicationPrincipal>(() => Thread.CurrentPrincipal as IApiApplicationPrincipal);
        }

        private ILog _logger = NullLogger.Instance();
        [Inject]
        public ILog Logger
        {
            get { return _logger; }
            set { _logger = value; }
        }

        protected readonly Lazy<IApiApplicationPrincipal> _user;
        public new IApiApplicationPrincipal User { get { return _user.Value; } }

        public IAsyncDocumentSession Session { get; set; }

        public Action<ICommand> AlternativeExecuteCommand { get; set; }
        public Func<ICommand, object> AlternativeExecuteCommandWithResult { get; set; }

        [NonAction]
        public void ExecuteCommand(ICommand cmd)
        {
            if (AlternativeExecuteCommand != null)
                AlternativeExecuteCommand(cmd);
            else
                DefaultExecuteCommand(cmd);
        }

        [NonAction]
        public TResult ExecuteCommand<TResult>(ICommand<TResult> cmd)
        {
            if (AlternativeExecuteCommandWithResult != null)
                return (TResult)AlternativeExecuteCommandWithResult(cmd);
            return DefaultExecuteCommand(cmd);
        }

        protected void DefaultExecuteCommand(ICommand cmd)
        {
            cmd.Session = Session;
            cmd.Execute();
        }

        protected TResult DefaultExecuteCommand<TResult>(ICommand<TResult> cmd)
        {
            ExecuteCommand((ICommand)cmd);
            return cmd.Result;
        }

        public override Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext,
                                                               CancellationToken cancellationToken)
        {
            if (!User.Identity.IsAuthenticated)
                return base.ExecuteAsync(controllerContext, cancellationToken);

            using (Session = WebApiApplication.GetAccountDocumentStoreFor(User.Identity.Advanced.DatabaseName).OpenAsyncSession())
            {
                var result = base.ExecuteAsync(controllerContext, cancellationToken);

                return result.ContinueWith(x =>
                {
                    if (x.Exception == null)
                        Session.SaveChangesAsync().Wait(cancellationToken);

                    return x.Result;
                }, cancellationToken);
            }
        }
    }
}