using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using log4net;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.Core;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Raven.Indexes;
using Sfs.EStore.Api.Raven.Transformers;
using Sfs.EStore.Api.Data;
using Sfs.EStore.Api.NavVehicle_Interests;
using Type = Sfs.EStore.Api.NavVehicle_Interests.Type;
using System.Data;

namespace Sfs.EStore.Api.Controllers
{
    [MembershipUserAuthorise]
    public class WatchesController : RavenEStoreController
    {
        private readonly BusinessUnit _businessUnit;
        private readonly Vehicle_Interests_Service _client;
        private readonly SalesChannel _salesChannel;
        private readonly ILog _logger = LogManager.GetLogger(typeof(ExceptionFilter));

        public WatchesController(BusinessUnit businessUnit, SalesChannel salesChannel, Vehicle_Interests_Service client)
        {
            _businessUnit = businessUnit;
            _salesChannel = salesChannel;
            _client = client;
        }
        public class GetModel
        {
            public string Q { get; set; }
            public int? Top { get; set; }
            public int? Skip { get; set; }
        }

        public Task<HttpResponseMessage> Get([FromUri] GetModel query)
        {
            RavenQueryStatistics stats;
            var dbQuery = Session.Query<UserWatchedLot_Search.ReduceResult, UserWatchedLot_Search>()
                .Statistics(out stats)
                .Where(x => x.User == User.MembershipUser.Identity.Id);

            if (!string.IsNullOrWhiteSpace(query.Q))
                dbQuery = dbQuery.Search(x => x.Query, query.Q, options: SearchOptions.And);

            var result = dbQuery
                .TransformWith<LotIdToListingSummary, ListingSummaryViewModel>()
                .Skip(query.Skip ?? 0)
                .Take(query.Top ?? 10);

            return result.ToListAsyncWithTotalResultsHeader(Request, stats);
        }

        public Task<WatchStatusModel> Get(int lotNumber)
        {
            var lotId = Lot.IdFromLotNumber(lotNumber);
            return Session.LoadAsync<UserLotWatches>(UserLotWatches.GenerateId(User.MembershipUser.Identity.Id))
                .ContinueWith(t =>
                {
                    var watched = t.Result != null && t.Result.WatchedLots.Any(l => l == lotId);

                    return new WatchStatusModel { Watched = watched };
                });
        }

        public class WatchStatusModel
        {
            public bool Watched { get; set; }
        }

        public Task Put(int lotNumber)
        {
            AddVehicleInterest(lotNumber);

            return UpdateUserWatches(watches => watches.Add(lotNumber));
        }

        public Task Delete(int lotNumber)
        {
            return UpdateUserWatches(watches => watches.Remove(lotNumber));
        }

        private Task UpdateUserWatches(Action<UserLotWatches> update)
        {
            return Session
                .LoadAsync<UserLotWatches>(UserLotWatches.GenerateId(User.MembershipUser.Identity.Id))
                .ContinueWith(t =>
                {

                    var userWatches = t.Result ?? new UserLotWatches(User.MembershipUser.Identity.Id);
                    update(userWatches);
                    Session.StoreAsync(userWatches).Wait();
                });
        }

        private void AddVehicleInterest(int lotNumber)
        {

            var lot = Session.Include<Lot>(x => x.VehicleId).LoadAsync<Lot>(Lot.IdFromLotNumber(lotNumber)).Result;
            var vehicle = Session.LoadAsync<Vehicle>(lot.VehicleId).Result;

            if (vehicle.Registration.Plate.Length <= 0) return;

            using (var ctx = new NavisionContext())
            {
                using (ctx.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    var serialNo =
                        ctx.Set<Data.SerialNoInformation>()
                            .Where(b => b.ItemNo == "VEHICLE" & b.RegistrationNo == vehicle.Registration.Plate)
                            .Select(n => n.SerialNo).FirstOrDefault();

                    var contactNo = ctx.Set<Data.ContactUsername>().Where(x => x.SalesChannelNo == _salesChannel).FirstOrDefault(x => x.Username == User.MembershipUser.Identity.Name);

                    if (serialNo == null || contactNo == null) return;

                    var navInterest = new Vehicle_Interests
                    {
                        Type = Type.Web_Interest,
                        TypeSpecified = true,
                        Customer_No = string.Empty,
                        Contact_No = contactNo.ContactNo,
                        Sales_Person_Contact_No = string.Empty,
                        Serial_No = serialNo,
                        Date_Created = DateTime.UtcNow,
                        Date_CreatedSpecified = true,
                        Note = "Estore interest raised from customer saved vehicle.",
                        Value = 0,
                        ValueSpecified = false,
                        Global_Dimension_1_Code = _businessUnit.ToString()
                    };

                    try
                    {
                        _client.Create(ref navInterest);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex.Message);
                    }
                }
            }
        }
    }
}