using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Raven.Client;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Controllers
{
    public class UsersController : RavenEStoreController
    {
        public Task<HttpResponseMessage> Patch(string id, UserPatchModel bound)
        {
            return Task.Factory.StartNew(() => PatchUser(id, bound));
        }

        private HttpResponseMessage PatchUser(string username, UserPatchModel bound)
        {
            if (bound == null)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "No patch data specified");

            var user = Session.Query<MembershipUser>()
                .FirstOrDefaultAsync(x => x.Username == username)
                .Result;

            if (user == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    string.Format("User '{0}' not found.", username));

            if (bound.Role != null)
                user.Roles = ApplyArrayAction(user.Roles, bound.RoleAction, bound.Role);

            if (bound.Franchise != null)
                user.Franchises = ApplyArrayAction(user.Franchises, bound.FranchiseAction, bound.Franchise);

            if (bound.Password != null)
                user.SetPassword(bound.Password);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private string[] ApplyArrayAction(string[] source, ArrayPatchActions patchAction, string[] actionItems)
        {
            if (patchAction == ArrayPatchActions.Set)
                return actionItems;

            if (patchAction == ArrayPatchActions.Append)
                return source.Concat(actionItems).ToArray();

            if (patchAction == ArrayPatchActions.Add)
                return source.Concat(actionItems.Where(x => !source.Any(s => s.Equals(x)))).ToArray();

            if (patchAction == ArrayPatchActions.Remove)
            {
                var list = source.ToList();
                list.RemoveAll(x => actionItems.Any(i => i == x));
                return list.ToArray();
            }

            throw new NotSupportedException(string.Format("Action {0} is not supported", patchAction));
        }
    }
}