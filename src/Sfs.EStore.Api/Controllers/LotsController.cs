﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client.Linq;
using Sfs.EStore.Api.Calculators;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties;
using System.Xml.Serialization;
using System.IO;

namespace Sfs.EStore.Api.Controllers
{
    public class LotsController : RavenEStoreController
    {
        private readonly FuelCostCalculator _fuelCostCalculator;
        private readonly IVehicleEnricher[] _putVehicleEnrichers;
        private readonly IVehicleEnricher[] _getVehicleEnrichers;

        public LotsController(IGetVehicleEnricher[] getVehicleEnrichers = null, IPutVehicleEnricher[] putVehicleEnrichers = null,
            FuelCostCalculator fuelCostCalculator = null)
        {
            _fuelCostCalculator = fuelCostCalculator;
            _getVehicleEnrichers = (getVehicleEnrichers ?? new IVehicleEnricher[0]).ToArray();
            _putVehicleEnrichers = (putVehicleEnrichers ?? new IVehicleEnricher[0]).ToArray();
        }

        public class GetOptions
        {
            public string[] LimitToRoles { get; set; }
            public string Include { get; set; }
        }
        public Task<HttpResponseMessage> Get(int id, [FromUri]GetOptions options)
        {
            options = options ?? new GetOptions();

            var usernameObfuscator = new UsernameObfuscator(User.MembershipUser.Identity);

            return Session
                .Include<Lot>(x => x.VehicleId)
                .Include(v => v.VehicleId + "/Data")
                .LoadAsync<Lot>(Lot.IdFromLotNumber(id))
                .ContinueWith(t =>
                                  {
                                      var lot = t.Result;

                                      if (lot == null)
                                          throw new HttpResponseException(HttpStatusCode.NotFound);

                                      var vehicle = Session.LoadAsync<Vehicle>(lot.VehicleId).Result;
                                      var vehicleData = Session.LoadAsync<VehicleData>(vehicle.Id + "/Data").Result;

                                      if (options.LimitToRoles != null && options.LimitToRoles.Any() && !lot.VisibilityLimitedTo.Intersect(options.LimitToRoles).Any())
                                          return Request.CreateErrorResponse(HttpStatusCode.Forbidden,
                                              "Access is restricted to " + id);

                                      try
                                      {
                                          new CompositeVehicleEnricher(_getVehicleEnrichers)
                                              .Enrich(vehicle, vehicleData, Session.Advanced.GetMetadataFor(vehicle));
                                      }
                                      catch (Exception ex)
                                      {
                                          Logger.Error(
                                              string.Format("vehicle enrichment failed for tenant: {0}, with implementation: {1}",
                                                            User.Identity.Name, _getVehicleEnrichers.GetType().FullName),
                                              ex);
                                      }

                                      var mappingOptions =
                                          new ListingViewModelMappingOptions(User.MembershipUser.Identity,
                                              usernameObfuscator)
                                          {
                                              FuelCostCalculator = _fuelCostCalculator,
                                              Include = (options.Include ?? "").Split(new[] { ',' },
                                                  StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim())
                                          };

                                      var model = ListingViewModel.Map(lot, vehicle, vehicleData, mappingOptions);

                                      return Request.CreateResponse(HttpStatusCode.OK, model);
                                  });
        }

        public Task<HttpResponseMessage> Put([FromUri]int? id, [FromBody]LotPutModel bound)
        {
            if (bound != null && id.HasValue)
                bound.LotId = Lot.IdFromLotNumber(id.Value);

            return Task.Factory.StartNew(() => PutLot(bound));
        }

        public Task<HttpResponseMessage> Put([FromBody]LotPutModel bound)
        {
            return Task.Factory.StartNew(() => PutLot(bound));
        }

        private HttpResponseMessage PutLot(LotPutModel bound)
        {
           // Logger.Debug(Options.SerializeObject(bound));

            try
            {
                if (bound == null)
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "Message content missing or not in the correct format");

                return ExecuteCommand(new PutLotCommand(_putVehicleEnrichers,
                    Request,
                    ModelState)
                {
                    Model = bound
                });
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden, ex.Message);
            }
        }
    

        public Task<HttpResponseMessage> Delete(int? id)
        {
            if (!id.HasValue)
            {
                return Task.Factory.StartNew(() =>
                                             Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                                                                         "id not specified"));
            }

            return Task.Factory.StartNew(() => DeleteLot(Lot.IdFromLotNumber(id.Value)));
        }

        private HttpResponseMessage DeleteLot(string lotId)
        {
            var lot = Session
                .Include<Lot>(x => x.VehicleId)
                .LoadAsync<Lot>(lotId).Result;

            if (lot == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    string.Format("Lot with id '{0}' not found", lotId));

            if (lot.Status.In(LotStatusTypes.EndedWithSales, LotStatusTypes.EndedWithoutSales, LotStatusTypes.Reserved))
            {
                ModelState.AddModelError("LotId", "Cannot delete lot with status " + lot.Status);
                return Request.CreateErrorResponse(HttpStatusCode.Forbidden, ModelState);
            }

            lot.Cancel();

            return Request.CreateResponse(HttpStatusCode.Accepted, (object)null);
        }
    }
}


public static class Options
{
    public static string SerializeObject<T>(this T toSerialize)
    {
        XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

        using (StringWriter textWriter = new StringWriter())
        {
            xmlSerializer.Serialize(textWriter, toSerialize);
            return textWriter.ToString();
        }
    }
}