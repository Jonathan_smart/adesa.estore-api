using System;
using System.Web.Http;
using log4net;

namespace Sfs.EStore.Api.Controllers
{
    [AllowAnonymous]
    public class PingController : ApiController
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PingController));

        public string Get()
        {
            return "Pong";
        }

        public void Post()
        {
            Logger.Warn("About to throw an exception on purpose");
            try
            {
                throw new InvalidOperationException("This exception is expected - test throwing an exception");
            }
            catch (Exception ex)
            {
                Logger.Error("This exception is expected - test throwing an exception", ex);
                throw;
            }
        }
    }
}