using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties;

namespace Sfs.EStore.Api.Controllers
{
    public class ForgottenPasswordController : RavenEStoreController
    {
        private readonly IInitiateForgottenPasswordRetrievalCommand _initiateForgottenPasswordRetrievalCommand;

        public ForgottenPasswordController(IInitiateForgottenPasswordRetrievalCommand initiateForgottenPasswordRetrievalCommand)
        {
            _initiateForgottenPasswordRetrievalCommand = initiateForgottenPasswordRetrievalCommand;
        }

        public Task<HttpResponseMessage> Post(ForgottenPasswordModel model)
        {
            return Task.Factory.StartNew(() => InitiateForgottenPasswordRetrieval(model));
        }

        private HttpResponseMessage InitiateForgottenPasswordRetrieval(ForgottenPasswordModel model)
        {
            var user = Session.Query<MembershipUser>()
                .Where(x => x.Username == model.Username)
                .FirstOrDefaultAsync()
                .Result;

            if (user == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "User not found");

            if (user.IsLockedOut)
                return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "User is locked out");

            _initiateForgottenPasswordRetrievalCommand.User = user;
            ExecuteCommand(_initiateForgottenPasswordRetrievalCommand);
            return Request.CreateResponse(HttpStatusCode.Accepted);
        }
    }
}