using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.Core;
using Sfs.EStore.Api.Data;
using Sfs.EStore.Api.NavShipToAddressService;
using Sfs.EStore.Api.ThirdParties.Ports;

namespace Sfs.EStore.Api.Controllers
{
    [MembershipUserAuthorise]
    public class CustomerAddressesController : RavenEStoreController
    {
        private readonly SalesChannel _salesChannel;
        private readonly BusinessUnit _businessUnit;
        private readonly Ship_to_Address_Service _shipToAddressClient;

        public CustomerAddressesController(SalesChannel salesChannel, BusinessUnit businessUnit,
            Ship_to_Address_Service shipToAddressClient)
        {
            _salesChannel = salesChannel;
            _businessUnit = businessUnit;
            _shipToAddressClient = shipToAddressClient;
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Post(string id, CreateAddress form)
        {
            return Task.Factory.StartNew(() => CreateNewAddress(id, form));
        }

        private HttpResponseMessage CreateNewAddress(string customerNo, CreateAddress form)
        {
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

            var contactUsername =
                ExecuteCommand(new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));

            if (contactUsername == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Cannot find contact customer");

            var customer = ExecuteCommand(new GetContactCustomers(contactUsername.ContactNo, _businessUnit)
            {
                CustomerNo = customerNo
            }).FirstOrDefault();

            if (customer == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Cannot find contact customer (2)");

            var address = new Ship_to_Address
            {
                Name = form.Name ?? "",
                Customer_No = customer.No ?? "",
                Address = form.Address1 ?? "",
                Address_2 = form.Address2 ?? "",
                City = form.City ?? "",
                County = form.County ?? "",
                Post_Code = form.PostCode ?? ""
            };

            _shipToAddressClient.Create(ref address);

            return Request.CreateResponse(HttpStatusCode.OK, new ShipToAddressModel(address));
        }

        public class ShipToAddressModel
        {
            internal ShipToAddressModel(ShipToAddress entity)
            {
                Code = entity.Code;
                Name = entity.Name;
                Address1 = entity.Address1;
                Address2 = entity.Address2;
                City = entity.City;
                County = entity.County;
                PostCode = entity.PostCode;
            }

            public ShipToAddressModel(Ship_to_Address entity)
            {
                Code = entity.Code;
                Name = entity.Name;
                Address1 = entity.Address;
                Address2 = entity.Address_2;
                City = entity.City;
                County = entity.County;
                PostCode = entity.Post_Code;
            }

            public string Code { get; private set; }
            public string Name { get; private set; }
            public string Address1 { get; private set; }
            public string Address2 { get; private set; }
            public string City { get; private set; }
            public string County { get; private set; }
            public string PostCode { get; private set; }
        }

        public class CreateAddress
        {
            [Required]
            public string Name { get; set; }
            [Required]
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            [Required]
            public string City { get; set; }
            public string County { get; set; }
            [Required]
            public string PostCode { get; set; }
        }
    }
}