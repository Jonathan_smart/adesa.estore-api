using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Api.DomainEvents;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties;
using Slugify;

namespace Sfs.EStore.Api.Controllers
{
    public class MakeAnOfferController : RavenEStoreController
    {
        private readonly ICommandProcessor _commandProcessor;

        public MakeAnOfferController(ICommandProcessor commandProcessor)
        {
            _commandProcessor = commandProcessor;
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Post([FromUri]int listingId, MakeAnOfferPostModel @params)
        {
            return Task.Factory.StartNew(() => MakeAnOffer(listingId, @params));
        }

        private HttpResponseMessage MakeAnOffer(int listingId, MakeAnOfferPostModel @params)
        {
            var listing = Session.Include<Lot>(lot => lot.VehicleId)
                .LoadAsync<Lot>(Lot.IdFromLotNumber(listingId)).Result;
            var vehicle = Session.LoadAsync<Vehicle>(listing.VehicleId).Result;

            var cmd = new MakeAnOffer(Guid.NewGuid(),
                new MakeAnOffer.UserInfo
                {
                    AddressLine1 = @params.AddressLine1,
                    Email = @params.Email,
                    FullName = @params.FullName,
                    Phone = @params.Telephone,
                    PostCode = @params.PostCode,
                    Username = User.MembershipUser.Identity.Name
                },
                new MakeAnOffer.ListingInfo
                {
                    CurrentPrice = listing.CurrentPrice().Convert(),
                    Derivative = vehicle.Derivative,
                    ListingId = listing.LotNumber,
                    Title = listing.Title,
                    Make = vehicle.Make,
                    Model = vehicle.Model,
                    Slug = new SlugHelper().GenerateSlug(listing.Title),
                    VRM = vehicle.Registration.Plate
                },
                new Core.Money(@params.OfferAmount));

            if (@params.TenantEnrichment != null)
            {
                cmd.TenantEnrichment = new MakeAnOffer.TenantEnrichmentInfo
                {
                    LatestVisitAt = @params.TenantEnrichment.LatestVisitAt,
                    LatestVisitSource = @params.TenantEnrichment.LatestVisitSource,
                    LatestVisitType = @params.TenantEnrichment.LatestVisitType,
                    OriginalVisitAt = @params.TenantEnrichment.OriginalVisitAt,
                    OriginalVisitSource = @params.TenantEnrichment.OriginalVisitSource,
                    OriginalVisitType = @params.TenantEnrichment.OriginalVisitType
                };
            }
            _commandProcessor.Send(cmd);

            var listingVehicle = new ListingVehicle(listing, vehicle);
            
            DomainEvent.Raise(new MakeAnOfferReceived(User.MembershipUser.Identity, listingVehicle, @params));

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        public class MakeAnOfferPostModel
        {
            [Required]
            public string FullName { get; set; }
            public string AddressLine1 { get; set; }
            public string PostCode { get; set; }
            public string Telephone { get; set; }
            public string Email { get; set; }

            public decimal OfferAmount { get; set; }

            public TenantEnrichmentInfo TenantEnrichment { get; set; }

            public class TenantEnrichmentInfo
            {
                public string IpAddress { get; set; }

                public string OriginalVisitType { get; set; }
                public string OriginalVisitSource { get; set; }
                public DateTime? OriginalVisitAt { get; set; }
                public string LatestVisitType { get; set; }
                public string LatestVisitSource { get; set; }
                public DateTime? LatestVisitAt { get; set; }
            }
        }
    }
}