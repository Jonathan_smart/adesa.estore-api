using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.Core;
using Sfs.EStore.Api.Data;
using Sfs.EStore.Api.ThirdParties.Ports;

namespace Sfs.EStore.Api.Controllers
{
    [MembershipUserAuthorise]
    public class CustomersFinanceController : RavenEStoreController
    {
        private readonly FgaFinanceProxyBaseAddress _fgaFinanceProxyBaseAddress;
        private readonly SalesChannel _salesChannel;
        private readonly BusinessUnit _buesinessUnit;

        public CustomersFinanceController(FgaFinanceProxyBaseAddress fgaFinanceProxyBaseAddress, SalesChannel salesChannel, BusinessUnit buesinessUnit)
        {
            _fgaFinanceProxyBaseAddress = fgaFinanceProxyBaseAddress;
            _salesChannel = salesChannel;
            _buesinessUnit = buesinessUnit;
        }

        public Task<HttpResponseMessage> Get(string id)
        {
            return Task.Factory.StartNew(() => GetCustomersFinanceAvailability(id));
        }

        public Task<HttpResponseMessage> Get()
        {
            return Task.Factory.StartNew(() =>
            {
                var contactUsername = ExecuteCommand(
                new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));

                if (contactUsername == null)
                    return Request.CreateResponse(HttpStatusCode.OK, new FinanceLimitResult[0]);

                var customers = ExecuteCommand(new GetContactCustomers(contactUsername.ContactNo, _buesinessUnit));

                var availabilityList = customers.Select(CustomerFinanceAvailability).AsParallel().Where(x => x != null).ToArray();

                return Request.CreateResponse(HttpStatusCode.OK, availabilityList);
            });
        }

        private HttpResponseMessage GetCustomersFinanceAvailability(string customerNo)
        {
            var contactUsername = ExecuteCommand(
                new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));

            if (contactUsername == null)
                return Request.CreateResponse(HttpStatusCode.OK);

            var customer = ExecuteCommand(
                new GetContactCustomers(contactUsername.ContactNo, _buesinessUnit)
                {
                    CustomerNo = customerNo
                }).FirstOrDefault();

            if (customer == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    string.Format("Customer '{0}' not found", customerNo));

            if (string.IsNullOrWhiteSpace(customer.DealerCode))
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    string.Format("Customer '{0}' is missing dealer code", customerNo));

            var availability = CustomerFinanceAvailability(customer) ?? new FinanceLimitResult();

            return Request.CreateResponse(HttpStatusCode.OK, availability);
        }

        private FinanceLimitResult CustomerFinanceAvailability(Customer customer)
        {
            if (string.IsNullOrWhiteSpace(customer.DealerCode)) return null;

            int dealerCode;

            if (!int.TryParse(customer.DealerCode, out dealerCode)) return null;

            var normalFunding = ExecuteCommand(new FgaFinanceLimitAvailablity(dealerCode, "NormalFunding", _fgaFinanceProxyBaseAddress));
            var directPaymentFunding = ExecuteCommand(new FgaFinanceLimitAvailablity(dealerCode, "DirectPaymentPlan", _fgaFinanceProxyBaseAddress));

            var credit = new List<FinanceLimitResult.CreditDetail>();
            if (normalFunding != null)
                credit.Add(new FinanceLimitResult.CreditDetail
                {
                    Available = normalFunding.Amount,
                    Availability = normalFunding.Availability,
                    CreditType = "NormalFunding",
                    Message = normalFunding.Message
                });

            if (directPaymentFunding != null)
                credit.Add(new FinanceLimitResult.CreditDetail
                {
                    Available = directPaymentFunding.Amount,
                    Availability = directPaymentFunding.Availability,
                    CreditType = "DirectPaymentPlan",
                    Message = directPaymentFunding.Message
                });

            return new FinanceLimitResult
            {
                Credit = credit.ToArray(),
                Customer = new FinanceLimitResult.CustomerDetail
                {
                    DealerCode = customer.DealerCode,
                    Name = customer.Name,
                    No = customer.No,
                    City = customer.City
                }
            };
        }

        public class FinanceLimitResult
        {
            public FinanceLimitResult()
            {
                Credit = new CreditDetail[0];
            }

            public CreditDetail[] Credit { get; set; }
            public CustomerDetail Customer { get; set; }

            public class CustomerDetail
            {
                public string DealerCode { get; set; }
                public string Name { get; set; }
                public string No { get; set; }
                public string City { get; set; }
            }

            public class CreditDetail
            {
                public string CreditType { get; set; }
                public decimal Available { get; set; }
                public string Availability { get; set; }
                public string Message { get; set; }
            }
        }
    }
}