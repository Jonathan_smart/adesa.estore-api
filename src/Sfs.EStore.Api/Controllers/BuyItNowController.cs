using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Ninject.Activation;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Api.Data;
using Sfs.EStore.Api.DomainEvents.Handlers;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties.Ports;
using Sfs.Orders.Ports;
using Sfs.Orders.Ports.AddToBasket;

namespace Sfs.EStore.Api.Controllers
{
    public class BuyItNowController : RavenEStoreController
    {
        private readonly ICommandProcessor _commandProcessor;
        private readonly BasketLimit _basketLimit;
        private readonly SalesChannel _salesChannel;
        private readonly BusinessUnit _businessUnit;
        private readonly ISerialNoInformationByRegistrationQuery _serialNoQuery;
        private readonly IContactBasketQuery _contactBasketQuery;

        public BuyItNowController(ICommandProcessor commandProcessor,
            BasketLimit basketLimit,
            SalesChannel salesChannel,
            BusinessUnit businessUnit,
            ISerialNoInformationByRegistrationQuery serialNoQuery,
            IContactBasketQuery contactBasketQuery)
        {
            _commandProcessor = commandProcessor;
            _basketLimit = basketLimit;
            _salesChannel = salesChannel;
            _businessUnit = businessUnit;
            _serialNoQuery = serialNoQuery;
            _contactBasketQuery = contactBasketQuery;
        }

        public Task<HttpResponseMessage> Get(int lotNumber)
        {
            return Session.LoadAsync<Lot>(lotNumber)
                .ContinueWith(t =>
                                  {
                                      if (t.Result == null)
                                          return Request.CreateErrorResponse(HttpStatusCode.NotFound, "");

                                      return Request.CreateResponse(HttpStatusCode.OK, t.Result.BuyItNowPrice);
                                  });
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Post(int lotNumber, BuyItNowModel form)
        {
            return Task.Factory.StartNew(() =>
            {
                var lot = Session.Include<Lot>(x => x.VehicleId).LoadAsync<Lot>(Lot.IdFromLotNumber(lotNumber)).Result;
                var contactUsername =
                ExecuteCommand(new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));
                var customer = ExecuteCommand(new GetContactCustomers(contactUsername.ContactNo, _businessUnit))
                    .FirstOrDefault(cus => cus.No == form.CustomerNo);

                return BuyVehicle(lot, customer);
            });
        }

        private HttpResponseMessage BuyVehicle(Lot lot, Customer customer)
        {
            var contact =
                ExecuteCommand(new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));

            lot.BuyitNowSale = new BuyitNowSale()
            {
                User = User.MembershipUser.Identity.Id,
                Datetime = DateTime.Now,
                BuyItNowPrice = lot.BuyItNowPrice
            };

            var defaultSalesContact = ExecuteCommand(new GetDefaultSalesContact(customer.No));

            if (contact.Username == null)
                return Request.CreateErrorResponse((HttpStatusCode)HttpStatusCodes.GmSafeForbidden, "MISSING_USER_SETUP");

            var numberOfItemsInBasket = _contactBasketQuery.Handle(
                new ContactBasketQueryRequest(contact.ContactNo,
                    new ContactBasketQueryRequest.QueryRange(0, 0))).Total;

            if (numberOfItemsInBasket >= _basketLimit)
                return Request.CreateErrorResponse((HttpStatusCode)HttpStatusCodes.GmSafeForbidden, "BASKET_LIMIT_REACHED");

            var vehicle = Session.LoadAsync<Vehicle>(lot.VehicleId).Result;

            var serial = _serialNoQuery.Handle(new[] { vehicle.Registration.Plate }).First();

            var cmd = new AddToBasket(serial.Key,
                new AddToBasket.SerialNoPricesSalesChannel(lot.UniqueExternalReferenceNumber),
                lot.BuyItNowPrice.Convert(), _businessUnit, contact.ContactNo, defaultSalesContact, string.Empty, true)
            {
                CustomerNo = customer.No
            };

            try
            {
                _commandProcessor.Send(cmd);

                lot.Status = LotStatusTypes.EndedWithSales;

                DomainEvent.Raise(new AddVehicleInterestBuyItNow(lot, vehicle, cmd.ContactNo, cmd.Amount.Amount));

                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (CannotAddToBasket)
            {
                return Request.CreateErrorResponse((HttpStatusCode)HttpStatusCodes.GmSafeInternalServerError, "UNKNOWN");
            }
        }

        public class BuyItNowModel
        {
            public string CustomerNo { get; set; }
        }
    }
}