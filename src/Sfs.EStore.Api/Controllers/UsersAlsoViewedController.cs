﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Raven.Indexes;

namespace Sfs.EStore.Api.Controllers
{
    public class UsersAlsoViewedController : RavenEStoreController
    {
        public class UsersAlsoViewedGetModel
        {
            public int LotNumber { get; set; }
            public int? Top { get; set; }
            public string[] LimitToRoles { get; set; }
        }

        public Task<HttpResponseMessage> Get([FromUri]UsersAlsoViewedGetModel model)
        {
            return Task.Factory.StartNew(() => GetAlsoViewed(model));
        }

        private HttpResponseMessage GetAlsoViewed(UsersAlsoViewedGetModel model)
        {
            var lotId = Lot.IdFromLotNumber(model.LotNumber);
            var lot = Session.LoadAsync<Lot>(lotId).Result;

            if (lot == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                    string.Format("Access is denied to listing {0}", model.LotNumber));

            if (model.LimitToRoles != null && model.LimitToRoles.Any() &&
                !lot.VisibilityLimitedTo.Intersect(model.LimitToRoles).Any())
            {
                return Request.CreateErrorResponse(HttpStatusCode.Forbidden,
                    string.Format("Access is denied to listing {0}", model.LotNumber));
            }

            var viewedLots =
                Session.Query<SessionInteractions_UsersAlsoViewed.ReduceResult, SessionInteractions_UsersAlsoViewed>()
                    .Where(x => x.LotId == lotId)
                    .FirstOrDefaultAsync()
                    .Result;

            if (viewedLots == null)
                return Request.CreateResponse(HttpStatusCode.OK, new ListingSummaryViewModel[0]);

            var searchParams = new LotsGetParams
            {
                LotIds = viewedLots.LotsViewed.ToArray(),
                LimitToRoles = model.LimitToRoles
            };
            var query = new LotQueryBuilder(Session)
            {
                QueryForStatuses = User.Identity.Advanced.QueryStatusesLimitedTo.Any()
                    ? LotStatusTypeConversion.Convert(User.Identity.Advanced.QueryStatusesLimitedTo).ToArray()
                    : new[] {LotStatusTypes.Active, LotStatusTypes.Reserved}
            }.Build(searchParams)
                .Take(model.Top ?? 12);

            return Request.CreateResponse(HttpStatusCode.OK, query.ToListAsync().Result.ToArray());
        }
    }
}
