using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.EStore.Api.DomainEvents;
using Sfs.EStore.Api.ThirdParties;

namespace Sfs.EStore.Api.Controllers
{
    public class BookAServiceController : RavenEStoreController
    {
        private readonly IIntegrateBookingServices _bookAServiceIntegrator;

        public BookAServiceController(IIntegrateBookingServices bookAServiceIntegrator)
        {
            _bookAServiceIntegrator = bookAServiceIntegrator;
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Post(BookAServicePostModel @params)
        {
            return Task.Factory.StartNew(() => BookAService(@params));
        }

        private HttpResponseMessage BookAService(BookAServicePostModel @params)
        {
            if (!_bookAServiceIntegrator.Supported)
                return Request.CreateErrorResponse(HttpStatusCode.NotImplemented,
                    "Service booking requests are not supported for this account");

            _bookAServiceIntegrator.Session = Session;

            _bookAServiceIntegrator.BookService(User.MembershipUser.Identity, @params);
            DomainEvent.Raise(new ServiceBookingRequestReceived(User.MembershipUser.Identity, @params));

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        public class BookAServicePostModel
        {
            public string FullName { get; set; }
            public string AddressLine1 { get; set; }
            public string PostCode { get; set; }
            public string Telephone { get; set; }
            public string Email { get; set; }

            public string Registration { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string EngineSize { get; set; }
            public string Mileage { get; set; }

            public bool MotRequired { get; set; }
            public bool CourtesyVehicleRequired { get; set; }
            public string PreferredDate { get; set; }
            public string Comments { get; set; }
        }
    }
}