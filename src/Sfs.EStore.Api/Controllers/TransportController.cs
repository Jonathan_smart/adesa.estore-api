using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.Darker.CommandProcessor;
using Sfs.Orders.Ports;

namespace Sfs.EStore.Api.Controllers
{
    public class TransportController : RavenEStoreController
    {
        private readonly ICommandProcessor _commandProcessor;
        private readonly ISerialNoInformationByRegistrationQuery _serialNoInformationQuery;

        public TransportController(ICommandProcessor commandProcessor, ISerialNoInformationByRegistrationQuery serialNoInformationQuery)
        {
            _commandProcessor = commandProcessor;
            _serialNoInformationQuery = serialNoInformationQuery;
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Post(TransportRequestModel form)
        {
            return Task.Factory.StartNew(() => CalculateTransportCostsForVehicle(form));
        }

        private HttpResponseMessage CalculateTransportCostsForVehicle(TransportRequestModel form)
        {
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

            var serials = _serialNoInformationQuery.Handle(form.Items.Select(x => x.From.VehicleRegistrationNo));

            if (form.WithVat)
            {
                var cmd = new CalculateTransport(form.Items.Select(x => new CalculateTransport.TransportRequestItem
                {
                    Basket = new BasketEntryNo(x.Id),
                    ShippingAgent = x.ShippingAgent,
                    ShippingAgentService = x.ShippingAgentService,
                    ShippingMethod = x.ShippingMethod,
                    Vehicle = serials.First(s => s.RegistrationNo == x.From.VehicleRegistrationNo).Key,
                    To = new CalculateTransport.TransportRequestTo
                    {
                        CustomerNo = x.To.CustomerNo,
                        AddressCode = x.To.AddressCode
                    }
                }), true
                );
                _commandProcessor.Send(cmd);

                var results = cmd.Result;

                return Request.CreateResponse(HttpStatusCode.OK,
                    results.Select(x => new TransportItemResponseModel(x.Basket, x.Cost)));
            }
            else
            {
                var cmd = new CalculateTransport(form.Items.Select(x => new CalculateTransport.TransportRequestItem
                {
                    Basket = new BasketEntryNo(x.Id),
                    ShippingAgent = x.ShippingAgent,
                    ShippingAgentService = x.ShippingAgentService,
                    ShippingMethod = x.ShippingMethod,
                    Vehicle = serials.First(s => s.RegistrationNo == x.From.VehicleRegistrationNo).Key,
                    To = new CalculateTransport.TransportRequestTo
                    {
                        CustomerNo = x.To.CustomerNo,
                        AddressCode = x.To.AddressCode
                    }

                }), false
            );
                _commandProcessor.Send(cmd);

                var results = cmd.Result;

                return Request.CreateResponse(HttpStatusCode.OK,
                    results.Select(x => new TransportItemResponseModel(x.Basket, x.Cost)));
            }
        }

        public class TransportItemResponseModel
        {
            public TransportItemResponseModel(int id, decimal cost)
            {
                Id = id;
                Cost = cost;
            }

            public int Id { get; private set; }
            public decimal Cost { get; private set; }
        }

        public class TransportRequestModel
        {
            public TransportRequestItemModel[] Items { get; set; }
            public bool WithVat { get; set; }
        }

        public class TransportRequestItemModel
        {
            [Required]
            public int Id { get; set; }
            [Required]
            public string ShippingMethod { get; set; }
            public string ShippingAgent { get; set; }
            public string ShippingAgentService { get; set; }
            public TransportItemFromModel From { get; set; }
            public TransportItemToModel To { get; set; }
        }

        public class TransportItemFromModel
        {
            [Required]
            public string VehicleRegistrationNo { get; set; }
        }

        public class TransportItemToModel
        {
            [Required]
            public string CustomerNo { get; set; }
            public string AddressCode { get; set; }
        }
    }
}