﻿using log4net;
using Sfs.Core;
using Sfs.EStore.Api.Data;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.NavVehicle_Interests;
using Sfs.EStore.Api.ThirdParties.Ports;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Linq;

namespace Sfs.EStore.Api.Controllers
{
    public class MakeOfferController : RavenEStoreController
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(ExceptionFilter));
        private readonly Vehicle_Interests_Service _client;
        private readonly SalesChannel _salesChannel;
        private readonly BusinessUnit _businessUnit;

        public MakeOfferController(SalesChannel salesChannel, BusinessUnit businessUnit, Vehicle_Interests_Service client)
        {
            _client = client;
            _salesChannel = salesChannel;
            _businessUnit = businessUnit;
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Post([FromUri]string listingId, MakeOfferPostModel model)
        {
            return Task.Factory.StartNew(() => MakeAnOffer(listingId, model));
        }
        private HttpResponseMessage MakeAnOffer(string listingId, MakeOfferPostModel model)
        {
            var contact =
        ExecuteCommand(new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));

            var lot = Session.Include<Lot>(x => x.VehicleId).LoadAsync<Lot>(Lot.IdFromLotNumber(listingId)).Result;

            if (lot == null)
                return Request.CreateResponse(HttpStatusCode.NotFound, "ERROR LOT NOT FOUND");

            var vehicle = Session.LoadAsync<Vehicle>(lot.VehicleId).Result;

            using (var ctx = new NavisionContext())
            {
                using (ctx.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    var serialNo =
                 ctx.Set<Data.SerialNoInformation>()
                     .Where(b => b.ItemNo == "VEHICLE" & b.RegistrationNo == vehicle.Registration.Plate)
                     .Select(n => n.SerialNo).FirstOrDefault();

                    if (serialNo == null)
                        return Request.CreateResponse(HttpStatusCode.NotFound, "ERROR LOT NOT FOUND");

                    var navInterest = new Vehicle_Interests
                    {
                        Type = NavVehicle_Interests.Type.Offer_Made,
                        TypeSpecified = true,
                        Customer_No = string.Empty,
                        Contact_No = contact.ContactNo ?? string.Empty,
                        Sales_Person_Contact_No = string.Empty,
                        Serial_No = serialNo,
                        Date_Created = DateTime.UtcNow,
                        Date_CreatedSpecified = true,
                        Note = model.Comments,
                        Value = model.Offer,
                        ValueSpecified = true,
                        Global_Dimension_1_Code = _businessUnit.ToString(),
                    };

                    try
                    {
                        _client.Create(ref navInterest);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex.Message);
                    }
                }
            }


            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
    public class MakeOfferPostModel
    {
        public string LotId { get; set; }
        [Required]
        public decimal Offer { get; set; }
        public string Comments { get; set; }
    }
}