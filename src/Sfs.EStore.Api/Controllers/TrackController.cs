using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Raven.Client;
using Raven.Client.Extensions;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Controllers
{
    [AllowAnonymousEndUser]
    public class TrackController : RavenEStoreController
    {
        public Task<HttpResponseMessage> Post(int lotNumber, string session)
        {
            return Task.Factory.StartNew(() => TrackLotView(lotNumber, session));
        }

        public Task<HttpResponseMessage> Put(string session, string usertoken)
        {
            return Task.Factory.StartNew(() => LinkSessionToUser(session, usertoken));
        }

        private HttpResponseMessage LinkSessionToUser(string session, string usertoken)
        {
            Token token;
            try
            {
                token = UserToken.Decrypt(usertoken);
            }
            catch(ArgumentException)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid user token " + usertoken);
            }

            var interactions = Session.Query<SessionInteractions>()
                   .Where(x => x.SessionId == session)
                   .ToListAsync()
                   .Result.LastOrDefault();

            if (interactions == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Cannot find session with id " + session);

            var user = Session.Query<MembershipUser>()
                .FirstOrDefaultAsync(x => x.Username == token.Username)
                .Result;

            interactions.IsKnownUser(user);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private HttpResponseMessage 
            TrackLotView(int lotNumber, string session)
        {
            var lotId = Lot.IdFromLotNumber(lotNumber);
            var lot = Session.LoadAsync<Lot>(lotId).Result;

            if (lot == null || lot.Status != LotStatusTypes.Active) return Request.CreateResponse(HttpStatusCode.OK);

            var interactions = Session.Query<SessionInteractions>()
                .Where(x => x.SessionId == session)
                .ToListAsync()
                .Result.LastOrDefault();

            if (interactions == null)
            {
                interactions = new SessionInteractions
                {
                    SessionId = session
                };
                Session.StoreAsync(interactions).Wait();
            }

            if (User.MembershipUser.Identity.IsAuthenticated)
                interactions.IsKnownUser(User.MembershipUser.Identity);

            interactions.AddLot(lotId);

            var listingViewCount = Session.LoadAsync<ListingViewCount>(ListingViewCount.GenerateId(lot.Id)).Result;
            if (listingViewCount == null)
            {
                listingViewCount = new ListingViewCount(lot.Id);
                Session.StoreAsync(listingViewCount).Wait();
            }
            else
                listingViewCount.Increment();
            
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}