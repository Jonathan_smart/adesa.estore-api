using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Raven.Client;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Controllers
{
    public class SearchController : RavenEStoreController
    {
        public Task<HttpResponseMessage> Post(LotsGetParams search)
        {
            search = search ?? new LotsGetParams();

            RavenQueryStatistics stats;
            var query = new LotQueryBuilder(Session)
            {
                QueryForStatuses = User.Identity.Advanced.QueryStatusesLimitedTo.Any()
                    ? LotStatusTypeConversion.Convert(User.Identity.Advanced.QueryStatusesLimitedTo).ToArray()
                    : new[] { LotStatusTypes.Active, LotStatusTypes.Reserved }
            }.Build(search)
                .Statistics(out stats);

            int take;

            IQueryable<ListingSummaryViewModel> pagedQuery = query;

            if (int.TryParse(HttpUtility.UrlDecode(search.Top), out take))
                pagedQuery = pagedQuery.Take(take);

            int skip;
            if (int.TryParse(HttpUtility.UrlDecode(search.Skip), out skip))
                pagedQuery = pagedQuery.Skip(skip);

            return pagedQuery.ToListAsyncWithTotalResultsHeader(Request, stats);

        }
    }
}