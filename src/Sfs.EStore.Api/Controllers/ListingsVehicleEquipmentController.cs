﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Controllers
{
    public class ListingsVehicleEquipmentController : RavenEStoreController
    {
        [Route("listings/vehicle-equipment")]
        public HttpResponseMessage Get([FromUri]int[] id, int limit=10)
        {
            var lots = Session
                .Include<Lot>(x => x.VehicleId)
                .LoadAsync<Lot>(id.Select(Lot.IdFromLotNumber))
                .Result
                .Where(x => x != null).ToArray();
            var vehicles = Session.LoadAsync<Vehicle>(lots.Select(x => x.VehicleId)).Result;
            var equipment = lots.Join(vehicles,
                l => l.VehicleId,
                v => v.Id,
                (l, v) => new {l, v})
                .Select(x => new VehicleEquipment
                {
                    ListingId = x.l.LotNumber,
                    Equipment = ListingViewModel.EquipmentInfoModel.From(x.v.Specification.Equipment, limit)
                }).ToArray();

            return Request.CreateResponse(HttpStatusCode.OK, equipment);
        }

        public class VehicleEquipment
        {
            public int ListingId { get; set; }
            public ListingViewModel.EquipmentInfoModel Equipment { get; set; }
        }
    }
}
