using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties;

namespace Sfs.EStore.Api.Controllers
{
    public class ResetPasswordController : RavenEStoreController
    {
        private readonly IResetPasswordCommand _resetPasswordCommand;
        private readonly IValidateResetPasswordTokenCommand _validateResetPasswordToken;

        public ResetPasswordController(IResetPasswordCommand resetPasswordCommand, IValidateResetPasswordTokenCommand validateResetPasswordToken)
        {
            _resetPasswordCommand = resetPasswordCommand;
            _validateResetPasswordToken = validateResetPasswordToken;
        }

        public Task<HttpResponseMessage> Post(ResetPasswordPostModel model)
        {
            return Task.Factory.StartNew(() => ResetPassword(model));
        }

        public Task<HttpResponseMessage> GetValidate(string token)
        {
            return Task.Factory.StartNew(() => ValidateToken(token));
        }

        private HttpResponseMessage ValidateToken(string token)
        {
            _validateResetPasswordToken.Token = token;

            var isValid = ExecuteCommand(_validateResetPasswordToken);

            return Request.CreateResponse(HttpStatusCode.OK, new {isValid});
        }

        private HttpResponseMessage ResetPassword(ResetPasswordPostModel model)
        {
            _resetPasswordCommand.Args = model;
            var result = ExecuteCommand(_resetPasswordCommand);

            if (result.Success)
            {
                var authenticatedUser = ExecuteCommand(new AuthenticatedUserQuery { Username = result.Username, Password = model.NewPassword });
                return Request.CreateResponse(HttpStatusCode.OK, authenticatedUser);
            }

            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "");
        }
    }
}