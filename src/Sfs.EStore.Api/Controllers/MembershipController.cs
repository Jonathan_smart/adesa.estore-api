﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.DomainEvents;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Controllers
{
    public class MembershipController : RavenEStoreController
    {
        public Task<HttpResponseMessage> PutUpdate(MembershipUserModel membershipUser)
        {
            return Task.Factory.StartNew(() => UpdateMembershipUser(membershipUser));
        }

        public Task<HttpResponseMessage> Get([FromUri]UsersGetParams search)
        {
            RavenQueryStatistics stats;
            IQueryable<MembershipUser> query = new UsersQueryBuilder(Session).Build(search).Statistics(out stats);

            if (!string.IsNullOrWhiteSpace(search.SortBy))
            {
                switch (search.SortBy)
                {
                    case "username":
                        query = query.OrderBy(x => x.Username);
                        break;
                    case "-username":
                        query = query.OrderByDescending(x => x.Username);
                        break;
                    case "createdon":
                        query = query.OrderBy(x => x.CreatedOn);
                        break;
                    case "-createdon":
                        query = query.OrderByDescending(x => x.CreatedOn);
                        break;
                }
            }

            if (search.Skip.HasValue)
                query = query.Skip(search.Skip.Value);
            if (search.Top.HasValue)
                query = query.Take(search.Top.Value);

            return query.Select(x => new
            {
                x.ApprovedOn,
                x.CreatedOn,
                x.EmailAddress,
                x.FirstName,
                x.InvalidPasswordAttempts,
                x.IsApproved,
                x.IsLockedOut,
                x.LastLoggedInOn,
                x.LastName,
                x.LockedOutOn,
                x.PhoneNumber,
                x.Roles,
                x.Franchises,
                x.Username
            }).ToListAsyncWithTotalResultsHeader(Request, stats);
        }

        public Task<HttpResponseMessage> getUser(string userName)
        {
            return Task.Factory.StartNew(() => GetUser(userName));
        }

        [MembershipUserAuthorise, ActionName("changepassword")]
        public Task<HttpResponseMessage> PutChangePassword(PasswordChange passwordChange)
        {
            return Task.Factory.StartNew(() => ChangePassword(passwordChange));
        }

        [HttpPut, ActionName("upsert")]
        public Task<HttpResponseMessage> PutUser(string id, PutUserModel model)
        {
            return Task.Factory.StartNew(() =>
            {
                var username = id;
                var user = Session.Query<MembershipUser>()
                    .FirstOrDefaultAsync(x => x.Username == username)
                    .Result;

                var responseStatus = HttpStatusCode.OK;

                if (user == null)
                {
                    user = new MembershipUser
                    {
                        Username = username
                    }.Approve();
                    Session.StoreAsync(user).Wait();
                    responseStatus = HttpStatusCode.Created;
                }

                user.EmailAddress = model.Email ?? "";
                user.FirstName = model.FirstName ?? "";
                user.Roles = model.Roles ?? new string[0];

                var authenticatedUser = AuthenticationResponseModel_Authenticated.Create(user);
                DomainEvent.Raise(new UserAuthenticated(user));
                return Request.CreateResponse(responseStatus, authenticatedUser);
            });
        }

        public class PutUserModel
        {
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string[] Roles { get; set; }
        }

        private HttpResponseMessage UpdateMembershipUser(MembershipUserModel userModel)
        {
            var user = Session.Query<MembershipUser>().Where(x => x.Username == userModel.Username).ToListAsync().Result.FirstOrDefault();
            if (user == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "User not found");

            if (userModel.IsApproved.HasValue)
            {
                if (userModel.IsApproved.Value)
                    user.Approve();
                else
                    user.UnApprove();
            }

            if (userModel.IsLockedOut.HasValue)
            {
                if (userModel.IsLockedOut.Value) user.Lock();
                else user.UnLock();
            }

            user.FirstName = userModel.FirstName;
            user.LastName = userModel.LastName;
            user.PhoneNumber = userModel.PhoneNumber;
            user.EmailAddress = userModel.EmailAddress;

            user.Roles = userModel.Roles;

            return Request.CreateResponse(HttpStatusCode.OK, user);
        }

        private HttpResponseMessage ChangePassword(PasswordChange passwordChange)
        {
            var user = Session.Query<MembershipUser>()
                .Where(x => x.Username == User.MembershipUser.Identity.Name)
                .ToListAsync().Result
                .FirstOrDefault();

            if (user == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Current user does not exist");

            if (!user.VerifyPassword(passwordChange.CurrentPassword))
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Current password invalid");

            user.SetPassword(passwordChange.NewPassword);
            return Request.CreateResponse(HttpStatusCode.OK, "Password updated");
        }


        private HttpResponseMessage GetUser(string userName)
        {
            var user = Session.Query<MembershipUser>()
                .Where(x => x.Username == userName)
                .ToListAsync().Result
                .FirstOrDefault();

            return Request.CreateResponse(HttpStatusCode.OK, user);
        }
    }

    public class UsersQueryBuilder
    {
        private readonly IAsyncDocumentSession _session;

        public UsersQueryBuilder(IAsyncDocumentSession session)
        {
            _session = session;
        }

        public IRavenQueryable<MembershipUser> Build(UsersGetParams queryParams)
        {
            if (queryParams == null) throw new ArgumentNullException("queryParams");

            var query = _session.Query<MembershipUser>();
            if (!string.IsNullOrEmpty(queryParams.Q))
                query = query.Where(x => x.Username.StartsWith(queryParams.Q) ||
                    x.EmailAddress.StartsWith(queryParams.Q) ||
                    x.FirstName.StartsWith(queryParams.Q) ||
                    x.LastName.StartsWith(queryParams.Q));
            if (!string.IsNullOrEmpty(queryParams.Username))
                query = query.Where(x => x.Username.Contains(queryParams.Username));
            if (queryParams.IsApproved.HasValue)
                query = query.Where(x => x.IsApproved == queryParams.IsApproved.Value);
            if (queryParams.IsLockedOut.HasValue)
                query = query.Where(x => x.IsLockedOut == queryParams.IsLockedOut.Value);

            return query;
        }
    }

    public class MembershipUserModel
    {
        public string Username { get; set; }
        public bool? IsLockedOut { get; set; }
        public bool? IsApproved { get; set; }
        public string[] Roles { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
    }
}