﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Raven.Indexes;
using log4net;
using Raven.Abstractions.Extensions;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Controllers
{
    public class SalesController : RavenEStoreController
    {
        public Task<HttpResponseMessage> Get([FromUri]GetParams form)
        {
            return Task.Factory.StartNew(() => GetSales(form));
        }
        /// <summary>
        /// Auction Sales View
        /// </summary>
        /// <param name="Sales"></param>
        /// <returns></returns>
        private HttpResponseMessage GetSales(GetParams search)
        {
            var sales = search.Status == "Preview"
                ? Session.Query<Lot_Sales.ReduceResult, Lot_Sales>()
                    .Where(x => x.Title != null && x.AllowPreview > 0 )
                    .ToListAsync().Result
                : Session.Query<Lot_Sales.ReduceResult, Lot_Sales>()
                    .Where(x => x.Title != null && x.Available > 0 && x.EndDate >= DateTime.Now)
                    .ToListAsync().Result;

            var saleIds = sales.ToList().Select(x => x.Id).ToList();


            var results = sales.Select(x =>
            {
                var query = Session.Query<Lot>()
                    .Where(lot => lot.Sale.Id == x.Id)
                    .Where(lot=>lot.ListingType == ListingType.Auction || lot.ListingType == ListingType.AuctionWithBin);

                query = search.Status == "Preview" ? query.Where(lot => lot.Status == LotStatusTypes.Scheduled) : query.Where(lot => lot.Status == LotStatusTypes.Active);

                if (search.LimitToRoles != null)
                    query = query.Where(lot => lot.VisibilityLimitedTo.Any(tag => tag.In(search.LimitToRoles)));

                if (search.LimitToRoles != null)
                    query = query.Where(lot => lot.VisibilityLimitedTo.Any(tag => tag.In(search.LimitToRoles)));

                var available = query.CountAsync().Result;

                return new GetSalesModel
                {
                    Title = x.Title,
                    Id = x.Id,
                    Count = x.Count,
                    Active = x.Available,
                    Available = available,
                    Scheduled = x.Scheduled,
                    StartDate = String.Format("{0:f}", x.StartDate),
                    TimeRemaining = String.Format("{0} days, {1} hours, {2} minutes, {3} seconds", (x.EndDate - DateTime.Now).Days, (x.EndDate - DateTime.Now).Hours, (x.EndDate - DateTime.Now).Minutes, (x.EndDate - DateTime.Now).Seconds)
                };
            })
            .Where(x => x.Available > 0)
            .OrderBy(x=>x.StartDate)
            .ToList();

            var lotQuery = Session.Query<Lot>()
                .Where(x => x.Sale.Id.In(saleIds));

            return Request.CreateResponse(HttpStatusCode.OK, results);
        }

        public class GetParams
        {
            public string[] LimitToRoles { get; set; }
            public string Status { get; set; }
        }

        public class GetSalesModel
        {
            public string Id { get; set; }
            public string Title { get; set; }
            public int Count { get; set; }
            public int Active { get; set; }
            public int Available { get; set; }
            public int Scheduled { get; set; }
            public string StartDate { get; set; }
            public string TimeRemaining { get; set; }
        }

    }
}
