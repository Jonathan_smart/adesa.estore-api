using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Raven.Indexes;
using Sfs.EStore.Api.Raven.Transformers;
using Sfs.EStore.Api.ThirdParties.Ports;
using Sfs.Orders.Ports;

namespace Sfs.EStore.Api.Controllers
{
    [MembershipUserAuthorise]
    public class BasketDeleteController : RavenEStoreController
    {
        private readonly IContactBasketQuery _contactBasketQuery;
        private readonly ICommandProcessor _commandProcessor;
        private readonly SalesChannel _salesChannel;

        public BasketDeleteController(ICommandProcessor commandProcessor, IContactBasketQuery contactBasketQuery, SalesChannel salesChannel)
        {
            _commandProcessor = commandProcessor;
            _contactBasketQuery = contactBasketQuery;
            _salesChannel = salesChannel;
        }

        [Route("basket/{id}")]
        [HttpPost]
        public Task<HttpResponseMessage> Delete(int id)
        {
            return Task.Factory.StartNew(() => DeleteBasket(new BasketEntryNo(id)));
        }

        private HttpResponseMessage DeleteBasket(BasketEntryNo entryNo)
        {
            var contactUsername =
                ExecuteCommand(new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));

            if (contactUsername == null)
                return Request.CreateResponse(HttpStatusCode.NoContent);

            var item =
                _contactBasketQuery.Handle(new ContactBasketQueryRequest(contactUsername.ContactNo,
                    new ContactBasketQueryRequest.QueryRange(0, 1), new[] { entryNo }))
                    .FirstOrDefault();

            if (item == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "NO ITEM IN BASKET");

            var vehicles = Session.Query<Lot_Search.ReduceResult, Lot_Search>()
                .Where(
                    x =>
                        x.Status.In(new[] { LotStatusTypes.Active, LotStatusTypes.Reserved }) &&
                        x.RegistrationPlate.In(item.Registration.Plate))
                .TransformWith<LotIdToListingSummary, ListingSummaryViewModel>().ToListAsync().Result;

            var lotId = vehicles.Select(x => x.ListingId).FirstOrDefault();

            var lot = Session.Include<Lot>(x => x.VehicleId).LoadAsync<Lot>(Lot.IdFromLotNumber(lotId)).Result;

            if (lot == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "LISTING_NOT_FOUND");

            if (item == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, string.Format("Cannot find {0} in users basket", (int)entryNo));

            _commandProcessor.Send(new RemoveFromBasket(item.EntryNo));

            lot.Status = LotStatusTypes.Active;

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}