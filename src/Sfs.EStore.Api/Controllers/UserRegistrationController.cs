using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.DomainEvents;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties.UserRegistration;

namespace Sfs.EStore.Api.Controllers
{
    public class UserRegistrationController : RavenEStoreController
    {
        private readonly IRegisterNewUsersCommand _registerUserCommand;
        private readonly IValidateUserRegistrationRequestsCommand _validateUserRegistration;
        private readonly IValidatePasswordRulesCommand _passwordValidationRules;

        public UserRegistrationController(IRegisterNewUsersCommand registerUserCommand,
                                          IValidateUserRegistrationRequestsCommand validateUserRegistration,
                                          IValidatePasswordRulesCommand passwordValidationRules)
        {
            _registerUserCommand = registerUserCommand;
            _validateUserRegistration = validateUserRegistration;
            _passwordValidationRules = passwordValidationRules;
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Post(UserRegistrationPostModel userRegistrationModel)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        Logger.InfoFormat("New user registration request. Dry run?: {0}", userRegistrationModel.DryRun);
                        var isValid = ValidateNewUser(userRegistrationModel);
                        
                        if (!isValid)
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                        return userRegistrationModel.DryRun
                                   ? Request.CreateResponse(HttpStatusCode.OK, true)
                                   : RegisterNewUser(userRegistrationModel);
                    });
        }

        [ModelRequiredFilter, ActionName("confirm")]
        public Task<HttpResponseMessage> PostConfirm(string token)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        RegistrationConfirmationTicket ticket;
                        try
                        {
                            ticket = RegistrationConfirmation.Decrypt(token);
                        }
                        catch (ArgumentException)
                        {
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid token");
                        }

                        var user = Session.Query<MembershipUser>()
                            .FirstOrDefaultAsync(x => x.Username == ticket.Username)
                            .Result;

                        if (user == null)
                            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "User does not exist");

                        try
                        {
                            ExecuteCommand(new ApproveUserCommand(ticket.Username));
                            var authenticatedUser = AuthenticationResponseModel_Authenticated.Create(user);
                            DomainEvent.Raise(new UserAuthenticated(user));
                            return Request.CreateResponse(HttpStatusCode.OK, authenticatedUser);
                        }
                        catch (InvalidOperationException)
                        {
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid operation");
                        }
                    });
        }

        [ModelRequiredFilter, ActionName("validatepassword")]
        public Task<HttpResponseMessage> PostValidatePassword(string password)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        _passwordValidationRules.ModelState = ModelState;
                        _passwordValidationRules.Password = password;
                        var isValid = ExecuteCommand(_passwordValidationRules);

                        if (isValid)
                            return Request.CreateResponse(HttpStatusCode.OK, true);

                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    });
        }

        [ActionName("email_available")]
        public Task<HttpResponseMessage> GetEmailAvailable(string email)
        {
            return Task.Factory.StartNew(() =>
                                             {
                                                 var alreadyExists = ExecuteCommand(new EmailIsAvailableForNewUser(email));

                                                 return Request.CreateResponse(HttpStatusCode.OK,
                                                                               new
                                                                                   {
                                                                                       Taken = alreadyExists,
                                                                                       Valid = !alreadyExists
                                                                                   });
                                             });
        }

        [ActionName("username_available")]
        public Task<HttpResponseMessage> GetUsernameAvailable(string username)
        {
            return Task.Factory.StartNew(() =>
                                             {
                                                 var alreadyExists = Session.Query<MembershipUser>()
                                                     .Where(x => x.Username == username)
                                                     .AnyAsync()
                                                     .Result;

                                                 return Request.CreateResponse(HttpStatusCode.OK,
                                                                               new
                                                                                   {
                                                                                       Taken = alreadyExists,
                                                                                       Valid = !alreadyExists
                                                                                   });
                                             });
        }

        private HttpResponseMessage RegisterNewUser(UserRegistrationPostModel userRegistrationModel)
        {
            try
            {
                _registerUserCommand.Request = userRegistrationModel;
                ExecuteCommand(_registerUserCommand);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (UserAlreadyExistsException)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "User already exists");
            }
            catch (FailedToRegisterUserException ex)
            {
                Logger.Error(new
                {
                    ex.Message,
                    Registration = userRegistrationModel
                }, ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private bool ValidateNewUser(UserRegistrationPostModel userRegistrationModel)
        {
            _validateUserRegistration.ModelState = ModelState;
            _validateUserRegistration.Request = userRegistrationModel;
            var isValid = ExecuteCommand(_validateUserRegistration);
            return isValid;
        }
    }
}