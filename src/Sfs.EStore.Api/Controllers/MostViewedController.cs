﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Raven.Indexes;
using Sfs.EStore.Api.Raven.Transformers;

namespace Sfs.EStore.Api.Controllers
{
    public class MostViewedController : RavenEStoreController
    {
        public Task<HttpResponseMessage> Post(MostViewedParams mostViewedParams)
        {
            return Task.Factory.StartNew(() => GetMostViewed(mostViewedParams));
        }

        public class MostViewedParams : LotsGetParams
        {
            public TimeSpan? ViewingAge { get; set; }
        }

        private HttpResponseMessage GetMostViewed(MostViewedParams search)
        {
            search = search ?? new MostViewedParams();

            var query = new ListingIndexQueryBuilder<ListingViewCount_Search.ReduceResult, ListingViewCount_Search>(
                Session)
                .Build(search);

            if (search.ViewingAge.HasValue)
                query = query.Where(x => x.LastViewedOn >= SystemTime.UtcNow.Add(-search.ViewingAge.Value));

            int take;

            int.TryParse(HttpUtility.UrlDecode(search.Top), out take);

            var results = query.OrderByDescending(x => x.ViewCount)
                .OrderByDescending(x => x.LastViewedOn)
                .TransformWith<LotIdToListingSummary, ListingSummaryViewModel>()
                .Take(take)
                .ToListAsync()
                .Result;

            return Request.CreateResponse(HttpStatusCode.OK, results);
        }
    }
}
