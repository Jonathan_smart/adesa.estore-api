using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Raven.Abstractions.Data;
using Raven.Client;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Controllers
{
    public class FacetsController : RavenEStoreController
    {
        public Task<HttpResponseMessage> Get([FromUri]FacetsGetParams search)
        {
            var response = HttpContext.Current.Response;
            return Task.Factory.StartNew(() => GetFacets(search, response));
        }

        public Task<HttpResponseMessage> Post(FacetsGetParams search)
        {
            var response = HttpContext.Current.Response;
            return Task.Factory.StartNew(() => GetFacets(search ?? new FacetsGetParams(), response));
        }

        private HttpResponseMessage GetFacets(FacetsGetParams search, HttpResponse response)
        {
            search.Facet = string.IsNullOrEmpty(search.Facet) ? "LotFacets" : search.Facet;

            var query = new LotQueryBuilder(Session)
            {
                QueryForStatuses = User.Identity.Advanced.QueryStatusesLimitedTo.Any()
                    ? LotStatusTypeConversion.Convert(User.Identity.Advanced.QueryStatusesLimitedTo).ToArray()
                    : new[] {LotStatusTypes.Active, LotStatusTypes.Reserved}
            }.Build(search);

            var facets = query
                .ToFacetsAsync(string.Format("facets/{0}", search.Facet))
                .Result;

            var totalResults = query.CountAsync().Result;

            response.AppendHeader("X-TotalResults",
                       totalResults.ToString(CultureInfo.InvariantCulture));

            return Request.CreateResponse(HttpStatusCode.OK, new FacetResponse(facets));
        }

        public class FacetResponse
        {
            public FacetResponse(FacetResults facetResults)
            {
                Duration = facetResults.Duration;
                var results = facetResults.Results.Select((kv) =>
                {
                    var values = kv.Value.Values.Select(v => new FacetValue(v.Range, v.Hits)).ToArray();
                    return new
                    {
                        key = kv.Key,
                        value = new FacetResult(values)
                    };
                }).ToDictionary(kv => kv.key, kv => kv.value);

                Results = results;
            }

            public TimeSpan Duration { get; private set; }
            public Dictionary<string, FacetResult> Results { get; private set; }

            public class FacetResult
            {
                public FacetResult(IEnumerable<FacetValue> values)
                {
                    Values = values;
                }

                public IEnumerable<FacetValue> Values { get; private set; }
            }

            public class FacetValue
            {
                public FacetValue(string range, int hits)
                {
                    Range = range;
                    Hits = hits;
                }

                public string Range { get; private set; }
                public int Hits { get; private set; }
            }
        }
    }

    public class FacetsGetParams : LotsGetParams
    {
        public string Facet { get; set; }
    }
}