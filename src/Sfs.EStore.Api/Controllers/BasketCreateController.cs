using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Api.DomainEvents;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties.Ports;
using Sfs.Orders;
using Sfs.Orders.Ports;
using Sfs.Orders.Ports.AddToBasket;

namespace Sfs.EStore.Api.Controllers
{
    [MembershipUserAuthorise]
    public class BasketCreateController : RavenEStoreController
    {
        private readonly MembershipUser _membershipUser;
        private readonly IContactBasketQuery _contactBasketQuery;
        private readonly ISerialNoInformationByRegistrationQuery _serialNoQuery;
        private readonly ICommandProcessor _commandProcessor;
        private readonly BasketLimit _basketLimit;
        private readonly SalesChannel _salesChannel;
        private readonly BusinessUnit _businessUnit;

        public BasketCreateController(MembershipUser membershipUser,
            IContactBasketQuery contactBasketQuery,
            ISerialNoInformationByRegistrationQuery serialNoQuery,
            ICommandProcessor commandProcessor,
            BasketLimit basketLimit,
            SalesChannel salesChannel, BusinessUnit businessUnit)
        {
            _membershipUser = membershipUser;
            _contactBasketQuery = contactBasketQuery;
            _serialNoQuery = serialNoQuery;
            _commandProcessor = commandProcessor;
            _basketLimit = basketLimit;
            _salesChannel = salesChannel;
            _businessUnit = businessUnit;
        }

        [Route("lots/{lotnumber}/basket")]
        public Task<HttpResponseMessage> Post(int lotNumber)
        {
            var settings = User.Identity.Advanced.IntegrationSettings;
            var expiry = settings.ContainsKey("Sfs/Basket/Expiry") ? int.Parse(settings["Sfs/Basket/Expiry"]) : (int?)null;
            return Task.Factory.StartNew(() => CreateBasket(lotNumber, expiry));
        }

        [Route("lots/{lotnumber}/basket/retail/{markup}")]
        public Task<HttpResponseMessage> PostRetail(int lotNumber, double markup)
        {
            var settings = User.Identity.Advanced.IntegrationSettings;
            var expiry = settings.ContainsKey("Sfs/Basket/RetailExpiry") ? int.Parse(settings["Sfs/Basket/RetailExpiry"]) : (int?)null;
            return Task.Factory.StartNew(() => CreateBasket(lotNumber, expiry, markup,string.Empty, true));
        }

        private HttpResponseMessage CreateBasket(int lotNumber, int? expiry, double markup = 0, string customer = "", bool isRetail = false)
        {
            var contactUsername =
                ExecuteCommand(new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));

            if (contactUsername == null)
                return Request.CreateErrorResponse((HttpStatusCode)HttpStatusCodes.GmSafeForbidden, "MISSING_USER_SETUP");

            var defaultCustomer =
           ExecuteCommand(new GetDefaultCustomerForContact(contactUsername.ContactNo, _businessUnit));

            var defaultSalesContact =
                defaultCustomer != null ? ExecuteCommand(new GetDefaultSalesContact(defaultCustomer.No)) : null;

            var numberOfItemsInBasket = GetBaskets().Total;

            if (numberOfItemsInBasket >= _basketLimit)
                return Request.CreateErrorResponse((HttpStatusCode)HttpStatusCodes.GmSafeForbidden, "BASKET_LIMIT_REACHED");

            var lot = Session.Include<Lot>(x => x.VehicleId).LoadAsync<Lot>(Lot.IdFromLotNumber(lotNumber)).Result;

            if (lot == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "LISTING_NOT_FOUND");


            var vehicle = Session.LoadAsync<Vehicle>(lot.VehicleId).Result;

            var serial = _serialNoQuery.Handle(new[] { vehicle.Registration.Plate }).First();

            var cmd = new AddToBasket(serial.Key,
                new AddToBasket.SerialNoPricesSalesChannel(lot.UniqueExternalReferenceNumber),
                lot.BuyItNowPrice.Convert(), _businessUnit, contactUsername.ContactNo, defaultSalesContact ?? string.Empty, string.Empty,false, isRetail)
            {
                ExpiresIn = expiry.HasValue ? TimeSpan.FromMinutes(expiry.Value) : TimeSpan.Zero
            };

            Func<Exception, string, HttpResponseMessage> errorResponse = (ex, code) =>
            {
                DomainEvent.Raise(new ReservationAttemptFailed(lot, vehicle, _membershipUser, (ex.InnerException ?? ex).Message));
                return Request.CreateErrorResponse((HttpStatusCode)HttpStatusCodes.GmSafeInternalServerError, code);
            };

            try
            {
                _commandProcessor.Send(cmd);

                SaveMarkupBaseket((int)cmd.Result, markup, customer);

                DomainEvent.Raise(new ReservationCreated(vehicle.Registration.Plate));

                lot.Status = LotStatusTypes.Reserved;

                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Basket = new
                    {
                        Id = (int)cmd.Result
                    }
                });
            }
            catch (AlreadyReserved ex)
            {
                return errorResponse(ex, "ALREADY_RESERVED");
            }
            catch (AlreadySold ex)
            {
                return errorResponse(ex, "ON_SALES_ORDER");
            }
            catch (NotInStock ex)
            {
                return errorResponse(ex, "NOT_IN_STOCK");
            }
            catch (ContactNotSetForPurchasing ex)
            {
                return errorResponse(ex, "MISSING_USER_SETUP");
            }
            catch (CannotAddToBasket ex)
            {
                return errorResponse(ex, "UNKNOWN");
            }
        }

        private void SaveMarkupBaseket(int id, double markup, string customer)
        {
            if (markup > 0)
            {
                MarkupBasket ma = new MarkupBasket()
                {
                    BasketEntryNo = id,
                    Dealer = customer,
                    Markup = markup
                };

                Session.StoreAsync(ma).Wait();
            }
        }

        private IPagedSet<ContactBasketQueryResponse> GetBaskets(int[] entryNos = null)
        {
            var contactUsername =
                ExecuteCommand(new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));

            var range = new ContactBasketQueryRequest.QueryRange(0, 100);

            if (contactUsername == null)
                return EmptyPagesSet.Instance<ContactBasketQueryResponse>(range.Start, range.Max);

            return _contactBasketQuery.Handle(
                new ContactBasketQueryRequest(contactUsername.ContactNo,
                    range,
                    entryNos == null ? null : entryNos.Select(x => new BasketEntryNo(x)).ToArray()));
        }
    }
}