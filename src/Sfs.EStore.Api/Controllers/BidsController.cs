using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Raven.Indexes;
using Sfs.EStore.Api.Raven.Transformers;

namespace Sfs.EStore.Api.Controllers
{
    public class BidsController : RavenEStoreController
    {
        public class GetModel
        {
            public GetModel()
            {
                SortBy = "endTime";
                PageNumber = 1;
                PageSize = 20;
            }

            public string SortBy { get; set; }
            [Required]
            public string Status { get; set; }
            public int PageNumber { get; set; }
            public int PageSize { get; set; }
        }

        public Task<HttpResponseMessage> Get([FromUri]GetModel filter)
        {
            return Task.Factory.StartNew(() =>
                                             {

                                                 filter = filter ?? new GetModel();

                                                 RavenQueryStatistics stats;
                                                 var currentUsername = User.MembershipUser.Identity.Id;

                                                 var query = Session.Query<Lot_Bids.ReduceResult, Lot_Bids>();

                                                 query = query
                                                     .Where(x => x.Bidders.Any(user => user == currentUsername) || x.CurrentOfferUserId == currentUsername || x.BuyitNowUser != string.Empty);

                                                 if (!string.IsNullOrWhiteSpace(filter.SortBy))
                                                 {
                                                     if (filter.SortBy == "endTime")
                                                         query = query.OrderBy(x => x.EndTime);
                                                     else if (filter.SortBy == "-endTime")
                                                         query = query.OrderByDescending(x => x.EndTime);
                                                 }

                                                 if ("bidding".Equals(filter.Status,
                                                     StringComparison.InvariantCultureIgnoreCase))
                                                 {
                                                     query = query.Where(x => x.Status == LotStatusTypes.Active);
                                                 }
                                                 else if ("bidding.winning".Equals(filter.Status,
                                                     StringComparison.InvariantCultureIgnoreCase))
                                                 {
                                                     query = query.Where(x => x.Status == LotStatusTypes.Active &&
                                                                              x.CurrentOfferUserId == currentUsername);
                                                 }
                                                 else if ("bidding.outbid".Equals(filter.Status,
                                                     StringComparison.InvariantCultureIgnoreCase))
                                                 {
                                                     query = query.Where(x => x.Status == LotStatusTypes.Active &&
                                                                              x.CurrentOfferUserId != currentUsername);
                                                 }
                                                 else if ("lost".Equals(filter.Status,
                                                     StringComparison.InvariantCultureIgnoreCase))
                                                 {
                                                     query =
                                                         query.Where(
                                                             x =>
                                                                 (x.Status == LotStatusTypes.EndedWithSales &&
                                                                  x.CurrentOfferUserId != currentUsername) ||

                                                                  x.Status == LotStatusTypes.EndedWithSales
                                                                 && x.BuyitNowUser != currentUsername ||

                                                                 x.Status == LotStatusTypes.EndedWithoutSales &&
                                                                  x.CurrentOfferUserId == currentUsername ||
                                                                 //Buy it now Status Lost
                                                                 x.Status == LotStatusTypes.Reserved
                                                                 && x.BuyitNowUser != currentUsername);

                                                 }
                                                 else if ("won".Equals(filter.Status,
                                                     StringComparison.InvariantCultureIgnoreCase))
                                                 {
                                                     query =
                                                         query.Where(
                                                             x =>
                                                                 x.Status == LotStatusTypes.EndedWithSales &&
                                                                 x.CurrentOfferUserId == currentUsername
                                                                 && x.BuyitNowUser == string.Empty ||

                                                                 x.Status == LotStatusTypes.EndedWithSales &&
                                                                 x.BuyitNowUser == currentUsername ||
                                                                 //Buy it now Status Lost
                                                                 x.Status == LotStatusTypes.Reserved
                                                                 && x.BuyitNowUser == currentUsername);
                                                 }

                                                 var transformedQuery = query
                                                     .Statistics(out stats)
                                                     .TransformWith<LotIdToListingSummary, BidsListingSummaryViewModel>();

                                                 var resultQuery = transformedQuery
                                                     .Take(filter.PageSize)
                                                     .Skip((filter.PageNumber - 1) * filter.PageSize);

                                                 var resultList = resultQuery.ToListAsync().Result;

                                                 resultList.ToList().ForEach(x => x.BiddingInfo.SetFor(currentUsername));

                                                 return resultList.ToListAsyncWithTotalResultsHeader(Request, stats);

                                             });
        }

        public Task<BidViewModel[]> Get(int lotnumber)
        {
            var obfuscator = new UsernameObfuscator(User.MembershipUser.Identity);

            return Session.LoadAsync<Lot>(lotnumber)
                .ContinueWith(t =>
                {
                    if (t.Result.Bidding == null) return new BidViewModel[0];

                    return t.Result.Bidding.Bids
                        .Select(x => new BidViewModel
                        {
                            Amount = MoneyViewModel.From(x.Amount),
                            Date = x.Date,
                            User = obfuscator.Obfuscate(x.UserId)
                        })
                        .ToArray();
                });
        }

        public Task<BidOutcome> Post(int lotnumber, Money bidLimit)
        {
            return Task.Factory.StartNew(() =>
                                             {
                                                 var lot = Session.LoadAsync<Lot>(Lot.IdFromLotNumber(lotnumber)).Result;
                                                 var vehicle = Session.LoadAsync<Vehicle>(lot.VehicleId).Result;

                                                 var bidOutcome = lot.TakeBid(bidLimit,
                                                                              User.MembershipUser.Identity.Id, lot, vehicle, User.MembershipUser.Identity.Name);

                                                 return bidOutcome;
                                             });

        }
    }
}