﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Raven.Indexes;

namespace Sfs.EStore.Api.Controllers
{
    public class ValuationsController : RavenEStoreController
    {
        private readonly Uri _capProxyServiceApiBaseUrl;

        public ValuationsController(string capProxyServiceApiBaseUrl)
        {
            _capProxyServiceApiBaseUrl = new Uri(capProxyServiceApiBaseUrl);
        }

        public Task<HttpResponseMessage> Get()
        {
            return Task.Factory.StartNew(() => Request.CreateResponse(HttpStatusCode.OK, GetValuationsForCurrentUser()));
        }

        private SavedValuation[] GetValuationsForCurrentUser(int take = 10)
        {
            if (!User.MembershipUser.Identity.IsAuthenticated)
                return new SavedValuation[0];

            return (GetUserSavedValuation() ?? new UserSavedValuations(null))
                .Valuations
                .OrderByDescending(x => x.CreatedOn)
                .Take(take).ToArray();
        }

        public Task<HttpResponseMessage> Delete(Guid id)
        {
            return Task.Factory.StartNew(() => DeleteSavedValuationForCurrentUser(id));
        }

        private HttpResponseMessage DeleteSavedValuationForCurrentUser(Guid id)
        {
            var userSavedValuation = GetUserSavedValuation();
            if (userSavedValuation == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No user saved valuations found for current user");
            userSavedValuation.RemoveValuation(id);
            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        public Task<HttpResponseMessage> Post([FromUri]string vrm, [FromUri]int? mileage, PartExValuationModel form)
        {
            return Task.Factory.StartNew(() => GetValuation(vrm, mileage, form));
        }

        private HttpResponseMessage GetValuation(string vrm, int? mileage, PartExValuationModel form)
        {
            if (!mileage.HasValue)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "mileage value is required");

            if (string.IsNullOrWhiteSpace(vrm))
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "vrm value is required");

            if (IsVehicleForSale(vrm))
                return Request.CreateErrorResponse(HttpStatusCode.Forbidden, new ValuationException(
                                                                                 "You cannot get a valuation for this vehicle"));

            try
            {
                var valuation = ExecuteCommand(
                    new PartExchangeValuation(User.MembershipUser.Identity, vrm, mileage.Value, form, _capProxyServiceApiBaseUrl));

                return Request.CreateResponse(HttpStatusCode.OK, valuation);
            }
            catch (ValuationException ex)
            {
                Logger.Error("", ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        private bool IsVehicleForSale(string vrm)
        {
            return Session.Query<Lot_Search.ReduceResult, Lot_Search>()
                .AnyAsync(x => x.RegistrationPlate == vrm)
                .Result;
        }

        private UserSavedValuations GetUserSavedValuation()
        {
            var obj = Session.LoadAsync<UserSavedValuations>(UserSavedValuations.IdFrom(User.MembershipUser.Identity.Id)).Result;
            return obj;
        }
    }

    public class PartExValuationModel
    {
        public ContactDetail Contact { get; set; }

        public class ContactDetail
        {
            public string FullName { get; set; }
            public string PostCode { get; set; }
            public string Telephone { get; set; }
            public string Email { get; set; }
            public string ReferrerURL { get; set; }
        }

        public TenantEnrichmentInfo TenantEnrichment { get; set; }

        public class TenantEnrichmentInfo
        {
            public string IpAddress { get; set; }

            public string OriginalVisitType { get; set; }
            public string OriginalVisitSource { get; set; }
            public DateTime? OriginalVisitAt { get; set; }
            public string LatestVisitType { get; set; }
            public string LatestVisitSource { get; set; }
            public DateTime? LatestVisitAt { get; set; }
        }
    }
}
