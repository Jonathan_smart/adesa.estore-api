﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.EStore.Api.Calculators.VehicleTaxRates;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Controllers
{
    public class VehicleTaxController : RavenEStoreController
    {
        public Task<HttpResponseMessage> Get(int lotNumber)
        {
            return Task.Factory.StartNew(() =>
                                             {
                                                 var lot = Session
                                                     .Include<Lot>(x => x.VehicleId)
                                                     .LoadAsync<Lot>(Lot.IdFromLotNumber(lotNumber))
                                                     .Result;

                                                 if (lot == null)
                                                     return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                                                                                        string.Format(
                                                                                            "Lot with Id {0} does not exist",
                                                                                            lotNumber));

                                                 var vehicle = Session.LoadAsync<Vehicle>(lot.VehicleId).Result;

                                                 if (!vehicle.Registration.Date.HasValue)
                                                     return Request.CreateErrorResponse(HttpStatusCode.NotImplemented,
                                                                                        "Cannot handle due to unknown registration date");

                                                 var taxRate = new VehicleTaxRateCalculator()
                                                     .CalculateFor(
                                                         new VehicleTaxCalculationArgs(
                                                             vehicle.Registration.Date.Value,
                                                             vehicle.VehicleType == "CAR" ? VehicleType.Car : VehicleType.Van,
                                                             vehicle.Specification.Engine.CapacityCc,
                                                             co2GPerKm: (int?) vehicle.Specification.Environment.Co2,
                                                             fuelType: ParseFuelType(vehicle.FuelType)));

                                                 return Request.CreateResponse(HttpStatusCode.OK,
                                                                               new
                                                                                   {
                                                                                       DateOfFirstReigstration = vehicle.Registration.Date.Value,
                                                                                       EngineCapacity = vehicle.Specification.Engine.CapacityCc,
                                                                                       Cost12Months = MoneyViewModel.From(taxRate.Cost12Months),
                                                                                       Cost6Months = MoneyViewModel.From(taxRate.Cost6Months),
                                                                                       FuelType = vehicle.FuelType
                                                                                   });
                                             });
        }

        private static FuelTypes ParseFuelType(string fuelType)
        {
            var normalisedFuelType = (fuelType ?? "").ToLowerInvariant();

            if (normalisedFuelType == "") return FuelTypes.Unspecified;

            if (normalisedFuelType == "petrol" || normalisedFuelType == "diesel") return FuelTypes.Unspecified;

            return FuelTypes.Alternative;
        }
    }
}
