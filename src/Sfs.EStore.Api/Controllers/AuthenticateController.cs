using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.DomainEvents;

namespace Sfs.EStore.Api.Controllers
{
    internal class AuthCodes
    {
        public const string AuthFailed = "AUTH_FAILED";
        public const string LockedOut = "LOCKED_OUT";
        public const string NewLockedOut = "NEW_LOCKED_OUT";
        public const string AwaitingApproval = "AWAITING_APPROVAL";
    }

    public class AuthenticateController : RavenEStoreController
    {
        public Task<HttpResponseMessage> Post([FromBody]ValidateUserPostModel body)
        {
            return Task.Factory.StartNew(() => AuthenticateUser(body));
        }

        private HttpResponseMessage AuthenticateUser(ValidateUserPostModel body)
        {
            var response = ExecuteCommand(new AuthenticatedUserQuery { Username = body.Username, Password = body.Password });
            
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}
