using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.EStore.Api.DomainEvents;
using Sfs.EStore.Api.ThirdParties;

namespace Sfs.EStore.Api.Controllers
{
    public class CallbacksController : RavenEStoreController
    {
        private readonly IIntegrateCallbacks _callbacksIntegrator;

        public CallbacksController(IIntegrateCallbacks callbacksIntegrator)
        {
            _callbacksIntegrator = callbacksIntegrator;
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Post(CallbackPostModel callback)
        {
            return Task.Factory.StartNew(() => Callback(callback));
        }

        private HttpResponseMessage Callback(CallbackPostModel callbackRequest)
        {
            if (!_callbacksIntegrator.Supported)
                return Request.CreateErrorResponse(HttpStatusCode.NotImplemented,
                                                   "Callback requests are not supported for this client");

            _callbacksIntegrator.Session = Session;

            var contact = new CallbackRequestContact
                              {
                                  Email = callbackRequest.Email,
                                  FirstName = callbackRequest.FirstName,
                                  LastName = callbackRequest.LastName,
                                  TelephoneNumber = callbackRequest.TelephoneNumber
                              };
            var visitInfo = new CallbackRequestVisitInfo
            {
                OriginalVisit = new CallbackRequestVisitInfo.Visit
                {
                    Source = new CallbackRequestVisitInfo.VisitSource
                    {
                        Type = callbackRequest.OriginalVisit == null ? "" : callbackRequest.OriginalVisit.Source.Type,
                        Value = callbackRequest.OriginalVisit == null ? "" : callbackRequest.OriginalVisit.Source.Value
                    },
                    At = callbackRequest.OriginalVisit == null ? (DateTime?)null : callbackRequest.OriginalVisit.At
                },
                LastVisit = new CallbackRequestVisitInfo.Visit
                {
                    Source = new CallbackRequestVisitInfo.VisitSource
                    {
                        Type = callbackRequest.LastVisit == null ? "" : callbackRequest.LastVisit.Source.Type,
                        Value = callbackRequest.LastVisit == null ? "" : callbackRequest.LastVisit.Source.Value
                    },
                    At = callbackRequest.LastVisit == null ? (DateTime?)null : callbackRequest.LastVisit.At
                }
            };
            var success = _callbacksIntegrator.Callback(User.MembershipUser.Identity, contact, visitInfo,
                                                        callbackRequest.Message, callbackRequest.LotIds);

            if (success)
                DomainEvent.Raise(new CallbackRequestSent(User.MembershipUser.Identity, contact,
                                                          callbackRequest.Message, callbackRequest.LotIds));

            return Request.CreateResponse(HttpStatusCode.OK, new {success});
        }
    }

    public class CallbackPostModel
    {
        [Required]
        public string TelephoneNumber { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Message { get; set; }

        public int[] LotIds { get; set; }

        public Visit OriginalVisit { get; set; }
        public Visit LastVisit { get; set; }

        public class Visit
        {
            [Required]
            public VisitSource Source { get; set; }
            [Required]
            public DateTime At { get; set; }
        }

        public class VisitSource
        {
            [Required]
            public string Type { get; set; }
            [Required]
            public string Value { get; set; }
        }
    }
}