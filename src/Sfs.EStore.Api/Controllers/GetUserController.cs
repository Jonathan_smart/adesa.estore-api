using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Controllers
{
    [Obsolete]
    public class GetUserController : RavenEStoreController
    {
        public Task<HttpResponseMessage> Post([FromBody]GetUserPostModel model)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        var authenticatedUser = ExecuteCommand(new AuthenticatedUserQuery {Username = model.Username});

                        if (authenticatedUser == null)
                            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "User does not exist");

                        return Request.CreateResponse(HttpStatusCode.OK, authenticatedUser);
                    });
        }
    }
}