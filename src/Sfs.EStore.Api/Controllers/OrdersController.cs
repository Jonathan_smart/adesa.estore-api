﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties.Ports;
using Sfs.Orders.Ports;

namespace Sfs.EStore.Api.Controllers
{
    public class OrdersController : RavenEStoreController
    {
        private readonly ICommandProcessor _commandProcessor;
        private readonly ISalesOrdersQuery _salesOrderReader;
        private readonly IContactSalesOrdersQuery _contactSalesOrdersQuery;
        private readonly SalesChannel _salesChannel;
        private readonly BusinessUnit _businessUnit;

        public OrdersController(ICommandProcessor commandProcessor,
            ISalesOrdersQuery salesOrderReader, IContactSalesOrdersQuery contactSalesOrdersQuery,
            SalesChannel salesChannel, BusinessUnit businessUnit)
        {
            _commandProcessor = commandProcessor;
            _salesOrderReader = salesOrderReader;
            _salesChannel = salesChannel;
            _businessUnit = businessUnit;
            _contactSalesOrdersQuery = contactSalesOrdersQuery;
        }

        public Task<HttpResponseMessage> Get([FromUri] string[] ids)
        {
            return Task.Factory.StartNew(() => (ids == null || !ids.Any()) ? GetOrderHistoryForCurrentUser() : GetSalesOrders(ids));
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Post(CreateOrdersRequest form)
        {
            return Task.Factory.StartNew(() => CreateSalesOrderForVehicle(form));
        }

        private HttpResponseMessage GetSalesOrders(IEnumerable<string> ids)
        {
            var contactUsername = ExecuteCommand(
                new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));

            if (contactUsername == null)
                return Request.CreateResponse(HttpStatusCode.OK, new CompletedOrder[0]);

            var salesOrders = _salesOrderReader.Handle(ids.ToArray()).ToArray();

            var customersCommand = new GetContactCustomers(contactUsername.ContactNo, _businessUnit);
            customersCommand.Execute();
            var customerNos = customersCommand.Result.Select(x => x.No).ToArray();

            if (salesOrders.Select(x => x.SellToCustomer.No).Distinct().Any(x => !customerNos.Contains(x)))
                return Request.CreateErrorResponse(HttpStatusCode.Forbidden,
                    "User does not have access to customer sales order");

            return Request.CreateResponse(HttpStatusCode.OK, salesOrders.Select(CompletedOrder.Map));
        }

        private HttpResponseMessage GetOrderHistoryForCurrentUser()
        {
            var contactUsername = ExecuteCommand(
                new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));

            if (contactUsername == null)
                return Request.CreateResponse(HttpStatusCode.OK, new CompletedOrder[0]);

            var salesOrders = _contactSalesOrdersQuery.Handle(new GetForContactQuery(contactUsername.ContactNo,
                new GetForContactQuery.QueryRange(0, 50)));

            return Request.CreateResponse(HttpStatusCode.OK, salesOrders.Select(CompletedOrder.Map));
        }

        private HttpResponseMessage CreateSalesOrderForVehicle(CreateOrdersRequest form)
        {
            if (form.Items == null || !form.Items.Any())
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "no listings specified");

            if (form.Items.Any(x => x.FinanceRequired && x.Finance == null))
                ModelState.AddModelError("finance", "Finance is required but has not been specified");

            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

            try
            {
                var cmd = new OrderVehicles(
                    form.Items.Select(x =>
                    {
                        var item = new OrderVehicles.OrderItem
                        {
                            BasketEntryNo = new BasketEntryNo(x.Basket.EntryNo),
                            Shipping = new OrderVehicles.ItemShipping(x.Shipping.Method)
                            {
                                AddressCode = new AddressCode(x.Shipping.AddressCode),
                                Agent = x.Shipping.Agent,
                                Service = x.Shipping.Service
                            },
                            CustomerNo = x.CustomerNo,
                            Price = x.Price,
                            V5 = new OrderVehicles.ItemV5Request
                            {
                                ShipToAddressCode = new AddressCode(x.V5.ShipToAddressCode)
                            },
                            FinanceRequired = x.FinanceRequired,
                            Transport = new OrderVehicles.ItemTransport(x.Transport.Price),
                            BuyersFee = x.BuyersFee

                        };

                        if (x.FinanceRequired)
                        {
                            item.Finance = new OrderVehicles.ItemFinance(
                                x.Finance.Method, x.Finance.Agent, x.Finance.Service);
                        }

                        return item;
                    })
                        .ToArray());

                _commandProcessor.Send(cmd);

                var successes = cmd.Result.Where(x => x.Success).Select(x => x.OrderNo.ToString()).ToArray();
                var failures = cmd.Result.Where(x => !x.Success).Select(x => x.BasketEntryNo.ToString()).ToArray();

                return Request.CreateResponse(HttpStatusCode.OK, new { orderNos = successes, failed = failures });
            }
            catch (Exception ex)
            {
                Logger.Error(new
                {
                    Message = "Failed to checkout order",
                    Username = User.MembershipUser.Identity.Name,
                    BasketEntryNos = string.Join("; ", form.Items.Select(x => x.Basket.EntryNo))
                }, ex);
                return Request.CreateResponse((HttpStatusCode)HttpStatusCodes.GmSafeInternalServerError, "No order(s) succeeded");
            }
        }
    }

    public class CreateOrdersRequest
    {
        public CreateOrdersRequestSalesItem[] Items { get; set; }
    }

    public class CreateOrdersRequestSalesItem
    {
        public string CustomerNo { get; set; }
        public string SerialNo { get; set; }
        public bool FinanceRequired { get; set; }
        public CreateOrdersRequestFinance Finance { get; set; }
        public CreateOrdersRequestBasket Basket { get; set; }
        public CreateOrdersRequestTransport Transport { get; set; }
        public CreateOrdersRequestShipping Shipping { get; set; }
        public CreateOrdersRequestV5 V5 { get; set; }
        public decimal? Price { get; set; }
        public decimal? BuyersFee { get; set; }
    }

    [DataContract]
    public class CreateOrdersRequestFinance
    {
        [Required, DataMember(IsRequired = true)]
        public string Method { get; set; }
        [Required, DataMember(IsRequired = true)]
        public string Agent { get; set; }
        [Required, DataMember(IsRequired = true)]
        public string Service { get; set; }

        protected bool Equals(CreateOrdersRequestFinance other)
        {
            return string.Equals(Method, other.Method) && string.Equals(Agent, other.Agent) && Service == other.Service;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((CreateOrdersRequestFinance)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Method != null ? Method.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Agent != null ? Agent.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Service != null ? Service.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    [DataContract]
    public class CreateOrdersRequestBasket
    {
        [Required, DataMember(IsRequired = true), Range(minimum: 1, maximum: int.MaxValue)]
        public int EntryNo { get; set; }
    }

    [DataContract]
    public class CreateOrdersRequestTransport
    {
        [Required, DataMember(IsRequired = true)]
        public decimal Price { get; set; }
    }

    [DataContract]
    public class CreateOrdersRequestShipping
    {
        [Required, DataMember(IsRequired = true)]
        public string Method { get; set; }
        [DataMember]
        public string Agent { get; set; }
        [DataMember]
        public string Service { get; set; }
        [DataMember]
        public string AddressCode { get; set; }
    }

    [DataContract]
    public class CreateOrdersRequestV5
    {
        [DataMember]
        public string ShipToAddressCode { get; set; }
    }

    public class CompletedOrder
    {
        public DateTime Date { get; set; }
        public string Id { get; set; }
        public string Type { get; set; }
        public CustomerInfo SellToCustomer { get; set; }
        public OrderLine[] Lines { get; set; }
        public MoneyViewModel SubTotal { get; set; }
        public MoneyViewModel Vat { get; set; }
        public MoneyViewModel Total { get; set; }
        public ShippingDetail Shipping { get; set; }
       // public string[] ImagePaths { get; set; }
        public IEnumerable<OrderObject> ImagePaths { get; set; }

        internal static CompletedOrder Map(SalesOrderInfo info)
        {
            return new CompletedOrder
            {
                Date = info.Date,
                Id = info.No,
                Type = "Order",
                SubTotal = MoneyViewModel.From(info.Lines.SubTotal),
                Vat = MoneyViewModel.From(info.Lines.Vat),
                Total = MoneyViewModel.From(info.Lines.Total),
                Lines = info.Lines.Select(l => new OrderLine
                {
                    Description = l.Description,
                    Amount = MoneyViewModel.From(l.Amount),
                    Vat = MoneyViewModel.From(l.Vat),
                    Total = MoneyViewModel.From(l.Total),
                }).ToArray(),
                SellToCustomer = new CustomerInfo
                {
                    Name = info.SellToCustomer.Name,
                    No = info.SellToCustomer.No
                },
                Shipping = new ShippingDetail
                {
                    Address = new Address
                    {
                        Name = info.Shipping.Address.Name,
                        PostCode = info.Shipping.Address.PostCode
                    },
                    Method = info.Shipping.Method,
                    Agent = info.Shipping.Agent,
                    Service = info.Shipping.Service
                },
                ImagePaths = info.ImagePaths != null ? info.ImagePaths

                .ToArray() : null
            };
        }

        public class CustomerInfo
        {
            public string No { get; set; }
            public string Name { get; set; }
        }

        public class OrderLine
        {
            public string Description { get; set; }

            public MoneyViewModel Amount { get; set; }
            public MoneyViewModel Vat { get; set; }
            public MoneyViewModel Total { get; set; }
        }

        public class ShippingDetail
        {
            public Address Address { get; set; }
            public string Method { get; set; }
            public string Agent { get; set; }
            public string Service { get; set; }
        }

        public class Address
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public string PostCode { get; set; }
        }
    }
}
