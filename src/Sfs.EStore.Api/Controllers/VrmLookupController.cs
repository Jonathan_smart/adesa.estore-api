using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Raven.Client;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Raven.Indexes;

namespace Sfs.EStore.Api.Controllers
{
    public class VrmLookupController : RavenEStoreController
    {
        private readonly Uri _capProxyServiceApiBaseUrl;

        public VrmLookupController(string capProxyServiceApiBaseUrl)
        {
            _capProxyServiceApiBaseUrl = new Uri(capProxyServiceApiBaseUrl);
        }

        public Task<HttpResponseMessage> Get(string vrm)
        {
            return Task.Factory.StartNew(() => GetVrmData(vrm));
        }

        private HttpResponseMessage GetVrmData(string vrm)
        {
            try
            {
                var any = Session.Query<Lot_Search.ReduceResult, Lot_Search>().AnyAsync(x => x.RegistrationPlate == vrm).Result;
                if (any)
                    return Request.CreateErrorResponse(HttpStatusCode.Forbidden, new CapVrmLookupException(vrm, "You cannot get a valuation for this vehicle"));

                var result = ExecuteCommand(new CapVrmLookup(_capProxyServiceApiBaseUrl) {Vrm = vrm});
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (CapVrmLookupException ex)
            {
                Logger.Error("", ex);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}