using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties;

namespace Sfs.EStore.Api.Controllers
{
    public class SendToFriendController : RavenEStoreController
    {
        private readonly IIntegrateSendToFriendRequests _integrator;

        public SendToFriendController(IIntegrateSendToFriendRequests integrator)
        {
            _integrator = integrator;
        }

        [ModelRequiredFilter]
        public Task<HttpResponseMessage> Post([FromUri]int listingId, SendToFriendPostModel @params)
        {
            return Task.Factory.StartNew(() => SendToFriend(listingId, @params));
        }

        private HttpResponseMessage SendToFriend(int listingId, SendToFriendPostModel @params)
        {
            if (!_integrator.Supported)
                return Request.CreateErrorResponse(HttpStatusCode.NotImplemented,
                    "Send to friend is not supported for this account");

            _integrator.Session = Session;

            var listing = Session.Include<Lot>(lot => lot.VehicleId)
                .LoadAsync<Lot>(Lot.IdFromLotNumber(listingId)).Result;
            var vehicle = Session.LoadAsync<Vehicle>(listing.VehicleId).Result;

            var listingVehicle = new ListingVehicle(listing, vehicle);

            _integrator.SendToFriend(User.MembershipUser.Identity, @params, listingVehicle);
            //DomainEvent.Raise(new SendToFriendReceived(User.MembershipUser.Identity, listingVehicle, @params));

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        public class SendToFriendPostModel
        {
            public string SenderFullName { get; set; }
            public string SenderEmail { get; set; }

            public string RecipientFullName { get; set; }
            [Required]
            public string RecipientEmail { get; set; }

            [Required]
            public string Message { get; set; }
        }
    }
}