﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.Core;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Properties;
using Sfs.EStore.Api.Raven.Indexes;
using Sfs.EStore.Api.Raven.Transformers;
using Sfs.EStore.Api.ThirdParties.Ports;
using Sfs.Orders;
using Sfs.Orders.Ports;
using Sfs.EStore.Api.Data;
using Sfs.Orders.SfsNavWebIntegration;

namespace Sfs.EStore.Api.Controllers
{
    [MembershipUserAuthorise]
    public class BasketGetController : RavenEStoreController
    {
        private readonly IContactBasketQuery _contactBasketQuery;
        private readonly BusinessUnit _businessUnit;
        private readonly SalesChannel _salesChannel;

        public BasketGetController(IContactBasketQuery contactBasketQuery,
        BusinessUnit businessUnit, SalesChannel salesChannel)
        {
            _contactBasketQuery = contactBasketQuery;
            _businessUnit = businessUnit;
            _salesChannel = salesChannel;
        }

        [Route("basket")]
        public Task<HttpResponseMessage> Get([FromUri]int[] no, [FromUri]string cusNo)
        {
            return Task.Factory.StartNew(() => GetReservationsForCurrentUser(no, cusNo));
        }

        [Route("basket")]
        public Task<HttpResponseMessage> Get([FromUri]int[] no)
        {
            return Task.Factory.StartNew(() => GetReservationsForCurrentUser(no, string.Empty));
        }

        public class BasketGetModel
        {
            public int No { get; set; }
            public ImageModel Image { get; set; }
            public string Title { get; set; }
            public string RegistrationPlate { get; set; }
            public string RegistrationYap { get; set; }
            public MoneyViewModel Amount { get; set; }
            public decimal BuyersFee { get; set; }
            public bool PricesIncludeVat { get; set; }
            public DateTime? ExpiresAt { get; set; }
            public int? ListingId { get; set; }
            public int Mileage { get; set; }
            public string Type { get; set; }
            public int LotId { get; set; }
            public bool HasBuyitNow { get; set; }
            public MoneyViewModel Markup { get; set; }
            public string Dealer { get; set; }
        }

        private static decimal GetBuyerFee(string registration, string customerNo, int salesChannel)
        {
            if (customerNo == null) throw new ArgumentNullException("customerNo");
            if (customerNo.Length <= 0) return 0;
            using (var ctx = new NavisionContext())
            {
                var serialNo =
                    ctx.Set<Data.SerialNoInformation>()
                        .Where(b => b.ItemNo == "VEHICLE" & b.RegistrationNo == registration)
                        .Select(n => n.SerialNo).FirstOrDefault();

                using (var webIntergration = new WebIntegration())
                {
                    webIntergration.Url = Settings.Default.Sfs_EStore_Api_NavWebIntegrationService_WebIntegration;
                    webIntergration.UseDefaultCredentials = true;

                    var results = webIntergration.GfncCalculateBuyersFee(serialNo, customerNo, salesChannel);

                    return results;
                }
            }
        }
        private HttpResponseMessage GetReservationsForCurrentUser(int[] entryNos, string cusNo = "")
        {
            var baskets = GetBaskets(entryNos);

            var regs = baskets.Select(x => x.Registration.Plate).ToList();
            var vehicles = Session.Query<Lot_Search.ReduceResult, Lot_Search>()
                .Where(
                    x =>
                        x.Status.In(new[] { LotStatusTypes.Active, LotStatusTypes.Reserved, LotStatusTypes.EndedWithSales }) &&
                        x.RegistrationPlate.In(regs))
                .TransformWith<LotIdToListingSummary, ListingSummaryViewModel>().ToListAsync().Result;

            var output = baskets.Select(b =>
            {
                var listing = vehicles.FirstOrDefault(x => x.VehicleInfo.Registration.Plate == b.Registration.Plate);

                var retailBasket = GetMarkupBasket(b.EntryNo);

                var model = new BasketGetModel
                {
                    Amount = MoneyViewModel.From(b.Amount),
                    ExpiresAt = b.ExpiresAt,
                    Image = b.PrimaryImage == null
                        ? null
                        : new ImageModel
                        {
                            Container = b.PrimaryImage.Container,
                            Name = b.PrimaryImage.FileName
                        },
                    ListingId = listing == null ? (int?)null : listing.ListingId,
                    Mileage = b.Mileage,
                    No = b.EntryNo,
                    PricesIncludeVat = b.PricesIncludeVat,
                    RegistrationPlate = b.Registration.Plate,
                    RegistrationYap = b.Registration.Yap,
                    Title = b.Title,
                    BuyersFee = _businessUnit == "FCA" ? GetBuyerFee(b.Registration.Plate, cusNo, (int)_salesChannel) : 0,
                    Type = listing == null ? string.Empty : listing.ListingInfo.ListingType,
                    LotId = listing == null ? 0 : listing.ListingId,
                    HasBuyitNow = b.BuyitNow,
                    Markup = new MoneyViewModel((decimal)retailBasket.Markup, Entities.Currency.GBP),
                    Dealer = retailBasket.Dealer
                };

                return model;
            }).ToArray();

            return Request.CreateResponse(HttpStatusCode.OK, output);
        }

        private MarkupBasket GetMarkupBasket(int entryNo)
        {
            var markupObject = Session.Query<MarkupBasket>().Where(x => x.BasketEntryNo == entryNo).ToListAsync().Result.FirstOrDefault();
            var i = markupObject != null ? markupObject.Markup : 0;

            return new Entities.MarkupBasket
            {
                Dealer = markupObject != null ? markupObject.Dealer : "",
                Markup = markupObject != null ? markupObject.Markup : 0
            };
        }

        private IPagedSet<ContactBasketQueryResponse> GetBaskets(int[] entryNos = null)
        {
            var contactUsername =
                ExecuteCommand(new GetContactUsername(User.MembershipUser.Identity.Name, _salesChannel));

            var range = new ContactBasketQueryRequest.QueryRange(0, 100);

            if (contactUsername == null)
                return EmptyPagesSet.Instance<ContactBasketQueryResponse>(range.Start, range.Max);

            return _contactBasketQuery.Handle(
                new ContactBasketQueryRequest(contactUsername.ContactNo,
                    range,
                    entryNos == null ? null : entryNos.Select(x => new BasketEntryNo(x)).ToArray()));
        }
    }
}