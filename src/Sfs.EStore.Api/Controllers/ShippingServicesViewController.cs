﻿using Sfs.Core;
using Sfs.EStore.Api.Data;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Sfs.EStore.Api.Controllers
{
    public class ShippingServicesViewController : RavenEStoreController
    {
        private readonly SalesChannel _salesChannel;

        public ShippingServicesViewController(SalesChannel salesChannel)
        {
            _salesChannel = salesChannel;
        }

        public Task<HttpResponseMessage> Get([FromUri]Form form)
        {
            return Task.Factory.StartNew(() => GetShippingServices(form));
        }

        private HttpResponseMessage GetShippingServices(Form form)
        {
            using (var ctx = new NavisionContext())
            {
                bool isCommercial = true;
                var myOptions = new[] { 0, isCommercial ? 1 : 2 };

                var methodAgentServices = ctx.Set<TransportSalesPrice>().Where(x => x.SalesChannelNo == _salesChannel)
                    .Where(x => myOptions.Contains(x.CommercialVehicle))
                    .Where(x => x.ShipmentMethodCode == form.Method)
                    .Select(x => new { x.ShippingAgentCode, x.ShippingAgentServiceCode, x.ShipmentMethodCode })
                    .Distinct()
                    .Join(ctx.Set<Data.ShippingAgentService>(),
                        t => new { t.ShippingAgentCode, t.ShippingAgentServiceCode },
                        s => new { s.ShippingAgentCode, ShippingAgentServiceCode = s.Code },
                        (t, s) => new { s.Code, s.Description, t.ShipmentMethodCode, t.ShippingAgentCode })
                    .ToList();

                return Request.CreateResponse(HttpStatusCode.OK, methodAgentServices);
            }
        }


        public class Form
        {
            public string Method { get; set; }
        }

        public class ShippingAgentService
        {
            public ShippingAgentService(string code, string name, ShippingAgent agent)
            {
                if (code == null) throw new ArgumentNullException("code");
                if (name == null) throw new ArgumentNullException("name");
                if (agent == null) throw new ArgumentNullException("agent");

                Code = code;
                Name = name;
                Agent = agent;
            }

            public string Code { get; private set; }
            public string Name { get; private set; }
            public ShippingAgent Agent { get; private set; }
        }

        public class ShippingAgent
        {
            public ShippingAgent(string code, string name)
            {
                if (code == null) throw new ArgumentNullException("code");

                Code = code;
                Name = name;
            }

            public string Code { get; private set; }
            public string Name { get; private set; }
        }
    }
}
