using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Contacts.Adapters.Gm;
using Sfs.EStore.Contacts.Ports.Gm;

namespace Sfs.EStore.Api.Controllers
{
    [RoutePrefix("membership/{username}/buying-status")]
    public class UserBuyingStatusController : RavenEStoreController
    {
        private readonly ICommandProcessor _commandProcessor;
        private readonly IContactUsernameReaderDto _contactUsernameReader;

        public UserBuyingStatusController(ICommandProcessor commandProcessor, IContactUsernameReaderDto contactUsernameReader)
        {
            _commandProcessor = commandProcessor;
            _contactUsernameReader = contactUsernameReader;
        }

        [HttpGet, Route]
        public Task<HttpResponseMessage> GetBuyingStatus(string username)
        {
            return Task.Factory.StartNew(() =>
            {
                var usernames = _contactUsernameReader.GetForUsername(username);

                var setupForBuying = usernames.Any();

                return Request.CreateResponse(HttpStatusCode.OK,
                    new
                    {
                        SetUpForBuying = setupForBuying,
                        ContactNos = usernames.Select(x => x.ContactNo).ToArray()
                    });
            });
        }

        [HttpPut, Route("{customerNo}")]
        public Task<HttpResponseMessage> Put(string username, string customerNo)
        {
            Logger.InfoFormat("Setting buy access for {0} of customer {1}", username, customerNo);

            return Task.Factory.StartNew(() =>
            {
                try
                {
                    _commandProcessor.Send(new LinkEStoreUserToCustomer(username, customerNo));
                }
                catch (CustomerNotFound)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            string.Format("Cannot find customer '{0}'", customerNo));
                }
                catch (MissingDefaultContactForCustomer)
                {
                    Logger.WarnFormat("Web contact not set up for customer {0}", customerNo);

                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        string.Format("Default contact not found for customer '{0}'", customerNo));
                }

                return Request.CreateResponse(HttpStatusCode.Created);
            });
        }


        [HttpDelete, Route("{customerNo}")]
        public Task<HttpResponseMessage> Delete(string username, string customerNo)
        {
            Logger.InfoFormat("Revoking buy access for {0} of customer {1}", username, customerNo);

            return Task.Factory.StartNew(() =>
            {
                try
                {
                    _commandProcessor.Send(new RemoveEStoreUserToCustomerLink(username, customerNo));
                }
                catch (CustomerNotFound)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            string.Format("Cannot find customer '{0}'", customerNo));
                }
                catch (MissingDefaultContactForCustomer)
                {
                    Logger.WarnFormat("Web contact not set up for customer {0}", customerNo);

                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        string.Format("Default contact not found for customer '{0}'", customerNo));
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            });
        }
    }
}