using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.Core;
using Sfs.EStore.Api.Data;

namespace Sfs.EStore.Api.Controllers
{
    [MembershipUserAuthorise]
    public class ShippingMethodsController : RavenEStoreController
    {
        private readonly SalesChannel _salesChannel;

        public ShippingMethodsController(SalesChannel salesChannel)
        {
            _salesChannel = salesChannel;
        }

        public Task<HttpResponseMessage> Get()
        {
            return Task.Factory.StartNew(() => GetShippingMethods());
        }

        private HttpResponseMessage GetShippingMethods()
        {
            using (var ctx = new NavisionContext())
            {
                var methods = ctx.Set<TransportSalesPrice>().Where(x => x.SalesChannelNo == _salesChannel)
                    .GroupBy(x => x.ShipmentMethodCode)
                    .Select(x => new { ShipmentMethodCode = x.Key, RequiresShippingInfo = x.Any(y => y.ShippingAgentCode != "") })
                    .Join(ctx.Set<ShipmentMethod>(),
                        m => m.ShipmentMethodCode,
                        m => m.Code,
                        (a, b) => new { b.Code, b.Description, a.RequiresShippingInfo })
                    .ToList();

                return Request.CreateResponse(HttpStatusCode.OK,
                    methods.Select(x => new ShippingMethod(x.Code, x.Description, x.RequiresShippingInfo)).ToArray());
            }
        }

        public class ShippingMethod
        {
            public ShippingMethod(string code, string description, bool requiresAddress)
            {
                RequiresShippingInfo = requiresAddress;
                Description = description;
                Code = code;
            }

            public string Code { get; private set; }
            public string Description { get; private set; }
            public bool RequiresShippingInfo { get; private set; }
        }
    }
}