using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Sfs.Orders.Ports;

namespace Sfs.EStore.Api.Controllers
{
    [MembershipUserAuthorise]
    public class CheckoutFinanceOptionsController : RavenEStoreController
    {
        private readonly IFinanceOptionsQuery _financeOptionsDto;

        public CheckoutFinanceOptionsController(IFinanceOptionsQuery financeOptionsDto)
        {
            _financeOptionsDto = financeOptionsDto;
        }

        public Task<HttpResponseMessage> Get()
        {
            return Task.Factory.StartNew(() => GetFinanceOptions());
        }

        private HttpResponseMessage GetFinanceOptions()
        {
            var options = _financeOptionsDto.GetAll();

            return Request.CreateResponse(HttpStatusCode.OK,
                options.Select(x => new FinanceOption
                {
                    Agent = x.Agent,
                    Method = x.Method,
                    Description = x.Description,
                    Service = x.Service,
                    CreditType = x.CreditType
                }).ToArray());
        }

        public class FinanceOption
        {
            public string Description { get; set; }
            public string Method { get; set; }
            public string Agent { get; set; }
            public string Service { get; set; }
            public string CreditType { get; set; }
        }
    }
}