using Sfs.EStore.Api.FurtherVehicleInfoServiceReference;
using System;

namespace Sfs.EStore.Api.Services
{
    public class CarwebProxyService : ICarwebService, IDisposable
    {
        private readonly FurtherInfoServiceClient _client = new FurtherInfoServiceClient();

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                ((IDisposable)_client).Dispose();
            }
            // free native resources
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public SpecificationResult GetSpecificationsFor(string registration)
        {
            return _client.GetSpecificationsFor(new RegistrationPayload { Value = registration });
        }
    }
}