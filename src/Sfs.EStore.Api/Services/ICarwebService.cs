using Sfs.EStore.Api.FurtherVehicleInfoServiceReference;

namespace Sfs.EStore.Api.Services
{
    public interface ICarwebService
    {
        SpecificationResult GetSpecificationsFor(string registration);
    }
}