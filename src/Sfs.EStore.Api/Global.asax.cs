﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Mvc;
using log4net.Repository.Hierarchy;
using Mindscape.Raygun4Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Raven.Client;
using Raven.Client.Document;
using Sfs.EStore.Api.Authentication;
using log4net;
using log4net.Config;
using Mindscape.Raygun4Net.Messages;
using Serilog;

namespace Sfs.EStore.Api
{
    public class WebApiApplication : HttpApplication
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(WebApiApplication));
        private readonly static ConcurrentDictionary<string, Lazy<IDocumentStore>> Stores = new ConcurrentDictionary<string, Lazy<IDocumentStore>>();

        private static readonly LazyAccountDocumentStoreFactory AccountStores =
            new LazyAccountDocumentStoreFactory(Stores, ConfigurationManager.AppSettings["Raven/Url"]);

        public static IDocumentStore GetAccountDocumentStoreFor(string databaseName)
        {
            Logger.Debug("Dataase :" + databaseName);
            return AccountStores.GetAccountDocumentStoreFor(databaseName);
        }

        public static IDocumentStore GetApiDocumentStore()
        {
            const string databaseName = "eStore.api";
            return Stores.GetOrAdd(databaseName,
                                   dbName => new Lazy<IDocumentStore>(
                                                 () => new DocumentStore
                                                           {
                                                               Url = ConfigurationManager.AppSettings["Raven/Url"],
                                                               DefaultDatabase = databaseName
                                                           }.Initialize())).Value;
        }

        protected void Application_Error()
        {
        }

        protected void Application_Start()
        {
            XmlConfigurator.ConfigureAndWatch(new FileInfo(Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory, "log4net.config")));

            Log.Logger = new LoggerConfiguration()
                .WriteTo.Log4Net()
                .CreateLogger();

            var formatters = GlobalConfiguration.Configuration.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // All methods will use ExceptionFilter to deal with exceptions
            GlobalConfiguration.Configuration.Filters.Add(new ExceptionFilter());

            AreaRegistration.RegisterAllAreas();

            //WebApiConfig.Register(GlobalConfiguration.Configuration, NinjectWebCommon.Kernel);
            GlobalConfiguration.Configure(cfg => WebApiConfig.Register(cfg, NinjectWebCommon.Kernel));

            GlobalConfiguration.Configuration.MessageHandlers.Add(
                new AuthorizationHeaderHandler(GetApiDocumentStore(),
                                               GetAccountDocumentStoreFor));

            GlobalConfiguration.Configuration.MessageHandlers.Add(new WebApiUsageHandler());
        }

        protected void Application_End()
        {
            NinjectWebCommon.Kernel.Dispose();
        }
    }

    public class ExceptionFilter : ExceptionFilterAttribute
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(ExceptionFilter));

        public override void OnException(HttpActionExecutedContext context)
        {
            var client = new RaygunClient
            {
                User = Thread.CurrentPrincipal.Identity.Name
            };
            client.IgnoreFormFieldNames("Password");
            client.IgnoreHeaderNames("Authorization");

            var principal = Thread.CurrentPrincipal as IApiApplicationPrincipal;
            if (principal != null)
            {
                client.UserInfo = new RaygunIdentifierMessage(principal.MembershipUser.Identity.Name);
            }

            client.SendInBackground(context.Exception, null, new Hashtable
            {
                {"current_principal", Thread.CurrentPrincipal as IApiApplicationPrincipal},
                {"logged_from", "ExceptionFilter"}
            });
            _logger.Error(context.Exception.Message, context.Exception);
        }
    }
}
