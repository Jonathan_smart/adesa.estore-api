using System;
using System.Net.Http;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Commands
{
    /// <exception cref="InvalidOperationException">A required parameter is missing</exception>
    /// <exception cref="ValuationException">Failed to access valuation service</exception>
    public class CapValuation : Command<ValuationModel>
    {
        private readonly Uri _capProxyServiceApiBaseUrl;

        public CapValuation(Uri capProxyServiceApiBaseUrl)
        {
            _capProxyServiceApiBaseUrl = capProxyServiceApiBaseUrl;
        }

        public int? CapId { get; set; }
        public int? Mileage { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string VehicleType { get; set; }

        public override void Execute()
        {
            if (string.IsNullOrWhiteSpace(VehicleType))
                throw new InvalidOperationException("VehicleType property is required");

            if (!Mileage.HasValue)
                throw new InvalidOperationException("Mileage property is required");

            if (!CapId.HasValue)
                throw new InvalidOperationException("CapId property is required");

            if (!RegistrationDate.HasValue)
                throw new InvalidOperationException("RegistrationDate property is required");

            var apiResponse = new HttpClient {BaseAddress = _capProxyServiceApiBaseUrl}
                .GetAsync(string.Format("valuation?capid={0}&mileage={1}&registrationdate={2}&database={3}",
                                        CapId, Mileage, RegistrationDate.Value.ToString("yyyy-MM-dd"), Database(VehicleType)))
                .Result;

            if (!apiResponse.IsSuccessStatusCode)
                throw new ValuationException(string.Format("Status code {0} does not indicate success",
                                                           apiResponse.StatusCode));

            var json = apiResponse.Content.ReadAsAsync<JObject>().Result;

            Result = ValuationModel.FromCap(json);
        }

        private static string Database(string vehicleType)
        {
            return "VAN".Equals(vehicleType, StringComparison.InvariantCultureIgnoreCase)
                       ? "LCV"
                       : vehicleType;
        }
    }

    public class ValuationModel
    {
        public MoneyViewModel Retail { get; set; }
        public MoneyViewModel Clean { get; set; }
        public MoneyViewModel Average { get; set; }
        public MoneyViewModel Below { get; set; }

        public VehicleDetail Vehicle { get; set; }

        public static ValuationModel FromCap(JObject result)
        {
            var model = new ValuationModel
                            {
                                Average = new MoneyViewModel(result.Value<decimal>("Average"), Currency.GBP),
                                Below = new MoneyViewModel(result.Value<decimal>("Below"), Currency.GBP),
                                Clean = new MoneyViewModel(result.Value<decimal>("Clean"), Currency.GBP),
                                Retail = new MoneyViewModel(result.Value<decimal>("Retail"), Currency.GBP),
                                Vehicle = result.Value<JObject>("Vehicle").ToObject<VehicleDetail>()//JsonConvert.DeserializeObject<VehicleDetail>()// JObject.Parse()// result.Value<VehicleDetail>("Vehicle") ?? new VehicleDetail()
                            };

            return model;
        }

        public class VehicleDetail
        {
            public string Make { get; set; }
            public string Model { get; set; }
            public string Derivative { get; set; }
        }
    }

    [Serializable]
    public class ValuationException : Exception
    {
        public ValuationException()
        {
        }

        public ValuationException(string message) : base(message)
        {
        }

        public ValuationException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ValuationException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}