using System;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace Sfs.EStore.Api.Commands
{
    /// <exception cref="InvalidOperationException">A required parameter is missing</exception>
    /// <exception cref="ValuationException">Failed to access valuation service</exception>
    public class CapValuationByVrm : Command<ValuationModel>, IQuery<ValuationModel>
    {
        private readonly Uri _capProxyServiceApiBaseUrl;

        public CapValuationByVrm(Uri capProxyServiceApiBaseUrl)
        {
            _capProxyServiceApiBaseUrl = capProxyServiceApiBaseUrl;
        }

        public string Vrm { get; set; }
        public int Mileage { get; set; }

        public override void Execute()
        {
            if (string.IsNullOrWhiteSpace(Vrm))
                throw new InvalidOperationException("Vrm property is required");

            if (Mileage < 0)
                throw new InvalidOperationException("Mileage property is required");

            var apiResponse = new HttpClient { BaseAddress = _capProxyServiceApiBaseUrl }
                .GetAsync(string.Format("valuation?registrationNumber={0}&mileage={1}", Vrm.Replace(" ", "").Trim(), Mileage))
                .Result;

            if (!apiResponse.IsSuccessStatusCode)
                throw new ValuationException(string.Format("Status code {0} does not indicate success",
                                                           apiResponse.StatusCode));

            var json = apiResponse.Content.ReadAsAsync<JObject>().Result;

            Result = ValuationModel.FromCap(json);
        }
    }
}