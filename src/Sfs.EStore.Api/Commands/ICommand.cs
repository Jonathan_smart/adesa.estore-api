﻿using Raven.Client;

namespace Sfs.EStore.Api.Commands
{
    public interface ICommand
    {
        IAsyncDocumentSession Session { set; }

        void Execute();
    }

    public abstract class Command : ICommand
    {
        public IAsyncDocumentSession Session { get; set; }

        public abstract void Execute();

        protected virtual TResult Query<TResult>(IQuery<TResult> query)
        {
            query.Session = Session;

            query.Execute();
            return query.Result;
        }
    }

    public abstract class Command<TResult> : Command, ICommand<TResult>
    {
        public TResult Result { get; protected set; }
    }

    public interface ICommand<out TResult> : ICommand
    {
        TResult Result { get; }
    }

    public interface IQuery<out TResult> : ICommand<TResult>
    {
    }
}