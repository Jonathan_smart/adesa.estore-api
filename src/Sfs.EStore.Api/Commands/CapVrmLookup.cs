using System;
using System.Net.Http;
using System.Runtime.Serialization;

namespace Sfs.EStore.Api.Commands
{
    public class CapVrmLookup : Command<CapVrmLookupResult>
    {
        private readonly Uri _capProxyServiceApiBaseUrl;

        public CapVrmLookup(Uri capProxyServiceApiBaseUrl)
        {
            _capProxyServiceApiBaseUrl = capProxyServiceApiBaseUrl;
        }

        public string Vrm { get; set; }

        public override void Execute()
        {
            var response = new HttpClient {BaseAddress = _capProxyServiceApiBaseUrl}
                .GetAsync(string.Format("VrmLookup?id={0}", Vrm)).Result;

            if (!response.IsSuccessStatusCode)
                throw new CapVrmLookupException(Vrm, string.Format("Status code {0} does not indicate success",
                    response.StatusCode));

            Result = response.Content.ReadAsAsync<CapVrmLookupResult>().Result;
        }
    }

    public class CapVrmLookupResult
    {
        public int CapId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Derivative { get; set; }
        public string RegistrationNumber { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string VehicleType { get; set; }
        public string FuelType { get; set; }
        public string Transmission { get; set; }
        public string Colour { get; set; }
        public int? EngineCapacity { get; set; }
    }

    [Serializable]
    public class CapVrmLookupException : Exception
    {
        public CapVrmLookupException()
        {
        }

        public CapVrmLookupException(string vrm, string message)
            : base(message)
        {
            Vrm = vrm;
        }

        public CapVrmLookupException(string vrm, string message, Exception inner)
            : base(message, inner)
        {
            Vrm = vrm;
        }

        protected CapVrmLookupException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }

        public string Vrm { get; private set; }
    }
}