using System;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.DomainEvents;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Commands
{
    public class PartExchangeValuation : Command<SavedValuation>
    {
        private readonly Uri _capProxyServiceApiBaseUrl;

        public PartExchangeValuation(MembershipUserIdentity identity, string vrm, int mileage, PartExValuationModel request, 
            Uri capProxyServiceApiBaseUrl)
        {
            Identity = identity;
            Vrm = (vrm ?? "").Replace(" ", "").Trim();
            Mileage = mileage;
            Request = request;
            _capProxyServiceApiBaseUrl = capProxyServiceApiBaseUrl;
        }

        public MembershipUserIdentity Identity { get; private set; }
        public string Vrm { get; private set; }
        public int Mileage { get; private set; }
        public PartExValuationModel Request { get; private set; }

        public override void Execute()
        {
            var result = Query(new CapValuationByVrm(_capProxyServiceApiBaseUrl) { Vrm = Vrm, Mileage = Mileage});

            var valuation = new SavedValuation(Vrm, Mileage, result);

            Result = valuation;

            if (Identity.IsAuthenticated)
            {
                var userSavedValuation = GetOrCreateUserSavedValuation(Identity);
                userSavedValuation.AddValuation(valuation);
            }

            DomainEvent.Raise(new PartExchangeValuationCreated(Identity, valuation, Request));
        }

        private UserSavedValuations GetOrCreateUserSavedValuation(MembershipUserIdentity identity)
        {
            var savedValuations = Session.LoadAsync<UserSavedValuations>(UserSavedValuations.IdFrom(identity.Id)).Result;

            if (savedValuations != null) return savedValuations;

            savedValuations = new UserSavedValuations(identity.Id);
            Session.StoreAsync(savedValuations).Wait();

            return savedValuations;
        }
    }
}