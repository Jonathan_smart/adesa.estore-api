using Raven.Client;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.DomainEvents;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Commands
{
    public class AuthenticatedUserQuery : IQuery<IAuthenticationResponseModel>
    {
        public IAuthenticationResponseModel Result { get; private set; }
        public IAsyncDocumentSession Session { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public void Execute()
        {
            var user = Session.Query<MembershipUser>()
                .FirstOrDefaultAsync(x => x.Username == Username)
                .Result;

            if (user == null)
            {
                Result = new AuthenticationResponseModel_NotAuthenticated
                    {
                        IsAuthenticated = false,
                        Message = AuthCodes.AuthFailed
                    };
                return;
            }

            if (user.IsLockedOut)
            {
                Result = new AuthenticationResponseModel_NotAuthenticated
                    {
                        IsAuthenticated = false,
                        Message = AuthCodes.LockedOut
                    };
                return;
            }

            if (!user.VerifyPassword(Password))
            {
                const int maximumInvalidPasswordAttempts = 4;

                if (user.InvalidPasswordAttempts >= maximumInvalidPasswordAttempts)
                {
                    user.Lock();
                }

                Result = new AuthenticationResponseModel_NotAuthenticated
                    {
                        IsAuthenticated = false,
                        Message = user.IsLockedOut
                                      ? AuthCodes.NewLockedOut
                                      : AuthCodes.AuthFailed,
                    };
                return;
            }

            if (!user.IsApproved)
            {
                Result = new AuthenticationResponseModel_NotAuthenticated
                    {
                        IsAuthenticated = false,
                        Message = AuthCodes.AwaitingApproval
                    };
                return;
            }

            DomainEvent.Raise(new UserAuthenticated(user));

            Result = AuthenticationResponseModel_Authenticated.Create(user);
        }
    }
}