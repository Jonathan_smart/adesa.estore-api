using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.ModelBinding;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties;
using log4net;

namespace Sfs.EStore.Api.Commands
{
    public class PutLotCommand : Command<HttpResponseMessage>
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(PutLotCommand));
        private readonly IVehicleEnricher[] _putVehicleEnrichers;
        private readonly HttpRequestMessage _request;
        private readonly ModelStateDictionary _modelState;

        public PutLotCommand(IVehicleEnricher[] putVehicleEnrichers, HttpRequestMessage request, ModelStateDictionary modelState)
        {
            _putVehicleEnrichers = putVehicleEnrichers;
            _request = request;
            _modelState = modelState;
        }

        public LotPutModel Model { get; set; }

        public override void Execute()
        {
            if (Model == null)
                throw new InvalidOperationException("Model must be set before executing command");

            if (!string.IsNullOrEmpty(Model.Status) && !string.IsNullOrWhiteSpace(Model.LotId))
            {
                Result = UpdateStatus(Model.LotId, Model.Status);
                return;
            }

            Result = CreateOrUpdateLot(Model);
        }

        private HttpResponseMessage UpdateStatus(string lotId, string status)
        {
            var entities = LoadExistingEntitesFromLotId(lotId);

            if (entities == null)
            {
                _modelState.AddModelError(Model, x => x.LotId, "Error");
                return _request.CreateErrorResponse(HttpStatusCode.BadRequest, _modelState);
            }

            var lot = entities.Item1;

            var actions = new Dictionary<string, Action<Lot>>(StringComparer.InvariantCultureIgnoreCase)
                              {
                                  {"activate", l => l.Activate()},
                                  {"reserve", l => l.Reserve()},
                                  {"unreserve", l => l.UnReserve()},
                                  {"archive", l => l.Archive()},
                                  {"end", l => l.End()}
                              };

            if (!actions.ContainsKey(status))
            {
                return _request.CreateErrorResponse(HttpStatusCode.BadRequest,
                                                    string.Format("'{0}' is not a valid status update request", status));
            }

            try
            {
                actions[status](lot);
            }
            catch (InvalidOperationException ex)
            {
                var message = string.Format("'{0}' is not a valid status update request for {1} (Current status: {2})",
                                            status, lot.Id, lot.Status);
                Logger.Warn(message, ex);
                return _request.CreateErrorResponse(HttpStatusCode.BadRequest, message);
            }

            return CreateResponseFor(lot, HttpStatusCode.Accepted);
        }

        private HttpResponseMessage CreateOrUpdateLot(LotPutModel bound)
        {
            if (!ValidateCreateOrUpdate(bound))
                return _request.CreateErrorResponse(HttpStatusCode.BadRequest, _modelState);

            return !string.IsNullOrWhiteSpace(bound.LotId) ? UpdateExisting(bound) : CreateNewLot(bound);
        }

        private HttpResponseMessage CreateNewLot(LotPutModel bound)
        {
            if (!string.IsNullOrWhiteSpace(bound.RegistrationPlate))
            {
                var existingRegistration = LotExistsForRegistration(bound.RegistrationPlate);

                ExistingLotForRegistrationResult.ExistingListingSummry clashingListing;
                if (!existingRegistration.CanCoExistWith(bound, out clashingListing))
                {
                    _modelState.AddModelError("RegistrationPlate",
                                              string.Format(
                                                  "A lot already exists with registration {0} (Status: {2}). " +
                                                  "To update the existing lot, specify it's lot id ({1}) in the request.",
                                                  bound.RegistrationPlate, clashingListing.ListingId, clashingListing.SellingState));

                    return _request.CreateErrorResponse(HttpStatusCode.BadRequest, _modelState);
                }
            }

            var vehicle = new Vehicle();
            Session.StoreAsync(vehicle).Wait();

            var vehicleData = new VehicleData { Id = vehicle.Id + "/Data" };
            Session.StoreAsync(vehicleData).Wait();

            var lot = new Lot();
            Session.StoreAsync(lot).Wait();
            lot.LinkToVehicle(vehicle);

            bound.BindTo(lot, vehicle, vehicleData);
            Enrich(vehicle, vehicleData);

            return CreateResponseFor(lot);
        }

        private HttpResponseMessage UpdateExisting(LotPutModel bound)
        {
            var entitiesForLotId = LoadExistingEntitesFromLotId(bound.LotId);

            if (entitiesForLotId == null) return _request.CreateErrorResponse(HttpStatusCode.BadRequest, _modelState);

            if (entitiesForLotId.Item1.Status.In(LotStatusTypes.EndedWithSales, LotStatusTypes.EndedWithoutSales, LotStatusTypes.Reserved))
            {
                if (entitiesForLotId.Item1.Status.In(LotStatusTypes.Reserved)) return  CreateResponseFor(entitiesForLotId.Item1);

                _modelState.AddModelError("LotId",
                                          string.Format("Lot {0} cannot be update at status {1}",
                                                        entitiesForLotId.Item1.Id, entitiesForLotId.Item1.Status));
                return _request.CreateErrorResponse(HttpStatusCode.Forbidden, _modelState);
            }

            bound.BindTo(entitiesForLotId.Item1, entitiesForLotId.Item2, entitiesForLotId.Item3);
            Enrich(entitiesForLotId.Item2, entitiesForLotId.Item3);

            return CreateResponseFor(entitiesForLotId.Item1);
        }

        private void Enrich(Vehicle vehicle, VehicleData vehicleData)
        {
            try
            {
                new CompositeVehicleEnricher(_putVehicleEnrichers)
                    .Enrich(vehicle, vehicleData, Session.Advanced.GetMetadataFor(vehicle));
            }
            catch (VehicleEnrichmentException ex)
            {
                Logger.Error(string.Format("Vehicle enrichment failed. Vehicle: {0}; Enricher: {1}",
                                           ex.VehicleId, ex.Enricher), ex);
            }
        }

        private HttpResponseMessage CreateResponseFor(Lot lot, HttpStatusCode status = HttpStatusCode.OK)
        {
            return _request.CreateResponse(status, new LotPutModelResponse
            {
                LotId = lot.Id,
                Status = lot.Status.ToString()
            });
        }

        private bool ValidateCreateOrUpdate(LotPutModel bound)
        {
            var isValid = true;
            if (string.IsNullOrWhiteSpace(bound.Title))
            {
                _modelState.AddModelError(bound, x => x.Title, "The Title field is required.");
                isValid = false;
            }
            if (string.IsNullOrWhiteSpace(bound.Make))
            {
                _modelState.AddModelError(bound, x => x.Make, "The Make field is required.");
                isValid = false;
            }
            if (string.IsNullOrWhiteSpace(bound.Model))
            {
                _modelState.AddModelError(bound, x => x.Model, "The Model field is required.");
                isValid = false;
            }
            if (string.IsNullOrWhiteSpace(bound.VehicleType))
            {
                _modelState.AddModelError(bound, x => x.VehicleType, "The VehicleType field is required.");
                isValid = false;
            }

            if (bound.StartingPrice != null)
            {
                if (bound.BidIncrements == null)
                {
                    _modelState.AddModelError(bound, x => x.BidIncrements, "The BidIncrements field is required for Auction listings.");
                    isValid = false;
                }
            }
            else if (bound.BuyItNowPrice == null)
            {
                _modelState.AddModelError(bound, x => x.BuyItNowPrice, "The BuyItNowPrice field is required for FixedPrice listings.");
                isValid = false;
            }

            if (!bound.UniqueExternalReferenceNumber.HasValue)
            {
                _modelState.AddModelError(bound, x => x.UniqueExternalReferenceNumber, "The UniqueExternalReferenceNumber field is required.");
                isValid = false;
            }

            return isValid;
        }

        private Tuple<Lot, Vehicle, VehicleData> LoadExistingEntitesFromLotId(string lotId)
        {
            var lot = Session
                .Include<Lot>(x => x.VehicleId)
                .Include(x => x.VehicleId + "/Data")
                .LoadAsync<Lot>(lotId).Result;

            if (lot == null)
            {
                _modelState.AddModelError("LotId", string.Format("Lot with Id '{0}' cannot be found", lotId));
                return null;
            }

            var vehicle = Session.LoadAsync<Vehicle>(lot.VehicleId).Result;
            var vehicleData = Session.LoadAsync<VehicleData>(lot.VehicleId + "/Data").Result;

            return Tuple.Create(lot, vehicle, vehicleData);
        }

        private ExistingLotForRegistrationResult LotExistsForRegistration(string registrationPlate)
        {
            var lotsWithRegistration = new LotQueryBuilder(Session)
                             {
                                 QueryForStatuses = null
                             }
                .Build(new LotsGetParams
                           {
                               Registration = registrationPlate
                           })
                .Customize(x => x.WaitForNonStaleResultsAsOfNow())
                .ToListAsync()
                .Result;

            var result = Session.LoadAsync<Lot>(lotsWithRegistration.Select(x => Lot.IdFromLotNumber(x.ListingId)))
                .Result;

            if (Logger.IsDebugEnabled)
                Logger.DebugFormat("LotExistsForRegistration: {0} - Found {1} existing lots: \n{2}\n{3}",
                    registrationPlate,
                    lotsWithRegistration.Count,
                    string.Join("; ",
                        lotsWithRegistration.Select(
                            x => string.Format("{0}: {1} ({2} - {3})", x.ListingId, x.SellingStatus.SellingState, x.ListingInfo.StartTime, x.ListingInfo.EndTime))),
                    string.Join("; ",
                        result.Select(
                            x => string.Format("{0}: {1} ({2} - {3})", x.LotNumber, x.Status, x.StartDate, x.EndDate))));

            return new ExistingLotForRegistrationResult(result
                .Select(x =>
                    new ExistingLotForRegistrationResult.ExistingListingSummry(x.LotNumber,
                        x.Status.ToString(),
                        x.StartDate, x.EndDate)));
        }

        private class ExistingLotForRegistrationResult
        {
            public ExistingLotForRegistrationResult(IEnumerable<ExistingListingSummry> lots = null)
            {
                _lots = lots ?? new ExistingListingSummry[0];
            }

            private readonly IEnumerable<ExistingListingSummry> _lots;

            public bool CanCoExistWith(LotPutModel bound, out ExistingListingSummry clashingListing)
            {
                if (!_lots.Any())
                {
                    clashingListing = null;
                    return true;
                }

                clashingListing = _lots.FirstOrDefault(x => !CanCoExist(bound, x));
                return clashingListing == null;
            }

            private bool CanCoExist(LotPutModel bound, ExistingListingSummry withLot)
            {
                var allowedByStatus = withLot.SellingState.In(LotStatusTypes.Cancelled.ToString(),
                                                    LotStatusTypes.EndedWithSales.ToString(),
                                                    LotStatusTypes.EndedWithoutSales.ToString(),
                                                    LotStatusTypes.Archived.ToString());

                if (allowedByStatus) return true;

                var allowedByActiveDates = (withLot.StartTime.HasValue || withLot.EndTime.HasValue) &&
                                           ((withLot.StartTime.HasValue && bound.EndDate.HasValue &&
                                             (bound.EndDate <= withLot.StartTime)) ||
                                            (withLot.EndTime.HasValue && bound.StartDate.HasValue &&
                                             (bound.StartDate >= withLot.EndTime)));

                if (!allowedByActiveDates && Logger.IsInfoEnabled)
                {
                    Logger.InfoFormat(
                        "existingLotId: {0}; existingLotStartDate: {1}; existingLotEndDate: {2}; newLotStartDate: {3}; newLotEndDate: {4}",
                        withLot.ListingId, withLot.StartTime, withLot.EndTime, bound.StartDate, bound.EndDate);
                }

                return allowedByActiveDates;
            }

            public class ExistingListingSummry
            {
                public ExistingListingSummry(int listingId, string sellingState, DateTime? startTime, DateTime? endTime)
                {
                    ListingId = listingId;
                    SellingState = sellingState;
                    StartTime = startTime;
                    EndTime = endTime;
                }

                public int ListingId { get; private set; }
                public string SellingState { get; private set; }
                public DateTime? StartTime { get; private set; }
                public DateTime? EndTime { get; private set; }
            }
        }
    }
}