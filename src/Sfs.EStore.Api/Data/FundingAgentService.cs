﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    internal class FundingAgentService
    {
        public string Code { get; set; }
        public byte Credit { get; set; }
        public string Agent { get; set; }
        public string Method { get; set; }
        public string Description { get; set; }
        public string FundingAgentReference { get; set; }
    }

    internal class FundingAgentServiceMapper : EntityTypeConfiguration<FundingAgentService>
    {
        public FundingAgentServiceMapper()
        {
            ToTable("SFS$Funding Agent Services");
            HasKey(e => e.Code);
            Property(e => e.Code).HasColumnName("Funding Agent Service Code");

            Property(e => e.Credit).HasColumnName("Credit");
            Property(e => e.Agent).HasColumnName("Funding Agent Code");
            Property(e => e.Method).HasColumnName("Funding Method");
            Property(e => e.Description).HasColumnName("Description");
            Property(e => e.FundingAgentReference).HasColumnName("Funding Agent Reference");
        }
    }
}