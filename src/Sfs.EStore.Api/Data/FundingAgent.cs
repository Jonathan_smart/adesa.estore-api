﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    internal class FundingAgent
    {
        public string Agent { get; set; }
        public string BusinessUnit { get; set; }
    }

    internal class FundingAgentMapper : EntityTypeConfiguration<FundingAgent>
    {
        public FundingAgentMapper()
        {
            ToTable("SFS$Funding Agent");
            HasKey(e => e.Agent);
            Property(e => e.Agent).HasColumnName("Funding Agent");
            Property(e => e.BusinessUnit).HasColumnName("Business Unit");
        }
    }
}