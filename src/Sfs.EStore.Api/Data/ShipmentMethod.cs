﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    internal class ShipmentMethod
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public byte Vehicle { get; set; }
    }

    internal class ShipmentMethodMapper : EntityTypeConfiguration<ShipmentMethod>
    {
        public ShipmentMethodMapper()
        {
            ToTable("SFS$Shipment Method");
            HasKey(e => e.Code);
        }
    }
}