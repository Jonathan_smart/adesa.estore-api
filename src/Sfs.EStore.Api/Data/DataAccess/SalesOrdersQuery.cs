using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Sfs.Core;
using Sfs.Orders;
using Sfs.Orders.Ports;
using Sfs.SalesVehicleEnquiry.Navision.Entities;
using System;

namespace Sfs.EStore.Api.Data.DataAccess
{
    public class SalesOrdersQuery : ISalesOrdersQuery
    {
        private readonly BusinessUnit _businessUnit;

        public SalesOrdersQuery(BusinessUnit businessUnit)
        {
            _businessUnit = businessUnit;
        }

        public IEnumerable<SalesOrderInfo> Handle(params string[] orderNos)
        {
            return GetResults(rangeStart: 0, rangeMax: 100, orderNos: orderNos ?? new string[0]);
        }

        private IPagedSet<SalesOrderInfo> GetResults(int rangeStart, int rangeMax, IEnumerable<string> orderNos)
        {
            var range = new { Start = rangeStart, Max = rangeMax };

            using (var dbContext = new NavisionContext())
            {
                var query = dbContext.Set<SalesHeader>()
                    .Where(x => x.DocumentType == 1)
                    .GroupJoin(dbContext.Set<ShipmentMethod>(),
                        h => h.ShipmentMethodCode,
                        m => m.Code,
                        (h, m) => new { Header = h, ShipmentMethod = m.FirstOrDefault() })
                    .GroupJoin(dbContext.Set<ShippingAgent>(),
                        h => h.Header.ShippingAgentCode,
                        a => a.Code,
                        (h, a) => new { h.Header, h.ShipmentMethod, ShippingAgent = a.FirstOrDefault() })
                    .GroupJoin(dbContext.Set<ShippingAgentService>(),
                        h => new { h.Header.ShippingAgentCode, h.Header.ShippingAgentServiceCode },
                        s => new { s.ShippingAgentCode, ShippingAgentServiceCode = s.Code },
                        (h, s) =>
                            new { h.Header, h.ShipmentMethod, h.ShippingAgent, ShippingAgentService = s.FirstOrDefault() })
                    .OrderByDescending(x => x.Header.OrderDate).ThenByDescending(x => x.Header.No)
                    .AsQueryable();

                query = query.Where(x => orderNos.Contains(x.Header.No));

                var orders = query.Skip(range.Start).Take(range.Max).ToList();

                var resultOrderNos = orders.Select(o => o.Header.No).ToArray();
                var lines = dbContext.Set<SalesLine>()
                    .Include(x => x.SerialNoInformation)
                    .Where(x => resultOrderNos.Contains(x.DocumentNo))
                    .ToList();

                var result = orders.Select(order =>
                {
                    var orderLines = lines.Where(l => l.DocumentNo == order.Header.No).ToArray();
                    var SerialNos = orderLines.Select(line => line.TrackingSerialNo).ToArray();

                    return new SalesOrderInfo(
                        date: order.Header.OrderDate,
                        no: order.Header.No,
                        shipping:
                            new ShippingInfo(
                                new ShippingInfo.ShippingAddress(order.Header.ShipToName, order.Header.ShipToPostcode),
                                order.ShipmentMethod == null ? null : order.ShipmentMethod.Description,
                                order.ShippingAgent == null ? null : order.ShippingAgent.Name,
                                order.ShippingAgentService == null ? null : order.ShippingAgentService.Description),
                        lines:
                            new SalesOrderLines(
                                orderLines.Select(line =>
                                {
                                    var description = line.Description;

                                    return new SalesOrderLine(new Money(line.Amount),
                                        new Money(line.AmountIncVat - line.Amount), new Money(line.AmountIncVat),
                                        description);
                                }).ToArray()),
                        sellToCustomer:
                            new CustomerInfo(order.Header.SellToCustomerNo, order.Header.SellToCustomerName),
                            imagePaths: _businessUnit == "FGA" ? dbContext.Set<SerialNoAttachment>()
                            .Where(x => SerialNos.Contains(x.SerialNo) && x.VisibleToCustomers == 1)
                               .Select(c => new OrderObject
                               {
                                   FilePath = c.URL,
                                   id = 1, //Convert.ToInt32(c.Filename.Substring(c.Filename.LastIndexOf("IMG") + 4, 4)),
                                   sortOrder = c.SortOrder
                               }).OrderBy(x => x.id)
                            .ThenBy(x => x.sortOrder).ToList() : null
                        );
                }).ToArray();

                return new PagedSetWrapper<SalesOrderInfo>(result, range.Start, range.Max, query.Count());
            }
        }
    }
}