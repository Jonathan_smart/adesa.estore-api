﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    public class SerialCampaign
    {
        public string Campaign { get; set; }
        public int EntryNo { get; set; }
        public string SerialNo { get; set; }
    }

    public class SerialCampaignMapper : EntityTypeConfiguration<SerialCampaign>
    {
        public SerialCampaignMapper()
        {
            ToTable("SFS$Serial No_ Campaign");
            HasKey(e => new { e.EntryNo});
            Property(e => e.Campaign).HasColumnName("No_");
            Property(e => e.EntryNo).HasColumnName("Entry No_");
            Property(e => e.SerialNo).HasColumnName("Serial No_");

        }
    }
}