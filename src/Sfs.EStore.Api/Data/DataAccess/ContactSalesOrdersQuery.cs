using System.Data.Entity;
using System.Linq;
using Sfs.Core;
using Sfs.Orders;
using Sfs.Orders.Ports;
using Sfs.SalesVehicleEnquiry.Navision.Entities;
using System;

namespace Sfs.EStore.Api.Data.DataAccess
{
    public class ContactSalesOrdersQuery : IContactSalesOrdersQuery
    {
        private readonly BusinessUnit _businessUnit;

        public ContactSalesOrdersQuery(BusinessUnit businessUnit)
        {
            _businessUnit = businessUnit;
        }

        public IPagedSet<SalesOrderInfo> Handle(GetForContactQuery queryParams)
        {
            return GetResults(rangeStart: queryParams.Range.Start, rangeMax: queryParams.Range.Max,
                contactNo: queryParams.ContactNo);
        }

        private IPagedSet<SalesOrderInfo> GetResults(int rangeStart, int rangeMax, string contactNo)
        {
            var range = new { Start = rangeStart, Max = rangeMax };

            using (var dbContext = new NavisionContext())
            {
                var query = dbContext.Set<SalesHeader>()
                    .Where(x => x.DocumentType == 1)
                    .GroupJoin(dbContext.Set<ShipmentMethod>(),
                        h => h.ShipmentMethodCode,
                        m => m.Code,
                        (h, m) => new { Header = h, ShipmentMethod = m.FirstOrDefault() })
                    .GroupJoin(dbContext.Set<ShippingAgent>(),
                        h => h.Header.ShippingAgentCode,
                        a => a.Code,
                        (h, a) => new { h.Header, h.ShipmentMethod, ShippingAgent = a.FirstOrDefault() })
                    .GroupJoin(dbContext.Set<ShippingAgentService>(),
                        h => new { h.Header.ShippingAgentCode, h.Header.ShippingAgentServiceCode },
                        s => new { s.ShippingAgentCode, ShippingAgentServiceCode = s.Code },
                        (h, s) =>
                            new { h.Header, h.ShipmentMethod, h.ShippingAgent, ShippingAgentService = s.FirstOrDefault() })
                    .OrderByDescending(x => x.Header.OrderDate).ThenByDescending(x => x.Header.No)
                    .AsQueryable();

                var customerNos = dbContext.Set<Contact>()
                    .Where(x => x.GlobalDimension1Code == _businessUnit)
                    .Where(x => x.No == contactNo)
                    .Join(dbContext.Set<ContactRelationship>(), c => c.No, cr => cr.ContactNo, (c, cr) => cr)
                    .Where(x => x.SourceType == 1)
                    .Where(x => x.AuthorizedBy != "")
                    .Select(x => x.SourceNo);

                query = query.Where(x => customerNos.Contains(x.Header.SellToCustomerNo));

                var orders = query.Skip(range.Start).Take(range.Max).ToList();

                var resultOrderNos = orders.OrderByDescending(x => x.Header.OrderDate).Select(o => o.Header.No).ToArray();
                var lines = dbContext.Set<SalesLine>()
                    .Include(x => x.SerialNoInformation)
                    .Where(x => resultOrderNos.Contains(x.DocumentNo))
                    .ToList();

                var result = orders.Select(order =>
                {
                    var orderLines = lines.Where(l => l.DocumentNo == order.Header.No).ToArray();
                    var SerialNos = orderLines.Select(line => line.TrackingSerialNo).ToArray();
                    return new SalesOrderInfo(
                            date: order.Header.OrderDate,
                            no: order.Header.No,
                            shipping:
                                new ShippingInfo(
                                    new ShippingInfo.ShippingAddress(order.Header.ShipToName, order.Header.ShipToPostcode),
                                    order.ShipmentMethod == null ? null : order.ShipmentMethod.Description,
                                    order.ShippingAgent == null ? null : order.ShippingAgent.Name,
                                    order.ShippingAgentService == null ? null : order.ShippingAgentService.Description),
                            lines:
                                new SalesOrderLines(
                                    orderLines.Select(line =>
                                    {
                                        var isVehicleLine = line.No == ItemNo.Vehicle;
                                        var description = isVehicleLine
                                            ? string.Format("{0} - {1}", line.Description,
                                                line.SerialNoInformation.RegistrationNo)
                                            : line.Description;

                                        return new SalesOrderLine(new Money(line.Amount),
                                            new Money(line.AmountIncVat - line.Amount), new Money(line.AmountIncVat),
                                            description);
                                    }).ToArray()),
                            sellToCustomer:
                                new CustomerInfo(order.Header.SellToCustomerNo, order.Header.SellToCustomerName),
                            imagePaths: dbContext.Set<SerialNoAttachment>()
                            .Where(x => SerialNos.Contains(x.SerialNo) && x.VisibleToCustomers == 1).ToList()
                            .Select(c => new OrderObject
                            {
                                FilePath = c.URL,
                                id = _businessUnit.ToString() == "FGA" ? Convert.ToInt32(c.Filename.Substring(c.Filename.LastIndexOf("IMG") + 4, 4)) : 0,
                                sortOrder = c.SortOrder
                            }).OrderBy(x => x.id)
                            .ThenBy(x => x.sortOrder).ToList()
                            );
                }).ToArray();

                return new PagedSetWrapper<SalesOrderInfo>(result, range.Start, range.Max, query.Count());
            }
        }
    }
}