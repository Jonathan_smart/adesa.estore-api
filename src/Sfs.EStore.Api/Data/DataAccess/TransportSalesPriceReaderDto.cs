using System.Collections.Generic;
using System.Linq;
using Ports=Sfs.Orders.Ports;

namespace Sfs.EStore.Api.Data.DataAccess
{
    internal class TransportSalesPriceReaderDto : Ports.ICreateSalesOrderTransportSalesPriceQuery
    {
        private readonly Core.SalesChannel _salesChannel;
        private readonly Core.BusinessUnit _businessUnit;

        public TransportSalesPriceReaderDto(Core.SalesChannel salesChannel, Core.BusinessUnit businessUnit)
        {
            _salesChannel = salesChannel;
            _businessUnit = businessUnit;
        }

        public IEnumerable<Ports.TransportSalesPrice> GetForMethod(string method)
        {
            using (var dbContext = new NavisionContext())
            {
                var entity = dbContext.Set<TransportSalesPrice>()
                    .Where(x => x.GlobalDimension1Code == _businessUnit)
                    .Where(x => x.SalesChannelNo == _salesChannel)
                    .Where(x => x.ShipmentMethodCode == method)
                    .ToArray();

                return entity
                    .Select(x => new Ports.TransportSalesPrice(x.ShipmentMethodCode, x.ShippingAgentCode))
                    .ToArray();
            }
        }
    }
}