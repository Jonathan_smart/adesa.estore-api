﻿using System.Data.Entity;
using System.Linq;
using Sfs.Core;
using Sfs.EStore.Api.ThirdParties.Ports;
using Sfs.Orders;
using Sfs.Orders.Ports;

namespace Sfs.EStore.Api.Data.DataAccess
{
    public class ContactBasketQuery : IContactBasketQuery
    {
        private readonly BusinessUnit _businessUnit;

        public ContactBasketQuery(BusinessUnit businessUnit)
        {
            _businessUnit = businessUnit;
        }

        public IPagedSet<ContactBasketQueryResponse> Handle(ContactBasketQueryRequest request)
        {
            using (var dbContext = new NavisionContext())
            {
                var contactCustomersCmd = new GetContactCustomers(request.ContactNo, _businessUnit);
                contactCustomersCmd.Execute();

                var customerNos = contactCustomersCmd.Result.Select(x => x.No).ToArray();

                var query = dbContext.Set<Basket>()
                    .Include(x => x.SerialNoInformation)
                    .OrderByDescending(x => x.AddedToBasketOn)
                    .Where(x => customerNos.Contains(x.CustomerNo) || x.ContactNo == request.ContactNo)
                    .Where(x => x.BusinessUnit == _businessUnit)
                    .Select(x => new
                    {
                        x.No,
                        x.SerialNoInformation.Description,
                        x.SerialNoInformation.RegistrationNo,
                        x.SerialNoInformation.RegistrationYap,
                        x.SerialNoInformation.Mileage,
                        x.Amount,
                        x.AmountIncVat,
                        x.PricesIncludeVat,
                        x.ExpiryDate,
                        //x.BuyItNow,
                        Image = x.SerialNoInformation.Attachments.OrderBy(a => a.SortOrder).Where(a => a.VisibleToCustomers == 1).Take(1).FirstOrDefault()
                    });

                if (request.EntryNos != null && request.EntryNos.Length > 0)
                {
                    var entryNos = request.EntryNos.Select(x => (int)x).ToArray();

                    query = query.Where(x => entryNos.Contains(x.No));
                }

                var baskets = query.Skip(request.Range.Start).Take(request.Range.Max).ToArray().Select(x =>
                {
                    var title = x.Description;
                    return new ContactBasketQueryResponse(new BasketEntryNo(x.No),
                        title, new ContactBasketQueryResponse.RegistrationInfo(x.RegistrationNo, x.RegistrationYap),
                        (int)x.Mileage,
                        new Money(x.Amount), x.PricesIncludeVat == 1,
                        x.Image == null
                            ? null
                            : new ContactBasketQueryResponse.CloudFilesImage(x.Image.CloudFilesContainer,
                                x.Image.Filename),false,
                        x.ExpiryDate.FromNavision());
                }).ToArray();

                return new PagedSetWrapper<ContactBasketQueryResponse>(baskets,
                    request.Range.Start, request.Range.Max, query.Count());
            }
        }
    }
}
