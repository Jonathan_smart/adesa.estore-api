﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Sfs.EStore.Api.Data.DataAccess
{
    public class SalespersonPurchaser
    {
            public string Code { get; set; }
            public string ContactNo { get; set; }
            public string Business  { get; set; }
    }

    public class SalespersonPurchaserMapper : EntityTypeConfiguration<SalespersonPurchaser>
    {
        public SalespersonPurchaserMapper()
        {
            ToTable("SFS$Salesperson_Purchaser");
            HasKey(e => new { e.Code });
            Property(e => e.Code).HasColumnName("Code");
            Property(e => e.ContactNo).HasColumnName("Contact No_");
            Property(e => e.Business).HasColumnName("Global Dimension 1 Code");

        }
    }
}