﻿using System.Data.Entity.ModelConfiguration;
namespace Sfs.EStore.Api.Data.DataAccess
{
    namespace Sfs.EStore.Api.Data
    {
        public class SerialNoPricesSaleChannel
        {
            public int EntryNo { get; set; }
            public string SerialNo { get; set; }
            public int Open { get; set; }
            public string   SalesChannel  { get; set; }
        }

        public class SerialNo_PricesSaleChannelMapper : EntityTypeConfiguration<SerialNoPricesSaleChannel>
        {
            public SerialNo_PricesSaleChannelMapper()
            {
                ToTable("SFS$Serial No_ Prices Sale Channel");
                HasKey(e => new { e.EntryNo });
                Property(e => e.EntryNo).HasColumnName("Entry No_");
                Property(e => e.SerialNo).HasColumnName("Serial No_");
                Property(e => e.Open).HasColumnName("Open");
                Property(e => e.SalesChannel).HasColumnName("Sales Channel");

            }
        }
    }
}