﻿using System.Linq;
using Sfs.Orders.Ports;

namespace Sfs.EStore.Api.Data.DataAccess
{
    public class ShipToAddressReaderDto : ICalculateTransportShipToAddressQuery
    {
        public Orders.Ports.ShipToAddress GetForCustomerNoAndAddressCode(string customerNo, string addressCode)
        {
            using (var ctx = new NavisionContext())
            {
                var address = ctx.Set<ShipToAddress>()
                    .FirstOrDefault(x => x.CustomerNo == customerNo && x.Code == addressCode);
                return address == null ? null : new Orders.Ports.ShipToAddress {PostCode = address.PostCode};
            }
        }
    }
}