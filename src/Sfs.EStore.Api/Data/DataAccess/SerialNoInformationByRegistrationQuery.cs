﻿using System.Collections.Generic;
using System.Linq;
using Sfs.Core;
using Sfs.Orders.Ports;

namespace Sfs.EStore.Api.Data.DataAccess
{
    internal class SerialNoInformationByRegistrationQuery : ISerialNoInformationByRegistrationQuery
    {
        public IEnumerable<Orders.Ports.SerialNoInformation> Handle(IEnumerable<string> registrationNos)
        {
            using (var ctx = new NavisionContext())
            {
                return ctx.Set<SerialNoInformation>()
                    .Where(x => x.ItemNo == ItemNo.Vehicle && registrationNos.Contains(x.RegistrationNo))
                    .Select(x => new {x.SerialNo, x.ItemNo, x.VariantCode, x.RegistrationNo})
                    .ToArray()
                    .Select(x =>
                        new Orders.Ports.SerialNoInformation
                        {
                            Key = new SerialKey(new ItemNo(x.ItemNo), new VariantCode(x.VariantCode), new SerialNo(x.SerialNo)),
                            RegistrationNo = x.RegistrationNo
                        })
                    .ToArray();
            }
        }
    }
}