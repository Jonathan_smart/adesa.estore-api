using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    public class CustomerCampaign
    {
        public string CustomerNo { get; set; }
        public string FranchiseCode { get; set; }
        public int Type { get; set; }
        public string CustomerBusinessUnit { get; set; }
        public bool Inactive { get; set; }
        public bool Exclude { get; set; }
    }

    public class CustomerCampaignMapper : EntityTypeConfiguration<CustomerCampaign>
    {
        public CustomerCampaignMapper()
        {
            ToTable("SFS$Customer Campaign");
            HasKey(e => new { e.CustomerNo, e.FranchiseCode });
            Property(e => e.CustomerNo).HasColumnName("Customer No_");
            Property(e => e.Type).HasColumnName("Type");
            Property(e => e.FranchiseCode).HasColumnName("No_");
            Property(e => e.CustomerBusinessUnit).HasColumnName("Customer Business Unit");
            Property(e => e.Inactive).HasColumnName("Inactive");
            Property(e => e.Exclude).HasColumnName("Exclude");
        }
    }
}