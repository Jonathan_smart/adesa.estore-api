﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    public class ContactComms
    {
        public int EntryNo { get; set; }
        public string ContactNo { get; set; }
        public int Type { get; set; }
        public string Value { get; set; }
    }

    public class ContactCommsMapper : EntityTypeConfiguration<ContactComms>
    {
        public ContactCommsMapper()
        {
            ToTable("SFS$Contact Comms_ Method");
            HasKey(e => e.EntryNo);
            Property(e => e.EntryNo).HasColumnName("Entry No_");
            Property(e => e.ContactNo).HasColumnName("Contact No_");
            Property(e => e.Type).HasColumnName("Type");
            Property(e => e.Value).HasColumnName("Value");
        }
    }
}
