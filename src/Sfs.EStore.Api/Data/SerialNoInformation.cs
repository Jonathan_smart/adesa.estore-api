﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    internal class SerialNoInformation
    {
        public string ItemNo { get; set; }
        public string VariantCode { get; set; }
        public string SerialNo { get; set; }
        public string BrandCode { get; set; }
        public string CarlineCode { get; set; }
        public string Model { get; set; }
        public string RegistrationNo { get; set; }
        public string RegistrationYap { get; set; }
        public string Description { get; set; }
        public decimal QuantityInStock { get; set; }
        public byte CommercialVehicle { get; set; }
        public string VatProdPostingGroup { get; set; }
        public string GlobDim1Code { get; set; }
        public decimal Mileage { get; set; }

        public ICollection<SerialNoAttachment> Attachments { get; set; }
    }

    internal class SerialNoInformationMapper : EntityTypeConfiguration<SerialNoInformation>
    {
        public SerialNoInformationMapper()
        {
            ToTable("SFS$Serial No_ Information");
            HasKey(e => new { e.ItemNo, e.VariantCode, e.SerialNo });
            Property(e => e.ItemNo).HasColumnName("Item No_");
            Property(e => e.VariantCode).HasColumnName("Variant Code");
            Property(e => e.SerialNo).HasColumnName("Serial No_");
            Property(e => e.BrandCode).HasColumnName("Brand Code");
            Property(e => e.CarlineCode).HasColumnName("Carline Code");
            Property(e => e.RegistrationNo).HasColumnName("Registration No_");
            Property(e => e.RegistrationYap).HasColumnName("Year and Plate");
            Property(e => e.Description).HasColumnName("Description");
            Property(e => e.QuantityInStock).HasColumnName("Quantity in Stock");
            Property(e => e.CommercialVehicle).HasColumnName("Commercial Vehicle");
            Property(e => e.VatProdPostingGroup).HasColumnName("VAT Prod_ Posting Group");
            Property(e => e.GlobDim1Code).HasColumnName("Global Dimension 1 Code");

            HasMany(x => x.Attachments).WithOptional().HasForeignKey(x => new { x.ItemNo, x.VariantCode, x.SerialNo });
        }
    }
}