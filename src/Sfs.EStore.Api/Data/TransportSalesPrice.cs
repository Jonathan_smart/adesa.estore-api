﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    internal class TransportSalesPrice
    {
        public int EntryNo { get; set; }
        public string GlobalDimension1Code { get; set; }
        public string ShipmentMethodCode { get; set; }
        public string ShippingAgentCode { get; set; }
        public string ShippingAgentServiceCode { get; set; }
        public int SalesChannelNo { get; set; }
        public int CommercialVehicle { get; set; }
    }

    internal class TransportSalesPriceMapper : EntityTypeConfiguration<TransportSalesPrice>
    {
        public TransportSalesPriceMapper()
        {
            ToTable("SFS$Transport Sales Price");
            HasKey(e => e.EntryNo);
            Property(e => e.EntryNo).HasColumnName("Entry No_");
            Property(e => e.GlobalDimension1Code).HasColumnName("Global Dimension 1 Code");
            Property(e => e.ShipmentMethodCode).HasColumnName("Shipment Method Code");
            Property(e => e.ShippingAgentCode).HasColumnName("Shipping Agent Code");
            Property(e => e.ShippingAgentServiceCode).HasColumnName("Shipping Agent Service Code");
            Property(e => e.SalesChannelNo).HasColumnName("Sales Channel No_");
            Property(e => e.CommercialVehicle).HasColumnName("Commercial Vehicle");
        }
    }
}