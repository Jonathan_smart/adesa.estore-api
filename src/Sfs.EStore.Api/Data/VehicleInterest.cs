﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Sfs.EStore.Api.Entities
{
    internal class VehicleInterest
    {
        public DateTime Created { get; set; }
        public int Type { get; set; }
        public string ContactNo { get; set; }
        public string SerialNo { get; set; }
        public int LineNo { get; set; }
        public decimal MaxBid { get; set; }
    }

    internal class TransportSalesPriceMapper : EntityTypeConfiguration<VehicleInterest>
    {
        public TransportSalesPriceMapper()
        {
            ToTable("SFS$Vehicle Interest");
            HasKey(e => e.Type);
            HasKey(e => e.ContactNo);
            HasKey(e => e.SerialNo);
            HasKey(e => e.LineNo);
            Property(e => e.Type).HasColumnName("Type");
            Property(e => e.ContactNo).HasColumnName("Contact No_");
            Property(e => e.SerialNo).HasColumnName("Serial No_");
            Property(e => e.LineNo).HasColumnName("Line No_");
            Property(e => e.MaxBid).HasColumnName("Maximum Bid");
            Property(e => e.Created).HasColumnName("Date Created");
        }
    }
}