﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    public class Customer
    {
        public string No { get; set; }
        public string GlobalDimension1Code { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string DealerCode { get; set; }
        public string SalesPersonCode { get; set; }
    }

    public class CustomerMapper : EntityTypeConfiguration<Customer>
    {
        public CustomerMapper()
        {
            ToTable("SFS$Customer");
            HasKey(e => e.No);
            Property(e => e.No).HasColumnName("No_");
            Property(e => e.GlobalDimension1Code).HasColumnName("Global Dimension 1 Code");
            Property(e => e.Name).HasColumnName("Name");
            Property(e => e.City).HasColumnName("City");
            Property(e => e.DealerCode).HasColumnName("FGA Dealer Code");
            Property(e => e.SalesPersonCode).HasColumnName("Salesperson Code");
        }
    }
}