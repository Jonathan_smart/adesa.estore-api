﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    internal class SalesLine
    {
        public int DocumentType { get; set; }
        public string DocumentNo { get; set; }
        public int LineNo { get; set; }
        public string No { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountIncVat { get; set; }
        public string TrackingItemNo { get; set; }
        public string TrackingVariantCode { get; set; }
        public string TrackingSerialNo { get; set; }

        public SerialNoInformation SerialNoInformation { get; set; }
    }

    internal class SalesLineMapper : EntityTypeConfiguration<SalesLine>
    {
        public SalesLineMapper()
        {
            ToTable("SFS$Sales Line");
            HasKey(e => new { e.DocumentType, e.DocumentNo, e.LineNo });
            Property(e => e.No).HasColumnName("No_");
            Property(e => e.LineNo).HasColumnName("Line No_"); ;
            Property(e => e.DocumentType).HasColumnName("Document Type");
            Property(e => e.DocumentNo).HasColumnName("Document No_");
            Property(e => e.Description).HasColumnName("Description");
            Property(e => e.Amount).HasColumnName("Amount");
            Property(e => e.AmountIncVat).HasColumnName("Amount Including VAT");
            Property(e => e.TrackingItemNo).HasColumnName("Tracking Item No_");
            Property(e => e.TrackingVariantCode).HasColumnName("Tracking Variant Code");
            Property(e => e.TrackingSerialNo).HasColumnName("Tracking Serial No_");

            HasOptional(x => x.SerialNoInformation).WithMany().HasForeignKey(e =>
                new { e.TrackingItemNo, e.TrackingVariantCode, e.TrackingSerialNo });
        }
    }
}