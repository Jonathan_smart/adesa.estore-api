﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    internal class Basket
    {
        public int No { get; set; }
        public string ItemNo { get; set; }
        public string VariantCode { get; set; }
        public string SerialNo { get; set; }
        public string ContactNo { get; set; }
        public string SalesPersonContactNo { get; set; }
        public DateTime AddedToBasketOn { get; set; }
        public decimal Price { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountIncVat { get; set; }
        public byte PricesIncludeVat { get; set; }
        public string LocationCode { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int SalesChannelNo { get; set; }
        public string BusinessUnit { get; set; }

        public int Type { get; set; }
        //public byte BuyItNow { get; set; }

        public string CustomerNo { get; set; }
        public Customer Customer { get; set; }
        public SerialNoInformation SerialNoInformation { get; set; }
    }

    internal class BasketMapper : EntityTypeConfiguration<Basket>
    {
        public BasketMapper()
        {
            ToTable("SFS$Basket");
            HasKey(e => e.No);
            Property(e => e.No).HasColumnName("Entry No_");
            Property(e => e.ItemNo).HasColumnName("Item No_");
            Property(e => e.VariantCode).HasColumnName("Variant Code");
            Property(e => e.SerialNo).HasColumnName("Serial No_");
            Property(e => e.ContactNo).HasColumnName("Contact No_");
            Property(e => e.SalesPersonContactNo).HasColumnName("Sales Person Contact No_");
            Property(e => e.CustomerNo).HasColumnName("Customer No_");
            Property(e => e.AddedToBasketOn).HasColumnName("Date_Time Added To Basket");
            Property(e => e.Price).HasColumnName("Price");
            Property(e => e.Amount).HasColumnName("Amount");
            Property(e => e.AmountIncVat).HasColumnName("Amount Including VAT");
            Property(e => e.PricesIncludeVat).HasColumnName("Prices Including VAT");
            Property(e => e.LocationCode).HasColumnName("Location Code");
            Property(e => e.ExpiryDate).HasColumnName("Expiry Date");
            Property(e => e.SalesChannelNo).HasColumnName("Sales Channel No_");
            Property(e => e.BusinessUnit).HasColumnName("Global Dimension 1 Code");
            Property(e => e.Type).HasColumnName("Basket Entry Type");
            //Property(e => e.BuyItNow).HasColumnName("Buy It Now");

            HasOptional(e => e.Customer).WithMany().HasForeignKey(e => e.CustomerNo);
            HasOptional(e => e.SerialNoInformation).WithMany()
                .HasForeignKey(e => new { e.ItemNo, e.VariantCode, e.SerialNo });
        }
    }
}