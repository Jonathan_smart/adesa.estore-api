﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Sfs.SalesVehicleEnquiry.Navision.Entities
{
    public class SalesHeader
    {
        public int DocumentType { get; set; }
        public string No { get; set; }
        public string SellToCustomerNo { get; set; }
        public string SellToCustomerName { get; set; }
        public string BillToCustomerNo { get; set; }
        public string BillToCustomerName { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime ShipmentDate { get; set; }
        public string ShipmentMethodCode { get; set; }
        public string ShippingAgentCode { get; set; }
        public string ShippingAgentServiceCode { get; set; }
        public string PostingDescription { get; set; }
        public string ShipToCode { get; set; }
        public string ShipToName { get; set; }
        public string ShipToPostcode { get; set; }
    }

    internal class SalesHeaderMapper : EntityTypeConfiguration<SalesHeader>
    {
        public SalesHeaderMapper()
        {
            ToTable("SFS$Sales Header");
            HasKey(e => new { e.DocumentType, e.No });
            Property(e => e.No).HasColumnName("No_");
            Property(e => e.DocumentType).HasColumnName("Document Type");
            Property(e => e.SellToCustomerNo).HasColumnName("Sell-to Customer No_");
            Property(e => e.SellToCustomerName).HasColumnName("Sell-to Customer Name");
            Property(e => e.BillToCustomerNo).HasColumnName("Bill-to Customer No_");
            Property(e => e.BillToCustomerName).HasColumnName("Bill-to Name");
            Property(e => e.OrderDate).HasColumnName("Order Date");
            Property(e => e.ShipmentDate).HasColumnName("Shipment Date");
            Property(e => e.ShipmentMethodCode).HasColumnName("Shipment Method Code");
            Property(e => e.ShippingAgentCode).HasColumnName("Shipping Agent Code");
            Property(e => e.ShippingAgentServiceCode).HasColumnName("Shipping Agent Service Code");
            Property(e => e.PostingDescription).HasColumnName("Posting Description");
            Property(e => e.ShipToCode).HasColumnName("Ship-to Code");
            Property(e => e.ShipToName).HasColumnName("Ship-to Name");
            Property(e => e.ShipToPostcode).HasColumnName("Ship-to Post Code");
        }
    }
}