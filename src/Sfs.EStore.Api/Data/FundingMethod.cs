﻿using System.Data.Entity.ModelConfiguration;

namespace Sfs.EStore.Api.Data
{
    internal class FundingMethod
    {
        public string Method { get; set; }
        public string Agent { get; set; }
    }

    internal class FundingMethodMapper : EntityTypeConfiguration<FundingMethod>
    {
        public FundingMethodMapper()
        {
            ToTable("SFS$Funding Method");
            HasKey(e => e.Method);
            Property(e => e.Method).HasColumnName("Funding Method");
            Property(e => e.Agent).HasColumnName("Funding Agent");
        }
    }
}