﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

    public class SalesChannelBuyer
    {
        public int SalesChannel { get; set; }
        public int BuyerFee { get; set; }
    }


    internal class SalesChannelBuyerMapper : EntityTypeConfiguration<SalesChannelBuyer>
    {
        public SalesChannelBuyerMapper()
        {
            ToTable("SFS$Sales Channel");
            HasKey(e => new { e.SalesChannel});
         
            Property(e => e.SalesChannel).HasColumnName("Sales Channel No_");
            Property(e => e.BuyerFee).HasColumnName("Buyer Fee");
        }
    }
