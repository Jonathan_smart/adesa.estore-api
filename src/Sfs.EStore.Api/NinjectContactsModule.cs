using System.Security.Policy;
using Ninject.Modules;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Api.Properties;
using Sfs.EStore.Contacts.Adapters.Gm;
using Sfs.EStore.Contacts.Ports.Gm;

namespace Sfs.EStore.Api
{
    public class NinjectContactsModule : NinjectModule
    {
        public override void Load()
        {
            GmBindings();
        }

        private void GmBindings()
        {
            Kernel.Bind<IHandleRequests<LinkEStoreUserToCustomer>>().To<LinkEStoreUserToCustomerHandler>()
                .WhenNavisionInstance().IsGm()
                .WithConstructorArgument("serviceUrl", ctx => new Url(Settings.Default.Sfs_EStore_Api_NavWebIntegrationService_WebIntegration));

            Kernel.Bind<IHandleRequests<RemoveEStoreUserToCustomerLink>>().To<RemoveEStoreUserToCustomerLinkHandler>()
                .WhenNavisionInstance().IsGm()
                .WithConstructorArgument("serviceUrl", ctx => new Url(Settings.Default.Sfs_EStore_Api_NavWebIntegrationService_WebIntegration));

            Kernel.Bind<IContactUsernameReaderDto>().To<ContactUsernameReaderDto>()
                .WhenNavisionInstance().IsGm();

            Kernel.Bind<IContactUsernameWriter>().To<ContactUsernameWriter>()
                .WhenNavisionInstance().IsGm()
                .WithConstructorArgument("serviceUrl", ctx => new Url(Settings.Default.Sfs_EStore_Api_ContactUsernamesService_Contact_Usernames_Service));

            Kernel.Bind<ICustomerReaderDto>().To<CustomerReaderDto>()
                .WhenNavisionInstance().IsGm();
        }
    }
}