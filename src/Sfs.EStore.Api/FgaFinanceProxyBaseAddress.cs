﻿namespace Sfs.EStore.Api
{
    public struct FgaFinanceProxyBaseAddress
    {
        private readonly string _value;

        public FgaFinanceProxyBaseAddress(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }

        public static implicit operator string(FgaFinanceProxyBaseAddress item)
        {
            return item._value;
        }
    }
}