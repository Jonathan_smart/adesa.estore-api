using System;
using System.Collections.Concurrent;
using Raven.Abstractions.Util;
using Raven.Client;
using Raven.Client.Document;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api
{
    public class LazyAccountDocumentStoreFactory
    {
        private readonly ConcurrentDictionary<string, Lazy<IDocumentStore>> _stores;
        private readonly string _ravenUrl;

        public LazyAccountDocumentStoreFactory(ConcurrentDictionary<string, Lazy<IDocumentStore>> stores,
                                               string ravenUrl)
        {
            _stores = stores;
            _ravenUrl = ravenUrl;
        }

        public IDocumentStore GetAccountDocumentStoreFor(string databaseName)
        {
            Action<DocumentConvention> customizeConventions =
                conventions => conventions
                    // UserLotWatches
                    .RegisterIdConvention<UserLotWatches>(
                        (dbname, commands, user) => UserLotWatches.GenerateId(user.User))
                    .RegisterAsyncIdConvention<UserLotWatches>(
                        (dbname, commands, user) =>
                            new CompletedTask<string>(UserLotWatches.GenerateId(user.User)))
                    // ListingViewCount
                    .RegisterIdConvention<ListingViewCount>(
                        (dbname, commands, listing) => ListingViewCount.GenerateId(listing.LotId))
                    .RegisterAsyncIdConvention<ListingViewCount>(
                        (dbname, commands, listing) =>
                            new CompletedTask<string>(ListingViewCount.GenerateId(listing.LotId)));

            return _stores.GetOrAdd(databaseName, dbName => CreateDocumentStore(dbName, customizeConventions)).Value;
        }

        private Lazy<IDocumentStore> CreateDocumentStore(string databaseName,
                                                         Action<DocumentConvention> customizeConventions = null)
        {
            return new Lazy<IDocumentStore>(
                () =>
                    {
                        var docStore = new DocumentStore
                                           {
                                               Url = _ravenUrl,
                                               DefaultDatabase = databaseName
                                           };

                        if (customizeConventions != null)
                            customizeConventions(docStore.Conventions);

                        docStore.Initialize();

                        return docStore;
                    });
        }
    }
}