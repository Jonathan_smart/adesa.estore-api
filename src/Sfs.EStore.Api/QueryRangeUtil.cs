using System;
using System.Text.RegularExpressions;
using Raven.Abstractions.Indexing;

namespace Sfs.EStore.Api
{
    public class QueryRangeUtil
    {
        public static Tuple<object, object> StringToRange(string searchTerm)
        {
            var matches = Regex.Matches(searchTerm, @"\[(?<lower>[ILFD]x\d*(\.\d*)?|NULL) TO (?<upper>[ILFD]x\d*(\.\d*)?|NULL)\]");
            if (matches.Count == 0) return null;

            var match = matches[0];

            var lower = match.Groups["lower"];
            var upper = match.Groups["upper"];
            return new Tuple<object, object>(NumberUtil.StringToNumber(lower.Value), NumberUtil.StringToNumber(upper.Value));
        }
    }
}