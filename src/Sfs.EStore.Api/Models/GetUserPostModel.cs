using System.ComponentModel.DataAnnotations;

namespace Sfs.EStore.Api.Models
{
    public class GetUserPostModel
    {
        [Required(AllowEmptyStrings = false)]
        public string Username { get; set; }
    }
}