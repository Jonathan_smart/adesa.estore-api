using System;

namespace Sfs.EStore.Api.Models
{
    public class GrsAssetPostModel
    {
        public string VatStatus { get; set; }
        public string Transmission { get; set; }
        public string InteriorTrim { get; set; }
        public string FuelType { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string ExteriorColour { get; set; }
        public string Vin { get; set; }
        public int UpdateId { get; set; }
        public string AssetId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Derivative { get; set; }
        public string EngineDescription { get; set; }
        public string RegistrationPlate { get; set; }
        public int? Mileage { get; set; }

        public decimal BuyItNowPrice { get; set; }
    }
}