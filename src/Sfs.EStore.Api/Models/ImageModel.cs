using Newtonsoft.Json;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Models
{
    public class ImageModel
    {
        public static ImageModel From(VehicleImage entity)
        {
            return new ImageModel
            {
                Container = entity.Container,
                Name = entity.Name,
                Type = entity.Type,
                Url = entity.Url,
            };
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Url { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Container { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        public string ToUrl(int? width = null, int? height = null)
        {
            return ImageUrl.Create(this, width, height);
        }
    }
}