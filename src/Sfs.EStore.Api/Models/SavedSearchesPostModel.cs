using System;
using System.Collections.Generic;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Models
{
    public class SavedSearchesPostModel
    {
        public string Name { get; set; }

        public SavedSearchesPostModelAlertStatus AlertStatus { get; set; }

        public SavedSearchesPostModelCriteria Criteria { get; set; }

        public Dictionary<string, object> ToDictionary()
        {
            var dict = new Dictionary<string, object>();

            if (Criteria == null) return dict;

            if (!string.IsNullOrWhiteSpace(Criteria.Q)) dict.Add("q", Criteria.Q);
            if (!string.IsNullOrWhiteSpace(Criteria.VehicleType)) dict.Add("VehicleType", Criteria.VehicleType);
            if (!string.IsNullOrWhiteSpace(Criteria.Category)) dict.Add("Category", Criteria.Category);
            if (!string.IsNullOrWhiteSpace(Criteria.Make)) dict.Add("Make", Criteria.Make);
            if (!string.IsNullOrWhiteSpace(Criteria.Model)) dict.Add("Model", Criteria.Model);
            if (!string.IsNullOrWhiteSpace(Criteria.Derivative)) dict.Add("Derivative", Criteria.Derivative);
            if (Criteria.MaxMileage.HasValue) dict.Add("MaxMileage", Criteria.MaxMileage);
            if (!string.IsNullOrWhiteSpace(Criteria.Mileage)) dict.Add("Mileage", Criteria.Mileage);
            if (Criteria.MaxAge.HasValue) dict.Add("MaxAge", Criteria.MaxAge);
            if (!string.IsNullOrWhiteSpace(Criteria.Transmission)) dict.Add("Transmission", Criteria.Transmission);
            if (!string.IsNullOrWhiteSpace(Criteria.FuelType)) dict.Add("FuelType", Criteria.FuelType);
            if (Criteria.MaxPrice.HasValue) dict.Add("MaxPrice", Criteria.MaxPrice);
            if (Criteria.MinPrice.HasValue) dict.Add("MinPrice", Criteria.MinPrice);
            if (Criteria.NewSinceDays.HasValue) dict.Add("NewSinceDays", Criteria.NewSinceDays);

            return dict;
        }

        public void CopyTo(SavedSearch savedSearch)
        {
            savedSearch.Update(ToDictionary(), Name);
        }
    }

    public class SavedSearchesPostModelCriteria
    {
        public string Q { get; set; }
        public string VehicleType { get; set; }
        public string Category { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Derivative { get; set; }
        public int? MaxMileage { get; set; }
        public string Mileage { get; set; }
        public int? MaxAge { get; set; }
        public string Transmission { get; set; }
        public string FuelType { get; set; }
        public int? MaxPrice { get; set; }
        public int? MinPrice { get; set; }
        public double? NewSinceDays { get; set; }
    }

    public class SavedSearchesPostModelAlertStatus
    {
        public bool? Enabled { get; set; }
    }
}