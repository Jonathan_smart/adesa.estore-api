using System;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Models
{
    public class ListingVehicle
    {
        public ListingVehicle(Lot listing, Vehicle vehicle)
        {
            if (listing == null) throw new ArgumentNullException("listing");
            if (vehicle == null) throw new ArgumentNullException("vehicle");

            Listing = listing;
            Vehicle = vehicle;
        }

        public Lot Listing { get; private set; }
        public Vehicle Vehicle { get; private set; }
    }
}