namespace Sfs.EStore.Api.Models
{
    public enum ArrayPatchActions
    {
        Set, // Sets the source array to the specified value
        Append, // Appends to end of source and ignores duplicates
        Add, // Adds only is it doesn't already exist in the source array
        Remove // Removes if it exists in the source array
    }
}