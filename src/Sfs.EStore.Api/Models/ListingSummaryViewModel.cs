using System;
using System.Linq;
using Newtonsoft.Json;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Models
{
    public class CurrentOffer
    {
        public string UserId { get; set; }
    }

    public class BidsListingSummaryViewModel : ListingSummaryViewModel
    {
        public BiddingInfoModel BiddingInfo { get; set; }

        public class BiddingInfoModel
        {
            public MoneyViewModel MaximumBid { get; set; }
            public DateTime MaximumBidTime { get; set; }

            public string BiddingStatus { get; set; }

            [JsonIgnore]
            public CurrentOffer CurrentOffer { get; set; }

            [JsonIgnore]
            public MaximumBidLimit[] BidLimits { get; set; }


            public void SetFor(string userId)
            {
                BiddingStatus = CurrentOffer.UserId == userId
                                    ? BidsListingSummaryViewModel.BiddingStatus.Winning.ToString()
                                    : BidsListingSummaryViewModel.BiddingStatus.Outbid.ToString();

                if (!BidLimits.Any())
                {
                    return;
                }


                if (BidLimits.Any(x => x.UserId == userId))
                {
                    var highestUserBid = BidLimits.Where(x => x.UserId == userId)
                        .OrderBy(x => x.Amount.Amount)
                        .Last();

                    MaximumBid = MoneyViewModel.From(highestUserBid.Amount);
                    MaximumBidTime = highestUserBid.Date;
                }
            }
        }

        public enum BiddingStatus
        {
            Winning,
            Outbid
        }
    }

    public class ListingSummaryViewModel
    {
        public int ListingId { get; set; }
        public string Title { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AttentionGrabber { get; set; }

        public ConditionModel Condition { get; set; }

        public ImageModel PrimaryImage { get; set; }
        public ImageModel[] MoreImages { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ImageModel[] Images360 { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DiscountPriceInfoModel DiscountPriceInfo { get; set; }

        public ListingInfoModel ListingInfo { get; set; }

        public SellingStatusModel SellingStatus { get; set; }

        public VehicleInfoModel VehicleInfo { get; set; }

        public bool PricesIncludeVat { get; set; }


        public static ListingSummaryViewModel Map(Lot lot, Vehicle vehicle, VehicleData vehicleData)
        {
            if (lot == null) throw new ArgumentNullException("lot");
            if (vehicle == null) throw new ArgumentNullException("vehicle");
            if (vehicleData == null) throw new ArgumentNullException("vehicleData");

            var model = new ListingSummaryViewModel
            {
                AttentionGrabber = lot.AttentionGrabber,
                Condition = new ConditionModel { Name = lot.Condition.ToString() },
                DiscountPriceInfo = (lot.DiscountInfo == null && vehicleData.Valuations == null)
                    ? null
                    : new DiscountPriceInfoModel
                    {
                        RetailPrice = (vehicleData.Valuations == null || vehicleData.Valuations.Retail == null)
                            ? null
                            : MoneyViewModel.From(vehicleData.Valuations.Retail),
                        WasPrice = lot.DiscountInfo == null ? null : MoneyViewModel.From(lot.DiscountInfo.WasPrice)
                    },
                ListingId = lot.LotNumber,
                ListingInfo = new ListingInfoModel
                {
                    BuyItNowAvailable = lot.HasBuyItNow,
                    BuyItNowPrice = lot.HasBuyItNow ? MoneyViewModel.From(lot.BuyItNowPrice) : null,
                    EndTime = lot.EndDate,
                    ListingType = lot.ListingType.ToString(),
                    StartTime = lot.StartDate
                },
                MoreImages = vehicle.Images.Skip(1).Take(2).Select(x => new ImageModel
                {
                    Container = x.Container,
                    Name = x.Name
                }).ToArray(),
                PricesIncludeVat = lot.PricesIncludeVat,
                PrimaryImage = vehicle.Images.Any()
                    ? new ImageModel
                    {
                        Container = vehicle.Images.First().Container,
                        Name = vehicle.Images.First().Name
                    }
                    : null,
      
                SellingStatus = new SellingStatusModel
                {
                    BidCount = lot.Bidding == null ? 0 : lot.Bidding.Bids.Count(),
                    EndTime = lot.EndDate,
                    CurrentPrice = MoneyViewModel.From(lot.CurrentPrice()),
                    ReserveMet = lot.Bidding != null && lot.Bidding.HasMetReserve,
                    SellingState = lot.Status.ToString(),
                },
                Title = lot.Title,
                VehicleInfo = new VehicleInfoModel
                {
                    Registration = new VehicleInfoModel.RegistrationModel
                    {
                        Date = vehicle.Registration.Date,
                        Plate = vehicle.Registration.Plate,
                        Yap = vehicle.Registration.YearAndPlate
                    },
                    Specification = new VehicleInfoModel.SpecificationModel
                    {
                        Co2 = vehicle.Specification.Environment.Co2,
                        Economy = new EconomyInfoModel
                        {
                            Combined = vehicle.Specification.Economy.Combined,
                            ExtraUrban = vehicle.Specification.Economy.ExtraUrban,
                            Urban = vehicle.Specification.Economy.Urban
                        }
                    },
                    Derivative = vehicle.Derivative,
                    FuelType = vehicle.FuelType,
                    Make = vehicle.Make,
                    Mileage = vehicle.Mileage,
                    Model = vehicle.Model,
                    Transmission = vehicle.Transmission,
                    VatStatus = vehicle.VatStatus,
                    VehicleType = vehicle.VehicleType
                }
            };

            return model;
        }

        public class VehicleInfoModel
        {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public SourceInfo Source { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string Derivative { get; set; }
            public string VatStatus { get; set; }
            public string FuelType { get; set; }
            public string Transmission { get; set; }
            public string ExteriorColour { get; set; }
            public RegistrationModel Registration { get; set; }
            public int? Mileage { get; set; }
            public string VehicleType { get; set; }
            public SpecificationModel Specification { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public TrimInfo Trim { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public EngineInfo Engine { get; set; }

            public class RegistrationModel
            {
                public string Plate { get; set; }
                public DateTime? Date { get; set; }
                public string Yap { get; set; }
            }

            public class SpecificationModel
            {
                public EconomyInfoModel Economy { get; set; }
                public decimal? Co2 { get; set; }
            }

            public class SourceInfo
            {
                public string Code { get; set; }
                public string Name { get; set; }
            }

            public class TrimInfo
            {
                public string Code { get; set; }
                public string Name { get; set; }
            }

            public class EngineInfo
            {
                public string Description { get; set; }
            }
        }

        public class ConditionModel
        {
            public int Id { get; set; }

            private string _name;
            [JsonIgnore]
            public string Name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    Id = (int)(Enum.Parse(typeof(VehicleCondition), value));
                }
            }

            public static ConditionModel Create(VehicleCondition condition)
            {
                return new ConditionModel { Id = (int)condition };
            }
        }

        public class SellingStatusModel
        {
            public int BidCount { get; set; }
            public MoneyViewModel CurrentPrice { get; set; }
            public string SellingState { get; set; }
            public bool ReserveMet { get; set; }

            private DateTime? _endTime;

            [JsonIgnore]
            public DateTime? EndTime
            {
                get { return _endTime; }
                set
                {
                    _endTime = value;
                }
            }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public long? TimeLeft
            {
                get
                {
                    return EndTime.HasValue ? (long)(EndTime.Value - SystemTime.CheckForTimeDiff(SystemTime.UtcNow)).TotalMilliseconds : (long?)null;
                }
            }
        }

        public class ListingInfoModel
        {
            public bool BuyItNowAvailable { get; set; }
            public MoneyViewModel BuyItNowPrice { get; set; }
            public DateTime? EndTime { get; set; }
            public DateTime? StartTime { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public long? TimeRemaining
            {
                get
                {
                    return EndTime.HasValue ? (long)(EndTime.Value - SystemTime.CheckForTimeDiff(SystemTime.UtcNow)).TotalMilliseconds : (long?)null;
                }
            }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public long? TimeLeft
            {
                get
                {
                    return StartTime.HasValue ? (long)(StartTime.Value - SystemTime.CheckForTimeDiff(SystemTime.UtcNow)).TotalMilliseconds : (long?)null;
                }
            }
            public string ListingType { get; set; }
        }

        public class DiscountPriceInfoModel
        {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public MoneyViewModel RetailPrice { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public MoneyViewModel WasPrice { get; set; }
        }

        public class EconomyInfoModel
        {
            public decimal? Urban { get; set; }
            public decimal? ExtraUrban { get; set; }
            public decimal? Combined { get; set; }
        }
    }
}