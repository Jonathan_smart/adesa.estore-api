namespace Sfs.EStore.Api.Models
{
    public class ForgottenPasswordModel
    {
        public string Username { get; set; }
    }
}