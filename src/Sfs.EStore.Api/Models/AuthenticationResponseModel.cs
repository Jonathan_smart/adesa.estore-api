using System.Collections.Generic;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Models
{
    public interface IAuthenticationResponseModel
    {
        bool IsAuthenticated { get; }
    }
    public class AuthenticationResponseModel_Authenticated : IAuthenticationResponseModel
    {
        public bool IsAuthenticated { get; set; }
        public string Message { get; set; }
        public string Username { get; set; }
        public string[] Roles { get; set; }
        public string Token { get; set; }
        public Dictionary<string, string> Data { get; set; }
        private string _firstName;
        public string FirstName
        {
            get { return _firstName ?? ""; }
            set { _firstName = value; }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName ?? ""; }
            set { _lastName = value; }
        }

        private string _phoneNumber;
        public string PhoneNumber
        {
            get { return _phoneNumber ?? ""; }
            set { _phoneNumber = value; }
        }

        private string _emailAddress;
        public string EmailAddress
        {
            get { return _emailAddress ?? ""; }
            set { _emailAddress = value; }
        }

        public static AuthenticationResponseModel_Authenticated Create(MembershipUser user)
        {
            var token = UserToken.Encrypt(new Token(user.Username));
            user.HasSignedIn();
            
            return new AuthenticationResponseModel_Authenticated
                       {
                           IsAuthenticated = true,
                           Username = user.Username,
                           Roles = user.PseudoRoles(),
                           Token = token,
                           FirstName = user.FirstName,
                           LastName = user.LastName,
                           EmailAddress = user.EmailAddress,
                           PhoneNumber = user.PhoneNumber
                       };
        }
    }

    public class AuthenticationResponseModel_NotAuthenticated : IAuthenticationResponseModel
    {
        public bool IsAuthenticated { get; set; }
        public string Message { get; set; }
    }
}