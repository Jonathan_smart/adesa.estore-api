namespace Sfs.EStore.Api.Models
{
    public class UserLotBidStatusViewModel
    {
        public bool IsWinning { get; set; }
        public BidLimitViewModel BidLimit { get; set; }
    }
}