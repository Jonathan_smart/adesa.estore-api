using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Models
{
    public class BidOutcome
    {
        public static BidOutcome OutBid()
        {
            return new BidOutcome(BidResponse.OutBid, null,null);
        }

        public static BidOutcome Successful(BidResponse outcome, Money amount,Money bidAmount)
        {
            return new BidOutcome(outcome, amount, bidAmount);
        }
        public static BidOutcome NotAllowed(BidResponse outcome)
        {
            return new BidOutcome(outcome,null,null);
        }

        private BidOutcome(BidResponse outcome, Money amountAccepted,Money bidAmount)
        {
            Outcome = outcome;
            AmountAccepted = amountAccepted;
            BidAmount = bidAmount;
        }

        public BidResponse Outcome { get; private set; }
        public Money AmountAccepted { get; private set; }
        public Money BidAmount { get; private set; }

        public bool HasBeenAccepted()
        {
            return Outcome == BidResponse.Accepted || Outcome == BidResponse.AcceptedBelowReserve || Outcome ==BidResponse.AcceptedAboveReserve ;
        }


        public enum BidResponse
        {
            Accepted,
            AcceptedAboveReserve,
            AcceptedBelowReserve,
            OutBid,
            NotAllowed
        }
    }
}