using System;

namespace Sfs.EStore.Api.Models
{
    public class BidLimitViewModel
    {
        public DateTime Date { get; set; }
        public MoneyViewModel Amount { get; set; }
        public string User { get; set; }
    }
}