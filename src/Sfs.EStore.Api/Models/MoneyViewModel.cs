using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Models
{
    [DataContract]
    public class MoneyViewModel
    {
        [JsonConstructor]
        public MoneyViewModel(decimal amount, Currency currency)
        {
            Amount = amount;
            Currency = currency.ToString();
        }

        [DataMember]
        public decimal Amount { get; set; }
        [DataMember]
        public string Currency { get; set; }

        public Currency CurrencyType()
        {
            Currency currency;
            if (Enum.TryParse(Currency, true, out currency))
                return currency;

            return Entities.Currency.XXX;
        }

        public static MoneyViewModel From(Money money)
        {
            if (money == null) return null;
            return new MoneyViewModel(money.Amount, money.Currency);
        }

        public static MoneyViewModel From(Core.Money money)
        {
            if (money == null) return null;
            return new MoneyViewModel(money.Amount, (Currency)Enum.Parse(typeof(Currency), money.Currency.ToString()));
        }

        public override string ToString()
        {
            return string.Format("{0}{1}", Amount, Currency);
        }

        public Money ToMoney()
        {
            return new Money(Amount, CurrencyType());
        }

        public string ToCurrencyString()
        {
            // TODO: Lookup currency symbol based on currency string
            /*
             * foreach (var culture in cultures)
            {
                var lcid = culture.LCID;
                var regionInfo = new RegionInfo(lcid);
                var isoSymbol = regionInfo.ISOCurrencySymbol;

                if (!cultureIdLookup.ContainsKey(isoSymbol))
                {
                    cultureIdLookup[isoSymbol] = new List<Int32>();
                }

                cultureIdLookup[isoSymbol].Add(lcid);
                symbolLookup[isoSymbol] = regionInfo.CurrencySymbol;
            }
             * */
            string symbol = Currency;
            switch (Currency)
            {
                case "GBP":
                    symbol = "\u00a3";
                    break;
                case "EUR":
                    symbol = "\u20ac";
                    break;
            }
            return string.Format("{0}{1}", symbol, Amount.ToString("n0"));
        }

        public static Money ConvertToMoney(MoneyViewModel model)
        {
            return model == null
                ? null
                : new Money(model.Amount, model.CurrencyType());
        }
    }

    internal static class MoneyExtensions
    {
        public static Core.Money Convert(this Money @this)
        {
            return new Core.Money(@this.Amount,
                (Core.Currency) Enum.Parse(typeof (Core.Currency), @this.Currency.ToString()));
        }
    }
}