using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Models
{
    public class LotPutModel
    {
        public string LotId { get; set; }
        public int? UniqueExternalReferenceNumber { get; set; }

        public string Status { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Title { get; set; }
        public string AttentionGrabber { get; set; }
        public string Programme { get; set; }
        public VehicleCondition Condition { get; set; }
        public SaleDetail Sale { get; set; }
        public bool AllowPreview { get; set; }
        //public string SerialNo { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string ModelYear { get; set; }
        public string Derivative { get; set; }
        public string EngineDescription { get; set; }
        public int? EngineSizeCc { get; set; }
        public string Bodystyle { get; set; }

        public SourceInfo Source { get; set; }
        public string Vin { get; set; }
        public int? Mileage { get; set; }
        public bool PricesIncludeVat { get; set; }
        public string VatStatus { get; set; }
        public string Transmission { get; set; }
        public string FuelType { get; set; }
        public string RegistrationPlate { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string RegistrationYearAndPlate { get; set; }

        public string ExteriorColour { get; set; }
        public string InteriorDescription { get; set; }
        public TrimInfo Trim { get; set; }

        public string VehicleType { get; set; }

        public MoneyViewModel BuyItNowPrice { get; set; }
        public MoneyViewModel StartingPrice { get; set; }
        public MoneyViewModel BidIncrements { get; set; }
        public MoneyViewModel ReservePrice { get; set; }

        public BuyitNowSale BuyItNowSale { get; set; }

        private Image[] _images = new Image[0];
        public Image[] Images
        {
            get { return _images; }
            set { _images = value; }
        }

        private EquipmentItem[] _equipment = new EquipmentItem[0];
        public EquipmentItem[] Equipment
        {
            get { return _equipment; }
            set { _equipment = value; }
        }

        private DiscountPriceInfoModel _discountInfo = new DiscountPriceInfoModel();
        public DiscountPriceInfoModel DiscountPriceInfo
        {
            get { return _discountInfo; }
            set { _discountInfo = value; }
        }

        private Dictionary<string, Dictionary<string, object>> _data = new Dictionary<string, Dictionary<string, object>>();
        public Dictionary<string, Dictionary<string, object>> Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Category { get; set; }
        public int? CapId { get; set; }
        public string NavSystem { get; set; }
        public bool? AlloyWheels { get; set; }

        public decimal? Co2 { get; set; }
        public bool Co2Specified { get; set; }

        public string InsuranceGroup { get; set; }

        public decimal? EconomyUrban { get; set; }
        public bool EconomyUrbanSpecified { get; set; }
        public decimal? EconomyExtraUrban { get; set; }
        public bool EconomyExtraUrbanSpecified { get; set; }
        public decimal? EconomyCombined { get; set; }
        public bool EconomyCombinedSpecified { get; set; }
        public bool AllowOffers { get; set; }


        public string LimitVisibilityTo { get; set; }

        public VehicleGradesModel Grade { get; set; }

        public void BindTo(Lot lot, Vehicle vehicle, VehicleData vehicleData)
        {
            BindTo(lot);
            BindTo(vehicle);
            BindTo(vehicleData);
        }

        public void BindTo(Lot lot)
        {
            lot.UniqueExternalReferenceNumber = UniqueExternalReferenceNumber ?? 0;

            lot.StartDate = StartDate.HasValue ? StartDate.Value : (DateTime?)null;
            lot.EndDate = EndDate.HasValue ? EndDate.Value : (DateTime?)null;
            lot.Title = Title.Trim();
            lot.AttentionGrabber = AttentionGrabber;
            lot.Programme = string.IsNullOrWhiteSpace(Programme) ? null : Programme;
            lot.PricesIncludeVat = PricesIncludeVat;
            lot.VisibilityLimitedTo = (LimitVisibilityTo ?? "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();
            lot.Condition = Condition;


            if (lot.BuyitNowSale != null)
                lot.BuyitNowSale = BuyItNowSale;

            lot.AllowOffers = AllowOffers;

            if (Sale != null)
            {
                lot.Sale = new LotSale(Sale.Id, Sale.Title, AllowPreview);
            }

            lot.BuyItNowPrice = MoneyViewModel.ConvertToMoney(BuyItNowPrice);

            if (StartingPrice != null)
            {
                if (lot.Bidding == null ||
                    (!StartingPrice.Equals(lot.Bidding.StartingPrice) ||
                     !BidIncrements.Equals(lot.Bidding.BidIncrements) || !ReservePrice.Equals(lot.Bidding.ReservePrice)))
                {
                    lot.SetBidding(MoneyViewModel.ConvertToMoney(StartingPrice),
                        MoneyViewModel.ConvertToMoney(BidIncrements),
                        ReservePrice == null
                            ? null
                            : MoneyViewModel.ConvertToMoney(ReservePrice));
                }
            }
            else
            {
                lot.RemoveBidding();
            }

            lot.DiscountInfo.WasPrice = DiscountPriceInfo == null ? null : MoneyViewModel.ConvertToMoney(DiscountPriceInfo.WasPrice);
        }

        public void BindTo(Vehicle vehicle)
        {
            vehicle.Source = Source == null
                ? null
                : new VehicleSource(Source.Name, Source.Code);
            vehicle.Vin = Vin;
            vehicle.Derivative = Derivative.Trim();
            vehicle.FuelType = FuelType;
            vehicle.Transmission = Transmission;
            vehicle.Make = Make.Trim();
            vehicle.Model = Model.Trim();
            vehicle.Mileage = Mileage;
            vehicle.ExteriorColour = ExteriorColour;
            vehicle.InteriorDescription = InteriorDescription;
            vehicle.Trim = Trim == null ? null : new Entities.TrimInfo(Trim.Name, Trim.Code);
            vehicle.Registration = new Registration(RegistrationPlate, RegistrationDate, RegistrationYearAndPlate);
            vehicle.VatStatus = VatStatus;
            vehicle.Bodystyle = Bodystyle;
            vehicle.ModelYear = ModelYear;
            vehicle.VehicleType = VehicleType;
            vehicle.Category = Category;
            vehicle.CapId = CapId;
            vehicle.NavSystem = NavSystem;
            vehicle.AlloyWheels = AlloyWheels;
            vehicle.InsuranceGroup = InsuranceGroup;

            vehicle.Specification.Engine.Description = EngineDescription;
            vehicle.Specification.Engine.CapacityCc = EngineSizeCc;
            if (Co2Specified) vehicle.Specification.Environment.Co2 = Co2;

            BindEconomy(vehicle.Specification);

            vehicle.Specification.Equipment.Set("direct",
                Equipment.Select(x => new Equipment.EquipmentItem(x.Category, x.Name)).ToArray());

            vehicle.Images = (Images ?? new Image[0])
                .Select(x =>
                            {
                                if (!string.IsNullOrWhiteSpace(x.CloudFilesContainer))
                                {
                                    return VehicleImage.AsCloudFiles(x.CloudFilesContainer, x.Name, x.Type);
                                }
                                return VehicleImage.AsUrl(x.Url);
                            })
                .ToArray();

            if (Grade != null && Grade.Actual != null)
            {
                vehicle.Grade = new VehicleGrades(new VehicleGrade(Grade.Actual.Code, Grade.Actual.Name));
            }
        }

        private void BindEconomy(Specification specification)
        {
            var urban = EconomyUrbanSpecified ? EconomyUrban : specification.Economy.Urban;
            var extraUrban = EconomyExtraUrbanSpecified ? EconomyExtraUrban : specification.Economy.ExtraUrban;
            var combined = EconomyCombinedSpecified ? EconomyCombined : specification.Economy.Combined;

            specification.Economy = new Economy(urban, extraUrban, combined);
        }

        public void BindTo(VehicleData vehicleData)
        {
            vehicleData.Data = Data;
        }

        public class Image
        {
            public string Name { get; set; }
            public string Url { get; set; }
            public string Type { get; set; }
            public string CloudFilesContainer { get; set; }
        }

        public class TrimInfo
        {
            public string Code { get; set; }
            public string Name { get; set; }
        }

        public class SourceInfo
        {
            public string Code { get; set; }
            public string Name { get; set; }
        }

        [DataContract(Name = "DiscountInfo")]
        public class DiscountPriceInfoModel
        {
            [DataMember]
            public MoneyViewModel RetailPrice { get; set; }
            [DataMember]
            public MoneyViewModel WasPrice { get; set; }
        }

        public class EquipmentItem
        {
            [Required(AllowEmptyStrings = false)]
            public string Category { get; set; }
            [Required(AllowEmptyStrings = false)]
            public string Name { get; set; }
        }

        public class SaleDetail
        {
            public string Id { get; set; }
            public string Title { get; set; }
        }

        public class VehicleGradesModel
        {
            public Grade Actual { get; set; }

            public class Grade
            {
                public string Code { get; set; }
                public string Name { get; set; }
            }
        }
    }

    public class LotPutModelResponse
    {
        public string LotId { get; set; }
        public string Status { get; set; }
    }
}