using System.Linq;
using System.Runtime.Serialization;

namespace Sfs.EStore.Api.Models
{
    public class UserPatchModel
    {
        public string[] Role { get; set; }
        public ArrayPatchActions RoleAction { get; set; }

        public string[] Franchise { get; set; }
        public ArrayPatchActions FranchiseAction { get; set; }

        public string Password { get; set; }

        private RoleModel[] _roles;
        public RoleModel[] Roles
        {
            get { return _roles; }
            set
            {
                _roles = value;
                if (_roles == null) return;

                Role = _roles.Select(x => x.Name).ToArray();
            }
        }

        private FranchiseModel[] _franchises;
        public FranchiseModel[] Franchises
        {
            get { return _franchises; }
            set
            {
                _franchises = value;
                if (_franchises == null) return;

                Franchise = _franchises.Select(x => x.Name).ToArray();
            }
        }

        [DataContract(Name = "Role")]
        public class RoleModel
        {
            [DataMember]
            public string Name { get; set; }
        }

        [DataContract(Name = "Franchise")]
        public class FranchiseModel
        {
            [DataMember]
            public string Name { get; set; }
        }
    }
}