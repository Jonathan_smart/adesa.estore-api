using System.ComponentModel.DataAnnotations;

namespace Sfs.EStore.Api.Models
{
    public class ValidateUserPostModel
    {
        [Required(AllowEmptyStrings = false)]
        public string Username { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Password { get; set; }
    }
}