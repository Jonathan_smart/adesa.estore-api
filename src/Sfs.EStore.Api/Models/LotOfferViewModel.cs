using System;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Models
{
    public class LotOfferViewModel
    {
        public MoneyViewModel Amount { get; set; }
        public DateTime Date { get; set; }

        public static LotOfferViewModel From(ILotOffer currentOffer)
        {
            return new LotOfferViewModel
                       {
                           Amount = MoneyViewModel.From(currentOffer.Amount),
                           Date = currentOffer.Date
                       };
        }
    }
}