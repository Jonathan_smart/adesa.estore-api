using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Raven.Client.Linq;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Calculators;
using Sfs.EStore.Api.Calculators.VehicleTaxRates;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Models
{
	public class ListingViewModel
	{
		public int ListingId { get; set; }
		public string Title { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AuctionSalesChannel { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string AttentionGrabber { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string Programme { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public ConditionModel Condition { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public BuyitNowSale BuyitNowSale { get; set; }

		public ImageModel PrimaryImage { get; set; }
		public ImageModel[] MoreImages { get; set; }
		public ImageModel[] Images360 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public ListingSummaryViewModel.DiscountPriceInfoModel DiscountPriceInfo { get; set; }

		public FullListingInfoModel ListingInfo { get; set; }

		public FullSellingStatusModel SellingStatus { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public BiddingInfoModel BiddingStatus { get; set; }

		public FullVehicleInfoModel VehicleInfo { get; set; }

		public bool PricesIncludeVat { get; set; }
        public bool AllowOffers { get; set; }
        public string SerialNo { get; set; }

        private static class IncludeOptions
		{
			internal const string BiddingStatus = "biddingStatus";
			internal const string ListingInfo = "listingInfo";
			internal const string SellingStatus = "sellingStatus";
			internal const string VehicleInfo = "vehicleInfo";
			internal const string Images = "images";

			internal static bool ShouldInclude(IEnumerable<string> options, string allowed)
			{
				return options == null ||
					   string.IsNullOrWhiteSpace(allowed) ||
					   !options.Any() ||
					   options.Any(x => allowed.Equals(x, StringComparison.InvariantCultureIgnoreCase));
			}
		}

		public class ConditionModel
		{
			public int Id { get; set; }

			public static ConditionModel Create(VehicleCondition condition)
			{
				return new ConditionModel { Id = (int)condition };
			}
		}


		public static ListingViewModel Map(Lot lot, Vehicle vehicle, VehicleData vehicleData, ListingViewModelMappingOptions options)
		{
			var result = new ListingViewModel();

			result.ListingId = lot.LotNumber;
			result.Programme = lot.Programme;
			result.PricesIncludeVat = lot.PricesIncludeVat;
            result.AllowOffers = lot.AllowOffers;
            result.AuctionSalesChannel = lot.Sale.Title;
           
			result.Condition = ConditionModel.Create(lot.Condition);
            result.SerialNo = lot.SerialNo;

			if (lot.BuyitNowSale != null)
			{
				result.BuyitNowSale = new BuyitNowSale()
				{
					Datetime = lot.BuyitNowSale.Datetime,
					User = lot.BuyitNowSale.User,
					BuyItNowPrice = lot.BuyItNowPrice,
					IsBuyer = options.User.Id == lot.BuyitNowSale.User
				};
			}

			if (IncludeOptions.ShouldInclude(options.Include, IncludeOptions.Images))
			{
				result.PrimaryImage = vehicle.Images.Any() ? ImageModel.From(vehicle.Images.First()) : null;
				result.MoreImages = vehicle.Images.Where(x=>x.Type !="360").Select(ImageModel.From).ToArray();

				result.Images360 = vehicle.Images.Where(x => x.Type != null && x.Type.ToLower() == "360").Any()
						? vehicle.Images.Where( x=> x.Type != null && x.Type == "360").Take(36).Select(x => new ImageModel
						{
							Container = x.Container,
							Name = x.Name,

						}).Distinct().ToArray()
						: null;
			}

			if (IncludeOptions.ShouldInclude(options.Include, IncludeOptions.ListingInfo))
			{
				result.Title = lot.Title;
				result.AttentionGrabber = lot.AttentionGrabber;

				MoneyViewModel retailPrice = null;
				if (vehicleData != null && vehicleData.Valuations != null && vehicleData.Valuations.Retail != null)
					retailPrice = MoneyViewModel.From(vehicleData.Valuations.Retail);

				if (retailPrice != null || lot.DiscountInfo.WasPrice != null)
					result.DiscountPriceInfo = new ListingSummaryViewModel.DiscountPriceInfoModel
					{
						RetailPrice = retailPrice,
						WasPrice = MoneyViewModel.From(lot.DiscountInfo.WasPrice)
					};

				result.ListingInfo = new FullListingInfoModel
				{
					BuyItNowAvailable = lot.HasBuyItNow,
					BuyItNowPrice = MoneyViewModel.From(lot.BuyItNowPrice),
					EndTime = lot.EndDate,
					StartTime = lot.StartDate,
					ListingType = lot.ListingType.ToString()
				};

			}


			if (IncludeOptions.ShouldInclude(options.Include, IncludeOptions.SellingStatus))
				result.SellingStatus = new FullSellingStatusModel
				{
					ReserveMet = lot.Bidding == null || lot.Bidding.HasMetReserve,
					TimeLeft =
						!lot.Status.In(LotStatusTypes.Active, LotStatusTypes.Reserved,
							LotStatusTypes.Scheduled) || !lot.EndDate.HasValue ||
						lot.EndDate.Value < SystemTime.CheckForTimeDiff(SystemTime.UtcNow)
							? 0
							: (lot.EndDate.Value - SystemTime.CheckForTimeDiff(SystemTime.UtcNow)).TotalMilliseconds,
					SellingState = lot.Status.ToString(),
					CurrentPrice = MoneyViewModel.From(lot.CurrentPrice()),
					BidCount = lot.Bidding == null ? (int?)null : lot.Bidding.Bids.Count(),
					MinimumToBid = lot.Bidding == null ? null : MoneyViewModel.From(lot.Bidding.MinimumBidRequired),
					BidIncrement = MoneyViewModel.From(lot.Bidding != null ? lot.Bidding.BidIncrements : new Money(0, Currency.GBP)),
					Bids = lot.Bidding == null
						? null
						: lot.Bidding.Bids.Select((offer, index) => new FullSellingStatusModel.Bid
						{
							Amount = MoneyViewModel.From(offer.Amount),
							Time = offer.Date,
							User = options.UsernameObfuscator.Obfuscate(offer.UserId),
							Position = index
						}),
					MaxBid = lot.Bidding != null && lot.Bidding.Bids.Any() ? MoneyViewModel.From(lot.Bidding.BidLimits.OrderByDescending(x => x.Amount.Amount).ThenBy(y => y.Date).First().Amount) : null,
					BuyItNowPrice = MoneyViewModel.From(lot.BuyItNowPrice),
					ShowBin = lot.Bidding != null && (lot.HasBuyItNow && (!lot.Bidding.Bids.Any() || MoneyViewModel.From(lot.Bidding.BidLimits.OrderByDescending(x => x.Amount.Amount).ThenBy(y => y.Date).First().Amount).Amount < lot.BuyItNowPrice.Amount))
				};

			if (IncludeOptions.ShouldInclude(options.Include, IncludeOptions.BiddingStatus))
				result.BiddingStatus = BiddingInfoModel.From(lot, options.User.Id);


			if (IncludeOptions.ShouldInclude(options.Include, IncludeOptions.VehicleInfo))
			{
				var performance = vehicle.Specification.Performance;
				result.VehicleInfo = new FullVehicleInfoModel
				{
					BodyColour = vehicle.ExteriorColour,
					CapId = vehicle.CapId,
					InteriorDescription = vehicle.InteriorDescription,
					Derivative = vehicle.Derivative,
					EngineDescription = vehicle.Specification.Engine.Description,
					FuelType = vehicle.FuelType,
					Make = vehicle.Make=="FIAT PROFESSIONAL" ? "FIAT" : vehicle.Make,
					Mileage = vehicle.Mileage,
					Model = vehicle.Model,

                    ModelYear = vehicle.ModelYear,
					Registration = new FullVehicleInfoModel.RegistrationModel
					{
						Date = vehicle.Registration.Date,
						Plate = vehicle.Registration.Plate,
						Yap = vehicle.Registration.YearAndPlate
					},
					Grade = new FullVehicleInfoModel.GradeInfo
					{
						Actual = vehicle.Grade.Actual == null ? null : new FullVehicleInfoModel.GradeInfo.Grade
						{
							Name = vehicle.Grade.Actual.Name
						}
					},
					Trim = vehicle.Trim == null
						? null
						: new FullVehicleInfoModel.TrimInfo { Code = vehicle.Trim.Code, Name = vehicle.Trim.Name },
					Source = vehicle.Source == null
						? null
						: new FullVehicleInfoModel.SourceInfo { Code = vehicle.Source.Code, Name = vehicle.Source.Name },
					Specification = new FullVehicleInfoModel.SpecificationModel
					{
						InsuranceGroup = vehicle.InsuranceGroup,
						Co2 = vehicle.Specification.Environment.Co2,
						Economy = new EconomyInfoModel
						{
							Combined =
								vehicle.Specification.Economy.
									Combined,
							ExtraUrban =
								vehicle.Specification.Economy.
									ExtraUrban,
							Urban =
								vehicle.Specification.Economy.
									Urban
						},
						Performance = new PerformanceInfoModel
						{
							MaximumSpeedMph = performance.MaximumSpeedMph,
							AccelerationTo100KphInSecs = performance.AccelerationTo100KphInSecs,
							MaximumTorqueNm = performance.MaximumTorqueNm,
							MaximumTorqueLbFt = performance.MaximumTorqueLbFt,
							MaximumTorqueAtRpm = performance.MaximumTorqueAtRpm,
							MaximumPowerInBhp = performance.MaximumPowerInBhp,
							MaximumPowerInKw = performance.MaximumPowerInKw,
							MaximumPowerAtRpm = performance.MaximumPowerAtRpm
						},
						Dimensions = DimensionsInfoModel.From(vehicle.Specification.Dimensions),
						Engine = EngineInfoModel.From(vehicle.Specification.Engine),
						Equipment = EquipmentInfoModel.From(vehicle.Specification.Equipment)
					},
					Transmission = vehicle.Transmission,
					VatStatus = vehicle.VatStatus,
					VehicleType = vehicle.VehicleType
				};

				var taxRate = new ListingViewModelVehicleTaxRateCalculator().CalculateFor(result);
				if (taxRate != null)
					result.VehicleInfo.Specification.TaxRate = new TaxRateModel
					{
						Band = taxRate.Band,
						RoadTax12Months = MoneyViewModel.From(taxRate.Cost12Months)
					};

				if (options.FuelCostCalculator != null && vehicle.Specification.Economy.Combined.HasValue)
				{
					var pencePerMile = options.FuelCostCalculator.PencePerUnitDistanceTravelled(
						FuelTypesParser.Parse(vehicle.FuelType),
						vehicle.Specification.Economy.Combined.Value);

					if (pencePerMile != null)
						result.VehicleInfo.Specification.Economy.CostPerUnitDistanceTravelled =
							MoneyViewModel.From(pencePerMile);
				}
			}

			return result;
		}

		public class DimensionsInfoModel
		{
			public DimensionsInfoModel(decimal? kerbWeightMin = null, decimal? kerbWeightMax = null,
				int? lengthMm = null, int? widthMm = null, int? heightMm = null,
				decimal? wheelbaseForVans = null)
			{
				KerbWeightMin = kerbWeightMin;
				KerbWeightMax = kerbWeightMax;
				LengthMm = lengthMm;
				WidthMm = widthMm;
				HeightMm = heightMm;
				WheelbaseForVans = wheelbaseForVans;

			}

			public decimal? KerbWeightMin { get; private set; }
			public decimal? KerbWeightMax { get; private set; }
			public int? LengthMm { get; private set; }
			public int? WidthMm { get; private set; }
			public int? HeightMm { get; private set; }
			public decimal? WheelbaseForVans { get; private set; }

			public static DimensionsInfoModel From(Dimensions dimensions)
			{
				return new DimensionsInfoModel(dimensions.KerbWeightMin, dimensions.KerbWeightMax,
										   dimensions.LengthMm, dimensions.WidthMm, dimensions.HeightMm,
										   dimensions.WheelbaseForVans);
			}
		}

		public class FullSellingStatusModel
		{
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public int? BidCount { get; set; }
			public MoneyViewModel CurrentPrice { get; set; }
			public string SellingState { get; set; }
			public bool ReserveMet { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public MoneyViewModel MinimumToBid { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public MoneyViewModel BidIncrement { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public double? TimeLeft { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public MoneyViewModel MaxBid { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public IEnumerable<Bid> Bids { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public MoneyViewModel BuyItNowPrice { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public bool ShowBin { get; set; }

			public class Bid
			{
				public string User { get; set; }
				public DateTime Time { get; set; }
				public MoneyViewModel Amount { get; set; }
				public int Position { get; set; }
			}
		}


		public class FullListingInfoModel
		{
			public bool BuyItNowAvailable { get; set; }
			public BuyitNowSale BuyItNowSale { get; set; }
			public MoneyViewModel BuyItNowPrice { get; set; }
			public DateTime? EndTime { get; set; }

			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public long? TimeRemaining
			{
				get
				{
					return EndTime.HasValue ? (long)(EndTime.Value - SystemTime.CheckForTimeDiff(SystemTime.UtcNow)).TotalMilliseconds : (long?)null;
				}
			}

			public DateTime? StartTime { get; set; }
			public string ListingType { get; set; }

		}

		public class FullVehicleInfoModel
		{
			public string Make { get; set; }
			public string Model { get; set; }
			public string Derivative { get; set; }
			public int? CapId { get; set; }
			public string VatStatus { get; set; }
			public string FuelType { get; set; }
			public string Transmission { get; set; }
			public RegistrationModel Registration { get; set; }
			public int? Mileage { get; set; }
			public string VehicleType { get; set; }
            public string SerialNo { get; set; }
            public SpecificationModel Specification { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public string ModelYear { get; set; }

			public string EngineDescription { get; set; }
			public string BodyColour { get; set; }
			public string InteriorDescription { get; set; }

			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public TrimInfo Trim { get; set; }

			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public SourceInfo Source { get; set; }

			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public GradeInfo Grade { get; set; }

			public class RegistrationModel
			{
				public string Plate { get; set; }
				public DateTime? Date { get; set; }
				public string Yap { get; set; }
			}

			public class SpecificationModel
			{
				[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
				public string InsuranceGroup { get; set; }
				[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
				public EconomyInfoModel Economy { get; set; }
				[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
				public decimal? Co2 { get; set; }
				[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
				public PerformanceInfoModel Performance { get; set; }
				[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
				public DimensionsInfoModel Dimensions { get; set; }
				[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
				public EngineInfoModel Engine { get; set; }
				[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
				public EquipmentInfoModel Equipment { get; set; }
				[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
				public TaxRateModel TaxRate { get; set; }
			}

			public class GradeInfo
			{
				public Grade Actual { get; set; }

				public class Grade
				{
					public string Name { get; set; }
				}
			}

			public class TrimInfo
			{
				public string Code { get; set; }
				public string Name { get; set; }
			}

			public class SourceInfo
			{
				public string Code { get; set; }
				public string Name { get; set; }
			}
		}

		public class EquipmentInfoModel
		{
			public EquipmentItemModel[] Items { get; set; }

			public class EquipmentItemModel
			{
				public string Category { get; set; }
				public string Name { get; set; }
			}

			public static EquipmentInfoModel From(Equipment equipment, int limit = Int32.MaxValue)
			{
				return new EquipmentInfoModel
				{
					Items = (equipment.Items ?? new Equipment.EquipmentItem[0])
						.Take(limit)
						.Select(x => new EquipmentItemModel
						{
							Category = x.Category,
							Name = x.Name
						})
						.ToArray()
				};
			}
		}

		public class EngineInfoModel
		{
			public EngineInfoModel(int? numberOfCylinders = null, int? numberOfValvesPerCylinder = null,
				string arrangementOfCylinders = null, int? capacityCc = null)
			{
				NumberOfCylinders = numberOfCylinders;
				NumberOfValvesPerCylinder = numberOfValvesPerCylinder;
				ArrangementOfCylinders = arrangementOfCylinders;
				CapacityCc = capacityCc;
			}

			public int? NumberOfCylinders { get; private set; }
			public int? NumberOfValvesPerCylinder { get; private set; }
			public string ArrangementOfCylinders { get; private set; }
			public int? CapacityCc { get; private set; }

			public static EngineInfoModel From(Engine engine)
			{
				return new EngineInfoModel(engine.NumberOfCylinders, engine.NumberOfValvesPerCylinder, engine.ArrangementOfCylinders, engine.CapacityCc);
			}
		}

		public class PerformanceInfoModel
		{
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public decimal? MaximumSpeedMph { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public decimal? AccelerationTo100KphInSecs { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public decimal? MaximumTorqueNm { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public decimal? MaximumTorqueLbFt { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public decimal? MaximumTorqueAtRpm { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public decimal? MaximumPowerInBhp { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public decimal? MaximumPowerInKw { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public decimal? MaximumPowerAtRpm { get; set; }
		}

		public class EconomyInfoModel
		{
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public decimal? Urban { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public decimal? ExtraUrban { get; set; }
			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public decimal? Combined { get; set; }

			[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
			public MoneyViewModel CostPerUnitDistanceTravelled { get; set; }
		}

		public class TaxRateModel
		{
			public char? Band { get; set; }
			public MoneyViewModel RoadTax12Months { get; set; }
		}

		public class BiddingInfoModel
		{
			public MoneyViewModel MaximumBid { get; set; }
			public DateTime MaximumBidTime { get; set; }
			public string BiddingStatus { get; set; }

			public static BiddingInfoModel From(Lot lot, string userId)
			{
				if (lot.Bidding == null) return null;
				if (lot.Bidding.BidLimits.All(x => x.UserId != userId)) return null;

				var model = new BiddingInfoModel();

				var highestUserBid = lot.Bidding.BidLimits.Where(x => x.UserId == userId)
					.OrderBy(x => x.Amount.Amount)
					.Last();

				model.MaximumBid = MoneyViewModel.From(highestUserBid.Amount);
				model.MaximumBidTime = highestUserBid.Date;

				model.BiddingStatus = lot.CurrentOffer.UserId == userId
										  ? "Winning"
										  : "Outbid";

				return model;
			}
		}
	}

	public class ListingViewModelMappingOptions
	{
		public ListingViewModelMappingOptions(MembershipUserIdentity user, UsernameObfuscator usernameObfuscator)
		{
			UsernameObfuscator = usernameObfuscator;
			User = user;
			Include = new string[0];
		}

		public MembershipUserIdentity User { get; private set; }
		public UsernameObfuscator UsernameObfuscator { get; private set; }
		public FuelCostCalculator FuelCostCalculator { get; set; }
		public IEnumerable<string> Include { get; set; }
	}
}