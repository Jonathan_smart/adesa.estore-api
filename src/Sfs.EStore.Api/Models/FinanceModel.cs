using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Sfs.EStore.Api.Models
{
    [DataContract]
    public class FinanceModel
    {
        [Required, DataMember(IsRequired = true)]
        public decimal Deposit { get; set; }

        [Required, DataMember(IsRequired = true)]
        public int Term { get; set; }

        [Required, DataMember(IsRequired = true)]
        public int AnnualMileage { get; set; }

        [Required, DataMember(IsRequired = true)]
        public string[] Product { get; set; }
    };
}