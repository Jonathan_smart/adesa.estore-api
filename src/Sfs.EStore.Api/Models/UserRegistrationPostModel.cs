using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Models
{
    public class UserRegistrationPostModel
    {
        public bool DryRun { get; set; }

        public string Username { get; set; }
        
        [DataType(DataType.Password)] 
        public string Password { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        public bool Approved { get; set; }
        
        public string CompanyName { get; set; }
        public string JobTitle { get; set; }
        public string Website { get; set; }

        private AddressModel _address = new AddressModel();
        public AddressModel Address
        {
            get { return _address; }
            set { _address = value ?? new AddressModel(); }
        }

        private Dictionary<string, string> _data = new Dictionary<string, string>();
        public Dictionary<string, string> Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public MembershipUser BindTo(MembershipUser target)
        {
            target.Username = Username;
            target.FirstName = FirstName;
            target.LastName = LastName;
            target.PhoneNumber = PhoneNumber;
            target.EmailAddress = EmailAddress;

            target.SetPassword(Password);
            if (Approved)
                target.Approve();

            return target;
        }

        public override string ToString()
        {
            return string.Format("Username: {0}, FirstName: {1}, LastName: {2}, PhoneNumber: {3}, EmailAddress: {4}, CompanyName: {5}, JobTitle: {6}, Website: {7}, Address: [{8}], Data: {9}", Username, FirstName, LastName, PhoneNumber, EmailAddress, CompanyName, JobTitle, Website, Address,
                string.Join("; ", (Data ?? new Dictionary<string, string>()).Select(kv => string.Format("{0}:{1}", kv.Key, kv.Value))));
        }
    }

    public class AddressModel
    {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string TownCity { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }

        public override string ToString()
        {
            return string.Format("Line1: {0}, Line2: {1}, TownCity: {2}, County: {3}, Postcode: {4}", Line1, Line2, TownCity, County, Postcode);
        }
    }

    public class PasswordChange
    {
        [Required(AllowEmptyStrings = false)]
        public string CurrentPassword { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string NewPassword { get; set; }
    }

    public class UsersGetParams
    {
        public string Q { get; set; }
        public string Username { get; set; }
        public bool? IsApproved { get; set; }
        public bool? IsLockedOut { get; set; }

        public int? Skip { get; set; }
        public int? Top { get; set; }
        public string SortBy { get; set; }
    }
}