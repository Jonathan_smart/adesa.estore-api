namespace Sfs.EStore.Api.Models
{
    public class ResetPasswordPostModel
    {
        public string Token { get; set; }
        public string NewPassword { get; set; }
    }
}