using System.Security.Policy;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Data.DataAccess;
using Sfs.EStore.Api.Properties;
using Sfs.Orders.Adapters;
using GmAdapters = Sfs.Orders.Gm.Adapters;
using SfsAdapters = Sfs.Orders.Sfs.Adapters;
using Sfs.Orders.Ports;
using Sfs.Orders.Ports.AddToBasket;

namespace Sfs.EStore.Api
{
    public class NinjectOrdersModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<TransportBusinessUnit>().ToMethod(ctx =>
            {
                var principal = ctx.Kernel.Get<IApiApplicationPrincipal>();
                string value;
                principal.Identity.Advanced.IntegrationSettings.TryGetValue("Sfs/TransportBusinessUnit", out value);

                return new TransportBusinessUnit(value);
            }).InRequestScope();

            Kernel.Bind<ISerialNoInformationByRegistrationQuery>().To<SerialNoInformationByRegistrationQuery>();

            Kernel.Bind<ICalculateTransportShipToAddressQuery>().To<ShipToAddressReaderDto>();

            Kernel.Bind<IHandleRequests<RemoveFromBasket>>().To<RemoveFromBasketHandler>()
                .WithConstructorArgument("navBasketServiceUrl",
                    ctx => new Url(Settings.Default.Sfs_EStore_Api_NavBasketServcie_Basket_Service));

            Kernel.Bind<IHandleRequests<AddToBasket>>().To<AddToBasketBasketHandler>()
                .WithConstructorArgument("navBasketServiceUrl",
                    ctx => new Url(Settings.Default.Sfs_EStore_Api_NavBasketServcie_Basket_Service));

            SfsBindings();
            GmBindings();
        }

        private void SfsBindings()
        {
            Kernel.Bind<IHandleRequests<OrderVehicles>>().To<SfsAdapters.CreateSalesOrdersHandler>()
                .WhenNavisionInstance().IsSfs()
                .WithConstructorArgument("serviceUrl", ctx => new Url(Settings.Default.Sfs_EStore_Api_NavWebIntegrationService_WebIntegration));

            Kernel.Bind<TransportChargeItem>().ToMethod(ctx => new TransportChargeItem("CHG0002"))
                .WhenNavisionInstance().IsSfs();

     
            Kernel.Bind<ICreateSalesOrderBasketItemsQuery>().To<CreateSalesOrderBasketItemsQuery>()
                .WhenNavisionInstance().IsSfs();

            Kernel.Bind<ICreateSalesOrderTransportSalesPriceQuery>().To<TransportSalesPriceReaderDto>()
                .WhenNavisionInstance().IsSfs();

            Kernel.Bind<IFinanceOptionsQuery>().To<FinanceOptionsDto>()
                .WhenNavisionInstance().IsSfs();

            Kernel.Bind<IHandleRequests<CalculateTransport>>().To<SfsAdapters.CalculateTransportHandler>()
                .WhenNavisionInstance().IsSfs()
                .WithConstructorArgument("serviceUrl", ctx => new Url(Settings.Default.Sfs_EStore_Api_NavWebIntegrationService_WebIntegration));

            Kernel.Bind<IContactSalesOrdersQuery>().To<ContactSalesOrdersQuery>()
                .WhenNavisionInstance().IsSfs();

            Kernel.Bind<ISalesOrdersQuery>().To<SalesOrdersQuery>()
                .WhenNavisionInstance().IsSfs();

            Kernel.Bind<IContactBasketQuery>().To<ContactBasketQuery>()
                .WhenNavisionInstance().IsSfs();
        }

        private void GmBindings()
        {
            Kernel.Bind<Orders.GmNavWebIntegration.WebIntegration>()
                .ToMethod(ctx => new Orders.GmNavWebIntegration.WebIntegration
                {
                    UseDefaultCredentials = true,
                    Url = Settings.Default.Sfs_EStore_Api_NavWebIntegrationService_WebIntegration
                })
                .WhenNavisionInstance().IsGm();

            Kernel.Bind<Orders.GmSalesOrderConfirmationService.Sales_Order_Confirmation_Service>()
                .ToMethod(ctx => new Orders.GmSalesOrderConfirmationService.Sales_Order_Confirmation_Service
                {
                    UseDefaultCredentials = true,
                    Url = Settings.Default.Sfs_EStore_Api_GmSalesOrderConfirmationService_Sales_Order_Confirmation_Service
                })
                .WhenNavisionInstance().IsGm();

            Kernel.Bind<IHandleRequests<OrderVehicles>>().To<GmAdapters.CreateSalesOrdersHandler>()
                .WhenNavisionInstance().IsGm()
                .WithConstructorArgument("salesPersonCode", "WEB");

            Kernel.Bind<TransportChargeItem>().ToMethod(ctx => new TransportChargeItem(null))
                .WhenNavisionInstance().IsGm();

            Kernel.Bind<ICreateSalesOrderBasketItemsQuery>().To<GmAdapters.DataAccess.CreateSalesOrderBasketItemsQuery>()
                .WhenNavisionInstance().IsGm();

            Kernel.Bind<ICreateSalesOrderTransportSalesPriceQuery>().To<GmAdapters.DataAccess.CreateSalesOrderTransportSalesPriceQuery>()
                .WhenNavisionInstance().IsGm();

            Kernel.Bind<IFinanceOptionsQuery>().To<GmAdapters.DataAccess.FinanceOptionsQuery>()
                .WhenNavisionInstance().IsGm();

            Kernel.Bind<IHandleRequests<CalculateTransport>>().To<GmAdapters.CalculateTransportHandler>()
                .WhenNavisionInstance().IsGm();

            Kernel.Bind<IContactSalesOrdersQuery>().To<GmAdapters.DataAccess.ContactSalesOrdersQuery>()
                .WhenNavisionInstance().IsGm();

            Kernel.Bind<ISalesOrdersQuery>().To<GmAdapters.DataAccess.SalesOrdersQuery>()
                .WhenNavisionInstance().IsGm();

            Kernel.Bind<IContactBasketQuery>().To<GmAdapters.DataAccess.ContactBasketQuery>()
                .WhenNavisionInstance().IsGm();
        }
    }
}