﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;
using log4net.Appender;
using log4net.Core;
using Mindscape.Raygun4Net;
using Mindscape.Raygun4Net.Messages;
using Sfs.EStore.Api.Authentication;

namespace Sfs.EStore.Api
{
    public class RaygunLog4NetAppender : AppenderSkeleton
    {
        protected override void Append(LoggingEvent loggingEvent)
        {
            var principal = Thread.CurrentPrincipal as IApiApplicationPrincipal;
            
            var builder = RaygunMessageBuilder.New
                .SetHttpDetails(HttpContext.Current)
                .SetEnvironmentDetails()
                .SetMachineName(Environment.MachineName)
                .SetVersion(typeof (RaygunLog4NetAppender).Assembly.GetName().Version.ToString())
                .SetClientDetails()
                .SetExceptionDetails(loggingEvent.ExceptionObject)
                .SetUser(GetUserInfo(principal));

            var message = builder
                .SetUserCustomData(new Dictionary<string, object>
                {
                    {"api.user", principal == null ? "" : principal.Identity.Name},
                    {"log4net.Level", loggingEvent.Level.ToString()},
                    {"log4net.LoggerName", loggingEvent.LoggerName},
                    {"log4net.Message", loggingEvent.RenderedMessage},
                    {"log4net.Repository.Name", loggingEvent.Repository.Name},
                    {"log4net.ThreadName", loggingEvent.ThreadName}
                })
                .Build();

            new RaygunClient().Send(message);
        }

        private static RaygunIdentifierMessage GetUserInfo(IApiApplicationPrincipal user)
        {
            if (user == null) return new RaygunIdentifierMessage("");

            return new RaygunIdentifierMessage(user.MembershipUser.Identity.Name);
        }
    }
}