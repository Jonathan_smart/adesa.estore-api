﻿using System;
using System.Linq;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Controllers;
using Sfs.EStore.Api.Authentication;

namespace Sfs.EStore.Api
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class MembershipUserAuthoriseAttribute : AuthorizeAttribute
    {
        public static readonly string[] EmptyArray = new string[0];
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext == null) throw new ArgumentNullException("actionContext");

            if (SkipAuthorization(actionContext) || IsAuthorized(actionContext))
                return;

            HandleUnauthorizedRequest(actionContext);
        }

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            if (actionContext == null) throw new ArgumentNullException("actionContext");

            var currentPrincipal = Thread.CurrentPrincipal as ApiApplicationPrincipal;

            if (currentPrincipal == null || !currentPrincipal.Identity.IsAuthenticated || !currentPrincipal.MembershipUser.Identity.IsAuthenticated)
                return false;

            var rolesSplit = SplitString(Roles);
            if (rolesSplit.Length > 0 && !rolesSplit.Any(x => currentPrincipal.MembershipUser.IsInRole(x.ToLower())))
                return false;

            return true;
        }

        private static bool SkipAuthorization(HttpActionContext actionContext)
        {
            var skipChecks = new Func<bool>[]
                                 {
                                     () =>
                                     actionContext.ActionDescriptor
                                         .GetCustomAttributes<AllowAnonymousAttribute>().Any(),
                                     () =>
                                     actionContext.ControllerContext.ControllerDescriptor
                                         .GetCustomAttributes<AllowAnonymousAttribute>().Any(),
                                     () =>
                                     actionContext.ActionDescriptor
                                         .GetCustomAttributes<AllowAnonymousEndUserAttribute>().Any(),
                                     () =>
                                     actionContext.ControllerContext.ControllerDescriptor
                                         .GetCustomAttributes<AllowAnonymousEndUserAttribute>().Any()
                                 };

            return skipChecks.Any(x => x());
        }

        internal static string[] SplitString(string original)
        {
            if (string.IsNullOrEmpty(original))
                return EmptyArray;

            return original.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Trim())
                .ToArray();
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class AllowAnonymousEndUserAttribute : Attribute
    {
    }
}