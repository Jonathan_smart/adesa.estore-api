using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using Raven.Client;
using ModelStateDictionary = System.Web.Http.ModelBinding.ModelStateDictionary;

namespace Sfs.EStore.Api
{
    public static class RavenToListAsyncExtensions
    {
        public static Task<HttpResponseMessage> ToListAsyncWithTotalResultsHeader<T>(this IQueryable<T> @this, HttpRequestMessage request, RavenQueryStatistics stats)
        {
            return @this.ToListAsync()
                .ContinueWith(t =>
                {
                    var response = request.CreateResponse(HttpStatusCode.OK, t.Result);

                    response.Headers.Add("X-TotalResults",
                                          stats.TotalResults.ToString(CultureInfo.InvariantCulture));

                    return response;
                });
        }

        public static HttpResponseMessage ToListAsyncWithTotalResultsHeader<T>(this IEnumerable<T> @this, HttpRequestMessage request, RavenQueryStatistics stats)
        {
            var response = request.CreateResponse(HttpStatusCode.OK, @this);

            response.Headers.Add("X-TotalResults",
                                 stats.TotalResults.ToString(CultureInfo.InvariantCulture));

            return response;
        }
    }
    public static class ModelStateExtensions
    {

        public static void AddModelError<TModel, TValue>(this ModelStateDictionary @this, TModel model, Expression<Func<TModel, TValue>> expression, string errorMessage)
        {
            @this.AddModelError(ExpressionHelper.GetExpressionText(expression), errorMessage);
        }

        public static void AddModelError<TModel, TValue>(this ModelStateDictionary @this, TModel model, Expression<Func<TModel, TValue>> expression, string format, params object[] args)
        {
            @this.AddModelError(model, expression, string.Format(format, args));
        }
    }
}