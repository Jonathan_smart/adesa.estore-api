using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Linq;
using Raven.Client.Indexes;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Raven.Indexes
{
    public class Lot_Sales : AbstractIndexCreationTask<Lot, Lot_Sales.ReduceResult>
    {
        public class ReduceResult
        {
            public string Id { get; set; }
            public string Title { get; set; }
            public int AllowPreview { get; set; }
            public int Count { get; set; }
            public int Available { get; set; }
            public int Scheduled { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
        }

        public Lot_Sales()
        {
            Map = lots => from lot in lots
                          let startDate = lot.StartDate
                          where startDate != null  
                          where lot.VisibilityLimitedTo[0].Length > 0 
                          select new ReduceResult
                                               {
                                                   Id = lot.Sale.Id,
                                                   Title = lot.Sale.Title,
                                                   AllowPreview = lot.Sale.AllowPreview == true ? 1 : 0,
                                                   Count = 1,
                                                   Available = (lot.Status == LotStatusTypes.Active) ? 1 : 0,
                                                   Scheduled = (lot.Status == LotStatusTypes.Scheduled) ? 1 : 0,
                                                   StartDate = (lot.Status == LotStatusTypes.Active) ? lot.StartDate.Value : lot.StartDate.Value.AddDays(365),
                                                   EndDate = (lot.Status == LotStatusTypes.Active) ? lot.EndDate.Value : lot.EndDate.Value.AddDays(-365)
                                               };

            Reduce = results => from sale in results
                                group sale by new { sale.Id, sale.Title}
                                    into g
                                    select new ReduceResult
                                               {
                                                   Id = g.Key.Id,
                                                   Title = g.Key.Title,
                                                   AllowPreview = g.Sum(x => x.AllowPreview),
                                                   Count = g.Sum(x => x.Count),
                                                   Available = g.Sum(x => x.Available),
                                                   Scheduled = g.Sum(x => x.Scheduled),
                                                   StartDate = g.Select(x => x.StartDate).Min(),
                                                   EndDate = g.Select(x => x.EndDate).Max()
                                               };
        }
    }
}