using System.Collections.Generic;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Raven.Transformers;

namespace Sfs.EStore.Api.Raven.Indexes
{
    public class SessionInteractions_UsersAlsoViewed : AbstractIndexCreationTask<SessionInteractions, SessionInteractions_UsersAlsoViewed.ReduceResult>
    {
        private const int MaximumMapResultsForASourceDocument = 50;

        public class ReduceResult : IHaveALotId
        {
            public string LotId { get; set; }
            public IEnumerable<string> LotsViewed { get; set; }
        }

        public SessionInteractions_UsersAlsoViewed()
        {
            Map = interactions =>
                from interaction in interactions
                from viewedLotRef in interaction.LotsViewed.OrderByDescending(x => x.Date).Take(MaximumMapResultsForASourceDocument)
                let lot = LoadDocument<Lot>(viewedLotRef.LotId)
                select new ReduceResult
                {
                    LotId = lot.Id,
                    LotsViewed = interaction.LotsViewed
                        .OrderByDescending(x => x.Date)
                        .Where(x => x.LotId != lot.Id)
                        .Select(x => x.LotId)
                };

            Reduce = results =>
                from result in results
                group result by result.LotId
                into g
                select new ReduceResult
                {
                    LotId = g.Key,
                    LotsViewed = g.SelectMany(x => x.LotsViewed).Distinct()
                };

            Store(x => x.LotId, FieldStorage.Yes);
        }
    }
}