using System;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Raven.Transformers;

namespace Sfs.EStore.Api.Raven.Indexes
{
    public class ListingViewCount_Search : AbstractIndexCreationTask<ListingViewCount, ListingViewCount_Search.ReduceResult>
    {
        public class ReduceResult : IHaveALotId, IFullListingSearchReduce
        {
            public int ViewCount { get; set; }
            public DateTime FirstViewedOn { get; set; }
            public DateTime LastViewedOn { get; set; }

            public string LotId { get; set; }
            public int LotNumber { get; set; }
            public ListingType ListingType { get; set; }
            public VehicleCondition Condition { get; set; }
            public DateTime CreatedAt { get; set; }
            public string SaleTitle { get; set; }
            public string SaleId { get; set; }

            public decimal CurrentPrice { get; set; }
            public decimal? CurrentOfferAmount { get; set; }
            public DateTime? CurrentOfferDate { get; set; }

            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public string VehicleId { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string Derivative { get; set; }
            public int? Mileage { get; set; }
            public string Transmission { get; set; }
            public string TransmissionSimple { get; set; }
            public string FuelType { get; set; }
            public string Engine { get; set; }
            public string Category { get; set; }
            public string[] Equipment { get; set; }
            public string ExteriorColour { get; set; }
            public string VehicleType { get; set; }
            public string RegistrationYaP { get; set; }
            public string RegistrationPlate { get; set; }
            public DateTime? RegistrationDate { get; set; }
            public string Source { get; set; }
            public LotStatusTypes Status { get; set; }
            public object[] Query { get; set; }

            public string[] VisibilityLimitedTo { get; set; }
            public string RandomOrderByValue { get; set; }
        }

        public ListingViewCount_Search()
        {
            Map = viewCounts => from view in viewCounts
                let lot = LoadDocument<Lot>(view.LotId)
                let vehicle = LoadDocument<Vehicle>(lot.VehicleId)
                select new
                {
                    ViewCount = view.Count,
                    FirstViewedOn = view.CreatedOn,
                    LastViewedOn = view.LastIncrementedOn,
                    lot.LotNumber,
                    LotId = lot.Id,
                    lot.CreatedAt,
                    lot.Status,
                    lot.ListingType,
                    lot.Condition,
                    SaleTitle = lot.Sale == null ? null : lot.Sale.Title,
                    SaleId = lot.Sale == null ? null : lot.Sale.Id,
                    vehicle.Make,
                    vehicle.Model,
                    vehicle.Derivative,
                    vehicle.FuelType,
                    vehicle.Transmission,
                    TransmissionSimple = vehicle.Transmission == null
                        ? "Unspecified"
                        : (vehicle.Transmission.ToLower().Contains("auto")
                            ? "Automatic"
                            : "Manual"),
                    vehicle.Mileage,
                    RegistrationPlate = vehicle.Registration.Plate,
                    RegistrationDate = vehicle.Registration.Date,
                    RegistrationYaP = vehicle.Registration.YearAndPlate,
                    Source = vehicle.Source == null ? null : vehicle.Source.Name,
                    CurrentPrice = lot.CurrentOffer != null
                        ? lot.CurrentOffer.Amount.Amount
                        : (lot.Bidding != null
                            ? lot.Bidding.StartingPrice.Amount
                            : (lot.HasBuyItNow ? lot.BuyItNowPrice.Amount : 0m)),
                    CurrentOfferAmount = lot.CurrentOffer == null
                        ? (decimal?) null
                        : lot.CurrentOffer.Amount.Amount,
                    StartTime = lot.StartDate ?? DateTime.MinValue,
                    EndTime = lot.EndDate ?? DateTime.MaxValue,
                    vehicle.VehicleType,
                    vehicle.Category,
                    Query = new object[]
                    {
                        lot.LotNumber,
                        lot.Id,
                        lot.Title,
                        vehicle.ExteriorColour,
                        vehicle.Make,
                        vehicle.FuelType,
                        vehicle.Model,
                        vehicle.Derivative,
                        vehicle.Registration.Plate
                    },
                    lot.VisibilityLimitedTo,
                    RandomOrderByValue = vehicle.RandomisedValue
                };

            Sort(x => x.ViewCount, SortOptions.Int);

            Index(x => x.SaleTitle, FieldIndexing.NotAnalyzed);
            Index(x => x.Query, FieldIndexing.Analyzed);
            Index(x => x.Make, FieldIndexing.NotAnalyzed);
            Index(x => x.Model, FieldIndexing.NotAnalyzed);
            Index(x => x.Derivative, FieldIndexing.NotAnalyzed);
            Index(x => x.VehicleType, FieldIndexing.NotAnalyzed);
            Index(x => x.RegistrationPlate, FieldIndexing.Default);
            Index(x => x.RegistrationYaP, FieldIndexing.NotAnalyzed);
            Index(x => x.Category, FieldIndexing.NotAnalyzed);
            Index(x => x.TransmissionSimple, FieldIndexing.NotAnalyzed);
            Index(x => x.FuelType, FieldIndexing.NotAnalyzed);
            Index(x => x.VisibilityLimitedTo, FieldIndexing.NotAnalyzed);

            Store(x => x.LotId, FieldStorage.Yes);
        }
    }
}