using System;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Raven.Transformers;

namespace Sfs.EStore.Api.Raven.Indexes
{
    public class Lot_Bids : AbstractIndexCreationTask<Lot, Lot_Bids.ReduceResult>
    {
        public class ReduceResult : IHaveALotId
        {
            public string LotId { get; set; }
            public int LotNumber { get; set; }
            public ListingType ListingType { get; set; }
            public string BuyitNowUser { get; set; }
            public LotStatusTypes Status { get; set; }
            public decimal? CurrentOfferAmount { get; set; }
            public string CurrentOfferUserId { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string[] Bidders { get; set; }
            public object[] Query { get; set; }
        }

        public Lot_Bids()
        {
            Map = lots => from lot in lots
                          let vehicle = LoadDocument<Vehicle>(lot.VehicleId)
                          select new
                                     {
                                         LotId = lot.Id,
                                         lot.LotNumber,
                                         lot.ListingType,
                                         BuyitNowUser = lot.BuyitNowSale.User ?? string.Empty,
                                         lot.Status,
                                         CurrentOfferAmount = lot.CurrentOffer.Amount,
                                         CurrentOfferUserId = lot.CurrentOffer.UserId,
                                         StartTime = lot.StartDate ?? DateTime.MinValue,
                                         EndTime = lot.EndDate ?? DateTime.MaxValue,
                                         vehicle.Make,
                                         vehicle.Model,
                                         Bidders = lot.Bidding == null
                                                       ? new string[0]
                                                       : lot.Bidding.Bids.Select(x => x.UserId),
                                         Query = new object[]
                                                     {
                                                         lot.LotNumber,
                                                         lot.Id,
                                                         lot.Title,
                                                         vehicle.ExteriorColour,
                                                         vehicle.Make,
                                                         vehicle.FuelType,
                                                         vehicle.Model,
                                                         vehicle.Derivative,
                                                         vehicle.Registration.Plate
                                                     }
                                     };

            Sort(x => x.LotNumber, SortOptions.Int);
            Sort(x => x.CurrentOfferAmount, SortOptions.Double);
            Index(x => x.Query, FieldIndexing.Analyzed);
            Index(x => x.Make, FieldIndexing.NotAnalyzed);
            Index(x => x.Model, FieldIndexing.NotAnalyzed);

            Store(x => x.LotId, FieldStorage.Yes);
        }
    }
}