using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Raven.Transformers;

namespace Sfs.EStore.Api.Raven.Indexes
{
    public class UserWatchedLot_Search : AbstractIndexCreationTask<UserLotWatches, UserWatchedLot_Search.ReduceResult>
    {
        public class ReduceResult : IHaveALotId
        {
            public string User { get; set; }
            public string LotId { get; set; }
            public string Status { get; set; }
            public object[] Query { get; set; }
        }

        public UserWatchedLot_Search()
        {
            Map = watches => from watch in watches
                             from lotId in watch.WatchedLots
                             let lot = LoadDocument<Lot>(lotId)
                             where lot != null
                             select new
                                        {
                                            watch.User,
                                            LotId = lotId,
                                            lot.Status,
                                            Query = new []
                                                    {
                                                        lot.Title
                                                    }
                                        };

            // This reduce is required because of a regression from RavenDb v2.5 -> v3
            // Set this pull request: https://github.com/ravendb/ravendb/pull/586
            Reduce = results =>
                from result in results
                group result by new
                {
                    result.User,
                    result.LotId,
                    result.Status,
                    result.Query
                }
                into g
                select new
                {
                    g.Key.User,
                    g.Key.LotId,
                    g.Key.Status,
                    g.Key.Query
                };

            Store(x => x.LotId, FieldStorage.Yes);
            Index(x => x.Query, FieldIndexing.Analyzed);
        }
    }
}