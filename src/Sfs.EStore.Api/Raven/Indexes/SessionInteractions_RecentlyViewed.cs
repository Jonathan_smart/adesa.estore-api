using System;
using System.Linq;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Raven.Transformers;

namespace Sfs.EStore.Api.Raven.Indexes
{
    public class SessionInteractions_RecentlyViewed : AbstractIndexCreationTask<SessionInteractions, SessionInteractions_RecentlyViewed.ReduceResult>
    {
        private const int MaximumMapResults = 15;

        public class ReduceResult : IHaveALotId
        {
            public string SessionId { get; set; }
            public string Username { get; set; }
            public DateTime Date { get; set; }
            public string LotId { get; set; }
            public LotStatusTypes LotStatus { get; set; }
            public string[] VisibilityLimitedTo { get; set; }
        }

        public SessionInteractions_RecentlyViewed()
        {
            Map = interactions => from i in interactions
                                  from l in i.LotsViewed.OrderByDescending(x => x.Date).Take(MaximumMapResults)
                                  let lot = LoadDocument<Lot>(l.LotId)
                                  where lot != null
                                  let vehicle = LoadDocument<Vehicle>(lot.VehicleId)
                                  where vehicle != null
                                  select new ReduceResult
                                      {
                                          SessionId = i.SessionId,
                                          Username = i.Username,
                                          Date = l.Date,
                                          LotId = l.LotId,
                                          LotStatus = lot.Status,
                                          VisibilityLimitedTo = lot.VisibilityLimitedTo ?? new string[0]
                                      };

            Store(x => x.LotId, FieldStorage.Yes);
        }
    }
}