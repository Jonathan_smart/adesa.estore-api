using System;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Raven.Transformers
{
    public interface IHaveALotId
    {
        string LotId { get; }
    }

    public interface IFullListingSearchReduce
    {
        string LotId { get; }
        int LotNumber { get; }
        ListingType ListingType { get; }
        VehicleCondition Condition { get; }
        DateTime CreatedAt { get; }
        string SaleTitle { get; }
        string SaleId { get; }

        decimal CurrentPrice { get; }
        decimal? CurrentOfferAmount { get; }
        DateTime? CurrentOfferDate { get; }

        DateTime StartTime { get; }
        DateTime EndTime { get; }
        string VehicleId { get; }
        string Make { get; }
        string Model { get; }
        string Derivative { get; }
        int? Mileage { get; }
        string Transmission { get; }
        string TransmissionSimple { get; }
        string FuelType { get; }
        string Engine { get; }
        string Category { get; }
        string[] Equipment { get; }
        string ExteriorColour { get; }
        string VehicleType { get; }
        string RegistrationYaP { get; }
        string RegistrationPlate { get; }
        string Source { get; }
        DateTime? RegistrationDate { get; }
        LotStatusTypes Status { get; }
        object[] Query { get; }

        string[] VisibilityLimitedTo { get; }
        string RandomOrderByValue { get; }
    }
}