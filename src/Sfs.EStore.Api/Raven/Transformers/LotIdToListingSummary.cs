using System;
using System.Linq;
using Raven.Client.Indexes;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Raven.Transformers
{
    public class LotIdToListingSummary : AbstractTransformerCreationTask<IHaveALotId>
    {
        public LotIdToListingSummary()
        {
            try
            {


                TransformResults = results =>
                    from result in results
                    let lot = LoadDocument<Lot>(result.LotId)
                    let vehicle = LoadDocument<Vehicle>(lot.VehicleId)
                    let vehicleData = LoadDocument<VehicleData>(string.Format("{0}/Data", lot.VehicleId))
                    select new
                    {

                        BiddingInfo = new
                        {
                            CurrentOffer = new
                            {
                                lot.CurrentOffer.UserId
                            },
                            BidLimits = lot.Bidding.BidLimits,
                            BuyItNowSale = lot.BuyitNowSale != null ? true : false
                        },
                        Condition = new
                        {
                            Name = lot.Condition
                        },
                        DiscountPriceInfo = new
                        {
                            RetailPrice = (vehicleData.Valuations == null || vehicleData.Valuations.Retail == null)
                                ? null
                                : vehicleData.Valuations.Retail,
                            WasPrice = lot.DiscountInfo.WasPrice
                        },
                        PrimaryImage = vehicle.Images.FirstOrDefault(),
                        MoreImages = vehicle.Images.Skip(1).Take(2),
                        ListingId = lot.LotNumber,
                        ListingInfo = new
                        {
                            BuyItNowAvailable = lot.HasBuyItNow,
                            BuyItNowPrice = lot.BuyItNowPrice,
                            EndTime = lot.EndDate,
                            StartTime = lot.StartDate,
                            ListingType = lot.ListingType.ToString(),
                        },
                        SellingStatus = new
                        {
                            BidCount = lot.Bidding.Bids.Count(),
                            CurrentPrice = lot.CurrentOffer != null
                                ? lot.CurrentOffer.Amount
                                : (lot.Bidding != null
                                    ? lot.Bidding.
                                        StartingPrice
                                    : (lot.HasBuyItNow
                                        ? lot.
                                            BuyItNowPrice
                                        : null)),
                            SellingState = lot.Status.ToString(),
                            EndTime = lot.EndDate,
                            ReserveMet = lot.Bidding == null || lot.Bidding.HasMetReserve
                        },
                        VehicleInfo = new
                        {
                            vehicle.Make,
                            vehicle.Model,
                            vehicle.Derivative,
                            vehicle.VatStatus,
                            vehicle.FuelType,
                            vehicle.Transmission,
                            vehicle.ExteriorColour,
                            Registration = new
                            {
                                vehicle.Registration.Plate,
                                vehicle.Registration.Date,
                                Yap =
                                    vehicle.Registration.YearAndPlate,
                            },
                            vehicle.Mileage,
                            vehicle.VehicleType,
                            Specification = new
                            {
                                vehicle.Specification.Economy,
                                vehicle.Specification.Environment.Co2
                            },
                            Trim = vehicle.Trim == null ? null : new
                            {
                                Code = vehicle.Trim.Code,
                                Name = vehicle.Trim.Name
                            },
                            Source = vehicle.Source == null ? null : new
                            {
                                Code = vehicle.Source.Code,
                                Name = vehicle.Source.Name
                            },
                            Engine = (vehicle.Specification.Engine == null || vehicle.Specification.Engine.Description == null) ? null : new
                            {
                                Description = vehicle.Specification.Engine.Description
                            }
                        },
                        PricesIncludeVat = lot.PricesIncludeVat,
                        Title = lot.Title,
                        AttentionGrabber = lot.AttentionGrabber,
                        Equipment = vehicle.Specification.Equipment.Items.Select(x => x.Name)
                    };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}