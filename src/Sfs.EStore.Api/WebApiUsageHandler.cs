using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using log4net;

namespace Sfs.EStore.Api
{
    public class WebApiUsageHandler : DelegatingHandler
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (WebApiUsageHandler));

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var id = Guid.NewGuid();
            if (request != null) LogRequest(id, request);

            return base.SendAsync(request, cancellationToken)
                .ContinueWith(task =>
                                  {
                                      LogResponse(id, task.Result);
                                      return task.Result;
                                  });
        }

        private void LogRequest(Guid id, HttpRequestMessage request)
        {
            if (!Logger.IsInfoEnabled) return;

            var content = request.Content == null
                              ? ""
                              : request.Content.ReadAsStringAsync().Result;

            if (content.Contains("password"))
                content = "**** REMOVED ****";

            Logger.Info(new
                            {
                                Id = id,
                                Timestamp = SystemTime.UtcNow,
                                RequestMethod = request.Method.Method,
                                Uri = request.RequestUri.ToString(),
                                Content = content,
                                Headers = ExtractHeaders(request.Headers),
                                IP = ((HttpContextBase) request.Properties["MS_HttpContext"]).Request.UserHostAddress
                            });
        }

        private void LogResponse(Guid id, HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                if (!Logger.IsDebugEnabled) return;

                Logger.Debug(new
                                 {
                                     Id = id,
                                     Timestamp = SystemTime.UtcNow,
                                     response.StatusCode,
                                     Content = response.Content == null
                                                   ? ""
                                                   : response.Content.ReadAsStringAsync().Result,
                                     Headers = ExtractHeaders(response.Headers)
                                 });
                return;
            }


            if (!Logger.IsWarnEnabled) return;

            Logger.Warn(new
                            {
                                Id = id,
                                Timestamp = SystemTime.UtcNow,
                                response.StatusCode,
                                Content = response.Content == null
                                              ? ""
                                              : response.Content.ReadAsStringAsync().Result,
                                Headers = ExtractHeaders(response.Headers)
                            });
        }

        protected string ExtractHeaders(HttpHeaders headers)
        {
            var list = headers.ToList()
                .Where(x => x.Key != "Cookie")
                .Select(x => string.Format("{0}:[{1}]", x.Key, string.Join(";", x.Value))).ToArray();

            return string.Join(";", list);
        }
    }
}