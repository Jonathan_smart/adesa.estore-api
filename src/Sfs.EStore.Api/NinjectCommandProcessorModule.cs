﻿using Ninject.Modules;
using Ninject.Syntax;
using Serilog;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Api.ThirdParties;

namespace Sfs.EStore.Api
{
    public class NinjectCommandProcessorModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ICommandProcessor>().ToMethod(ctx => new NinjectCommandProcessor(() => Kernel));
            Kernel.Bind<ILog>().ToMethod(ctx => new SerilogLogger(Log.Logger.ForContext(ctx.Request.Target.Type)));
        }
    }

    internal static class NavisionInstanceNinjectExtensions
    {
        public static NavisionInstnaceBindingAssertions<T> WhenNavisionInstance<T>(this IBindingWhenInNamedWithOrOnSyntax<T> @this)
        {
            return new NavisionInstnaceBindingAssertions<T>(@this.WhenTenantSetting("Sfs/Navision/InstanceName"));
        }

        public class NavisionInstnaceBindingAssertions<T>
        {
            private readonly KernalBindingExtensions.TenantSettingBindingAssertions<T> _binding;

            public NavisionInstnaceBindingAssertions(KernalBindingExtensions.TenantSettingBindingAssertions<T> binding)
            {
                _binding = binding;
            }

            public IBindingInNamedWithOrOnSyntax<T> IsGm()
            {
                return _binding.HasValue("GM");
            }

            public IBindingInNamedWithOrOnSyntax<T> IsSfs()
            {
                return _binding.HasValue("SFS");
            }
        }
    }
}