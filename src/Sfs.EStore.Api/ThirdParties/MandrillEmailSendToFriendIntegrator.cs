using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Mandrill;
using Raven.Client;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Models;
using Slugify;

namespace Sfs.EStore.Api.ThirdParties
{
    public class MandrillEmailSendToFriendIntegrator : IIntegrateSendToFriendRequests
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _recipientTemplateName;

        public MandrillEmailSendToFriendIntegrator(MandrillApi mandrillApi, string recipientTemplateName)
        {
            if (mandrillApi == null) throw new ArgumentNullException("mandrillApi");
            if (recipientTemplateName == null) throw new ArgumentNullException("recipientTemplateName");

            _mandrillApi = mandrillApi;
            _recipientTemplateName = recipientTemplateName;
        }

        public bool Supported { get { return true; } }

        public IAsyncDocumentSession Session { get; set; }

        public void SendToFriend(MembershipUserIdentity identity, SendToFriendController.SendToFriendPostModel args, ListingVehicle listingVehicle)
        {
            var recipientEmail = SendEmail(identity, listingVehicle, args,
                new EmailAddress { email = args.RecipientEmail, name = args.RecipientFullName }, _recipientTemplateName);

            recipientEmail.Wait();
        }

        private Task<EmailResult> SendEmail(MembershipUserIdentity user, ListingVehicle listingVehicle, SendToFriendController.SendToFriendPostModel args,
            EmailAddress toEmail, string templateName)
        {
            var message = new EmailMessage { to = new []{ toEmail } };

            message.AddGlobalVariable("USERNAME", user == null ? "" : (user.Name ?? ""));
            message.AddGlobalVariable("SENDER_FULL_NAME", args.SenderFullName ?? "");
            message.AddGlobalVariable("SENDER_EMAIL", args.SenderEmail ?? "");
            message.AddGlobalVariable("SENDER_MESSAGE", args.Message ?? "");

            message.AddGlobalVariable("RECIPIENT_FULL_NAME", args.RecipientFullName ?? "");
            message.AddGlobalVariable("RECIPIENT_EMAIL", args.RecipientEmail ?? "");

            message.AddGlobalVariable("LISTING_ID", listingVehicle.Listing.LotNumber.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("LISTING_SLUG", new SlugHelper().GenerateSlug(listingVehicle.Listing.Title));
            message.AddGlobalVariable("LISTING_TITLE", listingVehicle.Listing.Title);
            message.AddGlobalVariable("VEHICLE_IMAGE_URL", listingVehicle.Vehicle.Images.Any()
                ? listingVehicle.Vehicle.Images[0].ToUrl(width: 320, height: 240)
                : "");
            message.AddGlobalVariable("VEHICLE_REGISTRATION", listingVehicle.Vehicle.Registration.Plate);
            message.AddGlobalVariable("VEHICLE_MAKE", listingVehicle.Vehicle.Make);
            message.AddGlobalVariable("VEHICLE_MODEL", listingVehicle.Vehicle.Model);
            message.AddGlobalVariable("VEHICLE_DERIVATIVE", listingVehicle.Vehicle.Derivative);

            var currentPrice = listingVehicle.Listing.CurrentPrice();
            message.AddGlobalVariable("LISTING_CURRENT_PRICE", currentPrice != null ? currentPrice.ToCurrencyString() : "");
            message.AddGlobalVariable("LISTING_CURRENT_PRICE_AMOUNT", currentPrice != null ? currentPrice.Amount.ToString(CultureInfo.InvariantCulture) : "");
            message.AddGlobalVariable("LISTING_CURRENT_PRICE_CURRENCY", currentPrice != null ? currentPrice.Currency.ToString() : "");

            return _mandrillApi.SendMessageAsync(message, templateName, new TemplateContent[0])
                .ContinueWith(x => x.Result.FirstOrDefault());
        }
    }
}