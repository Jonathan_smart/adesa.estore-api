using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.ThirdParties
{
    public interface IInitiateForgottenPasswordRetrievalCommand : ICommand
    {
        MembershipUser User { get; set; }
    }

    public class ResetPasswordCommandResult
    {
        public ResetPasswordCommandResult(bool success, string username = null)
        {
            Success = success;
            Username = username;
        }

        public bool Success { get; private set; }
        public string Username { get; private set; }
    }
}