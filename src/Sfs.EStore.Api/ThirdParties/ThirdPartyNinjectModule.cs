﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Ninject.Modules;
using Sfs.EStore.Api.Authentication;

namespace Sfs.EStore.Api.ThirdParties
{
    public abstract class ThirdPartyNinjectModule : NinjectModule
    {
        protected bool IntegrationEnabled(Type type)
        {
            if (type == null) throw new ArgumentNullException("type");

            var settingName = string.Format("{0}/Enabled", type.FullName);

            var actualValue = Setting(settingName);
            return Boolean.TrueString.Equals(actualValue, StringComparison.CurrentCultureIgnoreCase);
        }

        protected bool IntegratesWith(string settingName, string expectedValue)
        {
            if (settingName == null) throw new ArgumentNullException("settingName");

            var actualValue = Setting(settingName);
            return actualValue == expectedValue;
        }

        protected bool AnyIntegratesWith(string settingName, string expectedValue)
        {
            if (settingName == null) throw new ArgumentNullException("settingName");

            var actualValue = Setting(settingName) ?? "";
            var values = actualValue.Split(new []{','}, StringSplitOptions.RemoveEmptyEntries);
            return values.Any(x => x.Trim().Equals(expectedValue, StringComparison.InvariantCultureIgnoreCase));
        }

        protected string Setting<T>(string name)
        {
            var fullName = string.Format("{0}/{1}", typeof(T).FullName, name);

            return Setting(fullName);
        }

        protected string Setting(string fullName)
        {
            var settings = CurrentIntegrationSettings();

            if (settings == null) return null;

            string value;
            return settings.TryGetValue(fullName, out value) ? value : null;
        }

        private IDictionary<string, string> CurrentIntegrationSettings()
        {
            var principal = (Thread.CurrentPrincipal as IApiApplicationPrincipal);
            return principal == null
                       ? null
                       : principal.Identity.Advanced.IntegrationSettings;
        }
    }
}