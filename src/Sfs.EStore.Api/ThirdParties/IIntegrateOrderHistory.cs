using System;
using System.Collections.Generic;
using Raven.Client;
using Sfs.EStore.Api.Authentication;

namespace Sfs.EStore.Api.ThirdParties
{
    public interface IIntegrateOrderHistory
    {
        bool Supported { get; }
        IAsyncDocumentSession Session { get; set; }

        IEnumerable<OrderHistoryItem> Get(MembershipUserIdentity user, DateTime fromDate, DateTime toDate);
    }

    public class NullOrderHistoryIntegration : IIntegrateOrderHistory
    {
        public bool Supported
        {
            get { return false; }
        }

        public IAsyncDocumentSession Session { get; set; }

        public IEnumerable<OrderHistoryItem> Get(MembershipUserIdentity user, DateTime fromDate, DateTime toDate)
        {
            throw new InvalidOperationException("Order history is not supported");
        }
    }

    public class OrderHistoryItem
    {
        public DateTime OrderDate { get; set; }
        public string SalesAdviceNumber { get; set; }

        private OrderHistoryVehicle[] _vehicles = new OrderHistoryVehicle[0];
        public OrderHistoryVehicle[] Vehicles
        {
            get { return _vehicles; }
            set { _vehicles = value; }
        }
    }

    public class OrderHistoryVehicle
    {
        public string RegistrationPlate { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string Description { get; set; }
        public decimal TotalPrice { get; set; }
    }
}