﻿using System;
using System.Threading.Tasks;
using Raven.Json.Linq;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.ThirdParties
{
    public interface IVehicleEnricher
    {
        bool ShouldEnrich(Vehicle vehicle, VehicleData vehicleData, RavenJObject metadata);
        
        /// <exception cref="VehicleEnrichmentException"></exception>
        Task Enrich(Vehicle vehicle, VehicleData vehicleData, RavenJObject metadata);
    }

    public interface IGetVehicleEnricher : IVehicleEnricher
    {

    }

    public class NullGetVehicleEnricher : IGetVehicleEnricher
    {
        private static readonly Task Task = Task.Factory.StartNew(() => { });

        public bool ShouldEnrich(Vehicle vehicle, VehicleData vehicleData, RavenJObject metadata)
        {
            return false;
        }

        public Task Enrich(Vehicle vehicle, VehicleData vehicleData, RavenJObject metadata)
        {
            return Task;
        }
    }

    public interface IPutVehicleEnricher : IVehicleEnricher
    {

    }

    public class NullPutVehicleEnricher : IPutVehicleEnricher
    {
        private static readonly Task Task = Task.Factory.StartNew(() => { });

        public bool ShouldEnrich(Vehicle vehicle, VehicleData vehicleData, RavenJObject metadata)
        {
            return false;
        }

        public Task Enrich(Vehicle vehicle, VehicleData vehicleData, RavenJObject metadata)
        {
            return Task;
        }
    }

    [Serializable]
    public class VehicleEnrichmentException : Exception
    {
        public string VehicleId { get; protected set; }
        public Type Enricher { get; protected set; }

        public VehicleEnrichmentException(string vehicleId, Type enricher)
        {
            VehicleId = vehicleId;
            Enricher = enricher;
        }

        public VehicleEnrichmentException(string vehicleId, Type enricher, string message)
            : base(message)
        {
            VehicleId = vehicleId;
            Enricher = enricher;
        }

        public VehicleEnrichmentException(string vehicleId, Type enricher, string message, Exception inner)
            : base(message, inner)
        {
            VehicleId = vehicleId;
            Enricher = enricher;
        }
    }
}