using System;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.ThirdParties
{
    public class ForgottenPasswordTokenResetPasswordCommand : IResetPasswordCommand
    {
        public IAsyncDocumentSession Session { get; set; }

        public void Execute()
        {
            if (Args == null)
                throw new InvalidOperationException("Args cannot be null");

            var forgottenPasswordToken = Session.Query<ForgottenPasswordToken>()
                .Customize(x => x.Include<ForgottenPasswordToken>(t => t.UserId))
                .Where(x => x.Token == Args.Token)
                .FirstOrDefaultAsync()
                .Result;

            if (forgottenPasswordToken == null)
            {
                Result = new ResetPasswordCommandResult(false);
                return;
            }

            var user = Session.LoadAsync<MembershipUser>(forgottenPasswordToken.UserId).Result;

            if (!user.IsLockedOut)
            {
                user.SetPassword(Args.NewPassword);

                Session.Delete(forgottenPasswordToken);

                Result = new ResetPasswordCommandResult(true, user.Username);
            }
            else
            {
                Session.Delete(forgottenPasswordToken);
                Result = new ResetPasswordCommandResult(false, user.Username);
            }
            
        }

        public ResetPasswordCommandResult Result { get; private set; }

        public ResetPasswordPostModel Args { get; set; }
    }

    public class ForgottenPasswordTokenValidateTokenCommand : IValidateResetPasswordTokenCommand
    {
        public IAsyncDocumentSession Session { get; set; }

        public bool Result { get; private set; }

        public string Token { get; set; }

        public void Execute()
        {
            var forgottenPasswordToken = Session.Query<ForgottenPasswordToken>()
                .Customize(x => x.Include<ForgottenPasswordToken>(t => t.UserId))
                .Where(x => x.Token == Token)
                .FirstOrDefaultAsync()
                .Result;

            Result = forgottenPasswordToken != null;
        }
    }
}