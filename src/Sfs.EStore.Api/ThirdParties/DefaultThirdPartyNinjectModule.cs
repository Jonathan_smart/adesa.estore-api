﻿using System;
using System.Configuration;
using Ninject.Web.Common;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Services;

namespace Sfs.EStore.Api.ThirdParties
{
    public class DefaultThirdPartyNinjectModule : ThirdPartyNinjectModule
    {
        public override void Load()
        {
            BindVehicleEnricherIntegration();
            BindServices();
            BindCallbackIntegration();
            BindRequestATestDriveImplementation();
            BindMakeAnOfferImplementation();
            BindSendToFriendImplementation();
            BindBookAServiceIntegration();
            BindOrderHistoryIntegration();
            BindForgottenPasswordRetrieval();
        }

        private void BindVehicleEnricherIntegration()
        {
            Bind<IGetVehicleEnricher>().To<CarwebVehicleEnricher>()
                .When(req => AnyIntegratesWith("VehicleEnricherImplementation",
                                            "Sfs.EStore.Api.ThirdParties.CarwebVehicleEnricher"));

            Bind<string>().ToMethod(ctx => ConfigurationManager.AppSettings["Cap/ProxyServiceApiBaseUrl"])
                .When(ctx => ctx.Target.Name == "capProxyServiceApiBaseUrl");
            Bind<Uri>().ToMethod(ctx => new Uri(ConfigurationManager.AppSettings["Cap/ProxyServiceApiBaseUrl"]))
                .When(ctx => ctx.Target.Name == "capProxyServiceApiBaseUrl");

            Bind<CapValuation>().ToSelf().InRequestScope();

            Bind<IGetVehicleEnricher>().To<NullGetVehicleEnricher>();
            Bind<IGetVehicleEnricher>().To<CapPricingVehicleEnricher>()
                .When(req => AnyIntegratesWith("VehicleEnricherImplementation",
                                               "Sfs.EStore.Api.ThirdParties.CapPricingVehicleEnricher"))
                .InRequestScope();

            Bind<IPutVehicleEnricher>().To<NullPutVehicleEnricher>();
            Bind<IPutVehicleEnricher>().To<CapPricingVehicleEnricher>()
                .When(req => AnyIntegratesWith("Sfs.EStore.Api.ThirdParties.IPutVehicleEnricher",
                                               "Sfs.EStore.Api.ThirdParties.CapPricingVehicleEnricher"))
                .InRequestScope();
        }

        private void BindServices()
        {
            Bind<ICarwebService>().To<CarwebProxyService>();
        }

        private void BindCallbackIntegration()
        {
            Bind<IIntegrateCallbacks>().To<NullCallbackIntegration>();

            Bind<IIntegrateCallbacks>().To<MandrillEmailCallbacksIntegrator>()
                .When(request => IntegratesWith("CallbacksImplementation",
                                                "Sfs.EStore.Api.ThirdParties.MandrillEmailCallbacksIntegrator"))
                .InRequestScope()
                .WithConstructorArgument("toEmailAddresses",
                                         ctx => Setting("CallbacksImplementation/Config/ToEmailAddresses"))
                .WithConstructorArgument("templateName",
                                         ctx => Setting("CallbacksImplementation/Config/TemplateName"));
        }

        private void BindBookAServiceIntegration()
        {
            Bind<IIntegrateBookingServices>().To<NullBookAServiceIntegration>();

            Bind<IIntegrateBookingServices>().To<MandrillEmailBookAServiceIntegrator>()
                .When(request => IntegratesWith("BookAServiceImplementation",
                                                "Sfs.EStore.Api.ThirdParties.MandrillEmailBookAServiceIntegrator"))
                .InRequestScope()
                .WithConstructorArgument("toEmailAddresses",
                                         ctx => Setting("BookAServiceImplementation/Config/ToEmailAddresses"))
                .WithConstructorArgument("templateName",
                                         ctx => Setting("BookAServiceImplementation/Config/TemplateName"));
        }

        private void BindRequestATestDriveImplementation()
        {
            Bind<IHandleRequests<RequestATestDrive>>().To<NullRequestATestDriveHandler>();

            Bind<IHandleRequests<RequestATestDrive>>().To<MandrillEmailRequestATestDriveHandler>()
                .When(request => IntegratesWith("RequestATestDriveImplementation",
                                                "Sfs.EStore.Api.ThirdParties.MandrillEmailRequestATestDriveIntegrator"))
                .InRequestScope()
                .WithConstructorArgument("toEmailAddresses",
                                         ctx => Setting("RequestATestDriveImplementation/Config/ToEmailAddresses"))
                .WithConstructorArgument("templateName",
                                         ctx => Setting("RequestATestDriveImplementation/Config/TemplateName"));
        }

        private void BindMakeAnOfferImplementation()
        {
            Bind<IHandleRequests<MakeAnOffer>>().To<NullMakeAnOfferHandler>();
            Bind<IHandleRequests<MakeAnOffer>>().To<MandrillEmailMakeAnOfferHandler>()
                .When(request => IntegratesWith("MakeAnOfferImplementation",
                    "Sfs.EStore.Api.ThirdParties.MandrillEmailMakeAnOfferIntegrator"))
                .InRequestScope()
                .WithConstructorArgument("toEmailAddresses",
                    ctx => Setting("MakeAnOfferImplementation/Config/ToEmailAddresses"))
                .WithConstructorArgument("templateName",
                    ctx => Setting("MakeAnOfferImplementation/Config/TemplateName"));
        }

        private void BindSendToFriendImplementation()
        {
            Bind<IIntegrateSendToFriendRequests>().To<NullSendToFriendRequestsIntegration>();

            Bind<IIntegrateSendToFriendRequests>().To<MandrillEmailSendToFriendIntegrator>()
                .When(request => IntegratesWith("SendToFriendImplementation",
                                                "Sfs.EStore.Api.ThirdParties.MandrillEmailSendToFriendIntegrator"))
                .InRequestScope()
                .WithConstructorArgument("recipientTemplateName",
                                         ctx => Setting("SendToFriendImplementation/Config/RecipientTemplateName"));
        }

        private void BindOrderHistoryIntegration()
        {
            Bind<IIntegrateOrderHistory>().To<NullOrderHistoryIntegration>();
        }

        private void BindForgottenPasswordRetrieval()
        {
            Bind<IInitiateForgottenPasswordRetrievalCommand>()
                .To<MandrillInitiateForgottenPasswordRetrievalCommand>()
                .When(request => IntegratesWith("IInitiateForgottenPasswordRetrievalCommand",
                                                "Sfs.EStore.Api.ThirdParties.MandrillInitiateForgottenPasswordRetrievalCommand"))
                .InRequestScope()
                .WithConstructorArgument("templateName",
                                         ctx => Setting("IInitiateForgottenPasswordRetrievalCommand/Config/TemplateName"));

            Bind<IResetPasswordCommand>()
                .To<ForgottenPasswordTokenResetPasswordCommand>()
                .When(request => IntegratesWith("IResetPasswordCommand",
                                                "Sfs.EStore.Api.ThirdParties.ForgottenPasswordTokenResetPasswordCommand"));

            Bind<IValidateResetPasswordTokenCommand>()
                .To<ForgottenPasswordTokenValidateTokenCommand>()
                .When(request => IntegratesWith("IValidateResetPasswordTokenCommand",
                                                "Sfs.EStore.Api.ThirdParties.ForgottenPasswordTokenValidateTokenCommand"));
        }
    }
}