using System;
using System.Globalization;
using System.Linq;
using Mandrill;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;
using Slugify;

namespace Sfs.EStore.Api.ThirdParties
{
    public class RequestATestDrive : Command
    {
        public RequestATestDrive(Guid id, UserInfo user, ListingInfo listing, string preferredDate) : base(id)
        {
            User = user;
            Listing = listing;
            PreferredDate = preferredDate;
        }

        public UserInfo User { get; private set; }
        public ListingInfo Listing { get; private set; }

        public string PreferredDate { get; private set; }

        public TenantEnrichmentInfo TenantEnrichment { get; set; }

        public class UserInfo
        {
            public string Username { get; set; }

            public string FullName { get; set; }
            public string AddressLine1 { get; set; }
            public string PostCode { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
        }

        public class ListingInfo
        {
            public int ListingId { get; set; }
            public string Slug { get; set; }

            public string Title { get; set; }
            public string VRM { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string Derivative { get; set; }
            public Money CurrentPrice { get; set; }
        }

        public class TenantEnrichmentInfo
        {
            public string IpAddress { get; set; }

            public string OriginalVisitType { get; set; }
            public string OriginalVisitSource { get; set; }
            public DateTime? OriginalVisitAt { get; set; }
            public string LatestVisitType { get; set; }
            public string LatestVisitSource { get; set; }
            public DateTime? LatestVisitAt { get; set; }
        }
    }

    public class NullRequestATestDriveHandler : RequestHandler<RequestATestDrive>
    {
        public NullRequestATestDriveHandler(ILog logger) : base(logger)
        {
        }
    }

    public class MandrillEmailRequestATestDriveHandler : RequestHandler<RequestATestDrive>
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;
        private readonly EmailAddress[] _toEmailAddresses;

        public MandrillEmailRequestATestDriveHandler(MandrillApi mandrillApi, string toEmailAddresses, string templateName,
            ILog logger) : base(logger)
        {
            if (mandrillApi == null) throw new ArgumentNullException("mandrillApi");
            if (string.IsNullOrEmpty(toEmailAddresses)) throw new ArgumentNullException("toEmailAddresses");
            if (string.IsNullOrEmpty(templateName)) throw new ArgumentNullException("templateName");

            _mandrillApi = mandrillApi;
            _templateName = templateName;

            _toEmailAddresses = toEmailAddresses.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => new EmailAddress { email = x })
                .ToArray();
        }

        public override RequestATestDrive Handle(RequestATestDrive command)
        {
            var message = new EmailMessage { to = _toEmailAddresses };

            message.AddGlobalVariable("USERNAME", command.User.Username ?? "");
            message.AddGlobalVariable("FULL_NAME", command.User.FullName ?? "");
            message.AddGlobalVariable("ADDRESS_LINE1", command.User.AddressLine1 ?? "");
            message.AddGlobalVariable("POSTCODE", command.User.PostCode ?? "");
            message.AddGlobalVariable("PHONE", command.User.Phone ?? "");
            message.AddGlobalVariable("EMAIL", command.User.Email ?? "");

            message.AddGlobalVariable("PREFERRED_DATE", command.PreferredDate ?? "");

            message.AddGlobalVariable("LISTING_ID", command.Listing.ListingId.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("LISTING_SLUG", new SlugHelper().GenerateSlug(command.Listing.Title));
            message.AddGlobalVariable("LISTING_TITLE", command.Listing.Title);
            message.AddGlobalVariable("VEHICLE_REGISTRATION", command.Listing.VRM);
            message.AddGlobalVariable("VEHICLE_MAKE", command.Listing.Make);
            message.AddGlobalVariable("VEHICLE_MODEL", command.Listing.Model);
            message.AddGlobalVariable("VEHICLE_DERIVATIVE", command.Listing.Derivative);
            var currentPrice = command.Listing.CurrentPrice;
            message.AddGlobalVariable("LISTING_CURRENT_PRICE", currentPrice != null ? currentPrice.ToCurrencyString() : "");
            message.AddGlobalVariable("LISTING_CURRENT_PRICE_AMOUNT", currentPrice != null ? currentPrice.Amount.ToString(CultureInfo.InvariantCulture) : "");
            message.AddGlobalVariable("LISTING_CURRENT_PRICE_CURRENCY", currentPrice != null ? currentPrice.Currency.ToString() : "");

            if (command.TenantEnrichment != null)
            {
                message.AddGlobalVariable("ENRICHMENT_ORIGINAL_VISIT_SOURCE", command.TenantEnrichment.OriginalVisitSource);
                message.AddGlobalVariable("ENRICHMENT_ORIGINAL_VISIT_TYPE", command.TenantEnrichment.OriginalVisitType);
                message.AddGlobalVariable("ENRICHMENT_ORIGINAL_VISIT_AT", command.TenantEnrichment.OriginalVisitAt.ToString());
                message.AddGlobalVariable("ENRICHMENT_LATEST_VISIT_SOURCE", command.TenantEnrichment.LatestVisitSource);
                message.AddGlobalVariable("ENRICHMENT_LATEST_VISIT_TYPE", command.TenantEnrichment.LatestVisitType);
                message.AddGlobalVariable("ENRICHMENT_LATEST_VISIT_AT", command.TenantEnrichment.LatestVisitAt.ToString());
                message.AddGlobalVariable("ENRICHMENT_IP_ADDRESS", command.TenantEnrichment.IpAddress ?? "");
            }

            _mandrillApi.SendMessageAsync(message, _templateName, new TemplateContent[0])
                .Wait();

            return command;
        }
    }
}