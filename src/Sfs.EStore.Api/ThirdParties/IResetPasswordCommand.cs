using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.ThirdParties
{
    public interface IResetPasswordCommand : ICommand<ResetPasswordCommandResult>
    {
        ResetPasswordPostModel Args { set; }
    }

    public interface IValidateResetPasswordTokenCommand : ICommand<bool>
    {
        string Token { set; }
    }
}