﻿using Ninject.Extensions.Conventions;
using Ninject.Web.Common;
using Sfs.EStore.Api.DomainEvents;
using Sfs.EStore.Api.DomainEvents.Handlers;
using Sfs.EStore.Api.EStoreActionsService;

namespace Sfs.EStore.Api.ThirdParties
{
    public class DomainEventHandlerNinjectModule : ThirdPartyNinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(x => x.FromThisAssembly()
                                 .Select(t => t == typeof(UpdateNavisionWithStatusChange))
                                 .BindAllInterfaces()
                                 .Configure(
                                     cfg => cfg
                                                .When(request =>
                                                      AnyIntegratesWith("Sfs.EStore.Api.DomainEvents.Handlers",
                                                                        "Sfs.EStore.Api.DomainEvents.Handlers.UpdateNavisionWithStatusChange"))
                                                .InRequestScope())
                );

            Kernel.Bind<IHandles<ReservationCreated>>().To<SendEmailOnReservationCreated>()
                .When(x => AnyIntegratesWith("Sfs.EStore.Api.DomainEvents.Handlers",
                                             "Sfs.EStore.Api.DomainEvents.Handlers.SendEmailOnReservationCreated"))
                .InRequestScope()
                .WithConstructorArgument("toEmailAddresses",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.SendEmailOnReservationCreated/Config/ToEmailAddresses"))
                .WithConstructorArgument("templateName",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.SendEmailOnReservationCreated/Config/TemplateName"));

            Kernel.Bind<IHandles<SalesOrderCreated>>().To<SendEmailOnSalesOrderCreated>()
                .When(x => AnyIntegratesWith("Sfs.EStore.Api.DomainEvents.Handlers",
                                             "Sfs.EStore.Api.DomainEvents.Handlers.SendEmailOnSalesOrderCreated"))
                .InRequestScope()
                .WithConstructorArgument("toEmailAddresses",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.SendEmailOnSalesOrderCreated/Config/ToEmailAddresses"))
                .WithConstructorArgument("templateName",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.SendEmailOnSalesOrderCreated/Config/TemplateName"));

            Kernel.Bind<IHandles<CallbackRequestSent>>()
                .To<EmailCallbackEnquiryConfirmation>()
                .When(request =>
                      IntegrationEnabled(typeof(EmailCallbackEnquiryConfirmation)))
                .InRequestScope()
                .WithConstructorArgument("templateName",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.EmailCallbackEnquiryConfirmation/Config/TemplateName"));

            Kernel.Bind<IHandles<TestDriveRequestReceived>>()
                .To<EmailTestDriveRequestReceivedConfirmation>()
                .When(request =>
                      IntegrationEnabled(typeof(EmailTestDriveRequestReceivedConfirmation)))
                .InRequestScope()
                .WithConstructorArgument("templateName",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.EmailTestDriveRequestReceivedConfirmation/Config/TemplateName"));

            Kernel.Bind<IHandles<MakeAnOfferReceived>>()
                .To<EmailMakeAnOfferConfirmation>()
                .When(request =>
                      IntegrationEnabled(typeof(EmailMakeAnOfferConfirmation)))
                .InRequestScope()
                .WithConstructorArgument("templateName",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.EmailMakeAnOfferConfirmation/Config/TemplateName"));

            Kernel.Bind<IHandles<ServiceBookingRequestReceived>>()
                .To<EmailServiceBookingRequestReceivedConfirmation>()
                .When(request =>
                      IntegrationEnabled(typeof(EmailServiceBookingRequestReceivedConfirmation)))
                .InRequestScope()
                .WithConstructorArgument("templateName",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.EmailServiceBookingRequestReceivedConfirmation/Config/TemplateName"));

            Kernel.Bind<IHandles<ReservationAttemptFailed>>()
                .To<EmailOnFailedReservationAttempt>()
                .When(request =>
                      AnyIntegratesWith("Sfs.EStore.Api.DomainEvents.Handlers",
                                        "Sfs.EStore.Api.DomainEvents.Handlers.EmailOnFailedReservationAttempt"))
                .InRequestScope()
                .WithConstructorArgument("templateName",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.EmailOnFailedReservationAttempt/Config/TemplateName"))
                .WithConstructorArgument("toEmailAddress",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.EmailOnFailedReservationAttempt/Config/ToEmailAddress"));

            Kernel.Bind<IHandles<AuctionBidHasBeenTaken>>().To<PublishNewBidHasBeenAcceptedMessage>();
            Kernel.Bind<IHandles<LotHasEndedWithSale>, IHandles<LotHasEndedWithoutSale>>().To<PublishAuctionLotEndedEvents>();
                //.InRequestScope()
                //.WithConstructorArgument("salesChannel",
                //    ctx => Setting("Sfs/SalesChannel"));

            Kernel.Bind<IHandles<PartExchangeValuationCreated>>()
                .To<EmailSellerPartExchangeValuationRequested>()
                .When(request =>
                      IntegrationEnabled(typeof(EmailSellerPartExchangeValuationRequested)))
                .InRequestScope()
                .WithConstructorArgument("templateName",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.EmailSellerPartExchangeValuationRequested/Config/TemplateName"))
                .WithConstructorArgument("toEmailAddress",
                                         ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.EmailSellerPartExchangeValuationRequested/Config/ToEmailAddress"));

            Kernel.Bind<IHandles<PartExchangeValuationCreated>>()
                 .To<EmailUserPartExchangeValuationConfirmation>()
                 .When(request =>
                       IntegrationEnabled(typeof(EmailUserPartExchangeValuationConfirmation)))
                 .InRequestScope()
                 .WithConstructorArgument("templateName",
                                          ctx => Setting("Sfs.EStore.Api.DomainEvents.Handlers.EmailUserPartExchangeValuationConfirmation/Config/TemplateName"));

            Kernel.Bind<eStore_Actions_Service>()
                .ToMethod(ctx => new eStore_Actions_Service { UseDefaultCredentials = true })
                .InTransientScope();

            Kernel.Bind<IHandles<UserAuthenticated>>().To<RefreshAuthenticatedUserFranchises>()
                .WhenTenantIntegrationEnabled();

            Kernel.Bind<IHandles<UserApproved>>().To<EmailUserWhenAccountApproved>()
                .WhenTenantIntegrationEnabled()
                .InRequestScope()
                .WithConstructorArgumentFromTenantSetting("templateName");

            Kernel.Bind<IHandles<AddVehicleInterestBid>>().To<VehicleInterestUpdate>();

            Kernel.Bind<IHandles<AddVehicleInterestBuyItNow>>().To<VehicleInterestUpdate>();
        }
    }
}