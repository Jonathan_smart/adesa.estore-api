using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Raven.Json.Linq;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.ThirdParties
{
    public class CompositeVehicleEnricher
    {
        private readonly IVehicleEnricher[] _enrichers;

        public CompositeVehicleEnricher(params IVehicleEnricher[] enrichers)
        {
            _enrichers = enrichers ?? new IVehicleEnricher[0];
        }

        /// <exception cref="VehicleEnrichmentException"></exception>
        public void Enrich(Vehicle vehicle, VehicleData vehicleData, RavenJObject metadata)
        {
            var tasks = new List<Task>();

            _enrichers.ToList()
                .ForEach(enricher =>
                             {
                                 if (enricher.ShouldEnrich(vehicle, vehicleData, metadata))
                                     tasks.Add(enricher.Enrich(vehicle, vehicleData, metadata));
                             });

            Task.WaitAll(tasks.ToArray());
        }
    }
}