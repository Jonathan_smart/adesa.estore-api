using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Mandrill;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.ThirdParties
{
    public class MandrillEmailCallbacksIntegrator : IIntegrateCallbacks
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;
        private readonly EmailAddress[] _toEmailAddresses;

        public MandrillEmailCallbacksIntegrator(MandrillApi mandrillApi, string toEmailAddresses, string templateName)
        {
            if (mandrillApi == null) throw new ArgumentNullException("mandrillApi");
            if (string.IsNullOrEmpty(toEmailAddresses)) throw new ArgumentNullException("toEmailAddresses");
            if (string.IsNullOrEmpty(templateName)) throw new ArgumentNullException("templateName");
            
            _mandrillApi = mandrillApi;
            _templateName = templateName;

            _toEmailAddresses = toEmailAddresses.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => new EmailAddress {email = x})
                .ToArray();
        }

        public bool Supported { get { return true; } }

        public IAsyncDocumentSession Session { get; set; }

        public bool Callback(MembershipUserIdentity user, CallbackRequestContact contact, CallbackRequestVisitInfo visitInfo,
            string message, int[] lotIds)
        {
            if (message == null) throw new ArgumentNullException("message");

            var lots = new Load[0];

            if (lotIds != null && lotIds.Length > 0)
                lots = Session
                    .Include<Lot>(x => x.VehicleId)
                    .LoadAsync<Lot>(lotIds.Select(Lot.IdFromLotNumber)).Result
                    .ToList()
                    .Select(x =>
                            new Load
                                {
                                    Lot = x,
                                    Vehicle = Session.LoadAsync<Vehicle>(x.VehicleId).Result
                                })
                    .ToArray();

            if (lots.Length == 0)
                lots = new Load[] {null};

            var results = new List<EmailResult>();

            Task.WaitAll(lots.Select(load => SendEmail(user, load, contact, visitInfo, message)
                                                 .ContinueWith(r => results.Add(r.Result)))
                             .ToArray());

            return results.All(x => x.Status.In(EmailResultStatus.Sent, EmailResultStatus.Queued));
        }

        private class Load
        {
            public Lot Lot { get; set; }
            public Vehicle Vehicle { get; set; }
        }

        private Task<EmailResult> SendEmail(MembershipUserIdentity user, Load load, CallbackRequestContact contact,
            CallbackRequestVisitInfo visitInfo, string callbackMessage)
        {
            var message = new EmailMessage { to = _toEmailAddresses };

            message.AddGlobalVariable("username", user == null ? "" : (user.Name ?? ""));
            message.AddGlobalVariable("first_name", user == null ? "" : contact.FirstName ?? "");
            message.AddGlobalVariable("last_name", user == null ? "" : contact.LastName ?? "");
            message.AddGlobalVariable("phone", contact.TelephoneNumber ?? "");
            message.AddGlobalVariable("email", contact.Email ?? "");

            message.AddGlobalVariable("callback_message", callbackMessage);

            message.AddGlobalVariable("lot_number", SafeLotString(load, x => x.LotNumber.ToString(CultureInfo.InvariantCulture)));
            message.AddGlobalVariable("lot_title",  SafeLotString(load, x => x.Title));
            message.AddGlobalVariable("vehicle_registration", SafeVehicleString(load, x => x.Registration.Plate));
            message.AddGlobalVariable("vehicle_make", SafeVehicleString(load, x => x.Make));
            message.AddGlobalVariable("vehicle_model", SafeVehicleString(load, x => x.Model));
            message.AddGlobalVariable("current_price",
                                      SafeLotString(load, x => x.CurrentPrice().ToString()));

            message.AddGlobalVariable("current_price_amount", SafeLotString(load, x => x.CurrentPrice().Amount.ToString(CultureInfo.InvariantCulture)));
            message.AddGlobalVariable("current_price_currency", SafeLotString(load, x => x.CurrentPrice().Currency.ToString()));
            message.AddGlobalVariable("originalvisit_source_type", visitInfo.OriginalVisit.Source.Type);
            message.AddGlobalVariable("originalvisit_source_value", visitInfo.OriginalVisit.Source.Value);
            message.AddGlobalVariable("originalvisit_at",
                visitInfo.OriginalVisit.At == null
                    ? ""
                    : visitInfo.OriginalVisit.At.Value.ToLocalTime().ToString()
                );
            message.AddGlobalVariable("lastvisit_source_type", visitInfo.LastVisit.Source.Type);
            message.AddGlobalVariable("lastvisit_source_value", visitInfo.LastVisit.Source.Value);
            message.AddGlobalVariable("lastvisit_at",
                visitInfo.LastVisit.At == null
                    ? ""
                    : visitInfo.LastVisit.At.Value.ToLocalTime().ToString()
                );

            return _mandrillApi.SendMessageAsync(message, _templateName, new TemplateContent[0])
                .ContinueWith(x => x.Result.FirstOrDefault());
        }

        private string SafeLotString(Load load, Func<Lot, string> value)
        {
            if (load == null || load.Lot == null) return "";

            return value(load.Lot);
        }

        private string SafeVehicleString(Load load, Func<Vehicle, string> value)
        {
            if (load == null || load.Vehicle == null) return "";

            return value(load.Vehicle);
        }
    }
}