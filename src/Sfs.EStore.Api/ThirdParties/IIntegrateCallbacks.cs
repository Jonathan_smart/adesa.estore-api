using System;
using Raven.Client;
using Sfs.EStore.Api.Authentication;

namespace Sfs.EStore.Api.ThirdParties
{
    public interface IIntegrateCallbacks
    {
        bool Supported { get; }
        IAsyncDocumentSession Session { get; set; }

        bool Callback(MembershipUserIdentity user, CallbackRequestContact contact, CallbackRequestVisitInfo visitInfo,
            string message, int[] lotIds);
    }

    public class CallbackRequestContact
    {
        public string TelephoneNumber { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class CallbackRequestVisitInfo
    {
        public Visit OriginalVisit { get; set; }
        public Visit LastVisit { get; set; }

        public class Visit
        {
            public VisitSource Source { get; set; }
            public DateTime? At { get; set; }
        }

        public class VisitSource
        {
            public string Type { get; set; }
            public string Value { get; set; }
        }
    }

    public class NullCallbackIntegration : IIntegrateCallbacks
    {
        public bool Supported
        {
            get { return false; }
        }

        public IAsyncDocumentSession Session { get; set; }

        public bool Callback(MembershipUserIdentity user, CallbackRequestContact contact, CallbackRequestVisitInfo visitInfo,
            string message, int[] lotIds)
        {
            throw new InvalidOperationException("Callbacks are not supported");
        }
    }
}