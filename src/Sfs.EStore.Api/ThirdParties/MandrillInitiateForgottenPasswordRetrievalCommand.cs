using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Mandrill;
using Newtonsoft.Json;
using Raven.Client;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.ThirdParties
{
    public class MandrillInitiateForgottenPasswordRetrievalCommand : IInitiateForgottenPasswordRetrievalCommand
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;

        public MandrillInitiateForgottenPasswordRetrievalCommand(MandrillApi mandrillApi, string templateName)
        {
            _mandrillApi = mandrillApi;
            _templateName = templateName;
        }

        public IAsyncDocumentSession Session { get; set; }

        public MembershipUser User { get; set; }

        public void Execute()
        {
            var token = UserToken.Encrypt(new Token(User.Username));
            Session.Query<ForgottenPasswordToken>()
                .Where(x => x.Token == token)
                .AnyAsync()
                .ContinueWith(x =>
                                  {
                                      if (!x.Result)
                                          Session.StoreAsync(
                                              new ForgottenPasswordToken(User.Id, token, SystemTime.UtcNow))
                                              .Wait();
                                  })
                .Wait();

            SendEmail(token).Wait();
        }

        private Task<EmailResult> SendEmail(string token)
        {
            var message = new EmailMessage { to = new[] { new EmailAddress { email = User.EmailAddress } } };

            message.AddGlobalVariable("first_name", User.FirstName ?? "");
            message.AddGlobalVariable("last_name", User.LastName ?? "");
            message.AddGlobalVariable("username", User.Username);
            message.AddGlobalVariable("email", User.EmailAddress);
            message.AddGlobalVariable("token",HttpUtility.UrlEncode(token));

            return _mandrillApi.SendMessageAsync(message, _templateName, new TemplateContent[0])
                .ContinueWith(x => x.Result.FirstOrDefault());
        }
    }

    public class ForgottenPasswordToken
    {
        [JsonConstructor]
        public ForgottenPasswordToken(string userId, string token, DateTime createdDate)
        {
            UserId = userId;
            Token = token;
            CreatedDate = createdDate;
        }

        public string UserId { get; private set; }
        public string Token { get; private set; }
        public DateTime CreatedDate { get; private set; }
    }
}