using System;
using System.Linq;
using System.Threading.Tasks;
using Mandrill;
using Raven.Client;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;

namespace Sfs.EStore.Api.ThirdParties
{
    public class MandrillEmailBookAServiceIntegrator : IIntegrateBookingServices
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;
        private readonly EmailAddress[] _toEmailAddresses;

        public MandrillEmailBookAServiceIntegrator(MandrillApi mandrillApi, string toEmailAddresses, string templateName)
        {
            if (mandrillApi == null) throw new ArgumentNullException("mandrillApi");
            if (string.IsNullOrEmpty(toEmailAddresses)) throw new ArgumentNullException("toEmailAddresses");
            if (string.IsNullOrEmpty(templateName)) throw new ArgumentNullException("templateName");
            
            _mandrillApi = mandrillApi;
            _templateName = templateName;

            _toEmailAddresses = toEmailAddresses.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => new EmailAddress {email = x})
                .ToArray();
        }

        public bool Supported { get { return true; } }

        public IAsyncDocumentSession Session { get; set; }
        
        public void BookService(MembershipUserIdentity identity, BookAServiceController.BookAServicePostModel @params)
        {
            SendEmail(identity, @params).Wait();
        }

        private Task<EmailResult> SendEmail(MembershipUserIdentity user, BookAServiceController.BookAServicePostModel @params)
        {
            var message = new EmailMessage { to = _toEmailAddresses };

            message.AddGlobalVariable("USERNAME", user == null ? "" : (user.Name ?? ""));
            message.AddGlobalVariable("FULL_NAME", @params.FullName ?? "");
            message.AddGlobalVariable("ADDRESS_LINE1", @params.AddressLine1 ?? "");
            message.AddGlobalVariable("POSTCODE", @params.PostCode ?? "");
            message.AddGlobalVariable("PHONE", @params.Telephone ?? "");
            message.AddGlobalVariable("EMAIL", @params.Email ?? "");

            message.AddGlobalVariable("REGISTRATION", @params.Registration ?? "");
            message.AddGlobalVariable("MAKE", @params.Make ?? "");
            message.AddGlobalVariable("MODEL", @params.Model ?? "");
            message.AddGlobalVariable("ENGINE_SIZE", @params.EngineSize ?? "");
            message.AddGlobalVariable("MILEAGE", @params.Mileage ?? "");
            message.AddGlobalVariable("MOT_REQUIRED", @params.MotRequired.ToString());
            message.AddGlobalVariable("COURTESY_VEHICLE_REQUIRED", @params.CourtesyVehicleRequired.ToString());
            message.AddGlobalVariable("PREFERRED_DATE", @params.PreferredDate ?? "");
            message.AddGlobalVariable("COMMENTS", @params.Comments ?? "");

            return _mandrillApi.SendMessageAsync(message, _templateName, new TemplateContent[0])
                .ContinueWith(x => x.Result.FirstOrDefault());
        }
    }
}