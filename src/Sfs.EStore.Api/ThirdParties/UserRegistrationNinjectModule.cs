using Ninject.Web.Common;
using Sfs.EStore.Api.ThirdParties.UserRegistration;

namespace Sfs.EStore.Api.ThirdParties
{
    public class UserRegistrationNinjectModule : ThirdPartyNinjectModule
    {
        public override void Load()
        {
            Bind<IValidatePasswordRulesCommand>()
                .To<Any6CharactersValidatePasswordRulesCommand>()
                .InRequestScope();

            Bind<IValidatePasswordRulesCommand>()
                .To<AnyNonBlankValidatePasswordRulesCommand>()
                .When(request => IntegratesWith("IValidatePasswordRulesCommand",
                                                "AnyNonBlankValidatePasswordRulesCommand"))
                .InRequestScope();

            Bind<IValidateUserRegistrationRequestsCommand>()
                .To<ValidateUserRegistrationRequestsCommand>()
                .InRequestScope();

            Bind<IValidateUserRegistrationRequestsCommand>()
                .To<ValidateGrsUserRegistrationRequestsCommand>()
                .When(request => IntegratesWith("IValidateUserRegistrationRequestsCommand",
                                                "Sfs.EStore.Api.ThirdParties.UserRegistration.ValidateGrsUserRegistrationRequestsCommand"))
                .InRequestScope();

            Bind<IRegisterNewUsersCommand>()
                .To<CreateNewUsersCommand>()
                .InRequestScope();

            Bind<IRegisterNewUsersCommand>()
                .To<MandrillEmailConfirmationRegisterNewUsersCommand>()
                .When(request => IntegratesWith("IRegisterNewUsersCommand",
                                                "Sfs.EStore.Api.ThirdParties.UserRegistration.MandrillEmailConfirmationRegisterNewUsersCommand"))
                .InRequestScope()
                .WithConstructorArgument("templateName",
                                         ctx => Setting("IRegisterNewUsersCommand/Config/TemplateName"))
                .WithPropertyValue("OverrideToEmailAddress", ctx => Setting("IRegisterNewUsersCommand/Config/OverrideToEmailAddress"));

            Bind<IRegisterNewUsersCommand>()
                .To<RegisterNewUsersThroughCustomerServices>()
                .WhenTenantIntegratesWith<RegisterNewUsersThroughCustomerServices>("IRegisterNewUsersCommand")
                .InRequestScope()
                .WithConstructorArgumentFromTenantSetting("userTemplateName")
                .WithConstructorArgumentFromTenantSetting("customerServiceEmailAddress")
                .WithConstructorArgumentFromTenantSetting("customerServiceTemplateName");
        }
    }
}