using Raven.Client;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;

namespace Sfs.EStore.Api.ThirdParties
{
    public interface IIntegrateBookingServices
    {
        bool Supported { get; }
        IAsyncDocumentSession Session { get; set; }
        void BookService(MembershipUserIdentity identity, BookAServiceController.BookAServicePostModel @params);
    }

    public class NullBookAServiceIntegration : IIntegrateBookingServices
    {
        public bool Supported
        {
            get { return false; }
        }

        public IAsyncDocumentSession Session { get; set; }

        public void BookService(MembershipUserIdentity identity, BookAServiceController.BookAServicePostModel @params)
        {
            throw new System.NotSupportedException();
        }
    }
}