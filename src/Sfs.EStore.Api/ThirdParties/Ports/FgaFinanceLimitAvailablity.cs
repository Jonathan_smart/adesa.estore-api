using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Sfs.EStore.Api.Commands;

namespace Sfs.EStore.Api.ThirdParties.Ports
{
    public class FgaFinanceLimitAvailablity : Command<FgaFinanceLimitAvailablity.FinanceLimit>
    {
        private static Lazy<HttpClient> _httpClientFactory;
        private readonly int _fgaDealerCode;
        private readonly string _creditLine;
        private readonly HttpClient _httpClient;

        public FgaFinanceLimitAvailablity(int fgaDealerCode, string creditLine, FgaFinanceProxyBaseAddress baseAddress)
        {
            _fgaDealerCode = fgaDealerCode;
            _creditLine = creditLine;

            if (_httpClientFactory == null)
            {
                _httpClientFactory = new Lazy<HttpClient>(() =>
                {
                    var client = new HttpClient { BaseAddress = new Uri(baseAddress) };
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    return client;
                });
            }
            _httpClient = _httpClientFactory.Value;
        }

        public class FinanceLimit
        {
            public FinanceLimit(decimal amount, string availability = "", string message = "")
            {
                Message = message;
                Availability = availability;
                Amount = amount;
            }

            public decimal Amount { get; private set; }
            public string Availability { get; private set; }
            public string Message { get; private set; }
        }

        internal class FinanceLimitResponse
        {
            public decimal Amount { get; set; }
            public string Availability { get; set; }
            public string Message { get; set; }
        }

        public override void Execute()
        {
            var response = _httpClient.GetAsync(string.Format("uk11?dealerCode={0}&creditline={1}", _fgaDealerCode, _creditLine));

            if (!response.Result.IsSuccessStatusCode)
                return;

            var result = response.Result.Content.ReadAsAsync<FinanceLimitResponse>().Result;

            Result = new FinanceLimit(result.Amount, result.Availability, result.Message);
        }
    }
}