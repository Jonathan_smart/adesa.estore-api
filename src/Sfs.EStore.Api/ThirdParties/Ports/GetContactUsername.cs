using System.Linq;
using Raven.Client;
using Sfs.Core;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Data;

namespace Sfs.EStore.Api.ThirdParties.Ports
{
    internal class GetContactUsername : IQuery<ContactUsername>
    {
        private readonly string _username;
        private readonly SalesChannel _salesChannel;

        public GetContactUsername(string username, SalesChannel salesChannel)
        {
            _username = username;
            _salesChannel = salesChannel;
        }

        public IAsyncDocumentSession Session { set; private get; }

        public void Execute()
        {
            using (var dbContext = new NavisionContext())
            {
                var contactUsername = dbContext.Set<ContactUsername>()
                    .FirstOrDefault(x => x.Username == _username &&
                                         x.SalesChannelNo == _salesChannel);

                Result = contactUsername;
            }
        }

        public ContactUsername Result { get; private set; }
    }
}