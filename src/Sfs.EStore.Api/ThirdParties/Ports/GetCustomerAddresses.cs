using System.Linq;
using Raven.Client;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Data;

namespace Sfs.EStore.Api.ThirdParties.Ports
{
    internal class GetCustomerAddresses : IQuery<ShipToAddress[]>
    {
        private readonly string[] _customerNos;

        public GetCustomerAddresses(params string[] customerNos)
        {
            _customerNos = customerNos ?? new string[0];
        }

        public IAsyncDocumentSession Session { set; private get; }

        public void Execute()
        {
            using (var dbContext = new NavisionContext())
            {
                dbContext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

                Result = dbContext.Set<ShipToAddress>()
                    .Where(x => _customerNos.Contains(x.CustomerNo))
                    .ToArray();
            }
        }

        public ShipToAddress[] Result { get; private set; }
    }
}