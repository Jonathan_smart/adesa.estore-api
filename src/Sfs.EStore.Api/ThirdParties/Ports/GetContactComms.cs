﻿using System.Linq;
using Raven.Client;
using Sfs.Core;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Data;

namespace Sfs.EStore.Api.ThirdParties.Ports
{
    public class GetContactComms : IQuery<string>
    {
        private readonly string _email;
        private readonly BusinessUnit _businessUnit;

        public GetContactComms(string email, BusinessUnit businessUnit)
        {
            _email = email;
            _businessUnit = businessUnit;
        }
        public IAsyncDocumentSession Session { set; private get; }

        public string Result { set; get; }

        public void Execute()
        {
            using (var dbContext = new NavisionContext())
            {
                dbContext.Database.ExecuteSqlCommand(
                         "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                Result = dbContext.Set<ContactUsername>().Join(
                    dbContext.Set<ContactComms>(), // the source table of the inner join
                     ContactUsername => ContactUsername.ContactNo,        // Select the primary key (the first part of the "on" clause in an sql "join" statement)
                     ContactComms => ContactComms.ContactNo,   // Select the foreign key (the second part of the "on" clause)
                     (ContactUsername, ContactComms) => new { ContactUsername = ContactUsername, ContactComms = ContactComms }) // selection
                    .Where(x => x.ContactUsername.Username == _email && x.ContactUsername.ApplicationAccount.ToLower() == _businessUnit)
                    .OrderByDescending(x=>x.ContactComms.EntryNo)
                    .Select(x => x.ContactComms.Value)
                   .FirstOrDefault();
            }
        }
    }
}