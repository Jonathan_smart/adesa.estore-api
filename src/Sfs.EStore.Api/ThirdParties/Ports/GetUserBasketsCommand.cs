using System;
using System.Data.Entity;
using System.Linq;
using Raven.Client;
using Sfs.Core;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Data;
using Sfs.EStore.Api.ThirdParties.Ports;

namespace Sfs.EStore.Api.ThirdParties
{
    internal class GetUserBasketsCommand : IQuery<GetUserBasketsCommand.Basket[]>
    {
        private readonly string _contactNo;
        private readonly BusinessUnit _businessUnit;

        public GetUserBasketsCommand(string contactNo, BusinessUnit businessUnit)
        {
            if (contactNo == null) throw new ArgumentNullException("contactNo");

            _contactNo = contactNo;
            _businessUnit = businessUnit;
        }

        public int[] EntryNos { get; set; }

        public IAsyncDocumentSession Session { set; private get; }

        public void Execute()
        {
            var customersCmd = new GetContactCustomers(_contactNo, _businessUnit);
            customersCmd.Execute();
            var customers = customersCmd.Result.Select(x => x.No).ToArray();

            using (var dbContext = new NavisionContext())
            {
                var query = dbContext.Set<Data.Basket>()
                    .Include(x => x.SerialNoInformation)
                    .OrderByDescending(x => x.AddedToBasketOn)
                    .Where(x => customers.Contains(x.CustomerNo) || x.ContactNo == _contactNo)
                    .Where(x => x.BusinessUnit == _businessUnit);

                if (EntryNos != null && EntryNos.Length > 0)
                    query = query.Where(x => EntryNos.Contains(x.No));

                var baskets = query.Take(100).ToArray().Select(x => new Basket(x)).ToArray();

                Result = baskets;
            }
        }

        public Basket[] Result { get; private set; }

        public class Basket
        {
            private readonly Data.Basket _data;

            public Basket(Data.Basket data)
            {
                _data = data;
                SerialNoInformation = new SerialNoInformationDetail(_data.SerialNoInformation);
            }

            public int No { get { return _data.No; } }
            public DateTime? ExpiryDate { get { return _data.ExpiryDate.FromNavision(); } }
            public decimal Price { get { return _data.Price; } }
            public SerialNoInformationDetail SerialNoInformation { get; private set; }
            public string SerialNo { get { return _data.SerialNo; } }
            public BasketType Type { get { return (BasketType)_data.Type; } }

            public class SerialNoInformationDetail
            {
                private readonly SerialNoInformation _serialNoInformation;

                public SerialNoInformationDetail(SerialNoInformation serialNoInformation)
                {
                    _serialNoInformation = serialNoInformation;
                }

                public string RegistrationNo { get { return _serialNoInformation.RegistrationNo; } }
            }

            public enum BasketType : int
            {
                Undefined = 0,
                SalesOrder = 1,
                AuctionLot = 2,
                SoR = 3
            }
        }
    }
}