﻿using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.Core;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Data;
using Sfs.EStore.Api.Data.DataAccess;

namespace Sfs.EStore.Api.ThirdParties.Ports
{
    /// <summary>
    /// Retrieve Default Sales Contact for Customer Services.
    /// </summary>
    internal class GetDefaultSalesContact : IQuery<string>
    {
        private readonly string _customerNo;

        public GetDefaultSalesContact(string customerNo)
        {
            _customerNo = customerNo;
        }

        public IAsyncDocumentSession Session { set; private get; }

        public void Execute()
        {
            using (var dbContext = new NavisionContext())
            {
                dbContext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

                Result =
                    dbContext.Set<Customer>()
                        .Where(x => x.No == _customerNo)
                        .Join(dbContext.Set<SalespersonPurchaser>(), r => r.SalesPersonCode, c => c.Code, (r, c) => c)
                        .Select(x => x.ContactNo)
                        .DefaultIfEmpty("")
                        .SingleOrDefault();

            }
        }

        public string Result { get; private set; }
    }
}