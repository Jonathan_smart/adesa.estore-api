﻿using System.Linq;
using Raven.Client;
using Sfs.Core;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Data;

namespace Sfs.EStore.Api.ThirdParties.Ports
{
    /// <summary>
    /// Retrieve Default Sales Contact for Customer Services.
    /// </summary>
    internal class GetDefaultCustomerForContact : IQuery<Customer>
    {
        private readonly string _contactNo;
        private readonly BusinessUnit _businessnit;

        public GetDefaultCustomerForContact(string contactNo, BusinessUnit businessnit)
        {
            _contactNo = contactNo;
            _businessnit = businessnit;
        }

        public IAsyncDocumentSession Session { set; private get; }

        public void Execute()
        {
            using (var dbContext = new NavisionContext())
            {
                dbContext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

                Result = dbContext.Set<ContactRelationship>() .Where(x => x.ContactNo == _contactNo && x.SourceType == 1 && x.AuthorizedBy != "" && x.IsDefault==1)
                    .Join(dbContext.Set<Customer>().Where(x=>x.GlobalDimension1Code == _businessnit), r => r.SourceNo, c => c.No, (r, c) => c)
                    .DefaultIfEmpty(null)
                    .FirstOrDefault();

            }
        }

        public Customer Result { get; private set; }
    }
}