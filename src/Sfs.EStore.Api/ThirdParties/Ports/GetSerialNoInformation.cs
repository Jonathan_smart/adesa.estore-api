using System.Linq;
using Raven.Client;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Data;

namespace Sfs.EStore.Api.ThirdParties
{
    internal class GetSerialNoInformation : IQuery<SerialNoInformation>
    {
        public IAsyncDocumentSession Session { set; private get; }

        public string RegistrationPlate { get; set; }

        public void Execute()
        {
            using (var dbContext = new NavisionContext())
            {
                dbContext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

                IQueryable<SerialNoInformation> query = dbContext.Set<SerialNoInformation>();

                if (!string.IsNullOrEmpty(RegistrationPlate))
                    query = query.Where(x => x.RegistrationNo == RegistrationPlate && x.ItemNo == "VEHICLE");

                Result = query.FirstOrDefault();
            }
        }

        public SerialNoInformation Result { get; private set; }
    }
}