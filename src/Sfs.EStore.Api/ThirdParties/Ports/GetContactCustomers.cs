using System.Linq;
using Raven.Client;
using Sfs.Core;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Data;

namespace Sfs.EStore.Api.ThirdParties.Ports
{
    internal class GetContactCustomers : IQuery<Customer[]>
    {
        private readonly string _contactNo;
        private readonly BusinessUnit _businessUnit;

        public GetContactCustomers(string contactNo, BusinessUnit businessUnit)
        {
            _contactNo = contactNo;
            _businessUnit = businessUnit;
        }

        public string CustomerNo { get; set; }
        
        public IAsyncDocumentSession Session { set; private get; }
        public void Execute()
        {
            using (var dbContext = new NavisionContext())
            {
                dbContext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

                var query = dbContext.Set<Contact>()
                   // .Where(x => x.GlobalDimension1Code == _businessUnit)
                    .Where(x => x.No == _contactNo)
                    .Join(dbContext.Set<ContactRelationship>(), c => c.No, cr => cr.ContactNo, (c, cr) => cr)
                    .Where(x => x.SourceType == 1)
                    .Where(x => x.AuthorizedBy != "")
                    .Join(dbContext.Set<Customer>(), cr => cr.SourceNo, c => c.No,
                        (cr, c) => new {IsDefault = cr.IsDefault == 1, Customer = c});

                if (!string.IsNullOrWhiteSpace(CustomerNo))
                    query = query.Where(x => x.Customer.No == CustomerNo);

                var customers = query
                    .OrderBy(x => x.Customer.Name).ThenBy(x => x.Customer.City)
                    .ToList();

                Result = customers.Select(x => x.Customer).ToArray();
            }
        }

        public Customer[] Result { get; private set; }

        public class CustomerWithAddresses
        {
            public Customer Customer { get; set; }
            public ShipToAddress Addresses { get; set; }
        }
    }
}