using System;
using System.Threading.Tasks;
using Raven.Json.Linq;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Entities;
using log4net;

namespace Sfs.EStore.Api.ThirdParties
{
    public class CapPricingVehicleEnricher : IGetVehicleEnricher, IPutVehicleEnricher
    {
        private const string MetaDataEnrichedKey = "Cap-Pricing-Enriched";
        private readonly CapValuation _command;

        public CapPricingVehicleEnricher(CapValuation command)
        {
            _command = command;
        }

        private static readonly ILog Logger = LogManager.GetLogger(typeof (CapPricingVehicleEnricher));

        public bool ShouldEnrich(Vehicle vehicle, VehicleData vehicleData, RavenJObject metadata)
        {
            return vehicle.CapId.HasValue &&
                   vehicle.CapId.Value > 0 &&
                   vehicle.Mileage.HasValue &&
                   vehicle.Registration.Date.HasValue &&
                   !string.IsNullOrWhiteSpace(vehicle.VehicleType) &&
                   !metadata.Value<DateTime?>(MetaDataEnrichedKey).HasValue;
        }

        public Task Enrich(Vehicle vehicle, VehicleData vehicleData, RavenJObject metadata)
        {
            return Task.Factory.StartNew(() =>
                             {
                                 _command.CapId = vehicle.CapId;
                                 _command.Mileage = vehicle.Mileage;
                                 _command.RegistrationDate = vehicle.Registration.Date;
                                 _command.VehicleType = vehicle.VehicleType;

                                 try
                                 {
                                     _command.Execute();
                                     var result = _command.Result;

                                     vehicleData.Valuations = vehicleData.Valuations ?? new ValuationData();
                                     vehicleData.Valuations.Average = result.Average.ToMoney();
                                     vehicleData.Valuations.Below = result.Below.ToMoney();
                                     vehicleData.Valuations.Clean = result.Clean.ToMoney();
                                     vehicleData.Valuations.Retail = result.Retail.ToMoney();

                                     metadata[MetaDataEnrichedKey] = SystemTime.UtcNow;
                                 }
                                 catch(Exception ex)
                                 {
                                     Logger.Info(string.Format("Failed to extract valuation data for {0}", vehicle.Id), ex);
                                 }
                             });
        }
    }
}