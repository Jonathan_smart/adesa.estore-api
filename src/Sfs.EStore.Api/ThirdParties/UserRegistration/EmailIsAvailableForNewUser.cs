using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    public class EmailIsAvailableForNewUser : Command<bool>, IQuery<bool>
    {
        private readonly string _email;

        public EmailIsAvailableForNewUser(string email)
        {
            _email = email;
        }

        public override void Execute()
        {
            var alreadyExists = Session.Query<MembershipUser>()
                .Where(x => x.EmailAddress == _email)
                .AnyAsync()
                .Result;

            Result = !alreadyExists;
        }
    }
}