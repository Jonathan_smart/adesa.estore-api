using System;
using System.Net;
using System.Web.Http;
using Ninject.Activation;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    /// <exception cref="UserAlreadyExistsException"></exception>
    /// <exception cref="FailedToRegisterUserException"></exception>
    public interface IRegisterNewUsersCommand : ICommand
    {
        UserRegistrationPostModel Request { set; }
    }

    public class NullRegisterNewUsersCommand : IRegisterNewUsersCommand
    {
        public IAsyncDocumentSession Session { get; set; }

        public void Execute()
        {
        }

        public UserRegistrationPostModel Request { get; set; }
    }

    public class ApproveUserCommand : Command
    {
        private readonly string _username;

        public ApproveUserCommand(string username)
        {
            _username = username;
        }

        public override void Execute()
        {
            var user = Session.Query<MembershipUser>()
                .Where(x => x.Username == _username)
                .FirstOrDefaultAsync()
                .Result;

            if (user == null)
                throw new InvalidOperationException(string.Format("User '{0}' does not exist", _username));

            user.Approve();
        }
    }
}