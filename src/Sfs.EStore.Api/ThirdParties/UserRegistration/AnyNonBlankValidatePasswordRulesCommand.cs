using System.Web.Http.ModelBinding;
using Sfs.EStore.Api.Commands;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    public class AnyNonBlankValidatePasswordRulesCommand : Command, IValidatePasswordRulesCommand
    {
        public override void Execute()
        {
            if (string.IsNullOrEmpty(Password))
            {
                ModelState.AddModelError("Password", "Password is required");
                Result = false;
            }
            else
            {
                Result = true;
            }
        }

        public bool Result { get; private set; }

        public string Password { get; set; }
        public ModelStateDictionary ModelState { get; set; }
    }
}