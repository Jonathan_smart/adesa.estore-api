using System.Text.RegularExpressions;
using System.Web.Http.ModelBinding;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    public class ValidateUserRegistrationRequestsCommand : Command<bool>, IValidateUserRegistrationRequestsCommand
    {
        private readonly IValidatePasswordRulesCommand _validatePasswordRules;

        public ValidateUserRegistrationRequestsCommand(IValidatePasswordRulesCommand validatePasswordRules)
        {
            _validatePasswordRules = validatePasswordRules;
        }

        public override void Execute()
        {
            Result = true;

            if (string.IsNullOrEmpty(Request.Username))
            {
                ModelState.AddModelError(Request, x => x.Username, "Username is required");
                Result = false;
            }

            if (string.IsNullOrEmpty(Request.FirstName))
            {
                ModelState.AddModelError(Request, x => x.FirstName, "First name is required");
                Result = false;
            }

            if (string.IsNullOrEmpty(Request.LastName))
            {
                ModelState.AddModelError(Request, x => x.LastName, "Last name is required");
                Result = false;
            }

            if (string.IsNullOrEmpty(Request.EmailAddress))
            {
                ModelState.AddModelError(Request, x => x.EmailAddress, "Email address is required");
                Result = false;
            }
            else if (!new Regex(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.([A-Z]{2,8}){1,2}$", RegexOptions.IgnoreCase)
                          .IsMatch(Request.EmailAddress))
            {
                ModelState.AddModelError(Request, x => x.EmailAddress, "Email address is not valid");
                Result = false;
            }
            else if (!Query(new EmailIsAvailableForNewUser(Request.EmailAddress)))
            {
                ModelState.AddModelError(Request, x => x.EmailAddress, "This email is already registered.");
                Result = false;
            }

            if (string.IsNullOrEmpty(Request.Password))
            {
                ModelState.AddModelError(Request, x => x.Password, "Password is required");
                Result = false;
            }
            else if (!PasswordIsValid(Request.Password))
            {
                Result = false;
            }
        }

        public UserRegistrationPostModel Request { get; set; }
        public ModelStateDictionary ModelState { get; set; }

        private bool PasswordIsValid(string password)
        {
            _validatePasswordRules.ModelState = ModelState;
            _validatePasswordRules.Password = password;

            return Query(_validatePasswordRules);
        }
    }
}