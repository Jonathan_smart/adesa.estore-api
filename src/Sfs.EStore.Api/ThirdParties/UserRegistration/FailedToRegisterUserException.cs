using System;
using System.Runtime.Serialization;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    [Serializable]
    public class FailedToRegisterUserException : Exception
    {
        public FailedToRegisterUserException()
        {
        }

        public FailedToRegisterUserException(string message)
            : base(message)
        {
        }

        public FailedToRegisterUserException(string message, Exception inner)
            : base(message, inner)
        {
        }

        protected FailedToRegisterUserException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}