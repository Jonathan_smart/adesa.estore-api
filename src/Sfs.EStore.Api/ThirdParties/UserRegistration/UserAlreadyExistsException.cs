using System;
using System.Runtime.Serialization;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    [Serializable]
    public class UserAlreadyExistsException : Exception
    {
        public UserAlreadyExistsException(string username)
        {
            Username = username;
        }

        public UserAlreadyExistsException(string username, string message)
            : base(message)
        {
            Username = username;
        }

        public UserAlreadyExistsException(string username, string message, Exception inner)
            : base(message, inner)
        {
            Username = username;
        }

        protected UserAlreadyExistsException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
            Username = info.GetString("Username");
        }

        protected string Username { get; private set; }
    }
}