using System.Web.Http.ModelBinding;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    public interface IValidateUserRegistrationRequestsCommand : ICommand<bool>
    {
        UserRegistrationPostModel Request { set; }
        ModelStateDictionary ModelState { set; }
    }
}