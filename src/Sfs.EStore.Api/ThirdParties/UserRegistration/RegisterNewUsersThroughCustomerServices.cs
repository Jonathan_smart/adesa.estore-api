using System;
using System.Linq;
using log4net;
using Mandrill;
using Raven.Abstractions.Extensions;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    public class RegisterNewUsersThroughCustomerServices : Command, IRegisterNewUsersCommand
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _userTemplateName;
        private readonly string _customerServiceEmailAddress;
        private readonly string _customerServiceTemplateName;

        private static readonly ILog Logger =
            LogManager.GetLogger(typeof (RegisterNewUsersThroughCustomerServices));

        public RegisterNewUsersThroughCustomerServices(MandrillApi mandrillApi, string userTemplateName,
            string customerServiceEmailAddress, string customerServiceTemplateName)
        {
            if (customerServiceEmailAddress == null) throw new ArgumentNullException("customerServiceEmailAddress");
            if (customerServiceTemplateName == null) throw new ArgumentNullException("customerServiceTemplateName");
            if (string.IsNullOrEmpty(userTemplateName))
                throw new ArgumentNullException("userTemplateName");

            _mandrillApi = mandrillApi;
            _userTemplateName = userTemplateName;
            _customerServiceEmailAddress = customerServiceEmailAddress;
            _customerServiceTemplateName = customerServiceTemplateName;
        }

        public UserRegistrationPostModel Request { get; set; }

        public override void Execute()
        {
            var userNameNotUnique = Session.Query<MembershipUser>()
                .Where(x => x.Username == Request.Username)
                .ToListAsync().Result
                .Any();

            if (userNameNotUnique)
                throw new UserAlreadyExistsException(Request.Username);

            var user = Request.BindTo(new MembershipUser()).UnApprove();

            Session.StoreAsync(user).Wait();

            try
            {
                SendRegistrationEmail(Request, new EmailAddress
                {
                    email = _customerServiceEmailAddress
                }, _customerServiceTemplateName);
            }
            catch (Exception ex)
            {
                Session.Delete(user);

                throw new FailedToRegisterUserException("Confirmation email failed to send",
                    ex);
            }
            try
            {
                SendRegistrationEmail(Request, new EmailAddress
                {
                    email = Request.EmailAddress,
                    name = string.Format("{0} {1}", Request.FirstName, Request.LastName)
                }, _userTemplateName);
            }
            catch (Exception ex)
            {
                Logger.Warn(new
                {
                    Message = "Failed to send registration confirmation to user.",
                    Request.FirstName,
                    Request.LastName,
                    Request.EmailAddress,
                    TemplateName = _userTemplateName
                }, ex);
            }
        }

        private void SendRegistrationEmail(UserRegistrationPostModel model, EmailAddress toEmailAddress, string templateName)
        {
            var message = new EmailMessage
            {
                to = new[]
                {
                    toEmailAddress
                }
            };

            message.AddGlobalVariable("FIRST_NAME", model.FirstName);
            message.AddGlobalVariable("LAST_NAME", model.LastName);
            message.AddGlobalVariable("EMAIL_ADDRESS", model.EmailAddress);
            message.AddGlobalVariable("PHONE_NUMBER", model.PhoneNumber ?? "");

            message.AddGlobalVariable("JOB_TITLE", model.JobTitle ?? "");
            message.AddGlobalVariable("COMPANY_NAME", model.CompanyName ?? "");
            message.AddGlobalVariable("COMPANY_WEBSITE", model.Website ?? "");
            message.AddGlobalVariable("ADDRESS_LINE1", model.Address.Line1 ?? "");
            message.AddGlobalVariable("ADDRESS_LINE2", model.Address.Line2 ?? "");
            message.AddGlobalVariable("ADDRESS_TOWN_CITY", model.Address.TownCity ?? "");
            message.AddGlobalVariable("ADDRESS_COUNTY", model.Address.County ?? "");
            message.AddGlobalVariable("ADDRESS_POSTCODE", model.Address.Postcode ?? "");

            if (model.Data != null)
            {
                model.Data.ForEach(kv => message.AddGlobalVariable(string.Format("DATA_{0}", kv.Key), kv.Value));
            }

            var mandrillResult = _mandrillApi
                .SendMessageAsync(message, templateName, new TemplateContent[0])
                .Result;
            if (mandrillResult.Any(x => x.Status == EmailResultStatus.Rejected || x.Status == EmailResultStatus.Invalid))
                throw new MandrillException("one or more emails were unsuccessful");
        }
    }
}