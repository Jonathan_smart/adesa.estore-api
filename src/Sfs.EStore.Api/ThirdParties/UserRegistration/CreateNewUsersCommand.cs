using System.Linq;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    public class CreateNewUsersCommand : Command, IRegisterNewUsersCommand
    {
        public UserRegistrationPostModel Request { get; set; }
        public override void Execute()
        {
            var userNameNotUnique = Session.Query<MembershipUser>()
                .Where(x => x.Username == Request.Username)
                .ToListAsync().Result
                .Any();

            if (userNameNotUnique)
                throw new UserAlreadyExistsException(Request.Username);

            var user = Request.BindTo(new MembershipUser());

            Session.StoreAsync(user).Wait();
        }
    }
}