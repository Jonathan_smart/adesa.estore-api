using System;
using System.Linq;
using System.Web;
using Mandrill;
using Raven.Abstractions.Extensions;
using Raven.Client;
using Raven.Client.Linq;
using Sfs.EStore.Api.Commands;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    public class MandrillEmailConfirmationRegisterNewUsersCommand : Command, IRegisterNewUsersCommand
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;

        public MandrillEmailConfirmationRegisterNewUsersCommand(MandrillApi mandrillApi, string templateName)
        {
            if (string.IsNullOrEmpty(templateName))
                throw new ArgumentNullException("templateName");

            _mandrillApi = mandrillApi;
            _templateName = templateName;
        }

        public UserRegistrationPostModel Request { get; set; }

        public string OverrideToEmailAddress { get; set; }

        public override void Execute()
        {
            var userNameNotUnique = Session.Query<MembershipUser>()
                .Where(x => x.Username == Request.Username)
                .ToListAsync().Result
                .Any();

            if (userNameNotUnique)
                throw new UserAlreadyExistsException(Request.Username);

            var user = Request.BindTo(new MembershipUser()).UnApprove();

            Session.StoreAsync(user).Wait();

            try
            {
                EmailRegistrationConfirmationRequest(Request);
            }
            catch (Exception ex)
            {
                Session.Delete(user);

                throw new FailedToRegisterUserException("Confirmation email failed to send",
                                                        ex);
            }
        }

        private void EmailRegistrationConfirmationRequest(UserRegistrationPostModel model)
        {
            var toEmailAddress = new EmailAddress
                                     {
                                         email = model.EmailAddress,
                                         name = string.Format("{0} {1}", model.FirstName, model.LastName)
                                     };

            if (!string.IsNullOrEmpty(OverrideToEmailAddress))
            {
                toEmailAddress = new EmailAddress
                                     {
                                         email = OverrideToEmailAddress
                                     };
            }

            var message = new EmailMessage
                              {
                                  to = new[]
                                           {
                                               toEmailAddress
                                           }
                              };

            message.AddGlobalVariable("first_name", model.FirstName);
            message.AddGlobalVariable("last_name", model.LastName);
            message.AddGlobalVariable("email_address", model.EmailAddress);
            message.AddGlobalVariable("phone_number", model.PhoneNumber ?? "");

            message.AddGlobalVariable("job_title", model.JobTitle ?? "");
            message.AddGlobalVariable("company_name", model.CompanyName ?? "");
            message.AddGlobalVariable("company_website", model.Website ?? "");
            message.AddGlobalVariable("address_line1", model.Address.Line1 ?? "");
            message.AddGlobalVariable("address_line2", model.Address.Line2 ?? "");
            message.AddGlobalVariable("address_town_city", model.Address.TownCity ?? "");
            message.AddGlobalVariable("address_county", model.Address.County ?? "");
            message.AddGlobalVariable("address_postcode", model.Address.Postcode ?? "");

            if (model.Data != null)
            {
                model.Data.ForEach(kv => message.AddGlobalVariable(string.Format("DATA_{0}", kv.Key), kv.Value));
                
            }

            var token = RegistrationConfirmation.Encrypt(
                new RegistrationConfirmationTicket(model.EmailAddress, model.EmailAddress));
            message.AddGlobalVariable("token", HttpUtility.UrlEncode(token));

            var mandrillResult = _mandrillApi
                .SendMessageAsync(message, _templateName, new TemplateContent[0])
                .Result;
            if (mandrillResult.Any(x => x.Status == EmailResultStatus.Rejected || x.Status == EmailResultStatus.Invalid))
                throw new MandrillException("one or more emails were unsuccessful");
        }
    }
}