using System.Web.Http.ModelBinding;
using Sfs.EStore.Api.Commands;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    public interface IValidatePasswordRulesCommand : IQuery<bool>
    {
        string Password { get; set; }
        ModelStateDictionary ModelState { set; }
    }
}