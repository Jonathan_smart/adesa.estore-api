﻿using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Sfs.EStore.Api.ThirdParties.UserRegistration
{
    public class RegistrationConfirmationTicket
    {
        [JsonConstructor]
        public RegistrationConfirmationTicket(string username, string emailAddress)
        {
            Username = username;
            EmailAddress = emailAddress;
        }

        public string Username { get; private set; }
        public string EmailAddress { get; private set; }
    }

    public static class RegistrationConfirmation
    {
        static RegistrationConfirmation()
        {
            CryptoServiceProvider = new TripleDESCryptoServiceProvider();

            if (ConfigurationManager.AppSettings["RegistrationConfirmation/CryptoServiceProvider/Key"] != null)
            {
                var key = ConfigurationManager.AppSettings["RegistrationConfirmation/CryptoServiceProvider/Key"];
                CryptoServiceProvider.Key = Convert.FromBase64String(key);
            }

            if (ConfigurationManager.AppSettings["RegistrationConfirmation/CryptoServiceProvider/IV"] != null)
            {
                var iv = ConfigurationManager.AppSettings["RegistrationConfirmation/CryptoServiceProvider/IV"];
                CryptoServiceProvider.IV = Convert.FromBase64String(iv);
            }
        }

        private static readonly TripleDESCryptoServiceProvider CryptoServiceProvider;

        /// <exception cref="ArgumentException"></exception>
        public static RegistrationConfirmationTicket Decrypt(string token)
        {
            const string exceptionMessage = "encryptedTicket is null.- or -encryptedTicket is an empty string (\"\").- or -encryptedTicket is of an invalid format.";

            if (token == null) throw new ArgumentNullException("token");

            try
            {
                var buffer = Convert.FromBase64String(token);
                var decryptedBytes = CryptoServiceProvider.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length);
                var decrypted = Encoding.ASCII.GetString(decryptedBytes);

                return JObject.Parse(decrypted).ToObject<RegistrationConfirmationTicket>();
            }
            catch (CryptographicException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
            catch (FormatException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
            catch (ArgumentException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
            catch (JsonReaderException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
        }

        public static string Encrypt(RegistrationConfirmationTicket ticket)
        {
            if (ticket == null) throw new ArgumentNullException("ticket");

            var source = JObject.FromObject(ticket).ToString(Formatting.None);
            var buffer = Encoding.ASCII.GetBytes(source);
            var encrypedBytes = CryptoServiceProvider.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length);
            return Convert.ToBase64String(encrypedBytes);
        }
    }
}