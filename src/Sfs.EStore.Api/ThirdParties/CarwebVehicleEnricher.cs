﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Raven.Json.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.FurtherVehicleInfoServiceReference;
using Sfs.EStore.Api.Services;
using log4net;

namespace Sfs.EStore.Api.ThirdParties
{
    public class CarwebVehicleEnricher : IGetVehicleEnricher
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CarwebVehicleEnricher));
        private static readonly TimeSpan NextEnrichAttemptDelay = TimeSpan.FromHours(6);

        private const string MetaDataFirstEnrichedAtKey = "Carweb-Equipment-Enriched";
        private const string MetaDataLastEnrichedAtKey = "Carweb-Last-Enriched-At";
        private const string MetaDataNextEnrichAttemptAfterKey = "Carweb-Next-Enrich-Attempt-After";
        private const string MetaDataEnrichAttemptsKey = "Carweb-Enrich-Attempts";
        private const string EquipmentSourceName = "carweb";
        
        private readonly ICarwebService _carwebService;
        
        public CarwebVehicleEnricher(ICarwebService carwebService)
        {
            _carwebService = carwebService;
        }

        public bool ShouldEnrich(Vehicle vehicle, VehicleData vehicleData, RavenJObject metadata)
        {
            var nextEnrichAttempt = metadata.Value<DateTime?>(MetaDataNextEnrichAttemptAfterKey);

            return !metadata.Value<DateTime?>(MetaDataFirstEnrichedAtKey).HasValue ||
                   (nextEnrichAttempt.HasValue && nextEnrichAttempt.Value < SystemTime.UtcNow);
        }

        public Task Enrich(Vehicle vehicle, VehicleData vehicleData, RavenJObject metadata)
        {
            return Task.Factory.StartNew(
                () =>
                    {
                        var specificationResult = _carwebService.GetSpecificationsFor(vehicle.Registration.Plate);

                        if (specificationResult.Error != null)
                        {
                            var error = specificationResult.Error;
                            Logger.InfoFormat("{0}\r\nCode:{1}\r\nSeverity:{2}",
                                               error.Message, error.Code, error.Severity);
                        }
                        else if (
                            !specificationResult.Vehicle.Registration.Value.Equals(vehicle.Registration.Plate,
                                                                                   StringComparison.
                                                                                       InvariantCultureIgnoreCase))
                            Logger.InfoFormat("Registrations do not match. our plate:{0}, their plate:{1}",
                                               vehicle.Registration.Plate,
                                               specificationResult.Vehicle.Registration.Value);
                        else
                        {
                            EnrichVehicle(vehicle, specificationResult);

                            metadata[MetaDataLastEnrichedAtKey] = SystemTime.UtcNow;

                            metadata[MetaDataEnrichAttemptsKey] =
                                (metadata.Value<int?>(MetaDataEnrichAttemptsKey) ?? 0) + 1;

                            if (!metadata.Value<DateTime?>(MetaDataFirstEnrichedAtKey).HasValue)
                                metadata[MetaDataFirstEnrichedAtKey] = metadata[MetaDataLastEnrichedAtKey];

                            if (ShouldScheduleAnotherEnrich(vehicle))
                            {
                                metadata[MetaDataNextEnrichAttemptAfterKey] =
                                    SystemTime.UtcNow.Add(NextEnrichAttemptDelay);
                            }
                            else if (metadata.Value<DateTime?>(MetaDataNextEnrichAttemptAfterKey).HasValue)
                            {
                                metadata.Remove(MetaDataNextEnrichAttemptAfterKey);
                                metadata[MetaDataNextEnrichAttemptAfterKey + "-Removed"] = SystemTime.UtcNow;
                            }
                        }
                    });
        }

        private static void EnrichVehicle(Vehicle vehicle, SpecificationResult specificationResult)
        {
            vehicle.Specification.Dimensions.HeightMm = specificationResult.Vehicle.Specification.Dimensions.HeightMm;
            vehicle.Specification.Dimensions.LengthMm = specificationResult.Vehicle.Specification.Dimensions.LengthMm;
            vehicle.Specification.Dimensions.WidthMm = specificationResult.Vehicle.Specification.Dimensions.WidthMm;
            vehicle.Specification.Dimensions.KerbWeightMax = specificationResult.Vehicle.Specification.Dimensions.KerbWeightMax;
            vehicle.Specification.Dimensions.KerbWeightMin = specificationResult.Vehicle.Specification.Dimensions.KerbWeightMin;
            vehicle.Specification.Dimensions.WheelbaseForVans = specificationResult.Vehicle.Specification.Dimensions.WheelbaseForVans;

            vehicle.Specification.Economy =
                new Economy(specificationResult.Vehicle.Specification.Economy.FuelConsumptionUrbanColdMpg,
                    specificationResult.Vehicle.Specification.Economy.FuelConsumptionExtraUrbanMpg,
                    specificationResult.Vehicle.Specification.Economy.FuelConsumptionCombinedMpg);

            vehicle.Specification.Engine.ArrangementOfCylinders = specificationResult.Vehicle.Specification.Engine.ArrangementOfCylinders;
            vehicle.Specification.Engine.CapacityCc = specificationResult.Vehicle.Specification.Engine.CapacityCc;
            vehicle.Specification.Engine.NumberOfCylinders = specificationResult.Vehicle.Specification.Engine.NumberOfCylinders;
            vehicle.Specification.Engine.NumberOfValvesPerCylinder = specificationResult.Vehicle.Specification.Engine.NumberOfValvesPerCylinder;

            vehicle.Specification.Environment.Co2 =
                (specificationResult.Vehicle.Specification.Environment.Co2.HasValue &&
                 specificationResult.Vehicle.Specification.Environment.Co2.Value > 0)
                    ? specificationResult.Vehicle.Specification.Environment.Co2
                    : null;
            
            vehicle.Specification.Performance.AccelerationTo100KphInSecs = specificationResult.Vehicle.Specification.Performance.AccelerationTo100KphInSecs;
            vehicle.Specification.Performance.MaximumSpeedMph = specificationResult.Vehicle.Specification.Performance.MaximumSpeedMph;
            vehicle.Specification.Performance.MaximumPowerAtRpm = specificationResult.Vehicle.Specification.Performance.MaximumPowerAtRpm;
            vehicle.Specification.Performance.MaximumPowerInBhp = specificationResult.Vehicle.Specification.Performance.MaximumPowerInBhp;
            vehicle.Specification.Performance.MaximumPowerInKw = specificationResult.Vehicle.Specification.Performance.MaximumPowerInKw;
            vehicle.Specification.Performance.MaximumTorqueAtRpm = specificationResult.Vehicle.Specification.Performance.MaximumTorqueAtRpm;
            vehicle.Specification.Performance.MaximumTorqueLbFt = specificationResult.Vehicle.Specification.Performance.MaximumTorqueLbFt;
            vehicle.Specification.Performance.MaximumTorqueNm = specificationResult.Vehicle.Specification.Performance.MaximumTorqueNm;

            if (specificationResult.Vehicle.SpecItemGroups != null)
            {
                var mappedItems =
                    specificationResult.Vehicle.SpecItemGroups
                        .Select(
                            x =>
                            new KeyValuePair<string, Dictionary<string, string>>(x.GroupName,
                                                                                 x.Specifications
                                                                                     .Select(y => y.Description)
                                                                                     .ToDictionary(i => i, i => i)));

                var items = mappedItems
                    .SelectMany(kv => kv.Value.Select(v => new Equipment.EquipmentItem(kv.Key, v.Key)))
                    .Where(IncludeedEquipmentPredicate)
                    .ToArray();

                vehicle.Specification.Equipment.Set(EquipmentSourceName, items);
            }
        }

        public static bool IncludeedEquipmentPredicate(Equipment.EquipmentItem x)
        {
            return !x.Name.Equals("Solid Paint", StringComparison.InvariantCultureIgnoreCase);
        }

        private static bool ShouldScheduleAnotherEnrich(Vehicle vehicle)
        {
            if (vehicle.Specification == null) return true;
            if (vehicle.Specification.Environment == null) return true;
            if (vehicle.Specification.Economy == null) return true;

            return (!vehicle.Specification.Environment.Co2.HasValue || vehicle.Specification.Environment.Co2 == 0) ||
                   (!vehicle.Specification.Economy.Urban.HasValue || vehicle.Specification.Economy.Urban == 0) ||
                   (!vehicle.Specification.Economy.Combined.HasValue || vehicle.Specification.Economy.Combined == 0) ||
                   (!vehicle.Specification.Economy.ExtraUrban.HasValue || vehicle.Specification.Economy.ExtraUrban == 0);
        }
    }
}