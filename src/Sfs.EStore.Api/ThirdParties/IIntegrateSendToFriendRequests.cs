using System;
using Raven.Client;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.ThirdParties
{
    public interface IIntegrateSendToFriendRequests
    {
        bool Supported { get; }
        IAsyncDocumentSession Session { get; set; }
        void SendToFriend(MembershipUserIdentity identity, SendToFriendController.SendToFriendPostModel args, ListingVehicle listingVehicle);
    }

    public class NullSendToFriendRequestsIntegration : IIntegrateSendToFriendRequests
    {
        public bool Supported
        {
            get { return false; }
        }

        public IAsyncDocumentSession Session { get; set; }
        public void SendToFriend(MembershipUserIdentity identity, SendToFriendController.SendToFriendPostModel args, ListingVehicle listingVehicle)
        {
            throw new InvalidOperationException("Not supported");
        }
    }
}