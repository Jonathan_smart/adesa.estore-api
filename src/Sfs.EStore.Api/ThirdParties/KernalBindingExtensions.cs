using System;
using System.Collections.Generic;
using System.Threading;
using Ninject.Syntax;
using Sfs.EStore.Api.Authentication;

namespace Sfs.EStore.Api.ThirdParties
{
    internal static class KernalBindingExtensions
    {
        public static IBindingInNamedWithOrOnSyntax<T> WhenTenantIntegrationEnabled<T>(
            this IBindingWhenInNamedWithOrOnSyntax<T> @this)
        {
            return @this.When(request =>
            {
                var settingName = string.Format("{0}/Enabled", typeof (T).FullName);
                var actualValue = Setting(settingName);

                return bool.TrueString.Equals(actualValue, StringComparison.CurrentCultureIgnoreCase);
            });
        }

        public static IBindingInNamedWithOrOnSyntax<T> WhenTenantIntegratesWith<T>(this IBindingWhenInNamedWithOrOnSyntax<T> @this, string settingName)
        {
            return @this.When(request => IntegratesWith(settingName, typeof(T).FullName));
        }

        /// <exception cref="ArgumentNullException"><paramref name="settingName"/> is <see langword="null" />.</exception>
        public static TenantSettingBindingAssertions<T> WhenTenantSetting<T>(this IBindingWhenInNamedWithOrOnSyntax<T> @this, string settingName)
        {
            return new TenantSettingBindingAssertions<T>(@this, settingName);
        }

        public class TenantSettingBindingAssertions<T>
        {
            private readonly IBindingWhenInNamedWithOrOnSyntax<T> _binding;
            private readonly string _settingName;

            public TenantSettingBindingAssertions(IBindingWhenInNamedWithOrOnSyntax<T> binding, string settingName)
            {
                _binding = binding;
                _settingName = settingName;
            }

            /// <exception cref="ArgumentNullException"><paramref name="expected"/> is <see langword="null" />.</exception>
            public IBindingInNamedWithOrOnSyntax<T> HasValue(string expected)
            {
                if (expected == null) throw new ArgumentNullException("expected");

                return _binding.When(request => expected.Equals(Setting(_settingName)));
            }
        }

        public static IBindingWithOrOnSyntax<T> WithConstructorArgumentFromTenantSetting<T>(this IBindingWithSyntax<T> @this,
            string name)
        {
            return @this.WithConstructorArgument(name, ctx => Setting<T>(name));
        }

        private static bool IntegratesWith(string settingName, string expectedValue)
        {
            if (settingName == null) throw new ArgumentNullException("settingName");

            var actualValue = Setting(settingName);
            return actualValue == expectedValue;
        }

        private static string Setting<T>(string name)
        {
            var fullName = string.Format("{0}/{1}", typeof(T).FullName, name);

            return Setting(fullName);
        }

        private static string Setting(string fullName)
        {
            var settings = CurrentIntegrationSettings();

            if (settings == null) return null;

            string value;
            return settings.TryGetValue(fullName, out value) ? value : null;
        }

        private static IDictionary<string, string> CurrentIntegrationSettings()
        {
            var principal = (Thread.CurrentPrincipal as IApiApplicationPrincipal);
            return principal == null
                ? null
                : principal.Identity.Advanced.IntegrationSettings;
        }
    }
}