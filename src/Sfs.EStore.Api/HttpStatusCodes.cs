﻿namespace Sfs.EStore.Api
{
    internal static class HttpStatusCodes
    {
        public const int GmSafeForbidden = 418;
        public const int GmSafeInternalServerError = 520;
    }
}
