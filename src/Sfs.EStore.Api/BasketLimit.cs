﻿using System.Globalization;

namespace Sfs.EStore.Api
{
    public struct BasketLimit
    {
        private readonly int _value;

        public BasketLimit(int value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value.ToString(CultureInfo.InvariantCulture);
        }

        public static implicit operator int(BasketLimit item)
        {
            return item._value;
        }
    }
}