using System;
using System.Text;

namespace Sfs.EStore.Api
{
    public static class StringExtensions
    {
        public static string FromBase64String(this string source)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(source));
        }

        public static string FormatWith(this string source, params object[] args)
        {
            return string.Format(source, args);
        }
    }

    public static class DateTimeExtensions
    {
        public static DateTime? FromNavision(this DateTime @this)
        {
            if (@this.Year == 1753)
                return null;

            return @this;
        }
    }
}