using System;
using System.Collections.Generic;
using System.Linq;

namespace Sfs.EStore.Api.Entities
{
    public class UserSavedSearch
    {
        public UserSavedSearch(string user)
        {
            Id = IdFrom(user);
            User = user;
        }

        public string Id { get; protected set; }

        public string User { get; protected set; }
        private List<SavedSearch> _searches = new List<SavedSearch>();
        public SavedSearch[] Searches
        {
            get { return _searches.ToArray(); }
            protected set { _searches = new List<SavedSearch>(value); }
        }

        public SavedSearch this[Guid id]
        {
            get { return _searches.FirstOrDefault(x => x.Id == id); }
        }

        public SavedSearch AddSearch(Dictionary<string, object> criteria, string name = null)
        {
            var newSearch = new SavedSearch(criteria, name);
            _searches.Add(newSearch);
            return newSearch;
        }

        public void RemoveSearch(Guid id)
        {
            var savedSearch = _searches.FirstOrDefault(x => x.Id == id);
            if (savedSearch != null)
                _searches.Remove(savedSearch);
        }

        public static string IdFrom(string userId)
        {
            return string.Format("{0}/SavedSearches", userId);
        }
    }

    public class SavedSearch
    {
        public SavedSearch(Dictionary<string, object> criteria, string name = null)
        {
            Id = Guid.NewGuid();
            SavedOn = SystemTime.UtcNow;
            Criteria = criteria;
            Name = name ?? "";
            if (string.IsNullOrWhiteSpace(Name))
            {
                Name = SavedOn.ToShortDateString();
            }

            AlertStatus = new SavedSearchAlertStatus(false);
        }

        public Guid Id { get; protected set; }

        public string Name { get; protected set; }
        public DateTime SavedOn { get; protected set; }
        public Dictionary<string, object> Criteria { get; protected set; }
        public SavedSearchAlertStatus AlertStatus { get; protected set; }

        public void Update(Dictionary<string, object> newCriteria = null, string newName = null)
        {
            if (newCriteria != null && newCriteria.Count > 0) Criteria = newCriteria;
            if (!string.IsNullOrEmpty(newName)) Name = newName;
            SavedOn = SystemTime.UtcNow;
        }

        public void EnableAlert()
        {
            AlertStatus = new SavedSearchAlertStatus(true, SystemTime.UtcNow);
        }

        public void DisableAlert()
        {
            AlertStatus = new SavedSearchAlertStatus(false);
        }

        public void Alerted()
        {
            AlertStatus = new SavedSearchAlertStatus(AlertStatus.Enabled, SystemTime.UtcNow);
        }

        public string ToQueryString()
        {
            var individuals = Criteria
                .Select(x => string.Format("{0}={1}", Char.ToLowerInvariant(x.Key[0]) + x.Key.Substring(1), x.Value))
                .ToArray();

            return string.Join("&", individuals);
        }
    }

    public struct SavedSearchAlertStatus
    {
        public SavedSearchAlertStatus(bool enabled, DateTime? lastNotifiedAt = null) : this()
        {
            Enabled = enabled;
            LastNotifiedAt = lastNotifiedAt;
        }

        public bool Enabled { get; private set; }
        public DateTime? LastNotifiedAt { get; private set; }
    }
}