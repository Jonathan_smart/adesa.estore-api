using System;
using System.Collections.Generic;
using Sfs.EStore.Api.Authentication;

namespace Sfs.EStore.Api.Entities
{
    public class SessionInteractions
    {
        public SessionInteractions()
        {
            LotsViewed = new ViewedLot[0];
        }

        public string Id { get; set; }
        public string SessionId { get; set; }
        
        private DateTime _createdOn = SystemTime.UtcNow;
        public DateTime CreatedOn
        {
            get { return _createdOn; }
            protected set { _createdOn = value; }
        }

        public string User { get; set; }
        public string Username { get; set; }
        public ViewedLot[] LotsViewed { get; set; }

        public void AddLot(string lotId)
        {
            LotsViewed = new List<ViewedLot>(LotsViewed) { new ViewedLot { LotId = lotId, Date = DateTime.UtcNow } }.ToArray();
        }

        public class ViewedLot
        {
            public string LotId { get; set; }
            public DateTime Date { get; set; }
        }

        public void IsKnownUser(IUserIdentifier user)
        {
            if (user == null) return;

            Username = user.Username;
            User = user.Id;
        }
    }
}