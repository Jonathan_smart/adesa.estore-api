using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sfs.EStore.Api.Entities
{
    public class Account
    {
        public Account()
        {
            SearchStatusesLimitedTo = new string[0];
        }
        public string Id { get; set; }

        public string Name { get; set; }

        [Required]
        public string AccessKeyId { get; set; }

        private Dictionary<string, string> _integrationSettings = new Dictionary<string, string>();
        public Dictionary<string, string> IntegrationSettings
        {
            get { return _integrationSettings; } 
            private set { _integrationSettings = value; }
        }

        public string DatabaseName { get; set; }
        public bool OverrideAllowedSearchResultStatuses { get; set; }
        public string[] SearchStatusesLimitedTo { get; set; }
    }
}