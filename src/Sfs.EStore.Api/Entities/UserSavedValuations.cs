using System;
using System.Collections.Generic;
using System.Linq;
using Sfs.EStore.Api.Commands;

namespace Sfs.EStore.Api.Entities
{
    public class UserSavedValuations
    {
        public UserSavedValuations(string userId)
        {
            Id = IdFrom(userId);
            User = userId;
        }

        public string Id { get; protected set; }
        public string User { get; protected set; }

        public static string IdFrom(string userId)
        {
            return userId + "/SavedValuations";
        }

        private List<SavedValuation> _valuations = new List<SavedValuation>();
        public SavedValuation[] Valuations
        {
            get { return _valuations.ToArray(); }
            protected set { _valuations = new List<SavedValuation>(value); }
        }

        public void AddValuation(SavedValuation result)
        {
            _valuations.Add(result);
        }

        public void RemoveValuation(Guid id)
        {
            var valuation = _valuations.FirstOrDefault(x => x.Id == id);
            if (valuation == null)
                return;
            _valuations.Remove(valuation);
        }
    }

    public class SavedValuation
    {
        public SavedValuation(string registration, int mileage, ValuationModel valuation)
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.Now;

            Registration = registration;
            Mileage = mileage;
            Valuation = valuation;
        }

        public Guid Id { get; protected set; }
        public DateTime CreatedOn { get; protected set; }

        public string Registration { get; protected set; }
        public int Mileage { get; protected set; }

        public ValuationModel Valuation { get; protected set; }
    }
}