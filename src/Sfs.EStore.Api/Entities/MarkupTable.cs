﻿
namespace Sfs.EStore.Api.Entities
{
    public class MarkupTable
    {
        public string CustomerCode { get; set; }
        public int MinMarkUp { get; set; }
    }
}