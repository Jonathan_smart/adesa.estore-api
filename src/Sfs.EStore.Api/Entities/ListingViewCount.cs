using System;
using System.Threading;

namespace Sfs.EStore.Api.Entities
{
    public class ListingViewCount
    {
        public ListingViewCount(string lotId, int initialCount = 1)
        {
            LotId = lotId;
            _count = initialCount;
        }

        private DateTime _createdOn = SystemTime.UtcNow;
        public DateTime CreatedOn
        {
            get { return _createdOn; }
            protected set { _createdOn = value; }
        }

        public string LotId { get; private set; }

        private int _count = 1;
        public int Count
        {
            get { return _count; }
            protected set { _count = value; }
        }

        private DateTime _lastIncrementedOn = SystemTime.UtcNow;
        public DateTime LastIncrementedOn
        {
            get { return _lastIncrementedOn; }
            protected set { _lastIncrementedOn = value; }
        }

        public int Increment()
        {
            _lastIncrementedOn = SystemTime.UtcNow;
            return Interlocked.Increment(ref _count);
        }

        private const string IdPrefix = "ListingViewCounts";
        public static string GenerateId(string listingId)
        {
            if (listingId.StartsWith(IdPrefix)) return listingId;

            return string.Format("{0}/{1}", IdPrefix, listingId);
        }
    }
}