using System;
using System.Linq;
using Sfs.EStore.Api.Models;
using System.Diagnostics;

namespace Sfs.EStore.Api.Entities
{
    //open ascending price auction
    public class EBayStyleBiddingStrategy
    {
        private readonly Lot _lot;
        private readonly Action<ILotOffer> _acceptBid;
        private readonly Action<MaximumBidLimit> _addUserBidLimit;

        public EBayStyleBiddingStrategy(Lot lot, Action<ILotOffer> acceptBid, Action<MaximumBidLimit> addUserBidLimit)
        {
            _lot = lot;
            _acceptBid = acceptBid;
            _addUserBidLimit = addUserBidLimit;
        }

        public BidOutcome TakeBid(Money amount, string userId)
        {
            if (_lot.Status != LotStatusTypes.Active) throw new InvalidOperationException();

            if (amount.Amount % _lot.Bidding.BidIncrements.Amount == 1)
            {
                return BidOutcome.NotAllowed(BidOutcome.BidResponse.NotAllowed);
            }

            var minimumBidRequired = _lot.Bidding.MinimumBidRequired;

            var currentBidLimits = _lot.Bidding.BidLimits.ToList();

            var bidLimit = currentBidLimits
                .FirstOrDefault(x => x.UserId == userId && x.Amount >= amount);

            if (bidLimit != null && amount <= bidLimit.Amount)
            {
                return BidOutcome.NotAllowed(BidOutcome.BidResponse.NotAllowed);
            }

            if (bidLimit == null)
            {
                bidLimit = new MaximumBidLimit(amount, userId);
                _addUserBidLimit(bidLimit);
            }

            Func<BidOutcome> successfullBid =
                () => _lot.Bidding.HasMetReserve
                          ? BidOutcome.Successful(_lot.CurrentOffer.Amount > _lot.Bidding.ReservePrice ? BidOutcome.BidResponse.AcceptedAboveReserve : BidOutcome.BidResponse.Accepted, _lot.CurrentOffer.Amount, amount)
                          : BidOutcome.Successful(BidOutcome.BidResponse.AcceptedBelowReserve, _lot.CurrentOffer.Amount, amount);

            var currentOffer = _lot.CurrentOffer;

            // Bid handler - no current bids
            if (currentBidLimits.Count == 0)
            {
                if (minimumBidRequired == amount)
                    _acceptBid(new HighestBidOffer(bidLimit.Id, bidLimit.Date, minimumBidRequired, bidLimit.UserId));
                else
                    _acceptBid(new AutoBidOffer(bidLimit.Id, bidLimit.Date, amount >= _lot.Bidding.ReservePrice ? _lot.Bidding.ReservePrice : amount, bidLimit.UserId));

                return successfullBid();
            }

            // Bid handler - already a bid of the same or more + increment
            var minimumRequiredAfterThisBid = _lot.Bidding.SpeculativeMinimumBidRequiredFrom(amount);

            var bidOfEqualOrMoreMinimumRequiredAfterThisBid = currentBidLimits
                .FirstOrDefault(b => b.Amount >= minimumRequiredAfterThisBid);

            if (bidOfEqualOrMoreMinimumRequiredAfterThisBid != null)
            {
                _acceptBid(new HighestBidOffer(bidLimit.Id, bidLimit.Date, amount, userId));

                if (bidOfEqualOrMoreMinimumRequiredAfterThisBid.Amount <= amount)
                    _acceptBid(new HighestBidOffer(bidOfEqualOrMoreMinimumRequiredAfterThisBid.Id, bidOfEqualOrMoreMinimumRequiredAfterThisBid.Date,
                                                   minimumRequiredAfterThisBid,
                                                   bidOfEqualOrMoreMinimumRequiredAfterThisBid.UserId));
                else
                    _acceptBid(new AutoBidOffer(bidOfEqualOrMoreMinimumRequiredAfterThisBid.Id, bidOfEqualOrMoreMinimumRequiredAfterThisBid.Date,
                                               minimumRequiredAfterThisBid,
                                                bidOfEqualOrMoreMinimumRequiredAfterThisBid.UserId));

                return BidOutcome.OutBid();
            }

            // Bid handler - already a bid of the same or more (excluding increment)
            var maxBidOfEqualOrMore = currentBidLimits.FirstOrDefault(b => b.Amount >= amount);

            if (maxBidOfEqualOrMore != null)
            {
                _acceptBid(new HighestBidOffer(bidLimit.Id, bidLimit.Date, amount, userId));

                _acceptBid(new HighestBidOffer(maxBidOfEqualOrMore.Id, maxBidOfEqualOrMore.Date,
                                               maxBidOfEqualOrMore.Amount, maxBidOfEqualOrMore.UserId));

                return BidOutcome.OutBid();
            }

            // Bid handler - no higher (or equal) bids
            var nextHighestBid = currentBidLimits
                .First(b => b.Amount == currentBidLimits.Max(m => m.Amount));

            var minimumRequiredFromNextHighest = amount >= _lot.Bidding.ReservePrice && nextHighestBid.Amount < _lot.Bidding.ReservePrice ? _lot.Bidding.ReservePrice : _lot.Bidding.SpeculativeMinimumBidRequiredFrom(nextHighestBid.Amount);

            if (amount >= minimumRequiredFromNextHighest)
            {
                if (bidLimit.Amount == minimumRequiredFromNextHighest && nextHighestBid.UserId != userId)
                {
                    _acceptBid(new HighestBidOffer(bidLimit.Id, bidLimit.Date,
                        minimumRequiredFromNextHighest, bidLimit.UserId));
                }
                else
                {
                    if (_lot.Bidding.HasMetReserve && nextHighestBid.UserId == userId)
                    {
                        _acceptBid(new HighestBidOffer(bidLimit.Id, bidLimit.Date, _lot.CurrentOffer.Amount, bidLimit.UserId));
                    }
                    else
                    {
                        if (_lot.Bidding.HasMetReserve)
                        {
                            _acceptBid(new HighestBidOffer(bidLimit.Id, bidLimit.Date, minimumRequiredFromNextHighest, bidLimit.UserId));
                        }
                        else
                        {
                            if (amount < _lot.Bidding.ReservePrice)
                            {
                                _acceptBid(new AutoBidOffer(bidLimit.Id, bidLimit.Date, amount, bidLimit.UserId));
                            }
                            else
                            {
                                _acceptBid(new AutoBidOffer(bidLimit.Id, bidLimit.Date, _lot.Bidding.ReservePrice, bidLimit.UserId));
                            }

                        }
                    }
                }

                return successfullBid();

            }

            throw new NoBidHandlerException();
        }
    }
}