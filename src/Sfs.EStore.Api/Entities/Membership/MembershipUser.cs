using System;
using System.Linq;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.DomainEvents;

namespace Sfs.EStore.Api.Entities
{
    public class MembershipUser : IUserIdentifier
    {
        public MembershipUser()
        {
            CreatedOn = SystemTime.UtcNow;
        }

        public string Id { get; set; }

        public string Username { get; set; }
        public PasswordHash Password { get; protected set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        
        public string FullName()
        {
            return string.Format("{0} {1}", FirstName, LastName).Trim();
        }

        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }

        public bool IsLockedOut { get; protected set; }
        public DateTime? LockedOutOn { get; protected set; }

        public bool IsApproved { get; protected set; }
        public DateTime? ApprovedOn { get; protected set; }

        private string[] _roles;
        public string[] Roles
        {
            get { return _roles ?? (_roles = new string[0]); }
            set { _roles = value; }
        }

        private string[] _franchises;
        public string[] Franchises
        {
            get { return _franchises ?? (_franchises = new string[0]); }
            set { _franchises = value; }
        }


        public string[] PseudoRoles()
        {
            return Roles.Concat(Franchises).Distinct().ToArray();
        }

        public int InvalidPasswordAttempts { get; protected set; }

        public DateTime CreatedOn { get; protected set; }
        public DateTime? LastLoggedInOn { get; protected set; }

        /// <exception cref="ArgumentNullException"></exception>
        public bool VerifyPassword(string password)
        {
            if (password == null) throw new ArgumentNullException("password");

            var verified = new HashHelper().VerifyHash(password, Password.Hash, Password.Salt);

            InvalidPasswordAttempts = verified ? 0 : (InvalidPasswordAttempts + 1);

            return verified;
        }

        public MembershipUser SetPassword(string password)
        {
            string hash, salt;
            new HashHelper().GetHashAndSalt(password, out hash, out salt);
            Password = new PasswordHash(salt, hash);

            return this;
        }

        public MembershipUser Approve()
        {
            if (IsApproved) return this;

            IsApproved = true;
            ApprovedOn = SystemTime.UtcNow;
            DomainEvent.Raise(new UserApproved(this));
            return this;
        }

        public MembershipUser UnApprove()
        {
            IsApproved = false;
            return this;
        }

        public MembershipUser Lock()
        {
            if (IsLockedOut) return this;

            IsLockedOut = true;
            LockedOutOn = SystemTime.UtcNow;

            return this;
        }

        public MembershipUser UnLock()
        {
            IsLockedOut = false;
            LockedOutOn = null;
            InvalidPasswordAttempts = 0;

            return this;
        }

        public void HasSignedIn()
        {
            LastLoggedInOn = SystemTime.UtcNow;
        }
    }
}