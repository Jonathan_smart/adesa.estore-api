using Newtonsoft.Json;

namespace Sfs.EStore.Api.Entities
{
    public class PasswordHash
    {
        [JsonConstructor]
        public PasswordHash(string salt, string hash)
        {
            Salt = salt;
            Hash = hash;
        }

        public string Salt { get; private set; }
        public string Hash { get; private set; }
    }
}