﻿
namespace Sfs.EStore.Api.Entities
{
    public class MarkupBasket
    {
        public int BasketEntryNo { get; set; }
        public double Markup { get; set; }
        public string Dealer { get; set; }
    }
}