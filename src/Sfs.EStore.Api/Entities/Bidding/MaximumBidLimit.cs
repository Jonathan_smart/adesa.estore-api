using System;

namespace Sfs.EStore.Api.Entities
{
    public class MaximumBidLimit
    {
        public MaximumBidLimit(Money amount, string userId)
        {
            Id = Guid.NewGuid();
            Date = SystemTime.UtcNow;
            Amount = amount;
            UserId = userId;
        }

        public Guid Id { get; protected set; }
        public DateTime Date { get; protected set; }
        public Money Amount { get; protected set; }
        public string UserId { get; protected set; }
    }
}