using System;
using Newtonsoft.Json;

namespace Sfs.EStore.Api.Entities
{
    public class BuyItNowOffer : ILotOffer
    {
        public BuyItNowOffer(Money amount, string userId)
        {
            Id = Guid.NewGuid();
            Date = SystemTime.UtcNow;
            Amount = amount;
            UserId = userId;
        }

        [JsonProperty]
        public Guid Id { get; protected set; }

        [JsonProperty]
        public DateTime Date { get; protected set; }
        [JsonProperty]
        public Money Amount { get; protected set; }
        [JsonProperty]
        public string UserId { get; protected set; }
    }
}