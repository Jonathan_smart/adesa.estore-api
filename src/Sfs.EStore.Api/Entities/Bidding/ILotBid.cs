using System;

namespace Sfs.EStore.Api.Entities
{
    public interface ILotBid : ILotOffer
    {
        Guid TakenFromId { get; }
    }
}