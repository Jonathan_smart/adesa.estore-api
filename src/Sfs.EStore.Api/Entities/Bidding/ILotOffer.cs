using System;

namespace Sfs.EStore.Api.Entities
{
    public interface ILotOffer
    {
        Guid Id { get; }
        DateTime Date { get; }
        Money Amount { get; }
        string UserId { get; }
    }
}