using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Sfs.EStore.Api.Entities
{
    public class Biddable
    {
        public Biddable(Money startingPrice, Money bidIncrements, Money reservePrice = null)
        {
            StartingPrice = startingPrice;
            BidIncrements = bidIncrements;
            ReservePrice = reservePrice;
            MinimumBidRequired = startingPrice;
        }

        public Money StartingPrice { get; private set; }
        public Money BidIncrements { get; private set; }

        public Money ReservePrice { get; private set; }
        public bool HasReserve { get { return ReservePrice != null; } }
        public bool HasMetReserve { get; internal set; }

        public Money MinimumBidRequired { get; internal set; }

        private List<MaximumBidLimit> _bidLimits = new List<MaximumBidLimit>();
        [JsonProperty]
        public IEnumerable<MaximumBidLimit> BidLimits
        {
            get { return _bidLimits; }
             set { _bidLimits = value.ToList(); }
        }

        private List<ILotOffer> _bids = new List<ILotOffer>();

        [JsonProperty]
        public IEnumerable<ILotOffer> Bids
        {
            get { return _bids; }
            //protected 
            set { _bids = value.ToList(); }
        }

        internal Money SpeculativeMinimumBidRequiredFrom(Money price)
        {
            return price + BidIncrements;
        }

        internal void AddLimit(MaximumBidLimit maximumBidLimit)
        {
            _bidLimits.Add(maximumBidLimit);
        }

        internal void AddBid(ILotOffer newBid)
        {
            _bids.Add(newBid);
        }
    }
}