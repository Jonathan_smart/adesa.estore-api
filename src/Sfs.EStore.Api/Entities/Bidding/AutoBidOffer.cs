using System;
using Newtonsoft.Json;

namespace Sfs.EStore.Api.Entities
{
    public class AutoBidOffer : ILotBid
    {
        public AutoBidOffer(Guid takenFromId, DateTime date, Money amount, string userId)
        {
            Id = Guid.NewGuid();
            TakenFromId = takenFromId;
            Date = date;
            Amount = amount;
            UserId = userId;
        }

        [JsonProperty]
        public Guid Id { get; protected set; }

        [JsonProperty]
        public DateTime Date { get; protected set; }
        [JsonProperty]
        public Money Amount { get; protected set; }
        [JsonProperty]
        public string UserId { get; protected set; }
        [JsonProperty]
        public Guid TakenFromId { get; protected set; }
    }
}