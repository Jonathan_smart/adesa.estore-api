namespace Sfs.EStore.Api.Entities
{
    public enum FuelTypes
    {
        Diesel,
        Petrol,
        Unspecified,
        Alternative
    }

    public static class FuelTypesParser
    {
        public static FuelTypes Parse(string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return FuelTypes.Unspecified;

            var invarientValue = value.ToLower();

            if (invarientValue.Contains("diesel")) return FuelTypes.Diesel;
            if (invarientValue.Contains("petrol")) return FuelTypes.Diesel;
            if (invarientValue.Contains("hyb")) return FuelTypes.Alternative;
            if (invarientValue.Contains("elec")) return FuelTypes.Alternative;

            return FuelTypes.Unspecified;
        }
    }
}