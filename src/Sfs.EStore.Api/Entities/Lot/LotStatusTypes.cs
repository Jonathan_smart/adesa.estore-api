using System;
using System.Collections.Generic;
using System.Linq;

namespace Sfs.EStore.Api.Entities
{
    public enum LotStatusTypes
    {
        Scheduled,
        Active,
        Reserved,
        EndedWithSales,
        EndedWithoutSales,
        Cancelled,
        Archived
    }

    internal static class LotStatusTypeConversion
    {
        public static IEnumerable<LotStatusTypes> Convert(params string[] values)
        {
            return values.Select(value => (LotStatusTypes) Enum.Parse(typeof (LotStatusTypes), value));
        }
    }

    public enum ListingType
    {
        Auction,
        AuctionWithBin,
        FixedPrice
    }

    public enum VehicleCondition
    {
        Prepped,
        Raw
    }
}