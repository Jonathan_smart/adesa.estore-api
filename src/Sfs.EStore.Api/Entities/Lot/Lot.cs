﻿using System;
using System.Activities.Validation;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;
using Raven.Client.Linq;
using Sfs.EStore.Api.DomainEvents;
using Sfs.EStore.Api.DomainEvents.Handlers;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Entities
{
    public class LotSale
    {
        [JsonConstructor]
        public LotSale(string id = null, string title = null, bool allowPreview = false)
        {
            Title = title;
            Id = id;
            AllowPreview = allowPreview;
        }

        public string Id { get; private set; }
        public string Title { get; private set; }
        public bool AllowPreview { get; private set; }
    }

    public class Lot
    {
        private string _id;
        public string Id
        {
            get { return _id; }
            set
            {
                if (!value.StartsWith("lots"))
                    value = "lots/" + value;

                _id = value;
            }
        }

        public int LotNumber { get { return int.Parse(Id.Substring(Id.LastIndexOf("/", StringComparison.Ordinal) + 1)); } }

        public string VehicleId { get; protected set; }

        public ListingType ListingType
        {
            get
            {
                if (Bidding == null) return ListingType.FixedPrice;

                return HasBuyItNow ? ListingType.AuctionWithBin : ListingType.Auction;
            }
        }

        public VehicleCondition Condition { get; set; }

        private LotSale _sale;
        public LotSale Sale
        {
            get { return _sale ?? new LotSale(); }
            set { _sale = value; }
        }

        public Money CurrentPrice()
        {
            if (CurrentOffer != null) return CurrentOffer.Amount;
            if (Bidding != null) return Bidding.StartingPrice;
            if (HasBuyItNow) return BuyItNowPrice;

            throw new InvalidOperationException("Lot should have either Bidding, BuyItNowPrice or both");
        }

        public bool PricesIncludeVat { get; set; }
        public bool AllowOffers { get; set; }

        private BuyitNowSale _buyitNowSale = new BuyitNowSale();
        public BuyitNowSale BuyitNowSale
        {
            get
            {

                    return _buyitNowSale;
            }
            set { _buyitNowSale = value; }
        }

        private ILotOffer _currentOffer;
        public ILotOffer CurrentOffer
        {
            get { return _currentOffer; }
            set
            {

                _currentOffer = value;


                HasBuyItNow = BuyItNowPrice != null && (CurrentOffer == null || CurrentOffer.Amount < BuyItNowPrice);

                if (Bidding != null)
                {
                    Bidding.HasMetReserve = Bidding.ReservePrice == null || (CurrentOffer != null && CurrentOffer.Amount >= Bidding.ReservePrice);
                    Bidding.MinimumBidRequired = CurrentOffer == null
                                                     ? Bidding.StartingPrice
                                                     : Bidding.SpeculativeMinimumBidRequiredFrom(CurrentOffer.Amount);
                }
            }
        }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string Title { get; set; }
        public string AttentionGrabber { get; set; }
        public string Programme { get; set; }

        private string _details = "";
        public string Details
        {
            get { return _details; }
            set { _details = value ?? ""; }
        }


        private string _serialNo = "";
        public string SerialNo
        {
            get { return _serialNo; }
            set { _serialNo = value ?? ""; }
        }

        public LotStatusTypes Status { get; set; }

        private Money _buyItNowPrice;
        public Money BuyItNowPrice
        {
            get { return _buyItNowPrice; }
            set
            {
                _buyItNowPrice = value;
                HasBuyItNow = (value != null && value.Amount > 0);
            }
        }

        public bool HasBuyItNow { get; private set; }

        private DiscountPriceInfoModel _discountInfo = new DiscountPriceInfoModel();
        public DiscountPriceInfoModel DiscountInfo
        {
            get { return _discountInfo; }
            private set { _discountInfo = value; }
        }

        public Biddable Bidding { get; set; }

        public void SetBidding(Money startingPrice, Money bidIncrements, Money reservePrice = null)
        {
            if (Bidding != null &&
                (startingPrice == Bidding.StartingPrice &&
                 bidIncrements == Bidding.BidIncrements &&
                 reservePrice == Bidding.ReservePrice))
            {
                return;
            }

            if (Bidding != null && Bidding.BidLimits.Any())
                throw new InvalidOperationException(
                    "Bidding parameters cannot be updated once a bid has been taken.");

            Bidding = new Biddable(startingPrice, bidIncrements, reservePrice);
        }

        public void RemoveBidding()
        {
            if (Bidding != null && Bidding.BidLimits.Any())
                throw new InvalidOperationException(
                    "Bidding option cannot be removed once a bid has been taken.");

            Bidding = null;
        }

        public static string IdFromLotNumber(int lotNumber)
        {
            return "lots/" + lotNumber;
        }

        public static string IdFromLotNumber(string lotNumber)
        {
            if (!lotNumber.StartsWith("lots/"))
                return "lots/" + lotNumber;

            return lotNumber;
        }

        public void Activate()
        {
            if (Status == LotStatusTypes.Active) return;

            Status = LotStatusTypes.Active;

            DomainEvent.Raise(new LotHasBeenActivated(Id, UniqueExternalReferenceNumber));
        }

        public void Reserve()
        {
            if (Status == LotStatusTypes.Reserved) return;

            //if (Status != LotStatusTypes.Active)
            //    throw new InvalidOperationException(string.Format("Can only reserve from {0}, {1} is not a valid status to transition from.", LotStatusTypes.Active, Status));

            Status = LotStatusTypes.Reserved;

            DomainEvent.Raise(new LotHasBeenReserved(Id, UniqueExternalReferenceNumber));
        }

        public void UnReserve()
        {
            if (Status == LotStatusTypes.Active) return;

            if (Status != LotStatusTypes.Reserved)
                throw new InvalidOperationException(string.Format("Cannot un-reserve from {0}", Status));

            Status = LotStatusTypes.Active;

            DomainEvent.Raise(new LotReservationHasBeenRemoved(Id, UniqueExternalReferenceNumber));
        }

        public void Archive()
        {
            if (Status == LotStatusTypes.Archived) return;

            Status = LotStatusTypes.Archived;

            DomainEvent.Raise(new LotHasBeenArchived(Id, UniqueExternalReferenceNumber));
        }

        public void End()
        {
            if (Status.In(LotStatusTypes.EndedWithSales, LotStatusTypes.EndedWithoutSales, LotStatusTypes.Archived)) return;

            if (ListingType == ListingType.FixedPrice)
            {
                Status = LotStatusTypes.Archived;
                DomainEvent.Raise(new LotHasEndedWithoutSale(this));
            }
            else
            {
                var sold = CurrentOffer != null &&
                           Bidding != null &&
                           (!Bidding.HasReserve || CurrentOffer.Amount >= Bidding.ReservePrice) || BuyitNowSale != null;

                Status = sold ? LotStatusTypes.EndedWithSales : LotStatusTypes.EndedWithoutSales;

                if (sold)
                    DomainEvent.Raise(new LotHasEndedWithSale(this));
                else
                    DomainEvent.Raise(new LotHasEndedWithoutSale(this));
            }
        }

        public void Cancel()
        {
            Status = LotStatusTypes.Cancelled;

            DomainEvent.Raise(new LotHasBeenCancelled(Id, UniqueExternalReferenceNumber));
        }

        public void BuyItNow(string userId)
        {
            if (!HasBuyItNow || BuyItNowPrice == null) throw new InvalidOperationException();

            //CurrentOffer = new BuyItNowOffer(BuyItNowPrice, username);
            AcceptBid(new BuyItNowOffer(BuyItNowPrice, userId));
            Status = LotStatusTypes.EndedWithSales;
            EndDate = SystemTime.UtcNow;

            DomainEvent.Raise(new LotHasEndedWithSale(this));
        }

        public BidOutcome TakeBid(Money amount, string userId, Lot lot, Vehicle vehicle, string currentUserName)
        {
            if (SystemTime.UtcNow > EndDate)
                throw new BidsDisabledException(Id);

            var initialCurrentOffer = CurrentOffer;

            var outcome = new EBayStyleBiddingStrategy(this, AcceptBid, x => Bidding.AddLimit(x))
                .TakeBid(amount, userId);

            DomainEvent.Raise(new AuctionBidHasBeenTaken(outcome, initialCurrentOffer, this));

            DomainEvent.Raise(new AddVehicleInterestBid(lot, vehicle, currentUserName, outcome, CurrentOffer));

            return outcome;
        }

        private void AcceptBid(ILotOffer newBid)
        {
            if (newBid == null) throw new ArgumentNullException("newBid");

            Bidding.AddBid(newBid);
            CurrentOffer = newBid;

        }


        public void LinkToVehicle(Vehicle vehicle)
        {
            Debug.Assert(vehicle.Id != null, "vehicle.LotId != null");

            VehicleId = vehicle.Id;
        }

        public int UniqueExternalReferenceNumber { get; set; }

        private string[] _visibilityLimitedTo = new string[0];
        public string[] VisibilityLimitedTo
        {
            get { return _visibilityLimitedTo; }
            set { _visibilityLimitedTo = value; }
        }


        private DateTime _createdAt = SystemTime.UtcNow;

        public DateTime CreatedAt
        {
            get { return _createdAt; }
            protected set { _createdAt = value; }
        }
    }

    public class DiscountPriceInfoModel
    {
        public Money WasPrice { get; set; }
    }


    public class BuyitNowSale
    {
        public string User { get; set; }
        public DateTime? Datetime { get; set; }
        public bool IsBuyer { get; set; }
        public Money BuyItNowPrice { get; set; }
    }
}
