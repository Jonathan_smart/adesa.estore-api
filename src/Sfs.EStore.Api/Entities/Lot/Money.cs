using System;
using Newtonsoft.Json;

namespace Sfs.EStore.Api.Entities
{
    public class Money : IEquatable<Money>, IComparable<Money>
    {
        private const int DecimalPlaces = 2;
        private readonly decimal _amount;
        private readonly Currency _currency;

        [JsonConstructor]
        public Money(decimal amount, Currency currency)
        {
            _amount = Math.Round(amount, DecimalPlaces);
            _currency = currency;
        }

        public static Money From(decimal amount, Currency currency = Currency.XXX)
        {
            return new Money(amount, currency);
        }

        public static Money Zero(Currency currency = Currency.XXX)
        {
            return new Money(0, currency);
        }

        public decimal Amount { get { return _amount; } }
        public Currency Currency { get { return _currency; } }

        public override string ToString()
        {
            return string.Format("{0} {1}", Amount, Currency);
        }

        public static Money operator +(Money left, Money right)
        {
            if (left.Currency != right.Currency)
            {
                throw DifferentCurrencies();
            }

            var sum = left.Amount + right.Amount;

            return new Money(sum, left.Currency);
        }

        public static Money operator -(Money left, Money right)
        {
            if (left.Currency != right.Currency)
            {
                throw DifferentCurrencies();
            }

            var sum = left.Amount - right.Amount;

            return new Money(sum, left.Currency);
        }

        public static Boolean operator ==(Money left, Money right)
        {
            if (ReferenceEquals(null, left) && !ReferenceEquals(null, right)) return false;
            if (ReferenceEquals(null, left)) return true;

            return left.Equals(right);
        }

        public static Boolean operator !=(Money left, Money right)
        {
            if (ReferenceEquals(null, left) && !ReferenceEquals(null, right)) return true;
            if (ReferenceEquals(null, left)) return false;

            return !left.Equals(right);
        }

        public static Boolean operator >(Money left, Money right)
        {
            if (ReferenceEquals(null, left) && !ReferenceEquals(null, right)) return false;
            if (ReferenceEquals(null, left)) return false;

            return left.CompareTo(right) > 0;
        }

        public static Boolean operator <(Money left, Money right)
        {
            if (ReferenceEquals(null, left) && !ReferenceEquals(null, right)) return true;
            if (ReferenceEquals(null, left)) return false;

            return left.CompareTo(right) < 0;
        }

        public static Boolean operator >=(Money left, Money right)
        {
            if (ReferenceEquals(null, left) && !ReferenceEquals(null, right)) return false;
            if (ReferenceEquals(null, left)) return true;

            return left.CompareTo(right) >= 0;
        }

        public static Boolean operator <=(Money left, Money right)
        {
            if (ReferenceEquals(null, left) && !ReferenceEquals(null, right)) return true;
            if (ReferenceEquals(null, left)) return true;

            return left.CompareTo(right) <= 0;
        }

        public bool Equals(Money other)
        {
            if (ReferenceEquals(null, other)) return false;

            return other._amount == _amount && Equals(other._currency, _currency);
        }

        public int CompareTo(Money other)
        {
            if (ReferenceEquals(null, other)) return 1;

            GuardCurrencies(other);

            return _amount.CompareTo(other._amount);
        }

        private void GuardCurrencies(Money other)
        {
            if (other.Currency != Currency)
                throw DifferentCurrencies();
        }

        private static InvalidOperationException DifferentCurrencies()
        {
            return new InvalidOperationException("Money values are in different currencies. Convert to the same " +
                                                 "currency before performing operations on the values.");
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (obj.GetType() != typeof(Money)) return false;
            return Equals((Money)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_amount.GetHashCode() * 397) ^ _currency.GetHashCode();
            }
        }

        public string ToCurrencyString()
        {
            // TODO: Lookup currency symbol based on currency string
            /*
             * foreach (var culture in cultures)
            {
                var lcid = culture.LCID;
                var regionInfo = new RegionInfo(lcid);
                var isoSymbol = regionInfo.ISOCurrencySymbol;

                if (!cultureIdLookup.ContainsKey(isoSymbol))
                {
                    cultureIdLookup[isoSymbol] = new List<Int32>();
                }

                cultureIdLookup[isoSymbol].Add(lcid);
                symbolLookup[isoSymbol] = regionInfo.CurrencySymbol;
            }
             * */
            var symbol = Currency.ToString();
            switch (Currency)
            {
                case Currency.GBP:
                    symbol = "\u00a3";
                    break;
                case Currency.EUR:
                    symbol = "\u20ac";
                    break;
            }

            return string.Format("{0}{1}", symbol, Amount.ToString("n0"));
        }

        public int Units()
        {
            return (int)((double)Amount*Math.Pow(10, DecimalPlaces));
        }
    }

    /// <summary>
    /// Represents world currency by numeric and alphabetic values, as per ISO 4217:
    /// http://www.iso.org/iso/currency_codes_list-1.
    /// </summary>
    [Serializable]
    public enum Currency : ushort
    {
        XXX = 999,
        USD = 840,
        EUR = 978,
        GBP = 826
    }
}