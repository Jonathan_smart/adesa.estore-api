using System;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Entities
{
    public static class ImageUrl
    {
        public static string CloudFilesImageProxyUrl = "http://d3l2oke4gxqliw.cloudfront.net/";

        public static string Create(VehicleImage image, int? width = null, int? height = null)
        {
            return Create(image.Url, image.Container, image.Name, width, height);
        }

        public static string Create(ImageModel image, int? width = null, int? height = null)
        {
            return Create(image.Url, image.Container, image.Name, width, height);
        }

        private static string Create(string url, string container, string name, int? width, int? height)
        {
            var dimensions = new Dimensions(width, height);

            if (!string.IsNullOrWhiteSpace(container))
            {
                var options = "-";
                if (dimensions.Width.HasValue || dimensions.Height.HasValue)
                {
                    options = "c_lpad,b_white";
                    if (dimensions.Width.HasValue)
                    {
                        options += ",w_" + dimensions.Width;
                    }
                    if (dimensions.Height.HasValue)
                    {
                        options += ",h_" + dimensions.Height;
                    }
                }

                return string.Format("{0}{1}/{2}/{3}", CloudFilesImageProxyUrl, container, options, name);
            }

            if (!string.IsNullOrEmpty(url))
            {
                if (!dimensions.Width.HasValue && !dimensions.Height.HasValue)
                    return url;

                if (url.Contains("?"))
                    return string.Format("{0}&width={1}&height={2}", url, dimensions.Width, dimensions.Height);

                return string.Format("{0}?width={1}&height={2}", url, dimensions.Width, dimensions.Height);
            }

            throw new InvalidOperationException(
                "Don't know how to handle image url. Model doesn't have a Url or Container");
        }
        
        private class Dimensions
        {
            public Dimensions(int? width, int? height)
            {
                Width = width;
                Height = height;
            }

            public int? Width { get; private set; }
            public int? Height { get; private set; }
        }
    }
}