namespace Sfs.EStore.Api.Entities
{
    public class Performance
    {
        public decimal? MaximumSpeedMph { get; set; }
        public decimal? AccelerationTo100KphInSecs { get; set; }
        public decimal? MaximumTorqueNm { get; set; }
        public decimal? MaximumTorqueLbFt { get; set; }
        public decimal? MaximumTorqueAtRpm { get; set; }
        public decimal? MaximumPowerInBhp { get; set; }
        public decimal? MaximumPowerInKw { get; set; }
        public decimal? MaximumPowerAtRpm { get; set; }

    }
}