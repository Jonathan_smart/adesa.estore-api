using System;
using System.Globalization;

namespace Sfs.EStore.Api.Entities
{
    public class Vehicle
    {
        public Vehicle()
        {
            RandomisedValue = new Random().NextDouble().ToString(CultureInfo.InvariantCulture);
        }

        public string Id { get; protected set; }

        public string RandomisedValue { get; private set; }

        public string Vin { get; set; }
        public VehicleSource Source { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Derivative { get; set; }
        public string ModelYear { get; set; }
        public int? Mileage { get; set; }
        public string Bodystyle { get; set; }
        public string Transmission { get; set; }
        public string FuelType { get; set; }
        public Registration Registration { get; set; }
        public string ExteriorColour { get; set; }
        public string InteriorDescription { get; set; }
        public TrimInfo Trim { get; set; }
        public string VehicleType { get; set; }
        public string VatStatus { get; set; }

        public string Category { get; set; }
        public int? CapId { get; set; }
        public string NavSystem { get; set; }
        public bool? AlloyWheels { get; set; }
        public string InsuranceGroup { get; set; }

        private Specification _specification;
        public Specification Specification
        {
            get
            {
                return _specification ??
                    (_specification = new Specification(new Performance(), new Economy(null, null, null), new Environment(), new Dimensions(), new Engine()));
            }
            set { _specification = value; }
        }

        private VehicleImage[] _images = new VehicleImage[0];
        public VehicleImage[] Images
        {
            get { return _images; }
            set { _images = value; }
        }

        private VehicleGrades _grade = new VehicleGrades(null);
        public VehicleGrades Grade
        {
            get { return _grade; }
            set { _grade = value; }
        }
    }

    public class VehicleGrades
    {
        public VehicleGrades(VehicleGrade actual)
        {
            Actual = actual;
        }

        public VehicleGrade Actual { get; private set; }
    }

    public class VehicleGrade
    {
        public VehicleGrade(string code, string name)
        {
            Code = code;
            Name = name;
        }

        public string Code { get; private set; }
        public string Name { get; private set; }
    }

    public class VehicleSource
    {
        public VehicleSource(string name, string code = null)
        {
            Code = code;
            Name = name;
        }

        public string Code { get; private set; }
        public string Name { get; private set; }
    }

    public class TrimInfo
    {
        public TrimInfo(string name, string code = null)
        {
            Code = code;
            Name = name;
        }

        public string Code { get; private set; }
        public string Name { get; private set; }
    }

    public class VehicleImage
    {
        public string Url { get; protected set; }

        public string Name { get; protected set; }
        public string Container { get; protected set; }
        public string Type { get; protected set; }

        public static VehicleImage AsUrl(string url)
        {
            return new VehicleImage
            {
                Url = url
            };
        }
        private static string removeLeadingZero(string fileName)
        {
            if (fileName.ToLower().Contains("_"))
            {
                var extentionFileName = fileName.Substring(fileName.LastIndexOf('_'));
                var originalFileName = fileName.Substring(0, fileName.LastIndexOf('_'));

                string _char = extentionFileName.Substring(1, 1);

                if (_char == "0")
                {
                    extentionFileName = extentionFileName.Remove(0, 2);
                    fileName = originalFileName + "_" + extentionFileName;
                }
            }

            return fileName;
        }
        public static VehicleImage AsCloudFiles(string containerName, string name,string type)
        {
            return new VehicleImage
            {
                Container = containerName,
                //Name = type=="360" ? removeLeadingZero(name): name,
                Name = name,
                Type = type 
            };
        }

        public string ToUrl(int? width = null, int? height = null)
        {
            return ImageUrl.Create(this, width, height);
        }
    }
}