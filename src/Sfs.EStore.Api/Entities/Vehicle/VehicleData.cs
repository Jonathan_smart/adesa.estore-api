using System;
using System.Collections.Generic;

namespace Sfs.EStore.Api.Entities
{
    public class VehicleData
    {
        public string Id { get; set; }

        private Dictionary<string, Dictionary<string, object>> _data = new Dictionary<string, Dictionary<string, object>>(StringComparer.InvariantCultureIgnoreCase);
        public Dictionary<string, Dictionary<string, object>> Data
        {
            get { return _data; }
            set { _data = value; }
        }

        private ValuationData _valuations = new ValuationData();
        public ValuationData Valuations
        {
            get { return _valuations; }
            set { _valuations = value; }
        }
    }

    public class ValuationData
    {
        public Money Retail { get; set; }
        public Money Clean { get; set; }
        public Money Average { get; set; }
        public Money Below { get; set; }
    }
}