namespace Sfs.EStore.Api.Entities
{
    public class Engine
    {
        public string Description { get; set; }
        public int? NumberOfCylinders { get; set; }
        public int? NumberOfValvesPerCylinder { get; set; }
        public string ArrangementOfCylinders { get; set; }
        public int? CapacityCc { get; set; }
    }
}