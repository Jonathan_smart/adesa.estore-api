﻿namespace Sfs.EStore.Api.Entities
{
    public class Specification
    {
        public Specification(Performance performance, Economy economy, Environment environment, Dimensions dimensions, Engine engine)
        {
            Performance = performance;
            Economy = economy;
            Environment = environment;
            Dimensions = dimensions;
            Engine = engine;
            Equipment = new Equipment();
        }

        public Performance Performance { get; set; }
        public Economy Economy { get; set; }
        public Environment Environment { get; set; }
        public Dimensions Dimensions { get; set; }
        public Engine Engine { get; set; }

        public Equipment Equipment { get; protected set; }
    }
}