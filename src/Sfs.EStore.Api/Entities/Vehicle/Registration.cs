using System;
using System.Linq;
using Newtonsoft.Json;

namespace Sfs.EStore.Api.Entities
{
    public class Registration
    {
        private readonly string _plate;
        private readonly DateTime? _date;
        private readonly string _yap;

        [JsonConstructor]
        public Registration(string plate, DateTime? date = null, string yearAndPlate = null)
        {
            _plate = (plate ?? "").ToUpper();

            if (date.HasValue && date.Value > DateTime.MinValue) _date =  date.Value;
            
            if (!string.IsNullOrWhiteSpace(yearAndPlate)) _yap = yearAndPlate;
            else if (_date.HasValue) _yap = ParseYap(_date.Value);
        }

        private static string ParseYap(DateTime date)
        {
            const int phase1ChangeOverMonth = 3;
            var phase2Months = new[] { 9, 10, 11, 12, 1, 2 };
            var year = date.ToString("yy");
            var plate = int.Parse(date.ToString("yy"));
            if (phase2Months.Any(x => x.Equals(date.Month)))
                plate += 50;

            if (date.Month < phase1ChangeOverMonth)
                plate -= 1;

            return string.Format("{0}:{1}", year, plate.ToString("00"));
        }

        public string Plate { get { return _plate; } }
        public DateTime? Date { get { return _date; } }
        public string YearAndPlate { get { return _yap; } }
    }
}