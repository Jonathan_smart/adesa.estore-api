namespace Sfs.EStore.Api.Entities
{
    public class Dimensions
    {
        public decimal? KerbWeightMin { get; set; }
        public decimal? KerbWeightMax { get; set; }
        public int? LengthMm { get; set; }
        public int? WidthMm { get; set; }
        public int? HeightMm { get; set; }
        public decimal? WheelbaseForVans { get; set; }
    }
}