namespace Sfs.EStore.Api.Entities
{
    public class Economy
    {
        public Economy(decimal? urban, decimal? extraUrban, decimal? combined)
        {
            Urban = urban;
            ExtraUrban = extraUrban;
            Combined = combined;
        }

        public decimal? Urban { get; private set; }
        public decimal? ExtraUrban { get; private set; }
        public decimal? Combined { get; private set; }
    }
}