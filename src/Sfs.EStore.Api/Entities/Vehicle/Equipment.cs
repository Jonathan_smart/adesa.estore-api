using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Sfs.EStore.Api.Entities
{
    public class Equipment
    {
        private List<EquipmentItem> _items = new List<EquipmentItem>();
        public EquipmentItem[] Items
        {
            get { return _items.ToArray(); }
            protected set { _items = new List<EquipmentItem>(value); }
        }

        public IEnumerable<EquipmentItem> Category(string category)
        {
            return _items.Where(x => x.Category == category);
        }

        //public Equipment Add(string category, string name)
        //{
        //    if (!_items.Any(x => x.Category == category && x.Name == name))
        //    {
        //        _items.Add(new EquipmentItem(category, name));
        //    }

        //    return this;
        //}

        //public Equipment Add(params EquipmentItem[] items)
        //{
        //    if (items == null) throw new ArgumentNullException("items");

        //    var newItems = items.Where(x => !_items.Any(i => i.Category == x.Category && i.Name == x.Name));
        //    _items.AddRange(newItems);

        //    return this;
        //}

        public Equipment Set(string source, params EquipmentItem[] items)
        {
            if (items == null) throw new ArgumentNullException("items");

            _items.RemoveAll(x => x.Source == source);
            var newItems = items.Where(x => !_items.Any(i => i.Category == x.Category && i.Name == x.Name))
                .Select(x => new EquipmentItem(x.Category, x.Name, source))
                .ToArray();
            _items.AddRange(newItems);

            return this;
        }

        public class EquipmentItem
        {
            [JsonConstructor]
            public EquipmentItem(string category, string name, string source = null)
            {
                if (category == null) throw new ArgumentNullException("category");
                if (name == null) throw new ArgumentNullException("name");

                if (category == "") throw new ArgumentOutOfRangeException("category", "category must not be an empty string");
                if (name == "") throw new ArgumentOutOfRangeException("name", "name must not be an empty string");

                Category = category.Trim();
                Name = name.Trim();
                Source = source;
            }

            public string Category { get; private set; }
            public string Name { get; private set; }
            public string Source { get; private set; }
        }
    }
}