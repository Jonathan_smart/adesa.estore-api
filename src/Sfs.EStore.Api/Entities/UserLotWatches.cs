using System.Collections.Generic;

namespace Sfs.EStore.Api.Entities
{
    public class UserLotWatches
    {
        public UserLotWatches(string user)
        {
            WatchedLots = new string[0];
            User = user;
        }

        public string[] WatchedLots { get; private set; }

        public string User { get; protected set; }

        public void Add(int lotNumber)
        {
            Add(Lot.IdFromLotNumber(lotNumber));
        }

        public UserLotWatches Add(params string[] ids)
        {
            var watches = new List<string>(WatchedLots);

            foreach (var id in ids ?? new string[0])
            {
                var lotId = Lot.IdFromLotNumber(id);
                if (!watches.Contains(lotId))
                {
                    watches.Add(lotId);
                    WatchedLots = watches.ToArray();
                }
            }

            return this;
        }

        public void Remove(int lotNumber)
        {
            var watches = new List<string>(WatchedLots);
            var lotId = Lot.IdFromLotNumber(lotNumber);
            if (watches.Contains(lotId))
            {
                watches.Remove(lotId);
                WatchedLots = watches.ToArray();
            }
        }

        public static string GenerateId(string userId)
        {
            return string.Format("{0}/UserLotWatches", userId);
        }
    }
}