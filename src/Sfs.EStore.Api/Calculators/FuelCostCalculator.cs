﻿using System;
using System.Collections.Generic;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Calculators
{
    public class FuelCostCalculator
    {
        private readonly Func<FuelTypes, Money> _fuelCosts;

        public FuelCostCalculator(Func<FuelTypes, Money> fuelCosts)
        {
            _fuelCosts = fuelCosts;
        }

        public Money PencePerUnitDistanceTravelled(FuelTypes fuel, decimal distancePerUnitOfFuel)
        {
            if (distancePerUnitOfFuel <= 0) return null;
            
            var costPerUnit = _fuelCosts(fuel);

            if (costPerUnit == null) return null;

            return new Money(Math.Round(costPerUnit.Amount / distancePerUnitOfFuel, 2), costPerUnit.Currency);
        }
    }

    public class StaticFuelCosts
    {
        private readonly Dictionary<FuelTypes, Money> _fuelCosts;

        public StaticFuelCosts(Dictionary<FuelTypes, Money> fuelCosts)
        {
            _fuelCosts = fuelCosts;
        }

        public Money CostOf(FuelTypes fuel)
        {
            Money cost;

            _fuelCosts.TryGetValue(fuel, out cost);
            
            return cost;
        }
    }
}