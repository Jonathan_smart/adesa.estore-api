﻿using System;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.Calculators.VehicleTaxRates
{
    public class VehicleTaxRateCalculator
    {
        public VehicleTaxRate CalculateFor(VehicleTaxCalculationArgs args)
        {
            var x = new CarsAndLightGoodsVehiclesRegisteredBefore1March2001TaxRateHandler();
            if (x.CanHandle(args)) return x.Handle(args);

            var y = new AlternativeFuelCarsRegisteredOnOrAfter1March2001TaxRateHandler();
            if (y.CanHandle(args)) return y.Handle(args);
            
            var z = new CarsRegisteredOnOrAfter1March2001TaxRateHandler();
            if (z.CanHandle(args)) return z.Handle(args);

            var v1 = new LightGoodsVehiclesRegisteredBetween1March2003And31December2006TaxRateHandler();
            if (v1.CanHandle(args)) return v1.Handle(args);

            var v2 = new LightGoodsVehiclesRegisteredBetween1January2009And31December2010TaxRateHandler();
            if (v2.CanHandle(args)) return v2.Handle(args);

            var v3 = new LightGoodsVehiclesRegisteredAfter1March2001TaxRateHandler();
            if (v3.CanHandle(args)) return v3.Handle(args);

            throw new ArgumentOutOfRangeException("args", args, "Can not handle specified args");
        }
    }

    public class ListingViewModelVehicleTaxRateCalculator
    {
        private readonly VehicleTaxRateCalculator _calculator = new VehicleTaxRateCalculator();
        public VehicleTaxRate CalculateFor(ListingViewModel model)
        {
            if (!model.VehicleInfo.Registration.Date.HasValue) return null;
            if (!model.VehicleInfo.Specification.Co2.HasValue || model.VehicleInfo.Specification.Co2 == 0) return null;

            var args = new VehicleTaxCalculationArgs(
                model.VehicleInfo.Registration.Date.Value,
                model.VehicleInfo.VehicleType == "CAR" ? VehicleType.Car : VehicleType.Van,
                model.VehicleInfo.Specification.Engine.CapacityCc,
                (int)(model.VehicleInfo.Specification.Co2.Value),
                FuelTypes.Unspecified);

            return _calculator.CalculateFor(args);
        }
    }
}
