using System;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Calculators.VehicleTaxRates
{
    internal class LightGoodsVehiclesRegisteredAfter1March2001TaxRateHandler : TaxRateHandler
    {
        public override bool CanHandle(VehicleTaxCalculationArgs args)
        {
            return args.RegistrationDate >= new DateTime(2001, 03, 01)
                   && args.VehicleType == VehicleType.Van;
        }

        public override VehicleTaxRate Handle(VehicleTaxCalculationArgs args)
        {
            return new VehicleTaxRate(Money.From(225, Currency.GBP),
                Money.From(123.75m, Currency.GBP));
        }
    }
}