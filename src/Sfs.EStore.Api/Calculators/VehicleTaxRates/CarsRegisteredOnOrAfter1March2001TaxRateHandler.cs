using System;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Calculators.VehicleTaxRates
{
    internal class CarsRegisteredOnOrAfter1March2001TaxRateHandler : TaxRateHandler
    {
        public override bool CanHandle(VehicleTaxCalculationArgs args)
        {
            return args.RegistrationDate >= new DateTime(2001, 03, 01) && args.VehicleType == VehicleType.Car;
        }

        public override VehicleTaxRate Handle(VehicleTaxCalculationArgs args)
        {
            if (args.Co2GPerKm <= 100) // Band A
                return new VehicleTaxRate(Money.Zero(Currency.GBP), null, 'A');

            if (args.Co2GPerKm <= 110) // Band B
                return new VehicleTaxRate(Money.From(20, Currency.GBP), null, 'B');

            if (args.Co2GPerKm <= 120) // Band C
                return new VehicleTaxRate(Money.From(30, Currency.GBP), null, 'C');

            if (args.Co2GPerKm <= 130) // Band D
                return new VehicleTaxRate(Money.From(110, Currency.GBP), Money.From(60.50m, Currency.GBP), 'D');

            if (args.Co2GPerKm <= 140) // Band E
                return new VehicleTaxRate(Money.From(130, Currency.GBP), Money.From(71.50m, Currency.GBP), 'E');

            if (args.Co2GPerKm <= 150) // Band F
                return new VehicleTaxRate(Money.From(145, Currency.GBP), Money.From(79.75m, Currency.GBP), 'F');

            if (args.Co2GPerKm <= 165) // Band G
                return new VehicleTaxRate(Money.From(185, Currency.GBP), Money.From(101.75m, Currency.GBP), 'G');

            if (args.Co2GPerKm <= 175) // Band H
                return new VehicleTaxRate(Money.From(210, Currency.GBP), Money.From(115.50m, Currency.GBP), 'H');

            if (args.Co2GPerKm <= 185) // Band I
                return new VehicleTaxRate(Money.From(230, Currency.GBP), Money.From(126.50m, Currency.GBP), 'I');

            if (args.Co2GPerKm <= 200) // Band J
                return new VehicleTaxRate(Money.From(270, Currency.GBP), Money.From(148.50m, Currency.GBP), 'J');

            if (args.RegistrationDate < new DateTime(2006, 03, 23) || args.Co2GPerKm <= 225) // Band K*
                return new VehicleTaxRate(Money.From(295, Currency.GBP), Money.From(162.25m, Currency.GBP), 'K');

            if (args.Co2GPerKm <= 255) // Band L
                return new VehicleTaxRate(Money.From(500, Currency.GBP), Money.From(275.00m, Currency.GBP), 'L');

            // Band M - args.Co2GPerKm > 255
            return new VehicleTaxRate(Money.From(515, Currency.GBP), Money.From(283.25m, Currency.GBP), 'M');
        }
    }
}