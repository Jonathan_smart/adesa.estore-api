using System;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Calculators.VehicleTaxRates
{
    internal class AlternativeFuelCarsRegisteredOnOrAfter1March2001TaxRateHandler : TaxRateHandler
    {
        public override bool CanHandle(VehicleTaxCalculationArgs args)
        {
            return args.RegistrationDate >= new DateTime(2001, 03, 01)
                   && args.VehicleType == VehicleType.Car
                   && args.FuelType == FuelTypes.Alternative;
        }

        public override VehicleTaxRate Handle(VehicleTaxCalculationArgs args)
        {
            if (args.Co2GPerKm <= 100) // Band A
                return new VehicleTaxRate(Money.Zero(Currency.GBP), null, 'A');

            if (args.Co2GPerKm <= 110) // Band B
                return new VehicleTaxRate(Money.From(10, Currency.GBP), null, 'B');

            if (args.Co2GPerKm <= 120) // Band C
                return new VehicleTaxRate(Money.From(20, Currency.GBP), null, 'C');

            if (args.Co2GPerKm <= 130) // Band D
                return new VehicleTaxRate(Money.From(100, Currency.GBP), Money.From(55.00m, Currency.GBP), 'D');

            if (args.Co2GPerKm <= 140) // Band E
                return new VehicleTaxRate(Money.From(120, Currency.GBP), Money.From(66.00m, Currency.GBP), 'E');

            if (args.Co2GPerKm <= 150) // Band F
                return new VehicleTaxRate(Money.From(135, Currency.GBP), Money.From(74.25m, Currency.GBP), 'F');

            if (args.Co2GPerKm <= 165) // Band G
                return new VehicleTaxRate(Money.From(170, Currency.GBP), Money.From(93.50m, Currency.GBP), 'G');

            if (args.Co2GPerKm <= 175) // Band H
                return new VehicleTaxRate(Money.From(195, Currency.GBP), Money.From(107.25m, Currency.GBP), 'H');

            if (args.Co2GPerKm <= 185) // Band I
                return new VehicleTaxRate(Money.From(215, Currency.GBP), Money.From(118.25m, Currency.GBP), 'I');

            if (args.Co2GPerKm <= 200) // Band J
                return new VehicleTaxRate(Money.From(255, Currency.GBP), Money.From(140.25m, Currency.GBP), 'J');

            if (args.RegistrationDate < new DateTime(2006, 03, 23) || args.Co2GPerKm <= 225) // Band K*
                return new VehicleTaxRate(Money.From(280, Currency.GBP), Money.From(154m, Currency.GBP), 'K');

            if (args.Co2GPerKm <= 255) // Band L
                return new VehicleTaxRate(Money.From(480, Currency.GBP), Money.From(264m, Currency.GBP), 'L');

            // Band M - args.Co2GPerKm > 255
            return new VehicleTaxRate(Money.From(495, Currency.GBP), Money.From(272.25m, Currency.GBP), 'M');
        }
    }
}