using System;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Calculators.VehicleTaxRates
{
    public class VehicleTaxCalculationArgs
    {
        public VehicleTaxCalculationArgs(DateTime registrationDate,
            VehicleType vehicleType,
            int? engineSizeCc = null,
            int? co2GPerKm = null,
            FuelTypes fuelType = FuelTypes.Unspecified)
        {
            Co2GPerKm = co2GPerKm;
            FuelType = fuelType;
            EngineSizeCc = engineSizeCc;
            RegistrationDate = registrationDate;
            VehicleType = vehicleType;
        }

        public DateTime RegistrationDate { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public int? EngineSizeCc { get; private set; }
        public int? Co2GPerKm { get; private set; }
        public FuelTypes FuelType { get; private set; }

        public override string ToString()
        {
            return string.Format("RegistrationDate:{0}; EngineSizeCc:{1}; Co2GPerKm:{2}; FuelType:{3}",
                RegistrationDate, EngineSizeCc, Co2GPerKm, FuelType);
        }
    }

    public enum VehicleType
    {
        Car,
        Van
    }
}