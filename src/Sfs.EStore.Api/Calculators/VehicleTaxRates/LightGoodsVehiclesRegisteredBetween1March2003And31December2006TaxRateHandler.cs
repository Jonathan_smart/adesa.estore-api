using System;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Calculators.VehicleTaxRates
{
    internal class LightGoodsVehiclesRegisteredBetween1March2003And31December2006TaxRateHandler : TaxRateHandler
    {
        public override bool CanHandle(VehicleTaxCalculationArgs args)
        {
            return args.RegistrationDate >= new DateTime(2003, 03, 01)
                && args.RegistrationDate < new DateTime(2006, 12, 31)
                && args.VehicleType == VehicleType.Van;
        }

        public override VehicleTaxRate Handle(VehicleTaxCalculationArgs args)
        {
            return new VehicleTaxRate(Money.From(140, Currency.GBP),
                Money.From(77, Currency.GBP));
        }
    }
}