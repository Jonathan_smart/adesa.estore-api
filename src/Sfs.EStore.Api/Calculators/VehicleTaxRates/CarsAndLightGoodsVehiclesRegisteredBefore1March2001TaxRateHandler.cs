using System;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Calculators.VehicleTaxRates
{
    internal class CarsAndLightGoodsVehiclesRegisteredBefore1March2001TaxRateHandler : TaxRateHandler
    {
        public override bool CanHandle(VehicleTaxCalculationArgs args)
        {
            return args.RegistrationDate < new DateTime(2001, 03, 01);
        }

        public override VehicleTaxRate Handle(VehicleTaxCalculationArgs args)
        {
            const int engineSizeRateCutoff = 1549;

            if (args.EngineSizeCc > engineSizeRateCutoff)
                return new VehicleTaxRate(Money.From(230, Currency.GBP),
                    Money.From(126.50m, Currency.GBP));

            return new VehicleTaxRate(Money.From(145, Currency.GBP),
                Money.From(79.75m, Currency.GBP));
        }
    }
}