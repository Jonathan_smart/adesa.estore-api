using System;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Calculators.VehicleTaxRates
{
    internal class LightGoodsVehiclesRegisteredBetween1January2009And31December2010TaxRateHandler : TaxRateHandler
    {
        public override bool CanHandle(VehicleTaxCalculationArgs args)
        {
            return args.RegistrationDate >= new DateTime(2009, 01, 01)
                   && args.RegistrationDate < new DateTime(2010, 12, 31)
                   && args.VehicleType == VehicleType.Van;
        }

        public override VehicleTaxRate Handle(VehicleTaxCalculationArgs args)
        {
            return new VehicleTaxRate(Money.From(140, Currency.GBP),
                Money.From(77, Currency.GBP));
        }
    }
}