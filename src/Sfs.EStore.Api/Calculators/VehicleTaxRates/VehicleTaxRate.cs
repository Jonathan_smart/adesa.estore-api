using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Calculators.VehicleTaxRates
{
    public class VehicleTaxRate
    {
        public VehicleTaxRate(Money cost12Months, Money cost6Months, char? band = null)
        {
            Cost6Months = cost6Months;
            Cost12Months = cost12Months;
            Band = band;
        }

        public Money Cost12Months { get; private set; }
        public Money Cost6Months { get; private set; }
        public char? Band { get; private set; }
    }
}