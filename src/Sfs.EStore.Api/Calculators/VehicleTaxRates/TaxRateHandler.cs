namespace Sfs.EStore.Api.Calculators.VehicleTaxRates
{
    internal abstract class TaxRateHandler
    {
        public abstract bool CanHandle(VehicleTaxCalculationArgs args);
        public abstract VehicleTaxRate Handle(VehicleTaxCalculationArgs args);
    }
}