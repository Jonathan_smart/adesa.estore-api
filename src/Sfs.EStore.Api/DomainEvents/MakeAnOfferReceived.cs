using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.DomainEvents
{
    public class MakeAnOfferReceived : IDomainEvent
    {
        private readonly MembershipUserIdentity _identity;
        private readonly ListingVehicle _listingVehicle;
        private readonly MakeAnOfferController.MakeAnOfferPostModel _params;

        public MakeAnOfferReceived(MembershipUserIdentity identity, ListingVehicle listingVehicle, MakeAnOfferController.MakeAnOfferPostModel @params)
        {
            _identity = identity;
            _listingVehicle = listingVehicle;
            _params = @params;
        }

        public MembershipUserIdentity Identity { get { return _identity; } }
        public ListingVehicle ListingVehicle { get { return _listingVehicle; } }
        public MakeAnOfferController.MakeAnOfferPostModel Params { get { return _params; } }
    }
}