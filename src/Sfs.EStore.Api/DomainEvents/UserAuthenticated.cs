using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.DomainEvents
{
    public class UserAuthenticated : IDomainEvent
    {
        public MembershipUser User { get; private set; }

        public UserAuthenticated(MembershipUser user)
        {
            User = user;
        }
    }
}