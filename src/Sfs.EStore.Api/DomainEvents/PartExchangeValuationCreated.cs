using System;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.DomainEvents
{
    public class PartExchangeValuationCreated : IDomainEvent
    {
        public PartExchangeValuationCreated(MembershipUserIdentity identity, SavedValuation valuation, PartExValuationModel request)
        {
            Identity = identity;
            Valuation = valuation;
            Request = request;
        }

        public MembershipUserIdentity Identity { get; private set; }
        public SavedValuation Valuation { get; set; }
        public PartExValuationModel Request { get; private set; }
    }
}