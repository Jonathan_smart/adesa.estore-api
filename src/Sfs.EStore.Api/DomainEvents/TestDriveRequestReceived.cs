using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.DomainEvents
{
    public class TestDriveRequestReceived : IDomainEvent
    {
        private readonly MembershipUserIdentity _identity;
        private readonly ListingVehicle _listingVehicle;
        private readonly RequestTestDriveController.RequestATestDrivePostModel _params;

        public TestDriveRequestReceived(MembershipUserIdentity identity, ListingVehicle listingVehicle, RequestTestDriveController.RequestATestDrivePostModel @params)
        {
            _identity = identity;
            _listingVehicle = listingVehicle;
            _params = @params;
        }

        public MembershipUserIdentity Identity { get { return _identity; } }
        public ListingVehicle ListingVehicle { get { return _listingVehicle; } }
        public RequestTestDriveController.RequestATestDrivePostModel Params { get { return _params; } }
    }
}