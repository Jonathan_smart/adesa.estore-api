﻿using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.DomainEvents
{
    public class LotHasBeenReserved : IDomainEvent
    {
        public LotHasBeenReserved(string lotId, int uniqueExternalReferenceNumber)
        {
            LotId = lotId;
            UniqueExternalReferenceNumber = uniqueExternalReferenceNumber;
        }

        public string LotId { get; private set; }
        public int UniqueExternalReferenceNumber { get; private set; }
    }

    public class LotReservationHasBeenRemoved : IDomainEvent
    {
        public LotReservationHasBeenRemoved(string lotId, int uniqueExternalReferenceNumber)
        {
            LotId = lotId;
            UniqueExternalReferenceNumber = uniqueExternalReferenceNumber;
        }

        public string LotId { get; private set; }
        public int UniqueExternalReferenceNumber { get; private set; }
    }

    public class LotHasBeenActivated : IDomainEvent
    {
        public LotHasBeenActivated(string lotId, int uniqueExternalReferenceNumber)
        {
            LotId = lotId;
            UniqueExternalReferenceNumber = uniqueExternalReferenceNumber;
        }

        public string LotId { get; private set; }
        public int UniqueExternalReferenceNumber { get; private set; }
    }

    public class LotHasBeenArchived : IDomainEvent
    {
        public LotHasBeenArchived(string lotId, int uniqueExternalReferenceNumber)
        {
            LotId = lotId;
            UniqueExternalReferenceNumber = uniqueExternalReferenceNumber;
        }

        public string LotId { get; private set; }
        public int UniqueExternalReferenceNumber { get; private set; }
    }

    public class LotHasBeenCancelled : IDomainEvent
    {
        public LotHasBeenCancelled(string lotId, int uniqueExternalReferenceNumber)
        {
            LotId = lotId;
            UniqueExternalReferenceNumber = uniqueExternalReferenceNumber;
        }

        public string LotId { get; private set; }
        public int UniqueExternalReferenceNumber { get; private set; }
    }

    public class LotHasEndedWithSale : IDomainEvent
    {
        private readonly Lot _lot;

        public LotHasEndedWithSale(Lot lot)
        {
            _lot = lot;
        }

        public Lot Lot { get { return _lot; } }
    }

    public class LotHasEndedWithoutSale : IDomainEvent
    {
        private readonly Lot _lot;

        public LotHasEndedWithoutSale(Lot lot)
        {
            _lot = lot;
        }

        public Lot Lot { get { return _lot; } }
    }
}