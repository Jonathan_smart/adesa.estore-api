using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;

namespace Sfs.EStore.Api.DomainEvents
{
    public class AuctionBidHasBeenTaken : IDomainEvent
    {
        private readonly BidOutcome _outcome;
        private readonly ILotOffer _previouslyWinningOffer;
        private readonly Lot _lot;

        public AuctionBidHasBeenTaken(BidOutcome outcome, ILotOffer previouslyWinningOffer, Lot lot)
        {
            _outcome = outcome;
            _previouslyWinningOffer = previouslyWinningOffer;
            _lot = lot;
        }

        public BidOutcome Bid { get { return _outcome; } }
        public Lot Lot { get { return _lot; } }
        public ILotOffer PreviouslyWinningOffer { get { return _previouslyWinningOffer; } }
    }
}