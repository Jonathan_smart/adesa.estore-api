using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Controllers;

namespace Sfs.EStore.Api.DomainEvents
{
    public class ServiceBookingRequestReceived : IDomainEvent
    {
        private readonly MembershipUserIdentity _identity;
        private readonly BookAServiceController.BookAServicePostModel _params;

        public ServiceBookingRequestReceived(MembershipUserIdentity identity, BookAServiceController.BookAServicePostModel @params)
        {
            _identity = identity;
            _params = @params;
        }

        public MembershipUserIdentity Identity { get { return _identity; } }
        public BookAServiceController.BookAServicePostModel Params { get { return _params; } }
    }
}