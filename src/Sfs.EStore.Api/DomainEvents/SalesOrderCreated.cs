namespace Sfs.EStore.Api.DomainEvents
{
    public class SalesOrderCreated : IDomainEvent
    {
        public SalesOrderCreated(string[] salesOrderIds)
        {
            SalesOrderIds = salesOrderIds;
        }

        public string[] SalesOrderIds { get; private set; }
    }
}