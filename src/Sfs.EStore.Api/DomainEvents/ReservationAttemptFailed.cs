using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.DomainEvents
{
    public class ReservationAttemptFailed : IDomainEvent
    {
        private readonly Lot _lot;
        private readonly Vehicle _vehicle;
        private readonly MembershipUser _user;
        private readonly string _errorMessage;

        public ReservationAttemptFailed(Lot lot, Vehicle vehicle, MembershipUser user, string errorMessage)
        {
            _lot = lot;
            _vehicle = vehicle;
            _user = user;
            _errorMessage = errorMessage;
        }

        public Lot Lot { get { return _lot; } }
        public Vehicle Vehicle { get { return _vehicle; } }
        public MembershipUser User { get { return _user; } }
        public string ErrorMessage { get { return _errorMessage; } }
    }
}