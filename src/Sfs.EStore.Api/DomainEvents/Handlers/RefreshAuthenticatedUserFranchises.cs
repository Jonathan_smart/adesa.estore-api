using System.Linq;
using Sfs.Core;
using Sfs.EStore.Api.Data;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.ThirdParties.Ports;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class RefreshAuthenticatedUserFranchises : IHandles<UserAuthenticated>
    {
        private readonly SalesChannel _salesChannel;
        private readonly BusinessUnit _businessUnit;

        public RefreshAuthenticatedUserFranchises(SalesChannel salesChannel, BusinessUnit businessUnit)
        {
            _salesChannel = salesChannel;
            _businessUnit = businessUnit;
        }

        public void Handle(UserAuthenticated args)
        {
            RefreshFranchises(args.User);
        }

        private void RefreshFranchises(MembershipUser user)
        {
            if (_salesChannel == 0) return;
            string saleschannel = _salesChannel.ToString();

            var getContactUsernameCommand = new GetContactUsername(user.Username, _salesChannel);
            getContactUsernameCommand.Execute();
            var contactUsername = getContactUsernameCommand.Result;

            if (contactUsername == null)
            {
                user.Franchises = new string[0];
                return;
            }

            using (var ctx = new NavisionContext())
            {
                ctx.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

                var franchises = ctx.Set<ContactRelationship>().Where(x => x.ContactNo == contactUsername.ContactNo && x.SourceType == 1)
                     .Join(ctx.Set<Customer>(), r => r.SourceNo, c => c.No, (r, c) => c)
                     .Join(ctx.Set<CustomerCampaign>(), c => c.No, f => f.CustomerNo, (c, f) => f).Where(x => !x.Inactive && !x.Exclude)
                     .Select(x => x.FranchiseCode).Distinct();
                      user.Franchises = franchises.ToArray();

                if (!franchises.Any())
                    franchises = ctx.Set<ContactRelationship>().Where(x => x.ContactNo == contactUsername.ContactNo && x.SourceType == 1)
                     .Join(ctx.Set<Customer>(), r => r.SourceNo, c => c.No, (r, c) => c)
                     .Select(x => x.DealerCode).Distinct();
                     user.Franchises = franchises.ToArray();
            }
        }
    }
}