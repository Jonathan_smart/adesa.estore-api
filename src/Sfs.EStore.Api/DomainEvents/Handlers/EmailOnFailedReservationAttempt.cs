using System.Globalization;
using Mandrill;
using Slugify;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class EmailOnFailedReservationAttempt : IHandles<ReservationAttemptFailed>
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _toEmailAddress;
        private readonly string _templateName;

        public EmailOnFailedReservationAttempt(MandrillApi mandrillApi, string toEmailAddress, string templateName)
        {
            _mandrillApi = mandrillApi;
            _toEmailAddress = toEmailAddress;
            _templateName = templateName;
        }

        public void Handle(ReservationAttemptFailed args)
        {
            try
            {
                SendEmail(args);
            }
            catch
            {
                // swallow
            }
        }

        private void SendEmail(ReservationAttemptFailed args)
        {
            var message = new EmailMessage
                              {
                                  to = new[]
                                           {
                                               new EmailAddress
                                                   {email = _toEmailAddress}
                                           }
                              };

            message.AddGlobalVariable("USER_FIRST_NAME", args.User.FirstName ?? "");
            message.AddGlobalVariable("USER_LAST_NAME", args.User.LastName ?? "");
            message.AddGlobalVariable("USER_FULL_NAME", args.User.FullName());
            message.AddGlobalVariable("USER_USERNAME", args.User.Username);
            message.AddGlobalVariable("USER_EMAIL", args.User.EmailAddress);
            message.AddGlobalVariable("LISTING_ID", args.Lot.LotNumber.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("LISTING_SLUG", new SlugHelper().GenerateSlug(args.Lot.Title));
            message.AddGlobalVariable("LISTING_TITLE", args.Lot.Title);
            message.AddGlobalVariable("LISTING_END_TIME", args.Lot.EndDate.HasValue ? args.Lot.EndDate.Value.ToString("dd MMM yyyy HH:mm:ss") : "");
            message.AddGlobalVariable("VEHICLE_REGISTRATION", args.Vehicle.Registration.Plate);
            message.AddGlobalVariable("VEHICLE_MAKE", args.Vehicle.Make);
            message.AddGlobalVariable("VEHICLE_MODEL", args.Vehicle.Model);
            message.AddGlobalVariable("VEHICLE_DERIVATIVE", args.Vehicle.Derivative);
            message.AddGlobalVariable("LISTING_CURRENT_PRICE", args.Lot.CurrentOffer != null ? args.Lot.CurrentOffer.Amount.ToCurrencyString() : "");
            message.AddGlobalVariable("LISTING_CURRENT_PRICE_AMOUNT", args.Lot.CurrentOffer != null ? args.Lot.CurrentOffer.Amount.Amount.ToString(CultureInfo.InvariantCulture) : "");
            message.AddGlobalVariable("LISTING_CURRENT_PRICE_CURRENCY", args.Lot.CurrentOffer != null ? args.Lot.CurrentOffer.Amount.Currency.ToString() : "");
            message.AddGlobalVariable("ERROR_MESSAGE", args.ErrorMessage);

            _mandrillApi.SendMessageAsync(message, _templateName, new TemplateContent[0])
                .Wait();
        }
    }
}