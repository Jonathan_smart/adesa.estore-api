﻿using System;
using System.Linq;
using log4net;
using Sfs.Core;
using Sfs.EStore.Api.Data;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.NavVehicle_Interests;
using Type = Sfs.EStore.Api.NavVehicle_Interests.Type;
using System.Data;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class AddVehicleInterestBid : IDomainEvent, IDisposable
    {
        private readonly NavisionContext _dbContext;
        private readonly int _AuctionNo;
        private readonly string _serialno;
        private readonly string _username;
        private readonly BidOutcome _bidOutcome;
        private readonly ILotOffer _offer;
        private readonly Lot _lot;

        public AddVehicleInterestBid(Lot lot, Vehicle vehicle, string username, BidOutcome bidoutcome, ILotOffer offer)
        {
            if (vehicle.Registration.Plate.Length <= 0) return;

            _dbContext = new NavisionContext();

            using (_dbContext.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                _serialno =
                    _dbContext.Set<SerialNoInformation>()
                        .Where(b => b.ItemNo == "VEHICLE" & b.RegistrationNo == vehicle.Registration.Plate)
                        .Select(n => n.SerialNo).FirstOrDefault();

                _AuctionNo = int.Parse(lot.Sale.Id);
                _offer = offer;
                _bidOutcome = bidoutcome;
                _lot = lot;
                _username = username;

            }
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                _dbContext.Dispose();
            }
            // free native resources
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public string Serialno { get { return _serialno; } }
        public int AuctionNo { get { return _AuctionNo; } }
        public Lot Lot { get { return _lot; } }
        public string Username { get { return _username; } }
        public BidOutcome BidOutcome { get { return _bidOutcome; } }
        public ILotOffer Offer { get { return _offer; } }

    }
    public class AddVehicleInterestBuyItNow : IDomainEvent, IDisposable
    {
        private readonly NavisionContext _dbContext = new NavisionContext();
        private readonly int _AuctionNo;
        private readonly string _serialno;
        private readonly string _contactno;
        private readonly decimal _price;

        public AddVehicleInterestBuyItNow(Lot lot, Vehicle vehicle, string contactno, decimal price)
        {
            if (vehicle.Registration.Plate.Length <= 0) return;

            _AuctionNo = int.Parse(lot.Sale.Id);

            using (_dbContext.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                _serialno =
                    _dbContext.Set<Data.SerialNoInformation>()
                        .Where(b => b.ItemNo == "VEHICLE" & b.RegistrationNo == vehicle.Registration.Plate)
                        .Select(n => n.SerialNo).FirstOrDefault();

                _contactno = contactno;

                _price = price;
            }
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                _dbContext.Dispose();
            }
            // free native resources
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int AuctionNo { get { return _AuctionNo; } }
        public string Serialno { get { return _serialno; } }
        public string ContactNo { get { return _contactno; } }
        public decimal Price { get { return _price; } }
    }
    public class AddVehicleInterestWatch : IDomainEvent, IDisposable
    {
        private readonly NavisionContext _dbContext = new NavisionContext();
        private readonly int _AuctionNo;
        private readonly string _serialno;
        private readonly ContactUsername _contact;

        public AddVehicleInterestWatch(Lot lot, Vehicle vehicle, string username)
        {
            if (vehicle.Registration.Plate.Length <= 0) return;

            _AuctionNo = int.Parse(lot.Sale.Id);

            using (_dbContext.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
            {
                _serialno =
                    _dbContext.Set<Data.SerialNoInformation>()
                        .Where(b => b.ItemNo == "VEHICLE" & b.RegistrationNo == vehicle.Registration.Plate)
                        .Select(n => n.SerialNo).FirstOrDefault();

                _contact =
                    _dbContext.Set<Data.ContactUsername>().Where(x => x.SalesChannelNo == 17).FirstOrDefault(x => x.Username == username);

            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                _dbContext.Dispose();
            }
            // free native resources
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public int AuctionNo { get { return _AuctionNo; } }
        public string Serialno { get { return _serialno; } }
        public ContactUsername Contact { get { return _contact; } }
    }

    public class VehicleInterestUpdate : IHandles<AddVehicleInterestBid>, IHandles<AddVehicleInterestWatch>, IHandles<AddVehicleInterestBuyItNow>, IDisposable
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(ExceptionFilter));
        private readonly Vehicle_Interests_Service _client;
        private readonly BusinessUnit _businessUnit;
        private readonly SalesChannel _saleschannel;
        private readonly NavisionContext _dbContext = new NavisionContext();

        public VehicleInterestUpdate(BusinessUnit businessUnit, SalesChannel saleschannel, Vehicle_Interests_Service client)
        {
            _businessUnit = businessUnit;
            _client = client;
            _saleschannel = saleschannel;
        }

        public void Handle(AddVehicleInterestBid args)
        {
            string business = _businessUnit == "REMARKETING" ? business = "GRS" : _businessUnit;

            var contact = _dbContext.Set<ContactUsername>().Where(x => x.ApplicationAccount.ToUpper() == business).FirstOrDefault(x => x.Username == args.Username);

            if ((args.Serialno == null) || (contact == null)) return;

            if (args.BidOutcome.Outcome == BidOutcome.BidResponse.OutBid)
            {
                using (_dbContext.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    //Get highest bidder.
                    var interest =
                        _dbContext.Set<VehicleInterest>()
                            .Where(x => x.SerialNo == args.Serialno)
                            .OrderByDescending(x => x.MaxBid).ThenByDescending(n => n.Created)
                            .First();

                    // Update highest bidder value.
                    if (interest != null)
                    {
                        Vehicle_Interests highestBidder = _client.Read(Type: "3", Contact_No: interest.ContactNo, Serial_No: interest.SerialNo, Line_No: interest.LineNo);

                        if (highestBidder != null && highestBidder.Value < args.Offer.Amount.Amount)
                        {
                            highestBidder.Value = args.Offer.Amount.Amount;
                            highestBidder.Note = "Estore Auction-Winning Bid";
                            _client.Update(ref highestBidder);
                        }
                    }


                    // Register Outbid Interest 
                    var newInterest = new Vehicle_Interests
                    {
                        Type = Type.Auction_Bid,
                        TypeSpecified = true,
                        Customer_No = string.Empty,
                        Contact_No = contact.ContactNo ?? string.Empty,
                        Sales_Person_Contact_No = string.Empty,
                        Serial_No = args.Serialno,
                        Date_Created = DateTime.UtcNow,
                        Date_CreatedSpecified = true,
                        Note = interest == null ? "Estore Auction-Bid Outbid" : "Estore Auction-Bid Outbid by " + interest.ContactNo,
                        Value = args.Offer.Amount.Amount - args.Lot.Bidding.BidIncrements.Amount,
                        Maximum_Bid = args.Offer.Amount.Amount - args.Lot.Bidding.BidIncrements.Amount,
                        Maximum_BidSpecified = true,
                        ValueSpecified = true,
                        Global_Dimension_1_Code = _businessUnit.ToString(),
                        Auction_No = args.AuctionNo,
                        Auction_NoSpecified = true,
                    };

                    try
                    {
                        _client.Create(ref newInterest);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex.Message);
                    }
                }
            }
            else
            {
                //Update Highest Bidder message
                using (_dbContext.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    //Get highest bidder.
                    var interest =
                        _dbContext.Set<VehicleInterest>()
                            .Where(x => x.SerialNo == args.Serialno)
                            .OrderByDescending(x => x.MaxBid).ThenBy(n => n.Created).FirstOrDefault();

                    if (interest != null)
                    {

                        Vehicle_Interests highestBidder = _client.Read("3", interest.ContactNo, interest.SerialNo,
                                interest.LineNo);

                        if (highestBidder != null)
                        {
                            highestBidder.Note = contact.ContactNo != interest.ContactNo
                                ? contact.ContactNo != null
                                    ? "Estore Auction - Outbid By Contact " + contact.ContactNo
                                    : "Estore Auction - Outbid"
                                : "Estore Auction - Outbid by Self";

                            _client.Update(ref highestBidder);
                        }
                    }
                }

                var navInterest = new Vehicle_Interests
                {
                    Type = Type.Auction_Bid,
                    TypeSpecified = true,
                    Customer_No = string.Empty,
                    Contact_No = contact.ContactNo ?? string.Empty,
                    Sales_Person_Contact_No = string.Empty,
                    Serial_No = args.Serialno,
                    Date_Created = DateTime.UtcNow,
                    Date_CreatedSpecified = true,
                    Note =
                            args.BidOutcome.Outcome == BidOutcome.BidResponse.AcceptedBelowReserve
                                ? "Estore Auction - Wining Bid Accepted Below Reserve"
                                : args.BidOutcome.Outcome == BidOutcome.BidResponse.Accepted ? "Estore Auction - Wining Bid Accepted At Reserve" : "Estore Auction - Wining Bid Accepted Above Reserve",
                    Value = args.BidOutcome.AmountAccepted.Amount,
                    Maximum_Bid = args.BidOutcome.BidAmount.Amount,
                    Maximum_BidSpecified = true,
                    ValueSpecified = true,
                    Global_Dimension_1_Code = _businessUnit.ToString(),
                    Auction_No = args.AuctionNo,
                    Auction_NoSpecified = true,
                };

                try
                {
                    _client.Create(ref navInterest);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.Message);
                }
            }
        }

        public void Handle(AddVehicleInterestWatch args)
        {
        }

        public void Handle(AddVehicleInterestBuyItNow args)
        {
            if ((args.Serialno == null) || (args.ContactNo.Length == 0)) return;

            var navInterest = new Vehicle_Interests
            {
                Type = Type.Auction_Bid,
                TypeSpecified = true,
                Customer_No = string.Empty,
                Contact_No = args.ContactNo,
                Sales_Person_Contact_No = string.Empty,
                Serial_No = args.Serialno,
                Date_Created = DateTime.UtcNow,
                Date_CreatedSpecified = true,
                Note = "Estore interest raised from Buy it Now.",
                Value = args.Price,
                ValueSpecified = true,
                Global_Dimension_1_Code = _businessUnit.ToString(),
                Buy_It_NowSpecified = true,
                Buy_It_Now = true
            };

            try
            {
                _client.Create(ref navInterest);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // dispose managed resources
                _dbContext.Dispose();
            }
            // free native resources
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
