using System;
using System.Linq;
using Mandrill;
using log4net;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class SendEmailOnReservationCreated : IHandles<ReservationCreated>
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SendEmailOnReservationCreated));

        private readonly EmailAddress[] _toEmailAddresses;
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;

        public SendEmailOnReservationCreated(MandrillApi mandrillApi, string toEmailAddresses, string templateName)
        {
            _mandrillApi = mandrillApi;
            _templateName = templateName;
            _toEmailAddresses = toEmailAddresses.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => new EmailAddress { email = x })
                .ToArray();
        }

        public void Handle(ReservationCreated args)
        {
            try
            {
                SendEmail(args);
            }
            catch (Exception ex)
            {
                Logger.Error("Failed to send email on reservation created.", ex);
            }
        }

        private void SendEmail(ReservationCreated args)
        {
            if (string.IsNullOrWhiteSpace(args.RegistrationPlate))
                throw new ArgumentException("registrationNo");

            var message = new EmailMessage { to = _toEmailAddresses };
            message.AddGlobalVariable("registrationNo", args.RegistrationPlate);

            var results = _mandrillApi.SendMessage(message, _templateName, new TemplateContent[0]);
            var succeededCount = results.Count(x => x.Status == EmailResultStatus.Sent);
            var failedCount = results.Count(x => x.Status == EmailResultStatus.Rejected || x.Status == EmailResultStatus.Invalid);

            if (failedCount > 0)
                Logger.WarnFormat("{0} emails succeeded, {1} emails failed; attempted to: {2}",
                                   succeededCount, failedCount,
                                   string.Join(", ", _toEmailAddresses.Select(x => x.email)));
        }
    }
}