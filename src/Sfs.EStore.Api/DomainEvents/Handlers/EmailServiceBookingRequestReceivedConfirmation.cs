using Mandrill;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class EmailServiceBookingRequestReceivedConfirmation : IHandles<ServiceBookingRequestReceived>
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;

        public EmailServiceBookingRequestReceivedConfirmation(MandrillApi mandrillApi, string templateName)
        {
            _mandrillApi = mandrillApi;
            _templateName = templateName;
        }
    
        public void Handle(ServiceBookingRequestReceived args)
        {
            if (string.IsNullOrWhiteSpace(args.Params.Email))
                return;

            try
            {
                SendEmail(args);
            }
            catch
            {
                // swallow
            }
        }

        private void SendEmail(ServiceBookingRequestReceived args)
        {
            var message = new EmailMessage
            {
                to = new[]
                {
                    new EmailAddress
                    {email = args.Params.Email}
                }
            };

            message.AddGlobalVariable("USERNAME", args.Identity == null ? "" : (args.Identity.Name ?? ""));
            message.AddGlobalVariable("FULL_NAME", args.Params.FullName ?? "");
            message.AddGlobalVariable("ADDRESS_LINE1", args.Params.AddressLine1 ?? "");
            message.AddGlobalVariable("POSTCODE", args.Params.PostCode ?? "");
            message.AddGlobalVariable("PHONE", args.Params.Telephone ?? "");
            message.AddGlobalVariable("EMAIL", args.Params.Email ?? "");

            message.AddGlobalVariable("REGISTRATION", args.Params.Registration ?? "");
            message.AddGlobalVariable("MAKE", args.Params.Make ?? "");
            message.AddGlobalVariable("MODEL", args.Params.Model ?? "");
            message.AddGlobalVariable("ENGINE_SIZE", args.Params.EngineSize ?? "");
            message.AddGlobalVariable("MILEAGE", args.Params.Mileage ?? "");
            message.AddGlobalVariable("MOT_REQUIRED", args.Params.MotRequired.ToString());
            message.AddGlobalVariable("COURTESY_VEHICLE_REQUIRED", args.Params.CourtesyVehicleRequired.ToString());
            message.AddGlobalVariable("PREFERRED_DATE", args.Params.PreferredDate ?? "");
            message.AddGlobalVariable("COMMENTS", args.Params.Comments ?? "");
            
            _mandrillApi.SendMessageAsync(message, _templateName, new TemplateContent[0])
                .Wait();
        }
    }
}