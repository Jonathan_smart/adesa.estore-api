using System.Globalization;
using Mandrill;
using Slugify;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class EmailTestDriveRequestReceivedConfirmation : IHandles<TestDriveRequestReceived>
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;

        public EmailTestDriveRequestReceivedConfirmation(MandrillApi mandrillApi, string templateName)
        {
            _mandrillApi = mandrillApi;
            _templateName = templateName;
        }

        public void Handle(TestDriveRequestReceived args)
        {
            if (string.IsNullOrWhiteSpace(args.Params.Email))
                return;

            try
            {
                SendEmail(args);
            }
            catch
            {
                // swallow
            }
        }

        private void SendEmail(TestDriveRequestReceived args)
        {
            var message = new EmailMessage
            {
                to = new[]
                {
                    new EmailAddress
                    {email = args.Params.Email}
                }
            };

            message.AddGlobalVariable("USERNAME", args.Identity == null ? "" : (args.Identity.Name ?? ""));
            message.AddGlobalVariable("FULL_NAME", args.Params.FullName ?? "");
            message.AddGlobalVariable("ADDRESS_LINE1", args.Params.AddressLine1 ?? "");
            message.AddGlobalVariable("POSTCODE", args.Params.PostCode ?? "");
            message.AddGlobalVariable("PHONE", args.Params.Telephone ?? "");
            message.AddGlobalVariable("EMAIL", args.Params.Email ?? "");

            message.AddGlobalVariable("PREFERRED_DATE", args.Params.PreferredDate ?? "");

            var listingVehicle = args.ListingVehicle;
            message.AddGlobalVariable("LISTING_ID", listingVehicle.Listing.LotNumber.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("LISTING_SLUG", new SlugHelper().GenerateSlug(listingVehicle.Listing.Title));
            message.AddGlobalVariable("LISTING_TITLE", listingVehicle.Listing.Title);
            message.AddGlobalVariable("VEHICLE_REGISTRATION", listingVehicle.Vehicle.Registration.Plate);
            message.AddGlobalVariable("VEHICLE_MAKE", listingVehicle.Vehicle.Make);
            message.AddGlobalVariable("VEHICLE_MODEL", listingVehicle.Vehicle.Model);
            message.AddGlobalVariable("VEHICLE_DERIVATIVE", listingVehicle.Vehicle.Derivative);

            var currentPrice = listingVehicle.Listing.CurrentPrice();
            message.AddGlobalVariable("LISTING_CURRENT_PRICE", currentPrice != null ? currentPrice.ToCurrencyString() : "");
            message.AddGlobalVariable("LISTING_CURRENT_PRICE_AMOUNT", currentPrice != null ? currentPrice.Amount.ToString(CultureInfo.InvariantCulture) : "");
            message.AddGlobalVariable("LISTING_CURRENT_PRICE_CURRENCY", currentPrice != null ? currentPrice.Currency.ToString() : "");

            _mandrillApi.SendMessageAsync(message, _templateName, new TemplateContent[0])
                .Wait();
        }
    }
}