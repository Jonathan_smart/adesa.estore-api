﻿using System;
using Sfs.EStore.Api.Entities;
using log4net;
using Sfs.EStore.Api.EStoreActionsService;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class UpdateNavisionWithStatusChange : IHandles<LotHasBeenReserved>,
                                                  IHandles<LotReservationHasBeenRemoved>,
                                                  IHandles<LotHasBeenActivated>,
                                                  IHandles<LotHasBeenArchived>,
                                                  IHandles<LotHasBeenCancelled>
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof (UpdateNavisionWithStatusChange));
        private readonly eStore_Actions_Service _client;

        public UpdateNavisionWithStatusChange(eStore_Actions_Service client)
        {
            _client = client;
        }

        public void Handle(LotHasBeenReserved args)
        {
            UpdateNavisionWithStatus(args.LotId, args.UniqueExternalReferenceNumber, LotStatusTypes.Reserved);
        }

        public void Handle(LotReservationHasBeenRemoved args)
        {
            UpdateNavisionWithStatus(args.LotId, args.UniqueExternalReferenceNumber, LotStatusTypes.Active);
        }

        public void Handle(LotHasBeenActivated args)
        {
            UpdateNavisionWithStatus(args.LotId, args.UniqueExternalReferenceNumber, LotStatusTypes.Active);
        }

        public void Handle(LotHasBeenArchived args)
        {
            UpdateNavisionWithStatus(args.LotId, args.UniqueExternalReferenceNumber, LotStatusTypes.Archived);
        }

        public void Handle(LotHasBeenCancelled args)
        {
            UpdateNavisionWithStatus(args.LotId, args.UniqueExternalReferenceNumber, LotStatusTypes.Cancelled);
        }

        private void UpdateNavisionWithStatus(string lotId, int uniqueExternalReferenceNumber, LotStatusTypes newStatus)
        {
            try
            {
                var action = new eStore_Actions
                                 {
                                     Direction = Direction.Inbound,
                                     DirectionSpecified = true,
                                     eStore_Action_Type = "STATUS",
                                     SNP_Sale_Ch_Entry_No = uniqueExternalReferenceNumber,
                                     SNP_Sale_Ch_Entry_NoSpecified = true,
                                     Action_Value = newStatus.ToString(),
                                     Channel_Type = "ESTORE"
                                 };

                Logger.InfoFormat("Calling Navision eStore_Actions page: SNP_Sale_Ch_Entry_No: {0}; Action_Value: {1}",
                    action.SNP_Sale_Ch_Entry_No,
                    action.Action_Value);

                _client.Create(ref action);
            }
            catch (Exception ex)
            {
                Logger.Error(
                    string.Format("[\"lot\":\"{0}\";\"uniqueExternalReferenceNumber\":\"{1}\";newStatus:\"{2}\"] Failed to insert eStore action", lotId, uniqueExternalReferenceNumber,
                                  newStatus),
                    ex);
            }
        }
    }
}