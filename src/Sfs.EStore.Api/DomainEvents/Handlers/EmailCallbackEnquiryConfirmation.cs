using Mandrill;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class EmailCallbackEnquiryConfirmation : IHandles<CallbackRequestSent>
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;

        public EmailCallbackEnquiryConfirmation(MandrillApi mandrillApi, string templateName)
        {
            _mandrillApi = mandrillApi;
            _templateName = templateName;
        }

        public void Handle(CallbackRequestSent args)
        {
            try
            {
                SendEmail(args);
            }
            catch
            {
                // swallow
            }
        }

        private void SendEmail(CallbackRequestSent args)
        {
            var message = new EmailMessage
                              {
                                  to = new[]
                                           {
                                               new EmailAddress
                                                   {email = args.Contact.Email}
                                           }
                              };

            message.AddGlobalVariable("USER_FIRST_NAME", args.Contact.FirstName ?? "");
            message.AddGlobalVariable("USER_LAST_NAME", args.Contact.LastName ?? "");
            message.AddGlobalVariable("USER_EMAIL", args.Contact.Email);
            message.AddGlobalVariable("USER_TELEPHONE", args.Contact.TelephoneNumber);
            message.AddGlobalVariable("ENQUIRY_MESSAGE", args.Message);
            
            _mandrillApi.SendMessageAsync(message, _templateName, new TemplateContent[0])
                .Wait();
        }
    }
}