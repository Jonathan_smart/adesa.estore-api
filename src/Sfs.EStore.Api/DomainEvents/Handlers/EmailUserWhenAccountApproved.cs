﻿using System.Linq;
using Mandrill;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class EmailUserWhenAccountApproved : IHandles<UserApproved>
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _templateName;

        public EmailUserWhenAccountApproved(MandrillApi mandrillApi, string templateName)
        {
            _mandrillApi = mandrillApi;
            _templateName = templateName;
        }

        public void Handle(UserApproved args)
        {
            var message = new EmailMessage
            {
                to = new[]
                {
                    new EmailAddress
                    {
                        email = args.EmailAddress
                    }
                }
            };

            message.AddGlobalVariable("USERNAME", args.Username);
            message.AddGlobalVariable("FIRST_NAME", args.FirstName);
            message.AddGlobalVariable("LAST_NAME", args.LastName);
            message.AddGlobalVariable("EMAIL_ADDRESS", args.EmailAddress);

            var mandrillResult = _mandrillApi
                .SendMessageAsync(message, _templateName, new TemplateContent[0])
                .Result;
            if (mandrillResult.Any(x => x.Status == EmailResultStatus.Rejected || x.Status == EmailResultStatus.Invalid))
                throw new MandrillException("one or more emails were unsuccessful");
        }
    }
}