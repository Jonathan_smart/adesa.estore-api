using Raven.Client;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Messages.Events;
using Shuttle.ESB.Core;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class PublishNewBidHasBeenAcceptedMessage : IHandles<AuctionBidHasBeenTaken>
    {
        private readonly IServiceBus _bus;
        private readonly IDocumentStore _docstore;
        private readonly IApiApplicationPrincipal _principal;

        public PublishNewBidHasBeenAcceptedMessage(IServiceBus bus, IDocumentStore docstore, IApiApplicationPrincipal principal)
        {
            _bus = bus;
            _docstore = docstore;
            _principal = principal;
        }

        public void Handle(AuctionBidHasBeenTaken args)
        {
            if (!args.Bid.HasBeenAccepted()) return;

            PublishMessage(args);
        }

        private void PublishMessage(AuctionBidHasBeenTaken args)
        {
            var session = _docstore.OpenSession();

            NewBidHasBeenAccepted.NewBidHasBeenAccepted_UserDetail previousWinningOfferUser = null;

            if (args.PreviouslyWinningOffer != null)
            {
                var user = session.Load<MembershipUser>(args.PreviouslyWinningOffer.UserId);

                previousWinningOfferUser = new NewBidHasBeenAccepted.NewBidHasBeenAccepted_UserDetail
                {
                    EmailAddress = user.EmailAddress,
                    FirstName = user.FirstName,
                    FullName = user.FullName(),
                    Id = user.Id,
                    LastName = user.LastName
                };
            }

            var currentOfferMembershipUser = session.Load<MembershipUser>(args.Lot.CurrentOffer.UserId);
            var currentOfferUser = new NewBidHasBeenAccepted.NewBidHasBeenAccepted_UserDetail
            {
                EmailAddress = currentOfferMembershipUser.EmailAddress,
                FirstName = currentOfferMembershipUser.FirstName,
                FullName = currentOfferMembershipUser.FullName(),
                Id = currentOfferMembershipUser.Id,
                LastName = currentOfferMembershipUser.LastName
            };

            var vehicle = session.Load<Vehicle>(args.Lot.VehicleId);

            _bus.Publish(new NewBidHasBeenAccepted
            {
                TenantId = _principal.Identity.AccessKeyId,
                ListingId = args.Lot.LotNumber,
                Listing = new NewBidHasBeenAccepted.NewBidHasBeenAccepted_ListingDetail
                {
                    Title = args.Lot.Title,
                    EndsAt = args.Lot.EndDate,
                    StartsAt = args.Lot.StartDate,
                    RegistrationPlate = vehicle.Registration.Plate,
                    Make = vehicle.Make,
                    Model = vehicle.Model,
                    Derivative = vehicle.Derivative
                },
                CurrentOffer = new NewBidHasBeenAccepted.NewBidHasBeenAccepted_OfferDetail
                {
                    Amount = new Messages.Money
                    {
                        Amount = args.Lot.CurrentOffer.Amount.Amount,
                        Currency = args.Lot.CurrentOffer.Amount.Currency.ToString()
                    },
                    Date = args.Lot.CurrentOffer.Date,
                    User = currentOfferUser
                },
                PreviouslyWinningOffer = args.PreviouslyWinningOffer == null
                    ? null
                    : new NewBidHasBeenAccepted.NewBidHasBeenAccepted_OfferDetail
                    {
                        Amount = new Messages.Money
                        {
                            Amount = args.PreviouslyWinningOffer.Amount.Amount,
                            Currency = args.PreviouslyWinningOffer.Amount.Currency.ToString()
                        },
                        Date = args.PreviouslyWinningOffer.Date,
                        User = previousWinningOfferUser
                    }
            });
        }
    }
}