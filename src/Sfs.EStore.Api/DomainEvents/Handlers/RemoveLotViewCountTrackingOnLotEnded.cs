﻿using log4net;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class RemoveLotViewCountTrackingOnLotEnded : IHandles<LotHasBeenArchived>,
                                                  IHandles<LotHasBeenCancelled>,
                                                  IHandles<LotHasEndedWithSale>,
                                                  IHandles<LotHasEndedWithoutSale>
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(RemoveLotViewCountTrackingOnLotEnded));

        public void Handle(LotHasBeenArchived args)
        {
            RemoveLotViewCount(args.LotId);
        }

        public void Handle(LotHasBeenCancelled args)
        {
            RemoveLotViewCount(args.LotId);
        }

        public void Handle(LotHasEndedWithSale args)
        {
            RemoveLotViewCount(args.Lot.Id);
        }

        public void Handle(LotHasEndedWithoutSale args)
        {
            RemoveLotViewCount(args.Lot.Id);
        }

        private void RemoveLotViewCount(string lotId)
        {
            // TODO: Delete lot view count from session
        }
    }
}