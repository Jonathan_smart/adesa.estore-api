﻿using System.Globalization;
using Mandrill;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class EmailSellerPartExchangeValuationRequested : IHandles<PartExchangeValuationCreated>
    {
        private readonly MandrillApi _mandrillApi;
        private readonly string _toEmailAddress;
        private readonly string _templateName;

        public EmailSellerPartExchangeValuationRequested(MandrillApi mandrillApi, string toEmailAddress, string templateName)
        {
            _mandrillApi = mandrillApi;
            _toEmailAddress = toEmailAddress;
            _templateName = templateName;
        }

        public void Handle(PartExchangeValuationCreated args)
        {
            try
            {
                SendEmail(args);
            }
            catch
            {
                // swallow
            }
        }

        private void SendEmail(PartExchangeValuationCreated args)
        {
            var message = new EmailMessage
            {
                to = new[]
                {
                    new EmailAddress
                    {email = _toEmailAddress}
                }
            };


            message.AddGlobalVariable("USERNAME", args.Identity.IsAuthenticated ? (args.Identity.Name ?? "") : "");
            message.AddGlobalVariable("FULL_NAME", args.Request.Contact.FullName ?? "");
            message.AddGlobalVariable("POSTCODE", args.Request.Contact.PostCode ?? "");
            message.AddGlobalVariable("PHONE", args.Request.Contact.Telephone ?? "");
            message.AddGlobalVariable("EMAIL", args.Request.Contact.Email ?? "");

            message.AddGlobalVariable("REGISTRATION", args.Valuation.Registration ?? "");
            message.AddGlobalVariable("MILEAGE", args.Valuation.Mileage.ToString(CultureInfo.InvariantCulture));
            message.AddGlobalVariable("BELOW_PRICE", args.Valuation.Valuation.Below.ToCurrencyString() ?? "");
            message.AddGlobalVariable("AVERAGE_PRICE", args.Valuation.Valuation.Average.ToCurrencyString() ?? "");
            message.AddGlobalVariable("CLEAN_PRICE", args.Valuation.Valuation.Clean.ToCurrencyString() ?? "");
            message.AddGlobalVariable("RETAIL_PRICE", args.Valuation.Valuation.Retail.ToCurrencyString() ?? "");
            message.AddGlobalVariable("MAKE", args.Valuation.Valuation.Vehicle.Make ?? "");
            message.AddGlobalVariable("MODEL", args.Valuation.Valuation.Vehicle.Model ?? "");
            message.AddGlobalVariable("DERIVATIVE", args.Valuation.Valuation.Vehicle.Derivative ?? "");


            if (args.Request.TenantEnrichment != null)
            {
                message.AddGlobalVariable("ENRICHMENT_ORIGINAL_VISIT_SOURCE", args.Request.TenantEnrichment.OriginalVisitSource);
                message.AddGlobalVariable("ENRICHMENT_ORIGINAL_VISIT_TYPE", args.Request.TenantEnrichment.OriginalVisitType);
                message.AddGlobalVariable("ENRICHMENT_ORIGINAL_VISIT_AT", args.Request.TenantEnrichment.OriginalVisitAt.ToString());
                message.AddGlobalVariable("ENRICHMENT_LATEST_VISIT_SOURCE", args.Request.TenantEnrichment.LatestVisitSource);
                message.AddGlobalVariable("ENRICHMENT_LATEST_VISIT_TYPE", args.Request.TenantEnrichment.LatestVisitType);
                message.AddGlobalVariable("ENRICHMENT_LATEST_VISIT_AT", args.Request.TenantEnrichment.LatestVisitAt.ToString());
                message.AddGlobalVariable("ENRICHMENT_IP_ADDRESS", args.Request.TenantEnrichment.IpAddress ?? "");
            }

            _mandrillApi.SendMessageAsync(message, _templateName, new TemplateContent[0])
                .Wait();
        }
    }
}