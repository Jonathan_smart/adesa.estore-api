using System;
using System.Linq;
using Mandrill;
using log4net;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class SendEmailOnSalesOrderCreated : IHandles<SalesOrderCreated>
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SendEmailOnSalesOrderCreated));

        private readonly MandrillApi _mandrillApi;
        private readonly EmailAddress[] _toEmailAddresses;
        private readonly string _templateName;

        public SendEmailOnSalesOrderCreated(MandrillApi mandrillApi, string toEmailAddresses, string templateName)
        {
            _mandrillApi = mandrillApi;
            _templateName = templateName;

            _toEmailAddresses = toEmailAddresses.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => new EmailAddress { email = x })
                .ToArray();
        }

        public void Handle(SalesOrderCreated args)
        {
            try
            {
                SendEmail(args);
            }
            catch (Exception ex)
            {
                Logger.Error("Failed to send email on sales order created.", ex);
            }
        }

        private void SendEmail(SalesOrderCreated args)
        {
            if (args.SalesOrderIds == null || !args.SalesOrderIds.Any())
                return;

            var message = new EmailMessage {to = _toEmailAddresses};
            message.AddGlobalVariable("ids", string.Join(", ", args.SalesOrderIds));

            var results = _mandrillApi.SendMessage(message, _templateName, new TemplateContent[0]);
            var succeededCount = results.Count(x => x.Status == EmailResultStatus.Sent);
            var failedCount =
                results.Count(x => x.Status == EmailResultStatus.Rejected || x.Status == EmailResultStatus.Invalid);

            if (failedCount > 0)
                Logger.ErrorFormat("{0} emails succeeded, {1} emails failed; attempted to: {2}",
                                   succeededCount, failedCount,
                                   string.Join(", ", _toEmailAddresses.Select(x => x.email)));
        }
    }
}