using System;
using System.Collections.Generic;
using System.Linq;
using Raven.Client;
using Sfs.Core;
using Sfs.Darker.CommandProcessor;
using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.ThirdParties.Ports;
using Sfs.EStore.Messages.Events;
using Sfs.Orders.Ports;
using Sfs.Orders.Ports.AddToBasket;
using Shuttle.ESB.Core;
using Sfs.EStore.Api.Controllers;
using log4net;

namespace Sfs.EStore.Api.DomainEvents.Handlers
{
    public class PublishAuctionLotEndedEvents : RavenEStoreController, IHandles<LotHasEndedWithSale>,
        IHandles<LotHasEndedWithoutSale>
    {
        private readonly IServiceBus _bus;
        private readonly IDocumentStore _docstore;
        private readonly IApiApplicationPrincipal _principal;
        private readonly log4net.ILog _logger = LogManager.GetLogger(typeof(PublishAuctionLotEndedEvents));

        private readonly ICommandProcessor _commandProcessor;
        private readonly BasketLimit _basketLimit;
        private readonly SalesChannel _salesChannel;
        private readonly BusinessUnit _businessUnit;
        private readonly ISerialNoInformationByRegistrationQuery _serialNoQuery;

        public PublishAuctionLotEndedEvents(IServiceBus bus, IDocumentStore docstore, IApiApplicationPrincipal principal, ICommandProcessor commandProcessor,
            BasketLimit basketLimit,
            SalesChannel salesChannel,
            BusinessUnit businessUnit,
            ISerialNoInformationByRegistrationQuery serialNoQuery)
        {
            _bus = bus;
            _docstore = docstore;
            _principal = principal;
            _commandProcessor = commandProcessor;
            _basketLimit = basketLimit;
            _salesChannel = salesChannel;
            _businessUnit = businessUnit;
            _serialNoQuery = serialNoQuery;
        }

        public void Handle(LotHasEndedWithSale args)
        {
            PublishMessage<AuctionLotHasEndedWithSale>(args.Lot);
        }

        public void Handle(LotHasEndedWithoutSale args)
        {
            PublishMessage<AuctionLotHasEndedWithoutSale>(args.Lot);
        }

        private void PublishMessage<TMessage>(Lot lot)
            where TMessage : AuctionLotHasEnded, new()
        {
            if (!IsAuctionLot(lot.ListingType)) return;

            _bus.Publish(CreateMessage<TMessage>(lot));
        }

        private TMessage CreateMessage<TMessage>(Lot lot)
            where TMessage : AuctionLotHasEnded, new()
        {
            var session = _docstore.OpenSession();
            var biddingUserIds = lot.Bidding.BidLimits.Select(x => x.UserId).Distinct();
            var biddingUsers = session.Load<MembershipUser>(biddingUserIds);
            var highestBidderUser = lot.Bidding.Bids.OrderByDescending(x => x.Amount).First();
            var highestBidder = biddingUsers.First(u => highestBidderUser != null && u.Id == highestBidderUser.UserId);
            var vehicle = session.Load<Vehicle>(lot.VehicleId);

            if (lot.Status == LotStatusTypes.EndedWithSales && lot.BuyitNowSale == null) AddBidToBasket(lot, highestBidder.Id);

            return new TMessage
            {
                TenantId = _principal.Identity.AccessKeyId,
                ListingId = lot.LotNumber,
                OutbidOffers = lot.Bidding.BidLimits
                .Where(x => x.UserId != highestBidder.Id)
                .Aggregate(new List<MaximumBidLimit>(), (workingSet, next) =>
                {
                    var userOffer = workingSet.FirstOrDefault(x => x.UserId == next.UserId);

                    if (userOffer == null) workingSet.Add(next);
                    else if (userOffer.Amount.Amount < next.Amount.Amount)
                    {
                        workingSet.Remove(userOffer);
                        workingSet.Add(next);
                    }

                    return workingSet;
                })
                .Select(bid =>
                {
                    var user = biddingUsers.First(u => u.Id == bid.UserId);

                    return new AuctionLotHasEnded.AuctionLotHasEnded_BidDetail
                    {
                        Amount = new Messages.Money
                        {
                            Amount = bid.Amount.Amount,
                            Currency = bid.Amount.Currency.ToString()
                        },
                        Date = bid.Date,
                        User = new AuctionLotHasEnded.AuctionLotHasEnded_UserDetail
                        {
                            Id = user.Id,
                            EmailAddress = user.EmailAddress,
                            FirstName = user.FirstName,
                            FullName = user.FullName(),
                            LastName = user.LastName
                        }
                    };
                }).ToArray(),
                HighestOffer = new AuctionLotHasEnded.AuctionLotHasEnded_BidDetail
                {
                    Amount = new Messages.Money
                    {
                        Amount = lot.CurrentOffer.Amount.Amount,
                        Currency = lot.CurrentOffer.Amount.Currency.ToString()
                    },
                    Date = lot.CurrentOffer.Date,
                    User = new AuctionLotHasEnded.AuctionLotHasEnded_UserDetail
                    {
                        Id = highestBidder.Id,
                        EmailAddress = highestBidder.EmailAddress,
                        FirstName = highestBidder.FirstName,
                        FullName = highestBidder.FullName(),
                        LastName = highestBidder.LastName
                    }
                },
                Listing = new AuctionLotHasEnded.AuctionLotHasEnded_ListingDetail
                {
                    Derivative = vehicle.Derivative,
                    EndsAt = lot.EndDate,
                    Make = vehicle.Make,
                    Model = vehicle.Model,
                    RegistrationPlate = vehicle.Registration.Plate,
                    StartsAt = lot.StartDate,
                    Title = lot.Title
                }
            };
        }

        private static bool IsAuctionLot(ListingType listingType)
        {
            return listingType != ListingType.FixedPrice;
        }

        private void AddBidToBasket(Lot lot, string userName)
        {
            try
            {
                using (var session = _docstore.OpenSession())
                {
                    var user = session.Load<MembershipUser>(userName);

                    //Requirement get Sales channnel for contact selection.
                    var contact =
                       ExecuteCommand(new GetContactUsername(user.Username, new SalesChannel(2)));

                    var vehicle = session.Load<Vehicle>(lot.VehicleId);

                    var serial = _serialNoQuery.Handle(new[] { vehicle.Registration.Plate }).First();

                    if (user.Username != null)
                    {
                        var cmd = new AddToBasket(serial.Key,
                            new AddToBasket.SerialNoPricesSalesChannel(lot.UniqueExternalReferenceNumber),
                            lot.Bidding.Bids.OrderByDescending(x => x.Amount).First().Amount.Convert(), _businessUnit, contact.ContactNo, null,
                            string.Empty, false, true)
                        {
                            CustomerNo = string.Empty
                        };

                        _commandProcessor.Send(cmd);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
        }
    }
}