namespace Sfs.EStore.Api.DomainEvents
{
    public class ReservationCreated: IDomainEvent
    {
        public ReservationCreated(string registrationPlate)
        {
            RegistrationPlate = registrationPlate;
        }

        public string RegistrationPlate { get; private set; }
    }
}