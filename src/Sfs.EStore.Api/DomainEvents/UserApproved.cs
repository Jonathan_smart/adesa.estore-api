﻿using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.DomainEvents
{
    // TODO: FUTURE WORK This isn't a domain event, it should happen out of process
    // and should only 'send' if the event actually succeeded
    public class UserApproved : IDomainEvent
    {
        private readonly MembershipUser _user;

        public UserApproved(MembershipUser user)
        {
            _user = user;
        }

        public string Username { get { return _user.Username; } }
        public string FirstName { get { return _user.FirstName; } }
        public string LastName { get { return _user.LastName; } }
        public string EmailAddress { get { return _user.EmailAddress; } }
    }
}