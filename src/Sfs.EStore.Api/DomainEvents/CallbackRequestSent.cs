using Sfs.EStore.Api.Authentication;
using Sfs.EStore.Api.ThirdParties;

namespace Sfs.EStore.Api.DomainEvents
{
    public class CallbackRequestSent : IDomainEvent
    {
        private readonly MembershipUserIdentity _identity;
        private readonly CallbackRequestContact _contact;
        private readonly string _message;
        private readonly int[] _listingIds;

        public CallbackRequestSent(MembershipUserIdentity identity, CallbackRequestContact contact, string message, int[] listingIds)
        {
            _identity = identity;
            _contact = contact;
            _message = message;
            _listingIds = listingIds;
        }

        public MembershipUserIdentity Identity { get { return _identity; } }
        public CallbackRequestContact Contact { get { return _contact; } }
        public string Message { get { return _message; } }
        public int[] ListingIds { get { return _listingIds; } }
    }
}