using System;
using System.Runtime.Serialization;

namespace Sfs.EStore.Api.Models
{
    [Serializable]
    public class BidsDisabledException : Exception
    {
        public BidsDisabledException(string lotId)
        {
            LotId = lotId;
        }

        public BidsDisabledException(string lotId, string message) : base(message)
        {
            LotId = lotId;
        }

        public BidsDisabledException(string lotId, string message, Exception inner) : base(message, inner)
        {
            LotId = lotId;
        }

        protected BidsDisabledException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
            LotId = info.GetString("LotId");
        }

        public string LotId { get; set; }

    }
}