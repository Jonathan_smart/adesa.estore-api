using System;
using System.Runtime.Serialization;

namespace Sfs.EStore.Api.Models
{
    [Serializable]
    public class NoBidHandlerException : Exception
    {
        public NoBidHandlerException()
        {
        }

        public NoBidHandlerException(string message) : base(message)
        {
        }

        public NoBidHandlerException(string message, Exception inner) : base(message, inner)
        {
        }

        protected NoBidHandlerException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }
    }
}