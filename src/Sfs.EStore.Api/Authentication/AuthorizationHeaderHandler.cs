using System;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Raven.Client;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.ThirdParties.Ports;
using Sfs.Core;

namespace Sfs.EStore.Api.Authentication
{
    public class AuthorizationHeaderHandler : DelegatingHandler
    {
        private readonly IDocumentStore _apiDocStore;
        private readonly Func<string, IDocumentStore> _accountDocStoreFactory;

        public AuthorizationHeaderHandler(IDocumentStore apiDocStore,
            Func<string, IDocumentStore> accountDocStoreFactory)
        {
            _apiDocStore = apiDocStore;
            _accountDocStoreFactory = accountDocStoreFactory;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Thread.CurrentPrincipal = NullApiApplicationPrincipal.Instance;
            if (HttpContext.Current != null)
                HttpContext.Current.User = Thread.CurrentPrincipal;

            string accessKeyId;
            var token = ExtractCredentialsFromHeaders(HttpContext.Current, out accessKeyId);

            if (string.IsNullOrWhiteSpace(accessKeyId))
                return base.SendAsync(request, cancellationToken);

            var account = GetAccountForAccessKeyId(accessKeyId);

            if (account == null)
                return base.SendAsync(request, cancellationToken);

            MembershipUser user = null;
            if (token != null)
            {
                using (var session = _accountDocStoreFactory(account.DatabaseName).OpenSession())
                {
                    user = session.Query<MembershipUser>().FirstOrDefault(x => x.Username == token.Username);

                    //Update document Store 
                    if (user != null)
                    {
                        string business = account.DatabaseName.Substring(account.DatabaseName.Length - 3, 3);
                        GetContactComms contactComms = new GetContactComms(user.Username, new BusinessUnit(business));
                        contactComms.Execute();
                        user.PhoneNumber = contactComms.Result;

                        session.SaveChanges();
                    }
                }
            }

            var userInfo = user == null ? MembershipUserInfo.Null() : MembershipUserInfo.CreateFrom(user);

            Thread.CurrentPrincipal = new ApiApplicationPrincipal(
                new AccountIdentity(account),
                new MembershipUserPrincipal(new MembershipUserIdentity(userInfo)));
            if (HttpContext.Current != null)
                HttpContext.Current.User = Thread.CurrentPrincipal;

            return base.SendAsync(request, cancellationToken);
        }

        private Account GetAccountForAccessKeyId(string accessKeyId)
        {
            using (var session = _apiDocStore.OpenSession())
            {
                var account = session.Query<Account>()
                    .FirstOrDefault(x => x.AccessKeyId == accessKeyId);

                return account;
            }
        }

        private const string Scheme = "SFS";

        private Token ExtractCredentialsFromHeaders(HttpContext context, out string apiKey)
        {
            apiKey = null;

            var authorization = context.Request.Headers["Authorization"];

            if (string.IsNullOrEmpty(authorization))
                return null;

            if (!authorization.StartsWith(Scheme))
                return null;

            try
            {
                var encodedAccessKeySignature = authorization.Substring(Scheme.Length).Trim();
                var accessKeySignature = Encoding.UTF8.GetString(Convert.FromBase64String(encodedAccessKeySignature));

                if (string.IsNullOrWhiteSpace(accessKeySignature))
                    return null;

                var parts = accessKeySignature.Split(':');

                if (parts.Length < 2)
                    return null;

                apiKey = parts[0];
                var encryptedCredentials = parts[1];

                if (string.IsNullOrWhiteSpace(encryptedCredentials))
                    return null;

                var credentials = UserToken.Decrypt(encryptedCredentials);

                return credentials;
            }
            catch (CryptographicException)
            {
                return null;
            }
            catch (FormatException)
            {
                return null;
            }
        }
    }

    public class Token
    {
        public Token(string username)
        {
            Username = username;
        }

        public string Username { get; private set; }
    }
}