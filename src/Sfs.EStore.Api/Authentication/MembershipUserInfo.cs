using System.Linq;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Authentication
{
    public class MembershipUserInfo
    {
        public MembershipUserInfo(string id, string username, string[] roles = null)
        {
            Id = id;
            Username = username;
            Roles = roles;
        }

        private string _id;
        public string Id
        {
            get { return _id; }
            protected set { _id = value ?? ""; }
        }

        private string _username = "";
        public string Username
        {
            get { return _username; }
            protected set { _username = value ?? ""; }
        }

        private string[] _roles = new string[0];
        public string[] Roles
        {
            get { return _roles; }
            protected set { _roles = (value ?? new string[0]).Where(x=>x!=null).Select(x => x.ToLower()).ToArray(); }
        }

        private string _phoneNumber;
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            protected set { _phoneNumber = value ?? ""; }
        }

        public static MembershipUserInfo CreateFrom(MembershipUser user)
        {
            return new MembershipUserInfo(user.Id, user.Username, user.PseudoRoles());
        }

        public static MembershipUserInfo CreateFrom(string id, string username, string[] roles = null)
        {
            return new MembershipUserInfo(id, username, roles);
        }

        private static readonly MembershipUserInfo NullInstance = new MembershipUserInfo("", "");
        public static MembershipUserInfo Null()
        {
            return NullInstance;
        }
    }
}
