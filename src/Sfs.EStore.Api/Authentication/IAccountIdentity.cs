using System.Collections.Generic;
using System.Security.Principal;
using Sfs.EStore.Api.Entities;

namespace Sfs.EStore.Api.Authentication
{
    public interface IAccountIdentity : IIdentity
    {
        string AccessKeyId { get; }
        IAccountIdentityAdvanced Advanced { get; }
    }

    public class NullAccountIdentity : IAccountIdentity
    {
        public string Name { get { return ""; } }

        public string AuthenticationType { get { return ""; } }

        public bool IsAuthenticated { get { return false; } }

        public string AccessKeyId { get { return ""; } }

        private readonly IAccountIdentityAdvanced _advanced = new NullAccountIdentityAdvanced();
        public IAccountIdentityAdvanced Advanced { get { return _advanced; } }
    }

    public class AccountIdentity : IAccountIdentity
    {
        private readonly Account _account;

        public AccountIdentity(Account account)
        {
            _account = account;
            Advanced = new AccountIdentityAdvanced(account);
        }

        public string Name { get { return _account == null ? "" : _account.Name; } }
        public string AccessKeyId { get { return _account.AccessKeyId; } }

        public string AuthenticationType
        {
            get { return null; }
        }

        public bool IsAuthenticated
        {
            get { return _account != null; }
        }

        public IAccountIdentityAdvanced Advanced { get; private set; }
    }

    public interface IAccountIdentityAdvanced
    {
        Dictionary<string,string> IntegrationSettings { get; }
        string DatabaseName { get; }

        string[] QueryStatusesLimitedTo { get; }
    }

    public class NullAccountIdentityAdvanced : IAccountIdentityAdvanced
    {
        private readonly Dictionary<string, string> _integrationSettings = new Dictionary<string, string>();
        public Dictionary<string, string> IntegrationSettings { get { return _integrationSettings; } }

        public string DatabaseName { get { return ""; } }

        private static readonly string[] QueryStatusesLimitedToNullInstange = new string[0];
        public string[] QueryStatusesLimitedTo
        {
            get { return QueryStatusesLimitedToNullInstange; }
        }
    }

    public class AccountIdentityAdvanced : IAccountIdentityAdvanced
    {
        private readonly Account _account;

        public AccountIdentityAdvanced(Account account)
        {
            _account = account;
        }

        public Dictionary<string, string> IntegrationSettings { get { return _account.IntegrationSettings; } }

        public string DatabaseName { get { return _account.DatabaseName; } }

        private static readonly string[] QueryStatusesLimitedToDefaultInstange = new string[0];
        public string[] QueryStatusesLimitedTo
        {
            get
            {
                return _account.OverrideAllowedSearchResultStatuses
                    ? _account.SearchStatusesLimitedTo ?? QueryStatusesLimitedToDefaultInstange
                    : QueryStatusesLimitedToDefaultInstange;
            }
        }
    }
}