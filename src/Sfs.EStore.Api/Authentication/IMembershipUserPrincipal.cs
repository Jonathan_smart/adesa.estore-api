using System.Linq;
using System.Security.Principal;

namespace Sfs.EStore.Api.Authentication
{
    public interface IMembershipUserPrincipal : IPrincipal
    {
        new MembershipUserIdentity Identity { get; }
    }

    public class NullMembershipUserPrincipal : IMembershipUserPrincipal
    {
        private readonly MembershipUserIdentity _identity = new MembershipUserIdentity();

        public bool IsInRole(string role) { return false; }

        public MembershipUserIdentity Identity
        {
            get { return _identity; }
        }

        IIdentity IPrincipal.Identity
        {
            get { return Identity; }
        }
    }

    public class MembershipUserPrincipal : IMembershipUserPrincipal
    {
        private readonly MembershipUserIdentity _identity;

        public MembershipUserPrincipal(MembershipUserIdentity identity)
        {
            _identity = identity;
        }

        bool IPrincipal.IsInRole(string role)
        {
            return IsInRole(role);
        }

        IIdentity IPrincipal.Identity
        {
            get { return Identity; }
        }

        public MembershipUserIdentity Identity { get { return _identity; } }

        public bool IsInRole(string role)
        {
            return Identity.Roles.Any(x => x == role);
        }
    }

    public class MembershipUserIdentity : IIdentity, IUserIdentifier
    {
        private readonly MembershipUserInfo _user;

        public MembershipUserIdentity()
        {
        }

        public MembershipUserIdentity(MembershipUserInfo user)
        {
            _user = user;
        }

        public string Id
        {
            get { return _user == null ? "" : _user.Id; }
        }

        string IUserIdentifier.Id { get { return Id; } }
        string IUserIdentifier.Username { get { return Name; } }

        public string Name
        {
            get { return _user == null ? "" : _user.Username; }
        }

        public string AuthenticationType
        {
            get { return null; }
        }

        public bool IsAuthenticated
        {
            get { return _user != null && _user.Username != ""; }
        }

        public string[] Roles { get { return _user.Roles; } }
    }
}