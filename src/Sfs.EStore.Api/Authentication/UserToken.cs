﻿using System;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Sfs.EStore.Api.Authentication
{
    public static class UserToken
    {
        private static readonly TripleDESCryptoServiceProvider CryptoServiceProvider;

        static UserToken()
        {
            CryptoServiceProvider = new TripleDESCryptoServiceProvider
                                        {
                                            Key = Encoding.ASCII.GetBytes("??????\x10(??????I?TR???R??"),
                                            IV = Encoding.ASCII.GetBytes("?)?\x16,?/\x1F")
                                        };

            //if (ConfigurationManager.AppSettings["GuestPass/CryptoServiceProvider/Key"] != null)
            //{
            //    var key = ConfigurationManager.AppSettings["GuestPass/CryptoServiceProvider/Key"];
            //    CryptoServiceProvider.Key = Convert.FromBase64String(key);
            //}

            //if (ConfigurationManager.AppSettings["GuestPass/CryptoServiceProvider/IV"] != null)
            //{
            //    var iv = ConfigurationManager.AppSettings["GuestPass/CryptoServiceProvider/IV"];
            //    CryptoServiceProvider.IV = Convert.FromBase64String(iv);
            //}
        }

        public static string Encrypt(Token token)
        {
            if (token == null) throw new ArgumentNullException("token");

            var source = JObject.FromObject(token).ToString(Formatting.None);
            var buffer = Encoding.ASCII.GetBytes(source);
            var encrypedBytes = CryptoServiceProvider.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length);
            return Convert.ToBase64String(encrypedBytes);
        }

        public static Token Decrypt(string encoded)
        {
            const string exceptionMessage = "encryptedTicket is null.- or -encryptedTicket is an empty string (\"\").- or -encryptedTicket is of an invalid format.";

            if (encoded == null) throw new ArgumentNullException("encoded");

            try
            {
                var buffer = Convert.FromBase64String(encoded);
                var decryptedBytes = CryptoServiceProvider.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length);
                var decrypted = Encoding.ASCII.GetString(decryptedBytes);

                return JObject.Parse(decrypted).ToObject<Token>();
            }
            catch (CryptographicException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
            catch (FormatException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
            catch (ArgumentException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
            catch (JsonReaderException ex)
            {
                throw new ArgumentException(exceptionMessage, ex);
            }
        }
    }
}