using System.Security.Principal;

namespace Sfs.EStore.Api.Authentication
{
    public interface IUserIdentifier
    {
        string Id { get; }
        string Username { get; }
    }

    public interface IApiApplicationPrincipal : IPrincipal
    {
        IMembershipUserPrincipal MembershipUser { get; }
        new IAccountIdentity Identity { get; }
    }

    public class NullApiApplicationPrincipal : IApiApplicationPrincipal
    {
        private NullApiApplicationPrincipal(){}

        public bool IsInRole(string role) { return false; }

        private readonly IAccountIdentity _identity = new NullAccountIdentity();
        public IAccountIdentity Identity { get { return _identity; } }

        private readonly IMembershipUserPrincipal _membershipUser = new NullMembershipUserPrincipal();
        public IMembershipUserPrincipal MembershipUser { get { return _membershipUser; } }

        IIdentity IPrincipal.Identity
        {
            get { return Identity; }
        }

        public static readonly ApiApplicationPrincipal Instance =
            new ApiApplicationPrincipal(new NullAccountIdentity(), new NullMembershipUserPrincipal());
    }

    public class ApiApplicationPrincipal : IApiApplicationPrincipal
    {
        private readonly IAccountIdentity _accountIdentity;
        private readonly IMembershipUserPrincipal _membershipUserPrincipal;

        public ApiApplicationPrincipal(IAccountIdentity accountIdentity, IMembershipUserPrincipal membershipUserPrincipal)
        {
            _accountIdentity = accountIdentity;
            _membershipUserPrincipal = membershipUserPrincipal ?? new NullMembershipUserPrincipal();
        }

        public IMembershipUserPrincipal MembershipUser { get { return _membershipUserPrincipal; } }

        bool IPrincipal.IsInRole(string role)
        {
            return IsInRole(role);
        }

        IIdentity IPrincipal.Identity
        {
            get { return Identity; }
        }

        public IAccountIdentity Identity { get { return _accountIdentity; } }
        public bool IsInRole(string role)
        {
            return false;
        }
    }
}