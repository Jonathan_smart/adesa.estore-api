namespace Sfs.EStore.Api.Authentication
{
    public class ApiConsumingApplication
    {
        public ApiConsumingApplication(string accessKeyId)
        {
            AccessKeyId = accessKeyId;
        }

        public string AccessKeyId { get; private set; }
    }
}