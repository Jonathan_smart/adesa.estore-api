using System;
using Sfs.EStore.Api.Authentication;

namespace Sfs.EStore.Api
{
    public class UsernameObfuscator
    {
        private readonly MembershipUserIdentity _currentUser;
        private const string ObfuscatedUsername = "";

        public UsernameObfuscator(MembershipUserIdentity user)
        {
            if (user == null) throw new ArgumentNullException("user");
            _currentUser = user;
        }

        public string Obfuscate(string userId)
        {
            return _currentUser.Id.Equals(userId)
                       ? _currentUser.Name
                       : ObfuscatedUsername;
        }
    }
}