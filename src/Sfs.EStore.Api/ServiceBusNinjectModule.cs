﻿using Ninject.Modules;
using Shuttle.Core.Data;
using Shuttle.ESB.Core;
using Shuttle.ESB.SqlServer;

namespace Sfs.EStore.Api
{
    public class ServiceBusNinjectModule : NinjectModule
    {
        public override void Load()
        {
            new ConnectionStringService().Approve();

            var subscriptionManager = SubscriptionManager.Default();

            Kernel.Bind<IServiceBus>().ToMethod(ctx => ServiceBus
                .Create(c => c.SubscriptionManager(subscriptionManager))
                .Start())
                .InSingletonScope();
        }
    }
}