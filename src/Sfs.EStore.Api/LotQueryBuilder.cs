﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web;
using log4net;
using Raven.Client;
using Raven.Client.Indexes;
using Raven.Client.Linq;
using Sfs.EStore.Api.Entities;
using Sfs.EStore.Api.Models;
using Sfs.EStore.Api.Raven.Indexes;
using Sfs.EStore.Api.Raven.Transformers;

namespace Sfs.EStore.Api
{
    public class LotsGetParams
    {
        public string[] LotIds { get; set; }
        public int[] Condition { get; set; }
        public string Q { get; set; }
        public string VehicleType { get; set; }
        public string[] VehicleTypes { get; set; }
        public string Registration { get; set; }
        public string Category { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Derivative { get; set; }
        public string Engine { get; set; }
        public string Equipment { get; set; }
        public string ExteriorColour { get; set; }
        public string Yap { get; set; }
        public string Source { get; set; }
        public int? MaxMileage { get; set; }
        public string Mileage { get; set; }
        public int? MaxAge { get; set; }
        public string Transmission { get; set; }
        public string FuelType { get; set; }
        public int? MaxPrice { get; set; }
        public int? MinPrice { get; set; }
        public string[] LimitToRoles { get; set; }
        public string Top { get; set; }
        public string Skip { get; set; }
        public string SortBy { get; set; }
        public double? NewSinceDays { get; set; }
        public int? LtAuction { get; set; }
        public int? LtBin { get; set; }
        public string LSale { get; set; }
        public int? LPreview { get; set; }
        public string[] Makes { get; set; }
    }

    public class ListingIndexQueryBuilder<TQuery, TIndexCreator>
        where TQuery : IFullListingSearchReduce
        where TIndexCreator : AbstractIndexCreationTask, new()
    {
        private readonly Regex _advancedSearchRegex = new Regex("( OR )|( AND )|([\\(\\)\"])");
        private static readonly ILog Logger = LogManager.GetLogger("Query");
        private readonly IAsyncDocumentSession _session;
        private LotStatusTypes[] _queryStatuses = { LotStatusTypes.Active };
        public LotStatusTypes[] QueryForStatuses { get { return _queryStatuses; } set { _queryStatuses = value ?? new LotStatusTypes[0]; } }

        public ListingIndexQueryBuilder(IAsyncDocumentSession session)
        {
            _session = session;
        }

        /// <exception cref="ArgumentNullException"><paramref name="search"/> is <see langword="null" />.</exception>
        public IRavenQueryable<TQuery> Build(LotsGetParams search)
        {
            if (search == null) throw new ArgumentNullException("search");

            var query = _session
                .Query<TQuery, TIndexCreator>()
                .Customize(x => x.Include<Lot>(l => l.VehicleId));

            if (search.LotIds != null && search.LotIds.Any())
            {
                var ids = search.LotIds.Select(Lot.IdFromLotNumber).ToArray();
                query = query.Where(x => x.LotId.In(ids));
            }

            var q = HttpUtility.UrlDecode(search.Q);
            if (!string.IsNullOrWhiteSpace(q))
            {
                if (_advancedSearchRegex.IsMatch(q))
                {
                    query = query.Search(x => x.Query, q);
                }
                else
                {
                    var terms = q.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    query = terms.Aggregate(query,
                        (current, term) =>
                            current.Search(x => x.Query, term.EndsWith("*") ? term : string.Format("{0}*", term),
                                options: SearchOptions.And,
                                escapeQueryOptions: EscapeQueryOptions.AllowPostfixWildcard));
                }
            }

            if (search.Condition != null && search.Condition.Length > 0)
            {
                var conditions = search.Condition.Select(x => (VehicleCondition)x).ToArray();
                query = query.Where(x => x.Condition.In(conditions));
            }

            if (!string.IsNullOrWhiteSpace(search.VehicleType))
                query = query.Where(x => x.VehicleType == search.VehicleType);
            else if (search.VehicleTypes != null && search.VehicleTypes.Any())
                query = query.Where(x => x.VehicleType.In(search.VehicleTypes));

            if (!string.IsNullOrWhiteSpace(search.Category))
                query = query.Where(x => x.Category == search.Category);

            if (!string.IsNullOrWhiteSpace(search.Registration))
            {
                var registration = search.Registration.Replace(" ", "");
                query = query.Where(x => x.RegistrationPlate == registration);
            }

            if (!string.IsNullOrWhiteSpace(search.Make))
                query = query.Where(x => x.Make == search.Make);

            if (!string.IsNullOrWhiteSpace(search.Model))
                query = query.Where(x => x.Model == search.Model);

            if (!string.IsNullOrWhiteSpace(search.Derivative))
                query = query.Where(x => x.Derivative == search.Derivative);

            if (!string.IsNullOrWhiteSpace(search.Transmission))
                query = query.Where(x => x.TransmissionSimple == search.Transmission);

            if (!string.IsNullOrWhiteSpace(search.FuelType))
                query = query.Where(x => x.FuelType == search.FuelType);

            if (!string.IsNullOrWhiteSpace(search.Equipment))
                query = query.Where(x => x.Equipment.Contains(search.Equipment));

            if (!string.IsNullOrWhiteSpace(search.Yap))
                query = query.Where(x => x.RegistrationYaP == search.Yap);

            if (!string.IsNullOrWhiteSpace(search.Source))
                query = query.Where(x => x.Source == search.Source);

            if (!string.IsNullOrWhiteSpace(search.ExteriorColour))
                query = query.Where(x => x.ExteriorColour == search.ExteriorColour);

            if (!string.IsNullOrWhiteSpace(search.Engine))
                query = query.Where(x => x.Engine == search.Engine);

            if (search.LtAuction.HasValue || search.LtBin.HasValue)
            {
                var lts = new List<ListingType>();
                if (search.LtAuction.HasValue && search.LtAuction.Value == 1)
                    lts.AddRange(new[] { ListingType.Auction, ListingType.AuctionWithBin });
                if (search.LtBin.HasValue && search.LtBin.Value == 1)
                    lts.AddRange(new[] { ListingType.FixedPrice, ListingType.AuctionWithBin });

                if (search.LtAuction.HasValue && search.LtAuction.Value != 1)
                {
                    lts.Remove(ListingType.AuctionWithBin);
                    lts.Remove(ListingType.Auction);
                }

                if (search.LtBin.HasValue && search.LtBin.Value != 1)
                {
                    lts.Remove(ListingType.AuctionWithBin);
                    lts.Remove(ListingType.FixedPrice);
                }

                if (lts.Count > 0)
                    query = query.Where(x => x.ListingType.In(lts));

                query = query.Where(x => x.Status != LotStatusTypes.Reserved);

            }
            else
            {
                query = query.Where(x => x.ListingType != ListingType.Auction && x.ListingType != ListingType.AuctionWithBin);
            }

            if (!string.IsNullOrWhiteSpace(search.LSale))
                query = query.Where(x => x.SaleTitle == search.LSale || x.SaleId == search.LSale);

            if (!string.IsNullOrWhiteSpace(search.Mileage))
            {
                var range = QueryRangeUtil.StringToRange(search.Mileage);
                if (range != null)
                {
                    if (range.Item1 != null)
                        query = query.Where(x => x.Mileage >= (int)range.Item1);

                    if (range.Item2 != null)
                        query = query.Where(x => x.Mileage <= (int)range.Item2);
                }
            }
            if (search.MaxAge.HasValue)
            {
                var minimumRegistrationDate = SystemTime.UtcNow.Date.AddYears(-search.MaxAge.Value);
                query = query.Where(x => x.RegistrationDate != null && x.RegistrationDate >= minimumRegistrationDate);
            }

            if (search.MaxPrice.HasValue && search.MaxPrice > 0)
                query = query.Where(x => x.CurrentPrice <= search.MaxPrice.Value);

            if (search.MinPrice.HasValue && search.MinPrice > 0)
                query = query.Where(x => x.CurrentPrice >= search.MinPrice.Value);

            if (search.NewSinceDays.HasValue)
            {
                var newSince = SystemTime.UtcNow.AddDays(-Math.Abs(search.NewSinceDays.Value));
                query = query.Where(x => x.CreatedAt >= newSince);
            }

            if (search.LimitToRoles != null)
                query = query.Where(x => x.VisibilityLimitedTo.Any(tag => tag.In(search.LimitToRoles)));

            if (search.LPreview.HasValue)
            {
                query = query.Where(x => x.Status.In(LotStatusTypes.Scheduled));
            }
            else
            {
                if (_queryStatuses.Length > 0)
                    query = query.Where(x => x.Status.In(_queryStatuses));
            }

            if (search.Makes != null && search.Makes.Any())
            {
                query = query.Where(x => x.Make.In(search.Makes));
            }

            if (search.SortBy != null)
                query = OrderBy(query, search.SortBy);

            return query;
        }

        private static IRavenQueryable<TQuery> OrderBy(IRavenQueryable<TQuery> @this, string sort)
        {
            Func<IRavenQueryable<TQuery>> defaultSort = () => @this.OrderBy(x => x.RandomOrderByValue);

            if (string.IsNullOrWhiteSpace(sort)) return defaultSort();

            var sortField = sort;
            var sortDirection = "asc";

            if (sort.StartsWith("-"))
            {
                sortField = sort.Substring(1, sort.Length - 1);
                sortDirection = "desc";
            }

            switch (sortField.ToLower())
            {
                case "id":
                    return OrderBy(@this, x => x.LotNumber, sortDirection);
                case "offerprice":
                case "price":
                    return OrderBy(@this, x => x.CurrentPrice, sortDirection);
                case "endtime":
                    return OrderBy(@this, x => x.EndTime, sortDirection);
                case "starttime":
                    return OrderBy(@this, x => x.StartTime, sortDirection);
                case "mileage":
                    return OrderBy(@this, x => x.Mileage, sortDirection);
                case "model":
                    return OrderBy(@this, x => x.Model, sortDirection);
                case "derivative":
                    return OrderBy(@this, x => x.Derivative, sortDirection);
                case "regdate":
                    return OrderBy(@this, x => x.RegistrationDate, sortDirection);
                default:
                    return defaultSort();
            }
        }

        private static IRavenQueryable<TQuery> OrderBy<TProperty>(IRavenQueryable<TQuery> @this,
                                                                  Expression<Func<TQuery, TProperty>> expression,
                                                                  string direction)
        {
            return "desc".Equals(direction, StringComparison.InvariantCultureIgnoreCase)
                ? @this.OrderByDescending(expression)
                : @this.OrderBy(expression);
        }
    }

    public class LotQueryBuilder : ListingIndexQueryBuilder<Lot_Search.ReduceResult, Lot_Search>
    {
        public LotQueryBuilder(IAsyncDocumentSession session)
            : base(session)
        {
        }

        public new IRavenQueryable<ListingSummaryViewModel> Build(LotsGetParams search)
        {
            var query = base.Build(search);

            return query.TransformWith<LotIdToListingSummary, ListingSummaryViewModel>();
        }
    }
}