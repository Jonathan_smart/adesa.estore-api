using System;
using Ninject;
using Sfs.Darker.CommandProcessor;

namespace Sfs.EStore.Api
{
    internal class NinjectCommandProcessor : ICommandProcessor
    {
        private readonly Func<IKernel> _kernel;

        public NinjectCommandProcessor(Func<IKernel> kernel)
        {
            _kernel = kernel;
        }

        public void Send<T>(T command) where T : class, IRequest
        {
            var handler = _kernel().Get<IHandleRequests<T>>();

            handler.Handle(command);
        }
    }
}